//
//  ContactUSController.swift
//  GenePlus
//
//  Created by pro on 1/15/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import Eureka

class ContactUSController: FormViewController {

    
    
    var shownIndexes : [IndexPath] = []

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        loadForm()
        // Do any additional setup after loading the view.
    }
    
    fileprivate func setupUI() {
        title = "Contact Us"
        setBackButton()
        tableView.backgroundColor = UIColor.white
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        
        
        LabelRow.defaultCellSetup = { cell, row in
            cell.textLabel?.font = UIFont.systemFont(ofSize: 15)
            cell.textLabel?.textColor = UIColor.lightGray
            cell.tintColor = UIColor.lightGray
        }
    }
    
    fileprivate func loadForm() {
        
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            
            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            animateCell(cell: cell)
        }
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}































