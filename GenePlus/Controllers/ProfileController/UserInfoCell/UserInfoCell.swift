//
//  UserInfoCell.swift
//  ApartmentManager
//
//  Created by pro on 7/27/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit
import Eureka
import SDWebImage

final class UserInfoRow: Row<UserInfoCell>, RowType {
    required init(tag: String?) {
        super.init(tag: tag)
        cellProvider = CellProvider<UserInfoCell>(nibName: "UserInfoCell")
    }
}

struct Users: Equatable {
    var name: String
    var pictureUrl: String
    var userType: String
}
func ==(lhs: Users, rhs: Users) -> Bool {
    return lhs.name == rhs.name
}

final class UserInfoCell: Cell<Users>, CellType {

    
    @IBOutlet weak var avatarUser: UIImageView!
    @IBOutlet weak var nameUser: UILabel!
    @IBOutlet weak var userType: UILabel!
    
    
    required init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    override func setup() {
        super.setup()
        // we do not want our cell to be selected in this case. If you use such a cell in a list then you might want to change this.
        selectionStyle = .none
        
        // configure our profile picture imageView
        avatarUser.contentMode = .scaleAspectFill
        avatarUser.layer.cornerRadius = avatarUser.frame.size.width / 2
        avatarUser.clipsToBounds = true

        
        
        // specify the desired height for our cell
        height = { return CGFloat(115) }()
        
        // set a light background color for our cell
        backgroundColor = UIColor.white
    }
    
    override func update() {
        super.update()
        
        // we do not want to show the default UITableViewCell's textLabel
        textLabel?.text = nil
        
        // get the value from our row
        guard let user = row.value else { return }
        
//        avatarUser.sd_setIndicatorStyle(.gray)
//        avatarUser.sd_addActivityIndicator()
//        avatarUser.sd_setImage(with: URL(string: user.pictureUrl), completed: nil)
        // set the texts to the labels
        nameUser.text = user.name
        userType.text = user.userType
    }
}
