//
//  HistoryController.swift
//  GenePlus
//
//  Created by pro on 1/12/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import ObjectMapper

class HistoryController: UIViewController {

    
    
    
    @IBOutlet weak var myTable: UITableView!
    
    
    
    var shownIndexes : [IndexPath] = []
    var histories = [OrderModel]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        configTable()
        readFile()
        // Do any additional setup after loading the view.
    }
    
    fileprivate func readFile() {
        if let path = Bundle.main.path(forResource: "HistoryItems", ofType: "plist") {
            if let arr = NSArray(contentsOfFile: path) {
                guard let models = Mapper<OrderModel>().mapArray(JSONObject: arr) else { return }
                histories = models
            }
        }
    }
    
    
    fileprivate func setupUI() {
        title = "History"
        setBackButton()
        applyAttributedNAV()
        whiteNavigationColor()
    }
    
    fileprivate func configTable() {
        myTable.register(nibWithCellClass: HistoryCell.self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}

extension HistoryController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return histories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: HistoryCell.self)
        let item = histories[indexPath.row]
        cell.configWith(value: item)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = Controllers.shared.historyDetailController()
        let item = histories[indexPath.row]
        vc.history = item
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            
            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            animateCell(cell: cell)
        }
    }
}

















