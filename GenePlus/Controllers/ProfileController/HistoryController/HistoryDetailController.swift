//
//  HistoryDetailController.swift
//  GenePlus
//
//  Created by pro on 1/16/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import ObjectMapper

class HistoryDetailController: BaseViewController {

    
    enum Cells: Int, CaseCountable {
        case TRAINER = 0
        case TRAINING
        case PACKAGE
        case PAYMENT
        case NOTE
    }
    
    @IBOutlet weak var myTable: UITableView!
    
    var shownIndexes : [IndexPath] = []

    
    var allotments = [AllotmentModel]()
    var paymentModel:PaymentModel?
    var history:OrderModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        readFile()
        // Do any additional setup after loading the view.
    }

    fileprivate func readFile() {
        
        if let path = Bundle.main.path(forResource: "AllotmentItems", ofType: "plist") {
            if let arr = NSArray(contentsOfFile: path) {
                guard let models = Mapper<AllotmentModel>().mapArray(JSONObject: arr) else { return }
                allotments = models
            }
        }
    }
    
    override func setupUI() {
        title = ""
        setBackButton()
        
        paymentModel = PaymentModel()
        paymentModel?.title = "dat.nguyen@bantayso.com"
        paymentModel?.image = UIImage(named: "paypal")!
        
    }
    
    override func configTable() {
        
        myTable.register(nibWithCellClass: ConfirmCell.self)
        myTable.register(nibWithCellClass: TrainingCell.self)
        myTable.register(nibWithCellClass: ConfirmPackageCell.self)
        myTable.register(nibWithCellClass: ConfirmPaymentCell.self)
        myTable.register(nibWithCellClass: NoteCell.self)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension HistoryDetailController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Cells.caseCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == Cells.TRAINER.rawValue {
            let cell = tableView.dequeueReusableCell(withClass: ConfirmCell.self)
            cell.nameTrainer.text = history?.trainer
            return cell
        }else if indexPath.row == Cells.TRAINING.rawValue {
            let cell = tableView.dequeueReusableCell(withClass: TrainingCell.self)
            cell.titleLBL.text = history?.title
            cell.detailLBL.text = "x\(history?.sets ?? 0)"
            return cell
        }else if indexPath.row == Cells.PACKAGE.rawValue {
            let cell = tableView.dequeueReusableCell(withClass: ConfirmPackageCell.self)
            cell.packTitle.text = history?.package
            cell.allotments = allotments
            return cell
        }else if indexPath.row == Cells.PAYMENT.rawValue {
            let cell = tableView.dequeueReusableCell(withClass: ConfirmPaymentCell.self)
            cell.iconRight.isHidden = true
            cell.titleLBL.text = paymentModel?.title
            cell.iconIMG.image = paymentModel?.image
            return cell
        }else if indexPath.row == Cells.NOTE.rawValue {
            let cell = tableView.dequeueReusableCell(withClass: NoteCell.self)
            cell.contentTXT.isUserInteractionEnabled = false
            cell.contentTXT.text = "I typically do this once I show up to the gym. It's part of the pre-game routine that I go through before working out. I put on my lifting shoes and knee sleeves, get out my lifting belt, write the date at the top of the page, and weigh myself."
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == Cells.TRAINER.rawValue {
            return 129
        }else if indexPath.row == Cells.TRAINING.rawValue {
            return 130
        }else if indexPath.row == Cells.PACKAGE.rawValue {
            return 190
        }else if indexPath.row == Cells.PAYMENT.rawValue {
            return 110
        }else if indexPath.row == Cells.NOTE.rawValue {
            return 175
        }
        
        return 50
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            
            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            animateCell(cell: cell)
        }
    }
    
    
}













