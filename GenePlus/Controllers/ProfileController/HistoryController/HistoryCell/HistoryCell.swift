
//
//  HistoryCell.swift
//  GenePlus
//
//  Created by pro on 1/16/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class HistoryCell: UITableViewCell {

    
    @IBOutlet weak var titleLBL: UILabel!
    
    @IBOutlet weak var trainerLBL: UILabel!
    
    @IBOutlet weak var setLBL: UILabel!
    
    @IBOutlet weak var packLBL: UILabel!
    
    @IBOutlet weak var timeLBL: UILabel!
    
    @IBOutlet weak var statusLBL: UILabel!
    
    
    func configWith(value: OrderModel) {
        
        titleLBL.text = value.title
        trainerLBL.text = value.trainer
        setLBL.text = "x\(value.sets ?? 0)"
        packLBL.text = value.package
        
        if let timeString = value.createdDate {
            let date = Utils.stringToDateFM(timeString, stringFormat: .YMD)
            timeLBL.text = Utils.timeAgoSince(date)
        }
        
        if value.status == OrderStatus.PENDING.rawValue {
            updateLBL(text: "PENDING", textColor: UIColor.orange.withAlphaComponent(0.6))
        }else if value.status == OrderStatus.REJECTED.rawValue {
            updateLBL(text: "REJECTED", textColor: UIColor.red.withAlphaComponent(0.8))
        }else if value.status == OrderStatus.SUCCESS.rawValue {
            updateLBL(text: "SUCCESS", textColor: UIColor.green.withAlphaComponent(0.8))
        }
        
        
    }
    
    fileprivate func updateLBL(text: String, textColor: UIColor) {
        statusLBL.text = text
        statusLBL.textColor = textColor
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}









