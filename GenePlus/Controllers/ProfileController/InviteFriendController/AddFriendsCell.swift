//
//  AddFriendsCell.swift
//  GenePlus
//
//  Created by pro on 1/16/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class AddFriendsCell: UITableViewCell {

    
    
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var phone: UILabel!
    
    @IBOutlet weak var checkBox: DLRadioButton!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        checkBox.isMultipleSelectionEnabled = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
