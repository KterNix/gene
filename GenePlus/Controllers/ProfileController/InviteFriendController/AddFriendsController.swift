//
//  AddFriendsController.swift
//  GenePlus
//
//  Created by pro on 1/16/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import Contacts
import MessageUI

class AddFriendsController: UIViewController {

    
    
    
    @IBOutlet weak var myTable: UITableView!
    
    
    var listContacts = [CNContact]()

    var filteredContacts = [CNContact]()
    var shownIndexes : [IndexPath] = []
    var contactsSelected:[String] = []
    var codeInvite:String = ""
    
    let searchController = UISearchController(searchResultsController: nil)
    lazy var inviteFooter:InviteFooter = UIView.instanceFromNib()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        configTabl()
        configSearchController()
        listenForKeyboard()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        whiteNavigationColor()
        applyAttributedNAV()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        clearNavigationColor()
    }
    
    fileprivate func setupUI() {
        title = "Choose Friends To Invite"
        setBackButton()
    }
    
    fileprivate func configTabl() {
        myTable.allowsMultipleSelection = true
        myTable.register(UINib(nibName: "AddFriendsCell", bundle: nil), forCellReuseIdentifier: "AddFriendsCell")
    }
    
    fileprivate func configSearchController() {
        searchController.searchResultsUpdater = self
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.sizeToFit()
        searchController.delegate = self
        searchController.searchBar.delegate = self
        searchController.searchBar.maskSearchBar()
        textColorCancelButton()
        navigationItem.titleView = self.searchController.searchBar
    }

    
    fileprivate func textColorCancelButton() {
        let attributes = [NSAttributedString.Key.foregroundColor : UIColor.mainColor()]
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setTitleTextAttributes(attributes, for: .normal)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}


extension AddFriendsController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listContacts.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddFriendsCell", for: indexPath) as! AddFriendsCell
        cell.selectionStyle = .none
        cell.checkBox.isSelected = cell.isSelected ? true : false
        let item = listContacts[indexPath.row]
        cell.name.text = item.givenName + item.familyName
        cell.phone.text = item.phoneNumbers.first?.value.stringValue
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? AddFriendsCell else { return }
        cell.checkBox.isSelected = cell.isSelected ? true : false
        let item = listContacts[indexPath.row]
        if let phoneNumber = item.phoneNumbers.first {
            contactsSelected.append(phoneNumber.value.stringValue)
            inviteFooter.sendmesButton.isEnabled = true
            inviteFooter.sendmesButton.backgroundColor = UIColor.mainColor()
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? AddFriendsCell else { return }
        cell.checkBox.isSelected = cell.isSelected ? true : false
        let item = listContacts[indexPath.row]
        let indexPhoneNumber = contactsSelected.index { (contact) -> Bool in
            if let phoneSelect = item.phoneNumbers.first {
                return contact.contains(phoneSelect.value.stringValue)
            }
            return false
        }
        if indexPhoneNumber != nil {
            contactsSelected.remove(at: indexPhoneNumber!)
        }
        
        if contactsSelected.count == 0 {
            inviteFooter.sendmesButton.isEnabled = false
            inviteFooter.sendmesButton.backgroundColor = UIColor.lightGray
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        inviteFooter.sendmesButton.addTarget(self, action: #selector(didTapSend), for: UIControl.Event.touchUpInside)
        inviteFooter.sendmesButton.isEnabled = false
        inviteFooter.sendmesButton.backgroundColor = UIColor.lightGray
        return inviteFooter
    }
    
    @objc fileprivate func didTapSend() {
        sendMessages()
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            
            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            animateCell(cell: cell)
        }
    }
}

extension AddFriendsController: MFMessageComposeViewControllerDelegate {
    
    
    fileprivate func sendMessages() {
        
        if MFMessageComposeViewController.canSendText() {
            DispatchQueue.global().async {
                let composeVC = MFMessageComposeViewController()
                composeVC.messageComposeDelegate = self
                composeVC.recipients = self.contactsSelected
                composeVC.body = "Use code '\(self.codeInvite)' to earn 10% discount when you booking training at Gene+"
                DispatchQueue.main.async {
                    self.present(composeVC, animated: true, completion: nil)
                }
            }
            
        }else{
            SwiftNotice.noticeOnStatusBar("SMS services are not available. Please try again !", autoClear: true, autoClearTime: 5)
        }
    }
    
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }
}

extension AddFriendsController: UISearchResultsUpdating, UISearchControllerDelegate, UISearchBarDelegate {
    func updateSearchResults(for searchController: UISearchController) {
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
    }
    
    
}





















