//
//  InviteFriendController.swift
//  GenePlus
//
//  Created by pro on 1/8/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import Contacts

class InviteFriendController: UIViewController {

    
    
    @IBOutlet weak var viewCode: UIView!
    
    @IBOutlet weak var codeLBL: UILabel!
    
    @IBOutlet weak var shareBTN: UIButton!
    @IBOutlet weak var inviteBTN: UIButton!
    
    @IBOutlet weak var viewAlpha: UIView!
    
    lazy var listContacts: [CNContact] = {
        let contactStore = CNContactStore()
        let keysToFetch = [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
            CNContactEmailAddressesKey,
            CNContactPhoneNumbersKey] as [Any]
        
        // Get all the containers
        var allContainers: [CNContainer] = []
        do {
            allContainers = try contactStore.containers(matching: nil)
        } catch {
            print("Error fetching containers")
        }
        
        var results: [CNContact] = []
        
        // Iterate all containers and append their contacts to our results array
        for container in allContainers {
            let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
            
            do {
                let containerResults = try contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch as! [CNKeyDescriptor])
                results.append(contentsOf: containerResults)
            } catch {
                print("Error fetching results for container")
            }
        }
        
        return results
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        let nsAttributed = [NSAttributedString.Key.foregroundColor : UIColor.white,NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 23)]
        self.navigationController?.navigationBar.titleTextAttributes = nsAttributed
        lightStatusbar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        defaultStatusbar()
        self.navigationController?.navigationBar.tintColor = UIColor.mainColor()
        let nsAttributed = [NSAttributedString.Key.foregroundColor : UIColor.mainColor(),NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 23)]
        self.navigationController?.navigationBar.titleTextAttributes = nsAttributed
    }
    
    fileprivate func setupUI() {
        title = "Invite Friends"
        setBackButton()
        clearNavigationColor()
        
        viewAlpha.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        viewCode.layer.cornerRadius = 5
        viewCode.layer.masksToBounds = true
        viewCode.layer.borderWidth = 1
        viewCode.layer.borderColor = UIColor.white.cgColor
        
        inviteBTN.layer.cornerRadius = 5
        inviteBTN.layer.masksToBounds = true
        
        
        
        let edgeInset:CGFloat = 10
        shareBTN.imageEdgeInsets = UIEdgeInsets(top: edgeInset, left: edgeInset, bottom: edgeInset, right: edgeInset)
        
        shareBTN.tintColor = UIColor.white
        shareBTN.setImage(UIImage(named: "shareIcon")?.withRenderingMode(.alwaysTemplate), for: UIControl.State.normal)

    }

    
    
    
    @IBAction func shareAction(_ sender: Any) {
        let shareItem = ActivityShare()
        shareItem.news = ""
        shareItem.url = ""
        share(object: shareItem)
    }
    
    func share(object : ActivityShare) {
        let vc = UIActivityViewController(activityItems: [object,object.image,object.news], applicationActivities: nil)
        DispatchQueue.main.async {
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func inviteAction(_ sender: Any) {
        let vc = Controllers.shared.addFriendsController()
        vc.listContacts = listContacts
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
