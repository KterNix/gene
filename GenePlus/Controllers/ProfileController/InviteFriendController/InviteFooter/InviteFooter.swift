//
//  InviteFooter.swift
//  GenePlus
//
//  Created by pro on 1/16/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class InviteFooter: UIView {

    
    @IBOutlet weak var sendmesButton: UIButton!

    
    
    override func awakeFromNib() {
        sendmesButton.layer.cornerRadius = Constant.buttonCornerRadius
        sendmesButton.layer.masksToBounds = true
    }
    
    
}
