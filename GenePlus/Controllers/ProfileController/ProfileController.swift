//
//  ProfileController.swift
//  GenePlus
//
//  Created by pro on 1/8/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import Eureka

class ProfileController: FormViewController {

    
    
    var shownIndexes : [IndexPath] = []

    var userType:UserType = .TRAINEE
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        loadForm()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        defaultStatusbar()
    }
    
    
    
    fileprivate func setupUI() {
        title = "Settings"
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        }
        whiteNavigationColor()
        applyAttributedNAV()

        if userType != .BUSINESS {
            setBackButton()
        }
        
        tableView.backgroundColor = UIColor.white
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false

        LabelRow.defaultCellSetup = { cell, row in
            cell.textLabel?.font = UIFont.systemFont(ofSize: 15)
            cell.textLabel?.textColor = UIColor.lightGray
            cell.tintColor = UIColor.lightGray
        }
        
    }
    
    
    fileprivate func loadForm() {
        form +++
        Section()
            <<< UserInfoRow { row in
                row.value = Users(name: "Dat Nguyen", pictureUrl: "", userType: "")
                }.onCellSelection({ [unowned self] (cell, row) in
                    let vc = Controllers.shared.profileDetailController()
                    let nav = UINavigationController(rootViewController: vc)
                    
                    DispatchQueue.main.async {
                        self.present(nav, animated: true, completion: nil)
                    }
                })
        +++ Section()
            <<< LabelRow() {
                $0.tag = "History"
                $0.title = "History"
                $0.cell.accessoryType = .disclosureIndicator
                }.onCellSelection({ [unowned self] (cell, row) in
                    let vc = Controllers.shared.historyController()
                    let nav = UINavigationController(rootViewController: vc)
                    
                    DispatchQueue.main.async {
                        self.present(nav, animated: true, completion: nil)
                    }
                })
        
        
            <<< LabelRow() {
                $0.title = "Invite friends"
                $0.cell.accessoryType = .disclosureIndicator
                }.onCellSelection({ [unowned self] (cell, row) in
                    let vc = Controllers.shared.inviteFriendController()
                    let nav = UINavigationController(rootViewController: vc)
                    DispatchQueue.main.async {
                        self.present(nav, animated: true, completion: nil)
                    }
                })
            
            <<< LabelRow() {
                $0.title = "Settings"
                $0.cell.accessoryType = .disclosureIndicator
                }.onCellSelection({ [unowned self] (cell, row) in
                    let vc = Controllers.shared.settingsController()
                    let nav = UINavigationController(rootViewController: vc)
                    
                    DispatchQueue.main.async {
                        self.present(nav, animated: true, completion: nil)
                    }
                })
            <<< LabelRow() {
                $0.title = "Help center"
                $0.cell.accessoryType = .disclosureIndicator
                }.onCellSelection({ [unowned self] (cell, row) in
                    let vc = Controllers.shared.helpCenterController()
                    let nav = UINavigationController(rootViewController: vc)
                    
                    DispatchQueue.main.async {
                        self.present(nav, animated: true, completion: nil)
                    }
                })
        
            <<< LabelRow() {
                $0.title = "Privacy"
                $0.cell.accessoryType = .disclosureIndicator
                }.onCellSelection({ [unowned self] (cell, row) in
                    let vc = Controllers.shared.privacyController()
                    let nav = UINavigationController(rootViewController: vc)
                    
                    DispatchQueue.main.async {
                        self.present(nav, animated: true, completion: nil)
                    }
                })
        
        if userType != .TRAINEE {
            let row = form.rowBy(tag: "History") as? LabelRow
            row?.hidden = true
            row?.evaluateHidden()
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            
            if indexPath.section != 0 {
                drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            }
            
            animateCell(cell: cell)
        }
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 115
        }
        return 60
    }
    
    
    
    
}
