//
//  ProfileDetailController.swift
//  GenePlus
//
//  Created by pro on 1/8/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import Eureka

class ProfileDetailController: FormViewController {

    
    
    var shownIndexes : [IndexPath] = []

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        loadForm()
        // Do any additional setup after loading the view.
    }

    
    fileprivate func setupUI() {
        title = "Profile"
        setBackButton()
        applyAttributedNAV()
        whiteNavigationColor()
        
        tableView.backgroundColor = UIColor.white
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        
        TextRow.defaultCellSetup = { cell, row in
            cell.textLabel?.font = UIFont.systemFont(ofSize: 15)
            cell.textLabel?.textColor = UIColor.lightGray
            cell.tintColor = UIColor.lightGray
        }
    }
    
    
    fileprivate func loadForm() {
        form +++
            Section()
            <<< UserInfoRow { row in
                row.value = Users(name: "Dat Nguyen", pictureUrl: "", userType: "")
                row.cell.accessoryType = .none
                }
        +++ Section()
            
            <<< TextRow() {
                $0.title = "Email"
                $0.value = "soul@gmail.com"
                $0.disabled = true
        }
            <<< TextRow() {
                $0.title = "Phone"
                $0.value = "0909090909"
        }
            <<< TextRow() {
                $0.title = "Address"
                $0.value = "47/25 Trần Quốc Toản Quận 3"
        }
            <<< TextRow() {
                $0.title = "Date of birth"
                $0.value = "Feb 12/2012"
            }
            <<< TextRow() {
                $0.title = "Gender"
                $0.value = "Male"
            }
            <<< TextRow() {
                $0.title = "Country"
                $0.value = "Việt Nam"
        }
            
            <<< TextRow() {
                $0.title = "Language"
                $0.value = "English"
            }
        +++ Section()
            <<< LabelRow() {
                $0.title = "Payment"
                $0.cell.accessoryType = .disclosureIndicator
                }.onCellSelection({ (cell, row) in
                    let vc = Controllers.shared.paymentController()
                    vc.presentFrom = .settings
                    let nav = UINavigationController(rootViewController: vc)
                    DispatchQueue.main.async {
                        self.present(nav, animated: true, completion: nil)
                    }
                })
            
            
        +++ Section()
            <<< CustomButtonRow { row in
                row.tag = "customButtonRow"
                row.cell.payButton.setTitle("SIGNOUT", for: UIControl.State.normal)
                row.cell.payButton.backgroundColor = UIColor.mainColor()
                row.cell.payButton.addTarget(self, action: #selector(self.signOut), for: UIControl.Event.touchUpInside)
        }
    }
    
    
    @objc fileprivate func signOut() {
        let vc = Controllers.shared.baseLoginController()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changeRootView(vc, completion_: nil)
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 115
        }
        return 60
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            
            
            if indexPath.section != 0 {
                drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            }
            
            animateCell(cell: cell)
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}











