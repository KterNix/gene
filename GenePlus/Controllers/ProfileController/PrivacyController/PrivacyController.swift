//
//  PrivacyController.swift
//  GenePlus
//
//  Created by pro on 1/15/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class PrivacyController: UIViewController {

    
    
    
    @IBOutlet weak var myWebView: UIWebView!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        loadWebView()
        // Do any additional setup after loading the view.
    }
    
    
    fileprivate func setupUI() {
        title = "Privacy"
        setBackButton()
        applyAttributedNAV()
        whiteNavigationColor()
        
        myWebView.delegate = self
    }
    fileprivate func loadWebView() {
        myWebView.delegate = self
        if let url = URL(string: "http://neolock.vn/FAQ.html") {
            let request = URLRequest(url: url)
            myWebView.loadRequest(request)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false

    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension PrivacyController: UIWebViewDelegate {
    func webViewDidStartLoad(_ webView: UIWebView) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

