//
//  CustomButtonRow.swift
//  Partner
//
//  Created by pro on 11/17/17.
//  Copyright © 2017 pro. All rights reserved.
//

import Foundation
import UIKit
import Eureka
import SDWebImage

final class CustomButtonRow: Row<CustomButtonCell>, RowType {
    required init(tag: String?) {
        super.init(tag: tag)
        cellProvider = CellProvider<CustomButtonCell>(nibName: "CustomButtonRow")
    }
}

struct CustomButton: Equatable {
    var title: String
}
func ==(lhs: CustomButton, rhs: CustomButton) -> Bool {
    return lhs.title == rhs.title
}

final class CustomButtonCell: Cell<CustomButton>, CellType {
    

    @IBOutlet weak var payButton:UIButton!
    
    required init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func setup() {
        super.setup()
        // we do not want our cell to be selected in this case. If you use such a cell in a list then you might want to change this.
        selectionStyle = .none
        
        payButton.layer.cornerRadius = 5
        payButton.layer.masksToBounds = true
        
        
        // specify the desired height for our cell
        height = { return CGFloat(60.0) }()
        
        // set a light background color for our cell
        backgroundColor = UIColor.white
    }
    
    
    override func update() {
        super.update()
        
        // we do not want to show the default UITableViewCell's textLabel
        textLabel?.text = nil
        
        // get the value from our row
        guard let user = row.value else { return }
        
        payButton.setTitle(user.title, for: UIControl.State.normal)
        // set the texts to the labels
    }
}
