//
//  PaymentCell.swift
//  GenePlus
//
//  Created by pro on 1/18/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class PaymentCell: UITableViewCell {

    
    
    @IBOutlet weak var iconPayment: UIImageView!
    @IBOutlet weak var nameCus: UILabel!
    
    
    func configWith(value: PaymentModel) {
        iconPayment.image = value.image
        nameCus.text = value.title
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
