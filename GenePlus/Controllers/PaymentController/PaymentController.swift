//
//  PaymentController.swift
//  GenePlus
//
//  Created by pro on 1/18/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import Braintree

enum PresentPaymentFrom {
    case continueBooking, settings
}

protocol PaymentControllerDelegate: class {
    func didSelectPayment(payment: PaymentModel, sender: PaymentController)
}



class PaymentController: UIViewController {

    
    
    
    
    @IBOutlet weak var myTable: UITableView!
    
    
    weak var delegate: PaymentControllerDelegate?
    
    
    var shownIndexes : [IndexPath] = []

    var paymentModel = [PaymentModel]()

    var presentFrom:PresentPaymentFrom = .settings
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        configTable()
        setupDataSource()
        // Do any additional setup after loading the view.
    }
    
    fileprivate func setupUI() {
        title = "Payment"
        setBackButton()
        applyAttributedNAV()
        whiteNavigationColor()
    }
    
    
    fileprivate func configTable() {
        myTable.register(UINib(nibName: "AddPaymentCell", bundle: nil), forCellReuseIdentifier: "AddPaymentCell")
        myTable.register(UINib(nibName: "PaymentCell", bundle: nil), forCellReuseIdentifier: "PaymentCell")
    }
    
    
    fileprivate func setupDataSource() {
        var payment:PaymentModel = PaymentModel()
        payment.title = "NGUYEN TAN DAT"
        payment.cardNumber = "****0664"
        payment.cvv = "999"
        payment.expirationMonth = 12
        payment.expirationYear = 20
        payment.postCode = "90000"
        payment.image = UIImage.fontAwesomeIcon(name:.ccVisa, textColor: UIColor.darkGray, size: CGSize(width: 40, height: 40))
        paymentModel.append(payment)
        
        payment = PaymentModel()
        payment.title = "dat.nguyen@bantayso.com"
//        payment.cardNumber = "4221092002560664"
//        payment.cvv = "999"
        payment.expirationMonth = 12
        payment.expirationYear = 20
        payment.postCode = "90000"
        payment.image = UIImage(named: "paypal")!
        paymentModel.append(payment)
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension PaymentController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentModel.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == paymentModel.count {
            let managePaymentCell = tableView.dequeueReusableCell(withIdentifier: "AddPaymentCell", for: indexPath) as! AddPaymentCell
            managePaymentCell.selectionStyle = .none
            return managePaymentCell
        }else{
            let paymentCell = tableView.dequeueReusableCell(withIdentifier: "PaymentCell", for: indexPath) as! PaymentCell
            paymentCell.configWith(value: paymentModel[indexPath.row])
            paymentCell.selectionStyle = .none
            return paymentCell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == paymentModel.count {
            let vc = Controllers.shared.addPaymentController()
            vc.delegate = self
            //            vc.paymentEdit = paymentModel[indexPath.row]
            //            vc.getIndexPath = indexPath.row
            DispatchQueue.main.async {
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else{
            let item = paymentModel[indexPath.row]

            if presentFrom == .continueBooking {
                delegate?.didSelectPayment(payment: item, sender: self)
                dismiss(animated: true, completion: nil)
                return
            }
            let vc = Controllers.shared.addPaymentController()
            vc.delegate = self
            vc.paymentEdit = item
            vc.getIndexPath = indexPath.row
            
            DispatchQueue.main.async {
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            
            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            animateCell(cell: cell)
        }
    }
    
}



extension PaymentController: AddPaymentControllerDelegate {
    func savePayment(sender: AddPaymentController, card: BTUICardFormView, indexPath: Int?) {
        if indexPath == nil {
            let newElement = PaymentModel()
            newElement.cardNumber = card.number
            newElement.cvv = card.cvv
            newElement.expirationMonth = card.expirationMonth.toInt()
            newElement.expirationYear = card.expirationYear.toInt()
            newElement.postCode = card.postalCode
            newElement.title = "*****\(card.number.substring(from:card.number.index(card.number.endIndex, offsetBy: -4)))"
            newElement.image = UIImage.fontAwesomeIcon(name:.ccVisa, textColor: UIColor.darkGray, size: CGSize(width: 40, height: 40))
            self.paymentModel.append(newElement)
        }else if indexPath != nil {
            guard let indeX = indexPath else { return }
            paymentModel[indeX].cardNumber = card.number
            paymentModel[indeX].cvv = card.cvv
            paymentModel[indeX].expirationMonth = card.expirationMonth.toInt()
            paymentModel[indeX].expirationYear = card.expirationYear.toInt()
            paymentModel[indeX].postCode = card.postalCode
            paymentModel[indeX].title = "*****\(card.number.substring(from:card.number.index(card.number.endIndex, offsetBy: -4)))"
        }
    }
}
















