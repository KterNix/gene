//
//  IncomingCell.swift
//  GenePlus
//
//  Created by pro on 7/9/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class IncomingCell: UITableViewCell {

    @IBOutlet weak var bubbleView: UIView!
    @IBOutlet weak var titleLBL: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupUI()
    }
    
    fileprivate func setupUI() {
        selectionStyle = .none
        bubbleView.cornerRadius = 5
        bubbleView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.3)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
