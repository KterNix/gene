//
//  ChatController.swift
//  GenePlus
//
//  Created by pro on 1/8/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class ChatController: BaseViewController {

    
    @IBOutlet weak var myTable: UITableView!
    @IBOutlet weak var myTextView: SAMTextView!
    
    @IBOutlet weak var sendBTN: UIButton!
    
    var messTest = [MessageModel]() {
        didSet {
            DispatchQueue.main.async {
                self.myTable.reloadData()
                self.myTable.scrollToRow(at: IndexPath(row: self.messTest.count - 1, section: 0), at: UITableView.ScrollPosition.bottom, animated: true)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        automaticallyAdjustsScrollViewInsets = false
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        defaultStatusbar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        lightStatusbar()
    }
    
    override func setupUI() {
        title = "Henry"
        setBackButton()
        whiteNavigationColor()
        applyAttributedNAV()
        
        myTextView.cornerRadius = 5
        myTextView.borderWidth = 1
        myTextView.borderColor = UIColor.lightGray.withAlphaComponent(0.3)
        
        myTextView.placeholder = "Text here..."
        view.window?.backgroundColor = UIColor.white
    }
    
    override func configTable() {
        myTable.register(nibWithCellClass: IncomingCell.self)
        myTable.register(nibWithCellClass: OutgoingCell.self)
        
        myTable.delegate = self
        myTable.dataSource = self
        
        var mes = MessageModel()
        
        mes.senderCode = "IN"
        mes.title = "Hi"
        messTest.append(mes)
        
    }
    
    
    @IBAction func sendAction(_ sender: Any) {
        dismissKeyboard()
        
        guard myTextView.hasText else { return }
        
        sendMessage(text: myTextView.text)
    }
    
    
    
    fileprivate func sendMessage(text: String) {
        let senderCode = "OUT"
        var mes = MessageModel()
    
        mes.senderCode = senderCode
        mes.title = text
        
        messTest.append(mes)
        
        myTextView.text = ""
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ChatController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messTest.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = messTest[indexPath.row]
        if item.senderCode != "OUT" {
            let cell = tableView.dequeueReusableCell(withClass: IncomingCell.self)
            cell.titleLBL.text = item.title
            return cell
        }
        let cell = tableView.dequeueReusableCell(withClass: OutgoingCell.self)
        cell.titleLBL.text = item.title
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
}








