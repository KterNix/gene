//
//  BaseChatController.swift
//  GenePlus
//
//  Created by pro on 1/15/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class BaseChatController: UIViewController {

    
    
    
    @IBOutlet weak var myTable: UITableView!
    
    
    var listChat = [String]()
    
    var shownIndexes : [IndexPath] = []
    
    lazy var baseChatHeader:BaseChatHeader = UIView.instanceFromNib()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        configTable()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        defaultStatusbar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        lightStatusbar()
    }

    fileprivate func setupUI() {
        applyAttributedNAV()
        whiteNavigationColor()
    }
    
    fileprivate func configTable() {
        myTable.register(nibWithCellClass: BaseChatCell.self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension BaseChatController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BaseChatCell", for: indexPath) as! BaseChatCell
        cell.selectionStyle = .none
        
        cell.titleLBL.text = "Henry"
        cell.descriptionLBL.text = "Hi"
        
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = Controllers.shared.chatController()
        let nav = UINavigationController(rootViewController: vc)
        
        DispatchQueue.main.async {
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return baseChatHeader
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            
            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            animateCell(cell: cell)
        }
    }
}


























