//
//  BaseChatCell.swift
//  GenePlus
//
//  Created by pro on 1/15/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class BaseChatCell: UITableViewCell {

    
    @IBOutlet weak var avatarImage: UIImageView!
    
    @IBOutlet weak var titleLBL: UILabel!
    
    @IBOutlet weak var descriptionLBL: UILabel!
    
    @IBOutlet weak var timeCreatedLBL: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupUI()
    }
    
    
    fileprivate func setupUI() {
        avatarImage.layer.cornerRadius = avatarImage.frame.size.width / 2
        avatarImage.layer.masksToBounds = true
        avatarImage.layer.borderWidth = 1
        avatarImage.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.2).cgColor
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
