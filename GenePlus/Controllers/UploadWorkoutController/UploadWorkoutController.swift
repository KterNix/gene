//
//  UploadWorkoutController.swift
//  GenePlus
//
//  Created by pro on 7/16/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import Eureka

class UploadWorkoutController: FormViewController {

    var shownIndexes : [IndexPath] = []
    var footerSizeHeight:CGFloat = 200

    lazy var footerColl:FooterColl? = UIView.instanceFromNib()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        loadForm()
        NotificationCenter.default.addObserver(self, selector: #selector(listenHeightFooter), name: NSNotification.Name.init("footerHeight"), object: nil)
        // Do any additional setup after loading the view.
    }
    
    @objc fileprivate func listenHeightFooter(notifi: Notification) {
        if let collHeight = notifi.userInfo?["collHeight"] as? CGFloat {
            var sizeSpace:CGFloat = 0
            if UIDevice.current.isIPad {
                sizeSpace = 300
            }else{
                sizeSpace = 100
            }
            
            
            footerSizeHeight = collHeight + sizeSpace
            self.tableView.reloadSections(IndexSet(integer: 3), with: UITableView.RowAnimation.automatic)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        defaultStatusbar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        lightStatusbar()
    }

    
    func updateTable(table: UITableView, withDuration duration: TimeInterval) {
        UIView.animate(withDuration: duration, animations: { () -> Void in
            table.beginUpdates()
            table.endUpdates()
        })
    }
    
    fileprivate func setupUI() {
        title = "Upload Your Workout"
        setBackButton()
        whiteNavigationColor()
        applyAttributedNAV()
        
        tableView.backgroundColor = UIColor.white
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        
        TextRow.defaultCellUpdate = { cell, row in
            cell.selectionStyle = .none
            cell.textLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
            cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
            cell.textLabel?.textColor = UIColor.darkGray
            cell.tintColor = UIColor.darkGray
            
        }
        
        
        PushRow<String>.defaultCellUpdate = { cell, row in
            cell.selectionStyle = .none
            cell.textLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
            cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
            cell.textLabel?.textColor = UIColor.darkGray
            cell.tintColor = UIColor.darkGray
            
        }
        
        TextAreaRow.defaultCellUpdate = { cell, row in
            cell.selectionStyle = .none
            cell.placeholderLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
            cell.textLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
            cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
            cell.textView.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.light)
            cell.textLabel?.textColor = UIColor.darkGray
            cell.tintColor = UIColor.darkGray
            
        }
    }
    
    fileprivate func loadForm() {
        form
        +++ Section(header: "Workout Information", footer: "")
            
            <<< TextRow() {
                $0.title = "Workout name"
                $0.placeholder = "Aa"
            }
            
            <<< PushRow<String>() {
                $0.title = "Sets"
                $0.options = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
                $0.value = "1"
        }
        
            <<< PushRow<String>() {
                $0.title = "Weight"
                $0.options = ["5", "10", "15", "20", "25", "30", "35", "40", "45", "50"]
                $0.value = "5"
        }
        
            <<< PushRow<String>() {
                $0.title = "Unit"
                $0.options = ["KG", "LBS"]
                $0.value = "KG"
        }
        
        +++ Section(header: "WORKOUT DESCRIPTION", footer: "")
            <<< TextAreaRow() {
                $0.placeholder = "Create a profile for the USER that only TRAINERS can see. Body weight, strength, weaknesses, body fat, flexibility, injures, health issues, notes, and TRAINER EXPERIENCE"
        }
            
        +++ Section(header: "NOTE", footer: "")
            <<< TextAreaRow() {
                $0.placeholder = "Create a profile for the USER that only TRAINERS can see. Body weight, strength, weaknesses, body fat, flexibility, injures, health issues, notes, and TRAINER EXPERIENCE"
            }
        
        +++ Section(header: "PICTURES", footer: "")
        
    }
    
    deinit {
        footerColl = nil
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension UploadWorkoutController {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            
            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            animateCell(cell: cell)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 || indexPath.section == 2 {
            if UIDevice.current.isIPad {
                return 300
            }
            return 200
        }
        return 60
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 3 {
            return footerColl
        }
        return nil
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section != 3 {
            return 0
        }
        return footerSizeHeight

    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
        return 200
    }
}






