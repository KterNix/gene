//
//  FooterColl.swift
//  GenePlus
//
//  Created by pro on 7/16/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import Photos

class FooterColl: UIView {

    @IBOutlet weak var myColl: UICollectionView!
    
    @IBOutlet weak var myBTN: UIButton!
    
    var imageList = [PHAsset]() {
        didSet {
            myColl.reloadData()
        }
    }
    
    let manager = PHImageManager.default()

    
    override func awakeFromNib() {
        super.awakeFromNib()
        configColl()
        
        myBTN.cornerRadius = 5
        
    }
    
    fileprivate func configColl() {
        
        myColl.register(UINib(nibName: "WorkoutImageCell", bundle: nil), forCellWithReuseIdentifier: "WorkoutImageCell")
        myColl.register(UINib(nibName: "InsertImageCell", bundle: nil), forCellWithReuseIdentifier: "InsertImageCell")
        myColl.register(UINib(nibName: "ImageLibraryCell", bundle: nil), forCellWithReuseIdentifier: "ImageLibraryCell")

        myColl.delegate = self
        myColl.dataSource = self
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        NotificationCenter.default.post(name: NSNotification.Name.init("footerHeight"), object: nil, userInfo: ["collHeight": self.myColl.contentSize.height])
    }
}

extension FooterColl: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageList.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == imageList.count {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InsertImageCell", for: indexPath) as! InsertImageCell
            return cell
        }
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageLibraryCell", for: indexPath as IndexPath) as! ImageLibraryCell
        cell.viewAlpha.isHidden = true
        let asset = imageList[indexPath.row]
        if cell.tag != 0 {
            manager.cancelImageRequest(PHImageRequestID(cell.tag))
        }
        let itemSize = frame.size.width / 3 - 16
        cell.tag = Int(manager.requestImage(for: asset,
                                            targetSize: CGSize(width: itemSize, height: itemSize),
                                            contentMode: .aspectFill,
                                            options: nil) { (result, _) in
                                            cell.imageBackground.image = result
        })
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == imageList.count {
            let vc = Controllers.shared.imageFromLibraryController()
            let nav = UINavigationController(rootViewController: vc)
            vc.delegate = self
            DispatchQueue.main.async {
                self.parentViewController?.present(nav, animated: true, completion: nil)
            }
        }
    }
    
    
    // MARK: - UICollectionViewDelegateFlowLayout methods
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemSize = frame.size.width / 3 - 16

//        let width = UIScreen.main.bounds.width * 0.32
//        let height = UIScreen.main.bounds.height * 0.179910045
        return CGSize(width: itemSize, height: itemSize)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        return UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
//    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension FooterColl: ImageFromLibraryControllerDelegate {
    func didSelectedImages(images: [PHAsset], sender: ImageFromLibraryController) {
        imageList = images
    }
}







