//
//  InsertImageCell.swift
//  GenePlus
//
//  Created by pro on 7/16/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class InsertImageCell: UICollectionViewCell {

    
    @IBOutlet weak var iconAdd: UIImageView!
    
    @IBOutlet weak var titleLBL: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cornerRadius = 10
        borderWidth = 1
        borderColor = UIColor.lightGray.withAlphaComponent(0.1)
    }

}
