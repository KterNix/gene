//
//  AddActivityController.swift
//  GenePlus
//
//  Created by pro on 1/19/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import Eureka

protocol AddActivityControllerDelegate: class {
    func didChooseExercise(item: ExercisesModel,sender: AddActivityController)
}




class AddActivityController: FormViewController {
    
    
    var listMuscle = ["All Exercises","Chest", "Abdominals", "Arm", "Back", "Shoulders", "Legs"]
    var listWorkout = ["Push up","Back flyes with resistance bands","Lat pulldown","Bent over rear delt row with head on bench"]
    var listReps = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "30", "40", "50", "60", "70", "80", "90", "100"]
    var listKG = ["0.5", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"]
    var listDuration = ["5", "10", "15", "20", "30", "40", "50", "60", "70", "80", "90", "100", "150", "200", "250", "300"]
    var units = ["KG", "LBS"]
    
    weak var delegate:AddActivityControllerDelegate?
    
    var shownIndexes : [IndexPath] = []

    
    lazy var section_workout_type = form.sectionBy(tag: "exercise_type")
    lazy var section_time_options = form.sectionBy(tag: "time_options")
    lazy var section_name_exercise = form.sectionBy(tag: "name_exercise")
    lazy var row_useexits = form.rowBy(tag: "use_exist") as? SwitchRow

    
    var exercisesModel = ExercisesModel()
    var setModel = SetModel()
    fileprivate var isUseExist:Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        loadForm()
        // Do any additional setup after loading the view.
    }
    
    
    fileprivate func setupUI() {
        title = "Add Exercise"
        setBackButton()
        whiteNavigationColor()
        applyAttributedNAV()
        
        tableView.backgroundColor = UIColor.white
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        
        LabelRow.defaultCellSetup = { cell, row in
            cell.textLabel?.font = UIFont.systemFont(ofSize: 15)
            cell.textLabel?.textColor = UIColor.lightGray
            cell.tintColor = UIColor.lightGray
        }
        
        PickerInputRow<String>.defaultCellSetup = { cell, row in
            cell.textLabel?.font = UIFont.systemFont(ofSize: 15)
            cell.textLabel?.textColor = UIColor.lightGray
            cell.tintColor = UIColor.lightGray
        }
        
        MultipleSelectorRow<String>.defaultCellSetup = { cell, row in
            cell.textLabel?.font = UIFont.systemFont(ofSize: 15)
            cell.textLabel?.textColor = UIColor.lightGray
            cell.tintColor = UIColor.lightGray
        }
        
        PushRow<String>.defaultCellSetup = { cell, row in
            cell.textLabel?.font = UIFont.systemFont(ofSize: 15)
            cell.textLabel?.textColor = UIColor.lightGray
            cell.tintColor = UIColor.lightGray
        }
    }
    
    
    fileprivate func loadForm() {
        
        form +++
            Section("Exercise Name")
           <<< SwitchRow() {
                $0.tag = "use_exist"
                $0.title = "Use exist exercises"
                $0.value = true
            }.onChange({ [unowned self] (row) in
                self.isUseExist = row.value!
                self.exercisesModel.name = row.value! ? self.listWorkout.first! : ""
                
                self.hiddenAction()
            })
            
            
            +++ Section("Name exercise") {
                $0.tag = "name_exercise"
            }
            <<< TextRow() {
                $0.title = "Name"
                $0.placeholder = "Enter your exercise name"
                exercisesModel.name = ""
                }.onChange({ [unowned self] (row) in
                    guard row.value != "" else {
                        SwiftNotice.noticeOnStatusBar("Please enter exercise name", autoClear: true, autoClearTime: 3)
                        return
                    }
                    self.exercisesModel.name = row.value!
                })
            
            
            +++ Section("Exercise type") {
                $0.tag = "exercise_type"
            }
            <<< PickerInputRow<String>() {
                    $0.title = "Choose type"
                    $0.cell.accessoryType = .disclosureIndicator
                    $0.value = listWorkout.first
                    $0.options = listWorkout
                    if isUseExist {
                        exercisesModel.name = listWorkout.first!
                    }
                }.onChange({ [unowned self] (row) in
                    guard row.value != "" else {
                        SwiftNotice.noticeOnStatusBar("Please choose exercise name", autoClear: true, autoClearTime: 5)
                        return
                    }
                    self.exercisesModel.name = row.value!
                })
            +++ Section("ADVANCE") {
                $0.tag = "time_options"
            }
            
            <<< PushRow<String>() {
                $0.title = "Unit"
                $0.cell.accessoryType = .disclosureIndicator
                $0.value = units.first
                $0.options = units
                setModel.unit = units.first!
                self.exercisesModel.unit = units.first!
                }.onChange({ [unowned self] (row) in
                    self.setModel.unit = row.value ?? "KG"
                    self.exercisesModel.unit = row.value ?? "KG"
                })
            
            <<< PushRow<String>() {
                $0.title = "Weight"
                $0.cell.accessoryType = .disclosureIndicator
                $0.value = listKG.first
                $0.options = listKG
                setModel.weight = listKG.first!
                }.onChange({ [unowned self] (row) in
                    guard row.value != "" else {
                        SwiftNotice.noticeOnStatusBar("Please choose weight", autoClear: true, autoClearTime: 5)
                        return
                    }
                    self.setModel.weight = row.value!
                })
            <<< PushRow<String>() {
                $0.title = "Repeat"
                $0.cell.accessoryType = .disclosureIndicator
                $0.value = listReps.first
                $0.options = listReps
                setModel.reps = listReps.first!
                }.onChange({ [unowned self] (row) in
                    guard row.value != "" else {
                        SwiftNotice.noticeOnStatusBar("Please choose repeat count", autoClear: true, autoClearTime: 5)
                        return
                    }
                    self.setModel.reps = row.value!
                })
            
            <<< PushRow<String>() {
                $0.title = "Duration"
                $0.cell.accessoryType = .disclosureIndicator
                $0.value = listDuration.first
                $0.options = listDuration
                setModel.duration = listDuration.first!
                }.onChange({ [unowned self] (row) in
                    self.setModel.duration = row.value!
                })
            
            +++ Section()
            <<< CustomButtonRow { row in
                row.tag = "customButtonRow"
                row.cell.payButton.setTitle("ADD EXERCISES", for: UIControl.State.normal)
                row.cell.payButton.backgroundColor = UIColor.mainColor()
                row.cell.payButton.addTarget(self, action: #selector(self.didAddWorkout), for: UIControl.Event.touchUpInside)
        }
        
        hiddenAction()
        
    }
    
    
    @objc fileprivate func didAddWorkout() {
        if !isUseExist {
            if exercisesModel.name == "" {
                SwiftNotice.noticeOnStatusBar("Please enter exercises first !", autoClear: true, autoClearTime: 5)
                return
            }
        }
        
        exercisesModel.setModel.append(setModel)
        delegate?.didChooseExercise(item: exercisesModel, sender: self)
        navigationController?.popViewController(animated: true)
    }
    
    
    fileprivate func hiddenAction() {
        if row_useexits!.value! {
            hiddenSection(hidden: false, dontUseExist: true)
        }else{
            hiddenSection(hidden: true, dontUseExist: false)
        }
    }
    
    
    fileprivate func hiddenSection(hidden: Condition, dontUseExist: Condition) {
        
        section_workout_type?.hidden = hidden
        section_workout_type?.evaluateHidden()
//        section_time_options?.hidden = hidden
//        section_time_options?.evaluateHidden()
        
        section_name_exercise?.hidden = dontUseExist
        section_name_exercise?.evaluateHidden()
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            
            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            
            animateCell(cell: cell)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
}










