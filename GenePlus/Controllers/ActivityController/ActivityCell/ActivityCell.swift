//
//  ActivityCell.swift
//  GenePlus
//
//  Created by pro on 1/17/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class ActivityCell: UITableViewCell {


    
    @IBOutlet weak var titleActivity: UILabel!
    
    @IBOutlet weak var activityIcon: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        // Initialization code
    }
    
    private func setupUI() {

        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
