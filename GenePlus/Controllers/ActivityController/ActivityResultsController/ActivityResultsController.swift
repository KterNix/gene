//
//  ActivityResultsController.swift
//  GenePlus
//
//  Created by pro on 1/25/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import Charts

class ActivityResultsController: UIViewController {

    @IBOutlet weak var chartView: PieChartView!
    
    
    var parties = ["A", "B", "C"]
    
    
    lazy var shareImage = UIImage(named: "shareSmall")?.withRenderingMode(.alwaysTemplate)
    lazy var shareButton:UIButton = {
        let b = UIButton(type: UIButton.ButtonType.custom)
        b.setImage(shareImage!, for: UIControl.State.normal)
        let sizeB:CGFloat = 30
        b.frame = CGRect(x: 0, y: 0, width: sizeB, height: sizeB)
        b.addTarget(self, action: #selector(didTapShare), for: UIControl.Event.touchUpInside)
        return b
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Result"
        
        setBackButton()
        applyAttributedNAV()
        whiteNavigationColor()
        
        let share = UIBarButtonItem(customView: shareButton)
        navigationItem.rightBarButtonItems = [share]
        
        
        
        setup(pieChartView: chartView)
        chartView.delegate = self
        
        chartView.entryLabelColor = .darkGray
        chartView.entryLabelFont = .systemFont(ofSize: 12, weight: .light)
        setDataCount(parties.count, range: 50)
        
        chartView.animate(xAxisDuration: 1.5, easingOption: .easeOutBack)

        // Do any additional setup after loading the view.
    }

    
    @objc fileprivate func didTapShare() {
        let shareItem = ActivityShare()
        shareItem.news = ""
        shareItem.url = ""
        share(object: shareItem)
    }
    
    
    
    private func share(object : ActivityShare) {
        let vc = UIActivityViewController(activityItems: [object,object.image,object.news], applicationActivities: nil)
        DispatchQueue.main.async {
            self.navigationController?.present(vc, animated: true, completion: nil)
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

extension ActivityResultsController: ChartViewDelegate {
    
}

extension ActivityResultsController {
    
    
    func setup(pieChartView chartView: PieChartView) {
        chartView.usePercentValuesEnabled = true
        chartView.drawSlicesUnderHoleEnabled = false
        chartView.holeRadiusPercent = 0.58
        chartView.transparentCircleRadiusPercent = 0.61
        chartView.chartDescription?.enabled = false
        chartView.setExtraOffsets(left: 5, top: 10, right: 5, bottom: 5)
        
        chartView.drawCenterTextEnabled = true
        
        let paragraphStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        paragraphStyle.lineBreakMode = .byTruncatingTail
        paragraphStyle.alignment = .center
        
        let centerText = NSMutableAttributedString(string: "Charts\nby Daniel Cohen Gindi")
        centerText.setAttributes([.font : UIFont(name: "HelveticaNeue-Light", size: 13)!,
                                  .paragraphStyle : paragraphStyle], range: NSRange(location: 0, length: centerText.length))
        centerText.addAttributes([.font : UIFont(name: "HelveticaNeue-Light", size: 11)!,
                                  .foregroundColor : UIColor.gray], range: NSRange(location: 10, length: centerText.length - 10))
        centerText.addAttributes([.font : UIFont(name: "HelveticaNeue-Light", size: 11)!,
                                  .foregroundColor : UIColor(red: 51/255, green: 181/255, blue: 229/255, alpha: 1)], range: NSRange(location: centerText.length - 19, length: 19))
        chartView.centerAttributedText = centerText
        
        chartView.drawHoleEnabled = true
        chartView.rotationAngle = 0
        chartView.rotationEnabled = true
        chartView.highlightPerTapEnabled = true
        
        let l = chartView.legend
        l.horizontalAlignment = .right
        l.verticalAlignment = .top
        l.orientation = .vertical
        l.drawInside = false
        l.xEntrySpace = 7
        l.yEntrySpace = 0
        l.yOffset = 0
        //        chartView.legend = l
    }
    
    func setDataCount(_ count: Int, range: UInt32) {
        let entries = (0..<count).map { (i) -> PieChartDataEntry in
            // IMPORTANT: In a PieChart, no values (Entry) should have the same xIndex (even if from different DataSets), since no values can be drawn above each other.
            return PieChartDataEntry(value: Double(arc4random_uniform(range) + range / 5),
                                     label: parties[i % parties.count],
                                     icon:  nil)
        }
        
        let set = PieChartDataSet(values: entries, label: "Exercises Results")
        set.drawIconsEnabled = false
        set.sliceSpace = 2
        
        
        set.colors = ChartColorTemplates.vordiplom()
            + ChartColorTemplates.joyful()
            + ChartColorTemplates.colorful()
            + ChartColorTemplates.liberty()
            + ChartColorTemplates.pastel()
            + [UIColor(red: 51/255, green: 181/255, blue: 229/255, alpha: 1)]
        
        let data = PieChartData(dataSet: set)
        
        let pFormatter = NumberFormatter()
        pFormatter.numberStyle = .percent
        pFormatter.maximumFractionDigits = 1
        pFormatter.multiplier = 1
        pFormatter.percentSymbol = " %"
        data.setValueFormatter(DefaultValueFormatter(formatter: pFormatter))
        
        data.setValueFont(.systemFont(ofSize: 11, weight: .light))
        data.setValueTextColor(.darkGray)
        
        chartView.data = data
        chartView.highlightValues(nil)
    }
    
}






























