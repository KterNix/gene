//
//  DownloadedController.swift
//  GenePlus
//
//  Created by pro on 1/22/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import GSKStretchyHeaderView

class DownloadedController: UIViewController {

    
    
    @IBOutlet weak var myTable: UITableView!
    
    
    var downloadDetailHeader: DownloadDetailHeader? = UIView.instanceFromNib()
    var stretchyHeaderView:GSKStretchyHeaderView?


    var listWorkouts = [WorkoutModel]()
    var listExercises = [ExercisesModel]()
    
    var playlist = PlaylistModel() {
        didSet {
            myTable.reloadData()
        }
    }

    var imageURL:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        configTabl()
        setupDataSource()

        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        lightStatusbar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        defaultStatusbar()
    }
    
    
    fileprivate func setupUI() {
        setBackButton()
        clearNavigationColor()
        updateNAVTint(color: .white)
    }
    
    fileprivate func configTabl() {
        
        downloadDetailHeader?.frame.size.height = 250
        downloadDetailHeader?.frame.size.width = myTable.frame.size.width

        stretchyHeaderView = GSKStretchyHeaderView(frame: CGRect(x: 0, y: 0, width: myTable.frame.size.width, height: 250))
        stretchyHeaderView?.expansionMode = .topOnly
        stretchyHeaderView?.contentShrinks = true
        stretchyHeaderView?.contentExpands = true
        stretchyHeaderView?.minimumContentHeight = 64
        stretchyHeaderView?.maximumContentHeight = 250
        stretchyHeaderView?.addSubview(downloadDetailHeader!)
        myTable.addSubview(stretchyHeaderView!)
        
        stretchyHeaderView?.stretchDelegate = self
        
        downloadDetailHeader?.backgroundImage.setImageWith(path: imageURL, imageType: .NORMAL_IMAGE)

        
        myTable.register(nibWithCellClass: ChooseWorkoutCell.self)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        guard scrollView.contentOffset.y < 0 else { return }
        var scale = 1 + fabsf(Float(scrollView.contentOffset.y)) / Float(scrollView.frame.size.height / 5)
        scale = max(0, scale)
        
//        downloadDetailHeader?.backgroundImage.transform = CGAffineTransform.identity.scaledBy(x: CGFloat(scale), y: CGFloat(scale))
//        downloadDetailHeader?.gradientView.transform = CGAffineTransform.identity.scaledBy(x: CGFloat(scale), y: CGFloat(scale))
        
    }
    
    func setupDataSource() {
        var a = PlaylistModel()
        var e = WorkoutModel()
        
        var f = ExercisesModel()
        var g = SetModel()
        
        g.name = "This Abs Circuit Workout Will Shock Your Core Into Shape"
        g.reps = "2"
        g.weight = "12"
        g.duration = "30"
        g.unit = "KG"
        g.description = "The main reason that we begin to train is that we are interested in making changes to our physique.\n\nWhen I began to take an interest in training during my teens, my goal was to look like a superhero.\n\nStrength and performance became the main goal as I began to compete in track at the collegiate level.\n\nWhat I learned at this time in my young athletic career was that the better I performed in the gym, the better my body looked."
        g.images = ["https://gogetfunding.com/wp-content/uploads/2015/12/4975401/img/gym-with-people-on-machines.jpg",
                    "https://www.fitness19.com/wp-content/uploads/2016/10/photodune-13379907-fit-people-working-out-in-weights-room-at-the-gym-xs.jpg",
                    "https://cdn-ami-drupal.heartyhosting.com/sites/muscleandfitness.com/files/styles/full_node_image_1090x614/public/media/Cami6.JPG?itok=ggHEtRTQ",
                    "https://blog.codyapp.com/wp-content/uploads/2013/04/beginner_gym_workout_legs_large.jpg",
                    "https://wallpapershome.com/images/wallpapers/girl-4400x2933-fitness-exercise-gym-workout-sportswear-motivation-11064.jpg",
                    "http://watchfit.com/wp-content/uploads/2015/09/training-alone_11-1024x683.jpg",
                    "https://lowellkabnickmd.com/wp-content/uploads/2015/03/iu-7.jpeg",
                    "http://www.thefitness.co.uk/wp-content/uploads/2016/01/gym-workout.jpg",
                    "http://aamirfit.com/wp-content/uploads/2016/12/gym_weekly_workout_schedule.jpg",
                    "https://wallpapersite.com/images/pages/pic_w/64.jpg",
                    "http://jasonferruggia.com/wp-content/uploads/2011/07/how-long-should-your-workout-last.jpg",
                    "https://thenypost.files.wordpress.com/2018/01/workout_yoga1a.jpg?quality=90&strip=all&strip=all",
                    "https://media.glamour.com/photos/59371f8ac610717ae9f5b2bf/master/pass/ledeworkoutathleisure.png",
                    "https://skinnyms.com/wp-content/uploads/2018/01/14-Day-Toned-Legs-Thighs-Workout-Challenge1.jpg",
                    "https://www.active.com/Assets/Fitness/2-week-plan.jpg",
                    "https://media.timeout.com/images/104101397/630/472/image.jpg",
                    "https://tigerfitness-tigerfitness.netdna-ssl.com/pub/media/magefan_blog/morning-workout.jpg",
                    "https://zazabava.com/wp-content/uploads/2017/07/slika-1.jpg",
                    "https://1y2u3hx8yml32svgcf0087imj-wpengine.netdna-ssl.com/wp-content/uploads/2018/06/The-Rocks-Workout-Playlist-Will-Pull-You-Out-of-Any-Fitness-Lull-752x472.jpg",
                    "https://www.bodybuilding.com/images/2018/april/the-best-ab-workout-for-a-six-pack-header-830x467.jpg",
                    "https://www.psychologies.co.uk/sites/default/files/styles/psy2_page_header/public/wp-content/uploads/2010/03/What-is-best-workout-for-you1.jpg",
                    "https://www.freetrainers.com/image/kJkyN6UGohmSJirg7JjLdmPpVF8OoFP51JkrhaUvBfM-ftd-lxiRpZkqR1uqd1ku.jpg",
                    "https://cdn-ami-drupal.heartyhosting.com/sites/muscleandfitness.com/files/media/badass-workout-braun-circuit-2.jpg"]
        
        f.name = "Smith tricep press"
        f.image = "https://cdn.muscleandstrength.com/sites/default/files/machine_press_exercise_for_chest_feature.jpg"
        f.unit = "KG"
        f.setModel.append(g)
        
        e.title = "Increase Your Back Width With 5 Moves"
        e.trainer = "Tommy"
        e.sets = "1"
        e.weight = "5"
        e.reps = "5"
        e.unit = "KG"
        e.images = ["https://www.bodybuilding.com/images/increase-your-back-width-with-5-moves-musclepharm-1-700xh.jpg"]
        
        listWorkouts.append(e)
        
        e = WorkoutModel()
        e.title = "Evan Centopani's 15-Minute Arm Blast"
        e.trainer = "David"
        e.sets = "1"
        e.weight = "10"
        e.reps = "15"
        e.unit = "KG"
        e.images = ["https://www.bodybuilding.com/images/2018/june/evan-centopanis-15-min-arm-workout-1-700xh.jpg"]
        listWorkouts.append(e)
        
        e = WorkoutModel()
        e.title = "4 Musts To Maximize Biceps Growth"
        e.trainer = "Tom"
        e.sets = "2"
        e.weight = "30"
        e.reps = "2"
        e.unit = "KG"
        e.images = ["https://www.bodybuilding.com/images/2018/june/4-musts-to-maximize-bicep-growth-header-830x467.jpg"]
        listWorkouts.append(e)
        
        e = WorkoutModel()
        e.title = "Get Uplifted With This Squat Workout From Meg Squats!"
        e.trainer = "Bob"
        e.sets = "1"
        e.weight = "20"
        e.reps = "10"
        e.unit = "KG"
        e.images = ["https://www.bodybuilding.com/images/2018/june/get-uplifted-with-this-squat-workout-from-meg-squats-header-830x467.jpg"]
        listWorkouts.append(e)
        
        e = WorkoutModel()
        e.title = "Abel Albonetti's Brutal Mass-Building Shoulder Workout"
        e.trainer = "Henry"
        e.sets = "1"
        e.weight = "10"
        e.reps = "5"
        e.unit = "KG"
        e.images = ["https://www.bodybuilding.com/images/2018/june/abel-albonettis-brutal-mass-building-shoulder-workout-header-400x225.jpg"]
        listWorkouts.append(e)
        
        e = WorkoutModel()
        e.title = "A Short Workout for Your Triceps Long Head"
        e.trainer = "Jenifer"
        e.sets = "1"
        e.weight = "10"
        e.reps = "5"
        e.unit = "KG"
        e.images = ["https://www.bodybuilding.com/images/2018/june/a-short-workout-for-your-triceps-long-head-header-muscletech-830x467.jpg"]
        listWorkouts.append(e)
        
        a.name = "Morning"
        a.timeCreated = "MON 2"
        a.workouts = listWorkouts
        a.exercises = [f]
        playlist = a
        
        
        
    }
    
    deinit {
        downloadDetailHeader = nil
        stretchyHeaderView = nil
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension DownloadedController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playlist.workouts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: ChooseWorkoutCell.self)
        cell.selectionStyle = .none
        let item = playlist.workouts[indexPath.row]
        cell.checkImage.isHidden = true
        cell.configWith(value: item)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = Controllers.shared.startWorkoutController()
        let nav = UINavigationController(rootViewController: vc)
        vc.listExercises = playlist.exercises
        
        DispatchQueue.main.async {
            self.present(nav, animated: true, completion: nil)
        }        
    }
}

extension DownloadedController: GSKStretchyHeaderViewStretchDelegate {
    func stretchyHeaderView(_ headerView: GSKStretchyHeaderView, didChangeStretchFactor stretchFactor: CGFloat) {
        
        downloadDetailHeader?.postLBL.alpha = stretchFactor
        downloadDetailHeader?.postTitle.alpha = stretchFactor
        downloadDetailHeader?.timeCreated.alpha = stretchFactor
        downloadDetailHeader?.titleLBL.alpha = stretchFactor

    }
}












