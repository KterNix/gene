//
//  WorkoutImagePreview.swift
//  GenePlus
//
//  Created by pro on 6/28/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class WorkoutImagePreview: BaseViewController {

    
    @IBOutlet weak var myColl: UICollectionView!
    
    
    var imagesList = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configColl()
    }
    
    override func setupUI() {
        setBackButton()
        applyAttributedNAV()
    }
    
    fileprivate func configColl() {
        
        myColl.register(UINib(nibName: "WorkoutImageCell", bundle: nil), forCellWithReuseIdentifier: "WorkoutImageCell")
        myColl.delegate = self
        myColl.dataSource = self
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension WorkoutImagePreview: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagesList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WorkoutImageCell", for: indexPath) as! WorkoutImageCell
        let item = imagesList[indexPath.row]
        cell.cellImage.setImageWith(path: item, imageType: .NORMAL_IMAGE)
        cell.cellImage.hero.id = "image_\(item)"
        cell.cellImage.hero.modifiers = [.forceNonFade,
                                        .spring(stiffness: 300, damping: 25)]
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = Controllers.shared.imagePreviewController()
        let nav = UINavigationController(rootViewController: vc)
        let item = imagesList[indexPath.row]
        nav.hero.isEnabled = true
        vc.imageURL = item
        DispatchQueue.main.async {
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let imgSize:CGFloat = collectionView.frame.size.width / 3
        
        return CGSize(width: imgSize, height: imgSize)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}







