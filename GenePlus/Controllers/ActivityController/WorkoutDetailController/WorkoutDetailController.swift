//
//  WorkoutDetailController.swift
//  GenePlus
//
//  Created by pro on 1/24/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class WorkoutDetailController: UIViewController {

    
    
    
    @IBOutlet weak var myTable: UITableView!
    
    var shownIndexes : [IndexPath] = []

    
    var workoutDetail = WorkoutModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        configTable()
        // Do any additional setup after loading the view.
    }
    
    
    fileprivate func setupUI() {
        setBackButton()
        applyAttributedNAV()
    }
    
    fileprivate func configTable() {
        
        myTable.register(UINib(nibName: "WorkoutDetailCell", bundle: nil), forCellReuseIdentifier: "WorkoutDetailCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension WorkoutDetailController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return workoutDetail.images.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WorkoutDetailCell", for: indexPath) as! WorkoutDetailCell
        let image = self.workoutDetail.images[indexPath.row]

        cell.imageWorkout.setImageWith(path: image, imageType: .NORMAL_IMAGE)
        cell.imageWorkout.hero.isEnabled = true
        cell.imageWorkout.hero.id = "image_\(self.workoutDetail.images[indexPath.row])"
        cell.imageWorkout.hero.modifiers = [.forceNonFade, .spring(stiffness: 300, damping: 25)]
        cell.detailTitleWorkout.text = "\(indexPath.row + 1). \(self.workoutDetail.descriptions[indexPath.row])"
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = Controllers.shared.imagePreviewController()
        vc.hero.isEnabled = true
        vc.imageURL = workoutDetail.images[indexPath.row]
        let nav = UINavigationController(rootViewController: vc)
        nav.hero.isEnabled = true
        DispatchQueue.main.async {
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 248
    }
    
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            
            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)

            
            
            cell.transform = CGAffineTransform(translationX: 0, y: 50)
            cell.alpha = 0
            
            UIView.beginAnimations("rotation", context: nil)
            UIView.setAnimationDuration(0.5)
            cell.transform = CGAffineTransform(translationX: 0, y: 0)
            cell.alpha = 1
            UIView.commitAnimations()
        }
    }
    
    
}


















