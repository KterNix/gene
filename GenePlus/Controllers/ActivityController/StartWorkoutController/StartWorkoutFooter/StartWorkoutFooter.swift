//
//  StartWorkoutFooter.swift
//  GenePlus
//
//  Created by pro on 2/7/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class StartWorkoutFooter: UIView {

    
    @IBOutlet weak var cancelBTN: UIButton!
    @IBOutlet weak var finishBTN: UIButton!
    
    
    override func awakeFromNib() {
        finishBTN.cornerRadius = 5
    }
}
