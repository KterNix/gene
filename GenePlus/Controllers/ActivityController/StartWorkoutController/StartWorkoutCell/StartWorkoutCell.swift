//
//  StartWorkoutCell.swift
//  GenePlus
//
//  Created by pro on 2/6/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class StartWorkoutCell: UITableViewCell {

    
    @IBOutlet weak var titleLBL: UILabel!
    
    @IBOutlet weak var picThumb: UIImageView!
    
    @IBOutlet weak var kgLBL: UILabel!
    @IBOutlet weak var repLBL: UILabel!
    
    @IBOutlet weak var menuBTN: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupUI()
    }
    
    private func setupUI() {
        let edgeInset:CGFloat = 8
        menuBTN.imageEdgeInsets = UIEdgeInsets(top: edgeInset, left: edgeInset, bottom: edgeInset, right: edgeInset)
        menuBTN.setImage(UIImage(named: "squareMenu")?.withRenderingMode(.alwaysTemplate), for: UIControl.State.normal)
        menuBTN.tintColor = UIColor.darkGray
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
