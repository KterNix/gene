//
//  StartWorkoutController.swift
//  GenePlus
//
//  Created by pro on 2/6/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import PopupDialog
import DropDown


class StartWorkoutController: BaseViewController {

    
    
    
    
    @IBOutlet weak var myTable: UITableView!
    
    var footerView:StartWorkoutFooter?
    var headerView:ExercisesDetailHeader?

    var listExercises = [ExercisesModel]()
    var setModelTitle = [String]()
    var playlist:PlaylistModel?
    
    var currentIndexPath:IndexPath?
    
    fileprivate var currentIndex:Int = 0
    
    var dropDown:DropDown?
    var dropDataSource = ["EDIT",
                          "START WORKOUT"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDropdown()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //MARK: - Set Height for tableHeaderView
        if let footerView = myTable.tableFooterView {
            
            let height = footerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
            var footerFrame = footerView.frame
            
            //Comparison necessary to avoid infinite loop
            if height != footerFrame.size.height {
                footerFrame.size.height = height
                footerView.frame = footerFrame
                myTable.tableFooterView = footerView
            }
        }
    }

    override func setupUI() {
        title = "Start Workout"
        setBackButton()
        applyAttributedNAV()
        whiteNavigationColor()
    }
    
    override func configTable() {
        footerView = UIView.instanceFromNib()
        myTable.tableFooterView = footerView
        
        footerView?.cancelBTN.addTarget(self, action: #selector(didTapCancel), for: UIControl.Event.touchUpInside)
        footerView?.finishBTN.addTarget(self, action: #selector(didTapFinish), for: UIControl.Event.touchUpInside)
        
        
        myTable.register(UINib(nibName: "StartWorkoutCell", bundle: nil), forCellReuseIdentifier: "StartWorkoutCell")
        
    }
    
    fileprivate func setupDropdown() {
        dropDown = DropDown()
        dropDown?.dataSource = dropDataSource
        dropDown?.bottomOffset = CGPoint(x: 0, y: 50)
        dropDown?.backgroundColor = UIColor.white
        dropDown?.cellHeight = 60
        dropDown?.downScaleTransform = .init(scaleX: 0.8, y: 0.8)
        dropDown?.dimmedBackgroundColor = UIColor.black.withAlphaComponent(0.8)
    }
    
    fileprivate func showDropdownAt(pView: UIView, indexPath: IndexPath) {
        dropDown?.anchorView = pView
        
        dropDown?.selectionAction = { rowIndex, item in
            self.dropDown?.clearSelection()
            if rowIndex == 0 {
                //MARK: - EDIT
                let vc = Controllers.shared.exercisesDetailController()
                let items = self.listExercises[indexPath.row].setModel
                vc.setList = items
                self.navigationController?.pushViewController(vc)
            }else{
               //MARK: - START WORKOUT
                let vc = Controllers.shared.workoutingController()
                self.currentIndexPath = indexPath
                let items = self.listExercises[indexPath.section].setModel
                let item = items[indexPath.row]
                vc.setModel = item
                self.navigationController?.pushViewController(vc)
            }
        }
        dropDown?.show()
    }

    @objc fileprivate func didTapFinish() {
        Utils.alertWithAction(title: "Finish work out ?", cancelTitle: "CANCEL", message: "Tap CONFIRM to continue.", actionTitles: ["CONFIRM"], actions: [{ (action) in
            self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
            }], actionStyle: UIAlertAction.Style.destructive, viewController: self, style: UIAlertController.Style.alert)
    }
    
    @objc fileprivate func didTapCancel() {
        Utils.alertWithAction(title: "Cancel work out ?", cancelTitle: "CANCEL", message: "Tap CONFIRM to continue.", actionTitles: ["CONFIRM"], actions: [{ (action) in
            self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
            }], actionStyle: UIAlertAction.Style.destructive, viewController: self, style: UIAlertController.Style.alert)
    }
    
    fileprivate func showPopup() {
        let vc = StartWorkoutPopup(nibName: "StartWorkoutPopup", bundle: nil)
        let popupController = PopupDialog(viewController: vc, buttonAlignment: NSLayoutConstraint.Axis.horizontal, transitionStyle: .zoomIn, preferredWidth: view.frame.size.width - 20, tapGestureDismissal: true, hideStatusBar: true) {
        }
        present(popupController, animated: true, completion: nil)
    }
    
    deinit {
        headerView = nil
        footerView = nil
        dropDown = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension StartWorkoutController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return listExercises.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listExercises[section].setModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StartWorkoutCell", for: indexPath) as! StartWorkoutCell
        cell.selectionStyle = .none
        
        let items = listExercises[indexPath.section].setModel
        let item = items[indexPath.row]
        let imagePath = listExercises[indexPath.section].image
        cell.titleLBL.text = "Set \(indexPath.row + 1)"
        cell.kgLBL.text = item.weight
        cell.repLBL.text = item.reps

        cell.picThumb.setImageWith(path: imagePath, imageType: .NORMAL_IMAGE)
        
        cell.picThumb.tag = indexPath.row
        cell.picThumb.isUserInteractionEnabled = true
        let picGesture = UITapGestureRecognizer(target: self, action: #selector(didTapPic))
        cell.picThumb.addGestureRecognizer(picGesture)
        cell.menuBTN.addTarget(self, action: #selector(didTapMenu), for: UIControl.Event.touchUpInside)
        
//        currentIndex = indexPath.row
        
        return cell
    }
    
    @objc fileprivate func didTapPic(sender: UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.location(in: self.myTable)
        guard let indexPath = self.myTable.indexPathForRow(at: buttonPosition) else { return }
        print(indexPath)
        let items = listExercises[indexPath.section].setModel
        let item = items[indexPath.row]
        
        let vc = Controllers.shared.workoutImagePreviewController()
        let nav = UINavigationController(rootViewController: vc)
        vc.imagesList = item.images
        DispatchQueue.main.async {
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    @objc fileprivate func didTapStart(sender: AnyObject) {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to: self.myTable)
        guard let indexPath = self.myTable.indexPathForRow(at: buttonPosition) else { return }
        currentIndexPath = indexPath
        let items = listExercises[indexPath.section].setModel
        let item = items[indexPath.row]
        //        showPopup()
        let vc = Controllers.shared.trainingTimeController()
        vc.totalTime = CGFloat(item.duration.toInt())
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 41
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        headerView = UIView.instanceFromNib()
        self.headerView?.titleLBL.text = self.listExercises[section].name
        self.headerView?.weightLBL.text = self.listExercises[section].unit
//        self.headerView?.trailingConstraint.constant = 0
//        self.view.layoutIfNeeded()
        
        return headerView
    }
    
    
    
    @objc fileprivate func didTapMenu(sender: AnyObject) {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to: self.myTable)
        guard let indexPath = self.myTable.indexPathForRow(at: buttonPosition) else { return }
        currentIndexPath = indexPath
        guard let cell = myTable.cellForRow(at: indexPath) as? StartWorkoutCell else { return }
        showDropdownAt(pView: cell.menuBTN, indexPath: indexPath)
    }
}

extension StartWorkoutController: TrainingTimeControllerDelegate {
    func didCompleteWorkout(sender: TrainingTimeController) {
        //MARK: - FINISH SET
        guard currentIndexPath != nil else { return }
//        let cell = myTable.cellForRow(at: currentIndexPath!) as! StartWorkoutCell
//        cell.startBTN.backgroundColor = UIColor.lightGray
//        cell.startBTN.setTitle("COMPLETED", for: UIControlState.normal)
//        cell.startBTN.isEnabled = false
    }
}





















