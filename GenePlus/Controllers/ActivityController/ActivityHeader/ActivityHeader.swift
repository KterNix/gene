//
//  ActivityHeader.swift
//  GenePlus
//
//  Created by pro on 1/18/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class ActivityHeader: UIView {

    @IBOutlet weak var backgroundImage: UIImageView!
    
    @IBOutlet weak var titleLBL: UILabel!
    @IBOutlet weak var detailTitleLBL: UILabel!
    @IBOutlet weak var gradientView: GradientViewTop!
    
}
