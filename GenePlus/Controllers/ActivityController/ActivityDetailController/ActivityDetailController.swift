//
//  ActivityDetailController.swift
//  GenePlus
//
//  Created by pro on 1/18/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import PopupDialog

class ActivityDetailController: UIViewController {

    
    @IBOutlet weak var myTable: UITableView!
    
    var shownIndexes : [IndexPath] = []
    
    var activityDetail = ActivityModel()

    lazy var activityDetailHeader: ActivityDetailHeader = UIView.instanceFromNib()
    lazy var activityDetailFooter: ActivityDetailFooter = UIView.instanceFromNib()

    var activityDetailCellHeight: CGFloat = 150
    
    fileprivate var listImages = ["http://mcmfitness.com/wp-content/uploads/2014/09/mens-fitness.jpg",
                                  "http://www.lfitness.net/wp-content/uploads/2013/04/fitness-models.jpg",
                                  "https://cdn-mf0.heartyhosting.com/sites/mensfitness.com/files/crossfit_barbell_back_squat_main.jpg"]
    
    lazy var resultButton: UIButton = {
        let b = UIButton(type: UIButton.ButtonType.custom)
        b.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        b.setTitle("Result", for: UIControl.State.normal)
        b.setTitleColor(UIColor.mainColor(), for: UIControl.State.normal)
        b.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        b.addTarget(self, action: #selector(didTapResult), for: UIControl.Event.touchUpInside)
        return b
    }()
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        configTable()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        defaultStatusbar()
        setBackButton()
        whiteNavigationColor()
        applyAttributedNAV()
        
        
        let result = UIBarButtonItem(customView: resultButton)
        navigationItem.rightBarButtonItem = result
    }
    
    func updateTable(table: UITableView, withDuration duration: TimeInterval) {
        UIView.animate(withDuration: duration, animations: { () -> Void in
            table.beginUpdates()
            table.endUpdates()
        })
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if let categoriesCell = myTable.cellForRow(at: IndexPath(row: 1, section: 0)) as? ActivityWorkoutImageCell {
            let collectionViewContentHeight = categoriesCell.myColl.contentSize.height
            let sizeSpace:CGFloat = 80
            self.activityDetailCellHeight = collectionViewContentHeight + sizeSpace
//            self.drawLineSeparatorFor(cell: categoriesCell, height: self.categoriesCellHeight - 2)
            self.updateTable(table: self.myTable, withDuration: 0.2)
            
        }
    }
    
    fileprivate func setupUI() {
        setBackButton()
    }
    
    fileprivate func configTable() {
        
        myTable.register(UINib(nibName: "ActivityDetailCell", bundle: nil), forCellReuseIdentifier: "ActivityDetailCell")
        myTable.register(UINib(nibName: "ActivityWorkoutImageCell", bundle: nil), forCellReuseIdentifier: "ActivityWorkoutImageCell")

    }
    
    
    @objc fileprivate func didTapResult() {
        let vc = Controllers.shared.activityResultsController()
        let nav = UINavigationController(rootViewController: vc)
        DispatchQueue.main.async {
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ActivityDetailController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ActivityDetailCell", for: indexPath) as! ActivityDetailCell
            cell.selectionStyle = .none
            cell.timeLBL.text = activityDetail.time
            cell.durationLBL.text = activityDetail.duration
            cell.repeatLBL.text = activityDetail.repeatWorkout
            
            return cell
        }else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ActivityWorkoutImageCell", for: indexPath) as! ActivityWorkoutImageCell
            cell.selectionStyle = .none
            cell.listImages = activityDetail.imageWorkout
            return cell
        }
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 177
        }else if indexPath.row == 1 {
            return activityDetailCellHeight
        }
        return 10
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 177
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 69
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return activityDetailHeader
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        activityDetailFooter.startBTN.addTarget(self, action: #selector(didTapStart), for: UIControl.Event.touchUpInside)
        return activityDetailFooter
    }
    
    @objc fileprivate func didTapStart() {
        showPopup()
        
    }
    
    fileprivate func showPopup() {
        let vc = StartWorkoutPopup(nibName: "StartWorkoutPopup", bundle: nil)
        let popupController = PopupDialog(viewController: vc, buttonAlignment: NSLayoutConstraint.Axis.horizontal, transitionStyle: .zoomIn, preferredWidth: view.frame.size.width - 20, tapGestureDismissal: true, hideStatusBar: true) {
            
        }
        present(popupController, animated: true, completion: nil)
        
        
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            
            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            
            animateCell(cell: cell)
        }
    }
    
}


















