//
//  ActivityWorkoutImageCell.swift
//  GenePlus
//
//  Created by pro on 1/19/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import SDWebImage
import Hero

class ActivityWorkoutImageCell: UITableViewCell {

    
    
    
    
    @IBOutlet weak var myColl: UICollectionView!
    
    var listImages = [String]() {
        didSet {
            DispatchQueue.main.async {
//                self.myColl.reloadData()
            }
        }
    }
    
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        myColl.delegate = self
        myColl.dataSource = self
        myColl.register(UINib(nibName: "ImageCell", bundle: nil), forCellWithReuseIdentifier: "item")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

extension ActivityWorkoutImageCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "item", for: indexPath) as! ImageCell
        cell.gradientView.isHidden = true
        DispatchQueue.global().async {
            let url = URL(string: self.listImages[indexPath.row])
            DispatchQueue.main.async {
                cell.imageView.sd_setImage(with: url, placeholderImage: nil, options: SDWebImageOptions.continueInBackground, completed: nil)
            }
        }
        
        
        cell.imageView.heroID = "image_\(self.listImages[indexPath.row])"
        cell.imageView.heroModifiers = [.scale(0.8)]
        cell.imageView.isOpaque = true
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = Controllers.shared.imagePreviewController()
        vc.imageURL = listImages[indexPath.row]
        let nav = UINavigationController(rootViewController: vc)
        nav.isHeroEnabled = true
        parentViewController?.present(nav, animated: true, completion: nil)

    }
    
    func viewController(forStoryboardName: String) -> UIViewController {
        return UIStoryboard(name: forStoryboardName, bundle: nil).instantiateInitialViewController()!
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let sizeWidth:CGFloat = (parentViewController?.view.frame.size.width)! / 3 - 16
//        let sizeHeight:CGFloat =
//        let sizeWidth:CGFloat = (parentViewController?.view.frame.size.width)! - 20
//        let sizeHeight:CGFloat = (parentViewController?.view.frame.size.height)! / 2
        return CGSize(width: sizeWidth, height: sizeWidth)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let edgeInset:CGFloat = 10
        return UIEdgeInsets(top: edgeInset, left: 0, bottom: edgeInset, right: 0)
        
    }
    
    
}



extension ActivityWorkoutImageCell: HeroViewControllerDelegate {
    func heroWillStartAnimatingTo(viewController: UIViewController) {
        
        if (viewController as? ImageController) != nil {
            myColl.heroModifiers = [.cascade(delta:0.015, direction:.bottomToTop, delayMatchedViews:true)]
        } else if (viewController as? ImageViewController) != nil {
            let cell = myColl.cellForItem(at: myColl.indexPathsForSelectedItems!.first!)!
            myColl.heroModifiers = [.cascade(delta: 0.015, direction: .radial(center: cell.center), delayMatchedViews: true)]
        } else {
            myColl.heroModifiers = [.cascade(delta:0.015)]
        }
    }
    func heroWillStartAnimatingFrom(viewController: UIViewController) {
        parentViewController?.view.heroModifiers = nil
        if (viewController as? ImageController) != nil {
            myColl.heroModifiers = [.cascade(delta:0.015), .delay(0.25)]
        } else {
            myColl.heroModifiers = [.cascade(delta:0.015)]
        }
        if let vc = viewController as? ImageViewController,
            let originalCellIndex = vc.selectedIndex,
            let currentCellIndex = vc.collectionView?.indexPathsForVisibleItems[0],
            let targetAttribute = myColl.layoutAttributesForItem(at: currentCellIndex) {
            myColl.heroModifiers = [.cascade(delta:0.015, direction:.inverseRadial(center:targetAttribute.center))]
            if !myColl.indexPathsForVisibleItems.contains(currentCellIndex) {
                // make the cell visible
                myColl.scrollToItem(at: currentCellIndex,
                                    at: originalCellIndex < currentCellIndex ? .bottom : .top,
                                    animated: false)
            }
        }
    }
}














