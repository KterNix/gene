//
//  ActivityDetailFooter.swift
//  GenePlus
//
//  Created by pro on 1/19/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class ActivityDetailFooter: UIView {

    @IBOutlet weak var removeBTN: UIButton!
    
    @IBOutlet weak var startBTN: UIButton!
    
    override func awakeFromNib() {
        
        removeBTN.layer.cornerRadius = Constant.buttonCornerRadius
        removeBTN.layer.masksToBounds = true
        
        startBTN.layer.cornerRadius = Constant.buttonCornerRadius
        startBTN.layer.masksToBounds = true
        
        
    }

}
