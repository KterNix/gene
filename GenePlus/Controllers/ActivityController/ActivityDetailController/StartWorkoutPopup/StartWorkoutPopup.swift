//
//  StartWorkoutPopup.swift
//  GenePlus
//
//  Created by pro on 1/22/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import MBCircularProgressBar

class StartWorkoutPopup: UIViewController {

    
    @IBOutlet weak var progressView: MBCircularProgressBarView!
    
    @IBOutlet weak var pauseBTN: UIButton!
    
    
    
    var timer:Timer?
    
    var totalTime:CGFloat = 100
    var countDownTime:CGFloat = 1
    
    var isPause:Bool = false
    var didStart:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pauseBTN.layer.cornerRadius = Constant.buttonCornerRadius
        pauseBTN.layer.masksToBounds = true
        
        
        progressView.value = totalTime
        progressView.maxValue = totalTime
        progressView.unitString = "s"
        
        setPauseBTN(title: "START")
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    
    fileprivate func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(updateProgess), userInfo: nil, repeats: true)
    }
    
    
    @objc fileprivate func updateProgess() {
        UIView.animate(withDuration: 0.1) {
            guard self.totalTime > 0 else {
                self.timer?.invalidate()
                return
            }
            self.totalTime -= self.countDownTime
            self.progressView.value = self.totalTime
        }
    }
    
    
    
    
    @IBAction func pauseAction(_ sender: Any) {
        if !didStart {
            didStart = true
            setPauseBTN(title: "PAUSE")
            startTimer()
            return
        }
        
        if isPause {
            setPauseBTN(title: "PAUSE")
            startTimer()
            isPause = false
        }else{
            setPauseBTN(title: "CONTINUE")
            timer?.invalidate()
            isPause = true
        }
    }
    
    
    fileprivate func setPauseBTN(title: String) {
        pauseBTN.setTitle(title, for: UIControl.State.normal)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        guard timer != nil else { return }
        timer?.invalidate()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
}















