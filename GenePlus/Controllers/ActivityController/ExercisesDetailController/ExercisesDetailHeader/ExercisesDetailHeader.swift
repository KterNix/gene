//
//  ExercisesDetailHeader.swift
//  GenePlus
//
//  Created by pro on 2/6/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class ExercisesDetailHeader: UIView {

    @IBOutlet weak var titleLBL: UILabel!
    
    @IBOutlet weak var weightLBL: UILabel!
    @IBOutlet weak var repsLBL: UILabel!
    
    
    @IBOutlet weak var trailingConstraint: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    
}
