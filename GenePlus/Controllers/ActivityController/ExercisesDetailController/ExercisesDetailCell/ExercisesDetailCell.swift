//
//  ExercisesDetailCell.swift
//  GenePlus
//
//  Created by pro on 2/6/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ExercisesDetailCell: UITableViewCell {

    
    
    @IBOutlet weak var setLBL: UILabel!
    
    @IBOutlet weak var weightTXT: UITextField!
    
    @IBOutlet weak var repsTXT: UITextField!
    @IBOutlet weak var durationTXT: UITextField!
    
    
    @IBOutlet weak var removeBTN: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        repsTXT.textAlignment = .center
        weightTXT.textAlignment = .center
        let edgeInset:CGFloat = 6
        removeBTN.imageEdgeInsets = UIEdgeInsets(top: edgeInset, left: edgeInset, bottom: edgeInset, right: edgeInset)
        
        removeBTN.tintColor = UIColor.lightGray.withAlphaComponent(0.5)
        removeBTN.setImage(UIImage(named: "close")?.withRenderingMode(.alwaysTemplate), for: UIControl.State.normal)
        
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
