//
//  ExercisesDetailController.swift
//  GenePlus
//
//  Created by pro on 2/6/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import Eureka

protocol ExercisesDetailControllerDelegate: class {
    func didChange(item: [SetModel], sender: ExercisesDetailController)
}



class ExercisesDetailController: FormViewController {

    
    
    
    lazy var footerBTN: ButtonView? = UIView.instanceFromNib()
    var shownIndexes : [IndexPath] = []

    
    var setList = [SetModel]()
    
    
    
    weak var delegate: ExercisesDetailControllerDelegate?
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        loadForm()
        listenForKeyboard()
    }
    
    fileprivate func setupUI() {
        setBackButton()
        
        tableView.backgroundColor = UIColor.white
        tableView.separatorStyle = .none
        
        
        LabelRow.defaultCellSetup = { cell, row in
            cell.textLabel?.font = UIFont.systemFont(ofSize: 15)
            cell.textLabel?.textColor = UIColor.lightGray
            cell.tintColor = UIColor.lightGray
        }
        
        TextRow.defaultCellSetup = { cell, row in
            cell.textLabel?.font = UIFont.systemFont(ofSize: 15)
            cell.textLabel?.textColor = UIColor.lightGray
            cell.tintColor = UIColor.lightGray
        }
    }
    
    fileprivate func loadForm() {
        
        form +++
            
        Section()
            
        <<< LabelRow() {
            $0.title = "Name"
        }
        
        <<< TextRow() {
            $0.title = "Unit"
        }
        
        <<< TextRow() {
            $0.title = "Repeat"
        }
        
        
    }
    
   
    
    @objc fileprivate func didTapSave() {
        delegate?.didChange(item: setList, sender: self)
        navigationController?.popViewController(animated: true)
    }
    
    deinit {
        footerBTN = nil
        removeNotifiKeyboard()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ExercisesDetailController {
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            animateCell(cell: cell)
        }
    }
    
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 60
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        footerBTN?.chooseButton.addTarget(self, action: #selector(didTapSave), for: UIControl.Event.touchUpInside)
        return footerBTN
    }
    
    
}










