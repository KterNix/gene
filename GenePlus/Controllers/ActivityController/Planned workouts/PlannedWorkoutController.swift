//
//  PlannedWorkoutController.swift
//  GenePlus
//
//  Created by pro on 2/6/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class PlannedWorkoutController: UIViewController {

    
    
    @IBOutlet weak var myTable: UITableView!
    
    var shownIndexes : [IndexPath] = []

    var listPlan = [PlaylistModel]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        configTable()
        setupDataSource()
        // Do any additional setup after loading the view.
    }
    
    fileprivate func setupUI() {
        title = "Planned Workouts"
        setBackButton()
        whiteNavigationColor()
        applyAttributedNAV()
    }

    fileprivate func configTable() {
        myTable.register(UINib(nibName: "CompleteWorkoutCell", bundle: nil), forCellReuseIdentifier: "CompleteWorkoutCell")

    }
    
    fileprivate func setupDataSource() {
        var a = PlaylistModel()
        var e = ExercisesModel()
        var s = SetModel()
        
        
        s.reps = "2"
        s.weight = "12"
        s.duration = "30"
        s.unit = "KG"
        
        
        e.name = "Smith tricep press"
        e.image = ""
        e.unit = "KG"
        e.setModel = [s]
        
        
        a.name = "Arm & Abs"
        a.timeCreated = "MON 2"
        a.exercises = [e,e,e]
        listPlan.append(a)
        
        a = PlaylistModel()
        a.name = "Arms & Abs Home"
        a.timeCreated = "TUE 3"
        a.exercises = [e]
        listPlan.append(a)
        
        a = PlaylistModel()
        a.name = "Back"
        a.timeCreated = "WED 5"
        a.exercises = [e,e,e,e,e]
        listPlan.append(a)
        
        a = PlaylistModel()
        a.name = "Back & Abs"
        a.timeCreated = "SUN 8"
        a.exercises = [e]
        listPlan.append(a)
        
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension PlannedWorkoutController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listPlan.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CompleteWorkoutCell", for: indexPath) as! CompleteWorkoutCell
        let item = listPlan[indexPath.row]
        cell.titleLBL.text = item.name
        cell.timeLBL.text = item.timeCreated
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = listPlan[indexPath.row]
        
        let vc = Controllers.shared.exercisesController()
        vc.listExercises = item.exercises
        vc.title = item.name
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            
            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            
            animateCell(cell: cell)
        }
    }
}




































