//
//  ExercisesCell.swift
//  GenePlus
//
//  Created by pro on 2/5/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class ExercisesCell: UITableViewCell {

    
    
    @IBOutlet weak var titleLBL: UILabel!
    
    
    @IBOutlet weak var setLBL: UILabel!
    
    
    @IBOutlet weak var imageExercise: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
