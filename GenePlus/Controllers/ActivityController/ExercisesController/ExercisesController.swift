//
//  ExercisesController.swift
//  GenePlus
//
//  Created by pro on 2/5/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class ExercisesController: UIViewController {

    
    @IBOutlet weak var myTable: UITableView!
    
    
    var shownIndexes : [IndexPath] = []

    var listExercises = [ExercisesModel]()
    
    fileprivate var currentIndex:Int = 0
    
    
    lazy var footerButton:ButtonView? = UIView.instanceFromNib()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        configTable()
        // Do any additional setup after loading the view.
    }
    
    fileprivate func setupUI() {
        setBackButton()
        applyAttributedNAV()
    }
    
    
    fileprivate func configTable() {
        myTable.register(UINib(nibName: "ExercisesCell", bundle: nil), forCellReuseIdentifier: "ExercisesCell")
        myTable.register(UINib(nibName: "AddExercisesCell", bundle: nil), forCellReuseIdentifier: "AddExercisesCell")
    }
    
    
    
    deinit {
        footerButton = nil
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}

extension ExercisesController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listExercises.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == listExercises.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddExercisesCell", for: indexPath) as! AddExercisesCell
            cell.selectionStyle = .none
            return cell
        }
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExercisesCell", for: indexPath) as! ExercisesCell
        cell.selectionStyle = .none
        let item = listExercises[indexPath.row]
        cell.titleLBL.text = item.name
        cell.setLBL.text = "\(item.setModel.count) sets"
        cell.imageExercise.image = UIImage(named: "\(item.image)")
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == listExercises.count {
            let vc = Controllers.shared.addActivityController()
            vc.delegate = self
            navigationController?.pushViewController(vc, animated: true)
            return
        }
        
        let vc = Controllers.shared.exercisesDetailController()
        vc.setList = listExercises[indexPath.row].setModel
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
        currentIndex = indexPath.row
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        footerButton?.chooseButton.setTitle("START WORKOUT", for: UIControl.State.normal)
        footerButton?.chooseButton.addTarget(self, action: #selector(didTapStart), for: UIControl.Event.touchUpInside)
        return footerButton
    }
    
    @objc fileprivate func didTapStart() {
        let vc = Controllers.shared.startWorkoutController()
        vc.listExercises = listExercises
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            
            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            
            animateCell(cell: cell)
        }
    }
}


extension ExercisesController: ExercisesDetailControllerDelegate {
    
    func didChange(item: [SetModel], sender: ExercisesDetailController) {
        listExercises[currentIndex].setModel = item
        let indexPath = IndexPath(row: currentIndex, section: 0)
        DispatchQueue.main.async {
            self.myTable.reloadRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
        }
    }
}

extension ExercisesController: AddActivityControllerDelegate {
    func didChooseExercise(item: ExercisesModel, sender: AddActivityController) {
        listExercises.append(item)
        print(listExercises)
        DispatchQueue.main.async {
            self.myTable.reloadData()
        }
    }
}































