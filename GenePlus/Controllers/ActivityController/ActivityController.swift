//
//  ActivityController.swift
//  GenePlus
//
//  Created by pro on 1/17/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import GSKStretchyHeaderView
import Charts

class ActivityController: BaseViewController {

    enum Cells: Int, CaseCountable {
        case ADD_PLAYLIST
        case MANAGE_PLALIST
        case COMPLETED_WORKOUT
        case DOWNLOADED
    }
    
    @IBOutlet weak var myTable: UITableView!
    
    var shownIndexes : [IndexPath] = []

    
    var listActivity = [ActivityModel]()
    var stretchyHeaderView:GSKStretchyHeaderView?
    var activityHeader:ActivityHeader? = UIView.instanceFromNib()
    var header:BaseHeader?
    
    //, "clock_calendar"
    //, "Planned workouts"
    fileprivate var listIcon = ["plus", "manageIcon", "completeIcon", "downloadIcon"]
    fileprivate var listOptions = ["Add Playlist", "Manage Playlist", "Completed workout", "Downloaded"]
    
    var chartItems = [30, 20, 10, 8, 12, 15, 26, 18, 12, 15]
    var discribeArray = ["15/05", "16/05", "17/05", "18/05", "19/05", "20/05", "21/05", "22/05", "23/05", "24/05"]
    
    var chartEntrys = [BarChartDataEntry]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDataSource()
        // Do any additional setup after loading the view.
    }

    
    override func setupUI() {
        defaultStatusbar()
        clearNavigationColor()
        applyAttributedNAV()
        
    }
    
    override func configTable() {
        
        activityHeader?.frame.size.height = 250
        activityHeader?.frame.size.width = myTable.frame.size.width
        
        stretchyHeaderView = GSKStretchyHeaderView(frame: CGRect(x: 0, y: 0, width: myTable.frame.size.width, height: 250))
        stretchyHeaderView?.expansionMode = .topOnly
        stretchyHeaderView?.contentShrinks = true
        stretchyHeaderView?.contentExpands = true
        stretchyHeaderView?.minimumContentHeight = 64
        stretchyHeaderView?.maximumContentHeight = 250
        stretchyHeaderView?.addSubview(activityHeader!)
        myTable.addSubview(stretchyHeaderView!)

        stretchyHeaderView?.stretchDelegate = self
        
        
        myTable.register(nibWithCellClass: AreaLineCell.self)
        myTable.register(nibWithCellClass: ActivityCell.self)
        
        
        myTable.backgroundColor = UIColor.white
    }
    
    fileprivate func setupDataSource() {
        var a = ActivityModel()
        
        a.title = "Push Up"
        a.time = "20:00"
        a.createdTime = "just now"
        a.duration = "20 Minutes"
        a.muscleGroup = "Shoulders group"
        a.repeatWorkout = "MON, TUE"
        a.imageWorkout = ["https://images.askmen.com/1080x540/2017/03/13-115153-the_5_day_workout.jpg","http://cdn-img.health.com/sites/default/files/styles/medium_16_9/public/styles/main/public/gettyimages-498315681.jpg?itok=qqWRWFw9", "https://media.self.com/photos/57617a01ecb631d76745ce73/master/pass/trainer-to-go-abs-workout.jpg"]
        listActivity.append(a)
        
        a = ActivityModel()
        a.title = "Back Flyes With Resistance Bands"
        a.time = "12:00"
        a.createdTime = "2 days ago"
        a.duration = "50 Minutes"
        a.muscleGroup = "Chest group"
        a.repeatWorkout = "Weekly"
        a.imageWorkout = ["https://www.bodybuilding.com/images/2016/july/10-best-chest-exercises-for-building-muscle-v2-1-700xh.jpg","https://cdn.muscleandstrength.com/sites/default/files/machine_press_exercise_for_chest_feature.jpg", "https://www.bodybuilding.com/images/2016/july/10-best-chest-exercises-for-building-muscle-header-v2-830x467.jpg"]
        listActivity.append(a)
        
        a = ActivityModel()
        a.title = "Lat Pulldown"
        a.time = "06:00"
        a.createdTime = "1 days ago"
        a.duration = "40 Minutes"
        a.muscleGroup = "Legs"
        a.repeatWorkout = "Daily"
        a.imageWorkout = ["https://www.building-muscle101.com/images/leg-workout-main.jpg","https://cdn-mf0.heartyhosting.com/sites/mensfitness.com/files/styles/gallery_slideshow_image/public/dumbbell_squat-the-30-best-legs-exercises-of-all-time.jpg?itok=KIUpjEjq", "https://3i133rqau023qjc1k3txdvr1-wpengine.netdna-ssl.com/wp-content/uploads/2014/10/Plie-Leg-Lift_Exercise.jpg"]
        listActivity.append(a)
        
        
        
        
        let c = BarChartDataEntry(x: 800, y: 20)
        let f = BarChartDataEntry(x: 810, y: 10)
        let d = BarChartDataEntry(x: 820, y: 30)
        chartEntrys = [c, f, d]
        
    }
    
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
     
        
        /*
         scrollView.contentOffset.y: -64
         scrollView.frame.size.height: size Height
         fabsf -> Chuyển số âm thành dương
         */
        
//        guard scrollView.contentOffset.y < 0 else { return }
//        var scale = 1 + fabsf(Float(scrollView.contentOffset.y)) / Float(scrollView.frame.size.height / 5)
//        scale = max(0, scale)
//        
//        
//        activityHeader?.backgroundImage.transform = CGAffineTransform.identity.scaledBy(x: CGFloat(scale), y: CGFloat(scale))
        
    }
    
    
    deinit {
        activityHeader = nil
        stretchyHeaderView = nil
        header = nil
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension ActivityController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        return listOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            //MARK: - ACTIVITY
            let cell = tableView.dequeueReusableCell(withClass: AreaLineCell.self)
            cell.selectionStyle = .none
            cell.detailBTN.addTarget(self, action: #selector(didTapChartDetail), for: UIControl.Event.touchUpInside)
            cell.configChart(chartEntrys: chartEntrys)

            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withClass: ActivityCell.self)
        cell.selectionStyle = .none
        let item = listOptions[indexPath.row]
        cell.titleActivity.text = item
        cell.activityIcon.image = UIImage(named: "\(listIcon[indexPath.row])")
        return cell
        
    }
    
    @objc fileprivate func didTapChartDetail() {
        let vc = Controllers.shared.chartController()
        let nav = UINavigationController(rootViewController: vc)
        
        DispatchQueue.main.async {
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.section == 1 else { return }
        switch indexPath.row {
        case Cells.ADD_PLAYLIST.rawValue:
            let vc = Controllers.shared.addPlaylistController()
            let nav = UINavigationController(rootViewController: vc)
            vc.presentSource = .NEW
            vc.delegate = self
            DispatchQueue.main.async {
                self.present(nav, animated: true, completion: nil)
            }
        case Cells.MANAGE_PLALIST.rawValue:
            let vc = Controllers.shared.managePlaylistController()
            let nav = UINavigationController(rootViewController: vc)
            DispatchQueue.main.async {
                self.present(nav, animated: true, completion: nil)
            }
        case Cells.COMPLETED_WORKOUT.rawValue:
            let vc = Controllers.shared.completeWorkoutController()
            
            let nav = UINavigationController(rootViewController: vc)
            DispatchQueue.main.async {
                self.present(nav, animated: true, completion: nil)
            }
            
        case Cells.DOWNLOADED.rawValue:
            
            let vc = Controllers.shared.downloadController()
            vc.presentDownloadFrom = .activity
            vc.title = "Downloaded"
            let nav = UINavigationController(rootViewController: vc)
            DispatchQueue.main.async {
                self.present(nav, animated: true, completion: nil)
            }
            
        default:
            break
        }
//        let vc = Controllers.shared.activityDetailController()
//        vc.activityDetail = listActivity[indexPath.row]
//        let nav = UINavigationController(rootViewController: vc)
//        DispatchQueue.main.async {
//            self.present(nav, animated: true, completion: nil)
//        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 250
        }
        return 60
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
       
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            
            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            
        }
    }
    
    //MARK: - HEADER
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        header = UIView.instanceFromNib()
        header?.backgroundColor = UIColor.clear
        
        if section == 0 {
            header?.titleLBL.text = "ACTIVITY"
        }else{
            header?.titleLBL.text = "MENU"
        }
        
        return header
    }
}

extension ActivityController: AddPlaylistControllerDelegate {
    func didAddPlaylist(sender: AddPlaylistController) {
        let vc = Controllers.shared.managePlaylistController()
        let nav = UINavigationController(rootViewController: vc)
        DispatchQueue.main.async {
            self.present(nav, animated: true, completion: nil)
        }
    }
}

extension ActivityController: GSKStretchyHeaderViewStretchDelegate {
    func stretchyHeaderView(_ headerView: GSKStretchyHeaderView, didChangeStretchFactor stretchFactor: CGFloat) {
        activityHeader?.titleLBL.alpha = stretchFactor
        activityHeader?.detailTitleLBL.alpha = stretchFactor
    }
}




















