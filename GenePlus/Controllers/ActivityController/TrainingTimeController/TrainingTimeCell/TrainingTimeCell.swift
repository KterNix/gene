//
//  TrainingTimeCell.swift
//  GenePlus
//
//  Created by pro on 5/16/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class TrainingTimeCell: UITableViewCell {

    
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var detailLBL: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
