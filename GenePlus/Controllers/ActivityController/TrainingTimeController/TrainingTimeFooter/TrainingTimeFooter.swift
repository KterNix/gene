//
//  TrainingTimeFooter.swift
//  GenePlus
//
//  Created by pro on 5/16/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import MBCircularProgressBar

class TrainingTimeFooter: UIView {

    @IBOutlet weak var progressView: MBCircularProgressBarView!
    
    @IBOutlet weak var startBTN: UIButton!
    
    @IBOutlet weak var cancelBTN: UIButton!
    
    @IBOutlet weak var degreeSeconds: UIButton!
    @IBOutlet weak var increaseSeconds: UIButton!
    
    
    
    override func awakeFromNib() {
        startBTN.cornerRadius = 5
        progressView.progressLineWidth = 5
        
        degreeSeconds.cornerRadius = 5
        degreeSeconds.borderWidth = 1
        degreeSeconds.borderColor = UIColor.lightGray
        
        increaseSeconds.cornerRadius = 5
        increaseSeconds.borderWidth = 1
        increaseSeconds.borderColor = UIColor.lightGray
        
        
    }

}
