//
//  TrainingTimeController.swift
//  GenePlus
//
//  Created by pro on 5/16/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import AudioToolbox

protocol TrainingTimeControllerDelegate: class {
    func didCompleteWorkout(sender: TrainingTimeController)
}


class TrainingTimeController: UIViewController {

    
    @IBOutlet weak var myTable: UITableView!
    
    
    let deviceIdiom = UIScreen.main.traitCollection.userInterfaceIdiom

    var tableFooter:TrainingTimeFooter?
    var tableFooter_Pad:TrainingTimeFooter_Pad?
    
    weak var delegate: TrainingTimeControllerDelegate?
    
    var timer:Timer?
    
    var totalTime:CGFloat = 2
    var countDownTime:CGFloat = 1

    
    var isPause:Bool = false
    var didStart:Bool = false
    
    let systemSoundID: SystemSoundID = 1016
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackButton()
        myTable.delegate = self
        myTable.dataSource = self
        
        configTable()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //MARK: - Set Height for tableHeaderView
        if let footerView = myTable.tableFooterView {
            
            let height = footerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
            var footerFrame = footerView.frame
            
            //Comparison necessary to avoid infinite loop
            if height != footerFrame.size.height {
                footerFrame.size.height = height
                footerView.frame = footerFrame
                myTable.tableFooterView = footerView
            }
        }
    }
    
    
    fileprivate func configTable() {
        if deviceIdiom == .phone {
            tableFooter = UIView.instanceFromNib()
            tableFooter?.startBTN.addTarget(self, action: #selector(didTapStartBTN), for: UIControl.Event.touchUpInside)
            tableFooter?.cancelBTN.addTarget(self, action: #selector(didTapCancelBTN), for: UIControl.Event.touchUpInside)
            tableFooter?.degreeSeconds.addTarget(self, action: #selector(didTapDegreeSeconds), for: UIControl.Event.touchUpInside)
            tableFooter?.increaseSeconds.addTarget(self, action: #selector(didTapIncreaseSeconds), for: UIControl.Event.touchUpInside)
            
            tableFooter?.progressView.value = totalTime
            tableFooter?.progressView.maxValue = totalTime
            tableFooter?.progressView.unitString = "s"
            
            myTable.tableFooterView = tableFooter
            
        }else{
            tableFooter_Pad = UIView.instanceFromNib()
            tableFooter_Pad?.startBTN.addTarget(self, action: #selector(didTapStartBTN), for: UIControl.Event.touchUpInside)
            tableFooter_Pad?.cancelBTN.addTarget(self, action: #selector(didTapCancelBTN), for: UIControl.Event.touchUpInside)
            tableFooter_Pad?.degreeSeconds.addTarget(self, action: #selector(didTapDegreeSeconds), for: UIControl.Event.touchUpInside)
            tableFooter_Pad?.increaseSeconds.addTarget(self, action: #selector(didTapIncreaseSeconds), for: UIControl.Event.touchUpInside)
            
            tableFooter_Pad?.progressView.value = totalTime
            tableFooter_Pad?.progressView.maxValue = totalTime
            tableFooter_Pad?.progressView.unitString = "s"
            
            myTable.tableFooterView = tableFooter_Pad
        }
        
        
        setPauseBTN(title: "START")
        
        
        myTable.register(nibWithCellClass: TrainingTimeCell.self)
    }
    
    @objc fileprivate func didTapStartBTN() {
        if !didStart {
            didStart = true
            setPauseBTN(title: "PAUSE")
            startTimer()
            return
        }
        
        if isPause {
            setPauseBTN(title: "PAUSE")
            startTimer()
            isPause = false
        }else{
            setPauseBTN(title: "CONTINUE")
            timer?.invalidate()
            isPause = true
        }
    }
    
    @objc fileprivate func didTapCancelBTN() {
        Utils.alertWithAction(title: "Cancel Workout ?", cancelTitle: "CANCEL", message: "Tap CONFIRM to continue.", actionTitles: ["CONFIRM"], actions: [{ (action) in
            self.timer?.invalidate()
            self.navigationController?.popViewController(animated: true)
            }], actionStyle: UIAlertAction.Style.destructive, viewController: self, style: UIAlertController.Style.alert)
    }
    
    @objc fileprivate func didTapDegreeSeconds() {
        guard totalTime > 30 else { return }
        totalTime -= 30
    }
    
    @objc fileprivate func didTapIncreaseSeconds() {
        totalTime += 30
        if deviceIdiom == .phone {
            tableFooter?.progressView.maxValue = totalTime
            tableFooter?.progressView.value = totalTime
        }else{
            tableFooter_Pad?.progressView.maxValue = totalTime
            tableFooter_Pad?.progressView.value = totalTime
        }
       
    }
    
    fileprivate func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateProgess), userInfo: nil, repeats: true)
    }
    
    
    @objc fileprivate func updateProgess() {
        UIView.animate(withDuration: 0.1) {
            guard self.totalTime > 0 else {
                self.timer?.invalidate()
                self.delegate?.didCompleteWorkout(sender: self)
                self.vibrateAndSound()
                self.navigationController?.popViewController(animated: true)
                return
            }
            self.totalTime -= self.countDownTime
            if self.deviceIdiom == .phone {
                self.tableFooter?.progressView.value = self.totalTime
            }else{
                self.tableFooter_Pad?.progressView.value = self.totalTime
            }
        }
    }
    
    private func vibrateAndSound() {
//        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
//        AudioServicesPlaySystemSound(1519) // Actuate `Peek` feedback (weak boom)
        AudioServicesPlaySystemSound(1520)
        AudioServicesPlaySystemSound(systemSoundID)
    }
    
    fileprivate func setPauseBTN(title: String) {
        if self.deviceIdiom == .phone {
            tableFooter?.startBTN.setTitle(title, for: UIControl.State.normal)
        }else{
            tableFooter_Pad?.startBTN.setTitle(title, for: UIControl.State.normal)
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        guard timer != nil else { return }
        timer?.invalidate()
    }
    
    
    deinit {
        timer = nil
        tableFooter = nil
        tableFooter_Pad = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension TrainingTimeController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: TrainingTimeCell.self)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 196
    }
    
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return 236
//    }
//
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        tableFooter = UIView.instanceFromNib()
//        tableFooter?.startBTN.addTarget(self, action: #selector(didTapStartBTN), for: UIControlEvents.touchUpInside)
//        setPauseBTN(title: "START")
//        tableFooter?.progressView.value = totalTime
//        tableFooter?.progressView.maxValue = totalTime
//        tableFooter?.progressView.unitString = "s"
//
//        return tableFooter
//    }
    
}




