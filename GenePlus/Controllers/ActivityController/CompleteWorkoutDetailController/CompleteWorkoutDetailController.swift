//
//  CompleteWorkoutDetailController.swift
//  GenePlus
//
//  Created by pro on 2/6/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class CompleteWorkoutDetailController: UIViewController {

    
    @IBOutlet weak var myTable: UITableView!
    
    lazy var shareImage = UIImage(named: "shareSmall")?.withRenderingMode(.alwaysTemplate)
    lazy var shareButton:UIButton = {
        let b = UIButton(type: UIButton.ButtonType.custom)
        b.setImage(shareImage!, for: UIControl.State.normal)
        let sizeB:CGFloat = 30
        b.frame = CGRect(x: 0, y: 0, width: sizeB, height: sizeB)
        b.addTarget(self, action: #selector(didTapShare), for: UIControl.Event.touchUpInside)
        return b
    }()
    
    
    lazy var headerView:ActivityResultHeader? = UIView.instanceFromNib()
    
    
    var items = ["Duration", "Burn", "Sets", "Reps", "Exercises"]
    var detail = ["10:00", "250Kcal", "10", "15", "2"]
    
    var chartItems = [100, 80, 90, 150, 200]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        configTable()
        // Do any additional setup after loading the view.
    }
    
    fileprivate func setupUI() {
        title = "Result"
        setBackButton()
        
        let share = UIBarButtonItem(customView: shareButton)
        navigationItem.rightBarButtonItems = [share]
    }
    
    fileprivate func configTable() {
        
//        myTable.tableHeaderView = headerView
//        myTable.tableHeaderView?.layer.zPosition = -11
        
        myTable.register(nibWithCellClass: AreaLineCell.self)
//        myTable.register(UINib(nibName: "CompleteWorkoutDetailCell", bundle: nil), forCellReuseIdentifier: "CompleteWorkoutDetailCell")
    }
    
    @objc fileprivate func didTapShare() {
        let shareItem = ActivityShare()
        shareItem.news = ""
        shareItem.url = ""
        share(object: shareItem)
    }
    
    
    
    private func share(object : ActivityShare) {
        let vc = UIActivityViewController(activityItems: [object,object.image,object.news], applicationActivities: nil)
        DispatchQueue.main.async {
            self.navigationController?.present(vc, animated: true, completion: nil)
        }
    }
    
    deinit {
        headerView = nil
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

extension CompleteWorkoutDetailController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "CompleteWorkoutDetailCell", for: indexPath) as! CompleteWorkoutDetailCell
//        cell.selectionStyle = .none
//        let item = items[indexPath.row]
//        let detailItem = detail[indexPath.row]
//
//        cell.titleLBL.text = item
//        cell.detailTitleLBL.text = detailItem
        
        
        let cell = tableView.dequeueReusableCell(withClass: AreaLineCell.self)
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
}





















































