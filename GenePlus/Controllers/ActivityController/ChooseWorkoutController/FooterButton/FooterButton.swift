//
//  FooterButton.swift
//  GenePlus
//
//  Created by pro on 6/27/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class FooterButton: UIView {

    @IBOutlet weak var myBTN: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        myBTN.cornerRadius = 5
    }

}
