//
//  ChooseWorkoutCell.swift
//  GenePlus
//
//  Created by pro on 6/27/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class ChooseWorkoutCell: UITableViewCell {

    @IBOutlet weak var imagePic: UIImageView!
    
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var titleLBL: UILabel!
    @IBOutlet weak var detailTitleLBL: UILabel!
    
    @IBOutlet weak var weightLBL: UILabel!
    @IBOutlet weak var repsLBL: UILabel!
    @IBOutlet weak var setLBL: UILabel!
    
    @IBOutlet weak var checkImage: UIImageView!
    
    func configWith(value: WorkoutModel) {
        
        titleLBL.text = value.title
        detailTitleLBL.text = value.trainer
        weightLBL.text = value.weight
        repsLBL.text = value.reps
        setLBL.text = value.sets
        
        if let imageWorkout = value.images.first {
            imagePic.setImageWith(path: imageWorkout, imageType: .NORMAL_IMAGE)
        }
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupUI()
    }
    
    
    private func setupUI() {
        parentView.cornerRadius = 1
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
