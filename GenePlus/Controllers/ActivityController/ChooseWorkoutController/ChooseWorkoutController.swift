//
//  ChooseWorkoutController.swift
//  GenePlus
//
//  Created by pro on 6/27/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import PYSearch


enum PresentSource {
    case NEW
    case MODIFY
}

protocol ChooseWorkoutControllerDelegate: class {
    func didAddWorkout(sender: ChooseWorkoutController)
}



class ChooseWorkoutController: UIViewController {

    
    @IBOutlet weak var myTable: UITableView!
    
    
    var shownIndexes : [IndexPath] = []

    var listWorkout = [WorkoutModel]() {
        didSet {
            myTable.reloadData()
        }
    }
    
    var footerView:FooterButton?
    
    lazy var searchBTN: UIButton = {
        let b = UIButton(type: .custom)
        b.setImage(UIImage(named: "searchNAV")?.withRenderingMode(.alwaysTemplate), for: UIControl.State.normal)
        let bSize:CGFloat = 30
        b.frame = CGRect(x: 0, y: 0, width: bSize, height: bSize)
        
        b.addTarget(self, action: #selector(didTapSearch), for: UIControl.Event.touchUpInside)
        
        return b
    }()
    
    var presentSource:PresentSource = .NEW
    weak var delegate:ChooseWorkoutControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        configTable()
        setupDataSource()
        // Do any additional setup after loading the view.
    }
    
    fileprivate func setupUI() {
        title = "Choose Workout"
        whiteNavigationColor()
        applyAttributedNAV()
        
        navigationController?.isHeroEnabled = true
        isHeroEnabled = true
        
        let search = UIBarButtonItem(customView: searchBTN)
        navigationItem.rightBarButtonItems = [search]
        
        if presentSource == .NEW {
            setBackButton()
        }else{
            hideBackButton()
        }
        
    }
    
    fileprivate func configTable() {
        myTable.register(nibWithCellClass: ChooseWorkoutCell.self)
        myTable.delegate = self
        myTable.dataSource = self
        myTable.allowsMultipleSelection = true
    }
    
    @objc fileprivate func didTapSearch() {
        presentSearchController()
    }
    
    fileprivate func setupDataSource() {
        var a = WorkoutModel()
        
        a.title = "Increase Your Back Width With 5 Moves"
        a.trainer = "Tommy"
        a.sets = "1"
        a.weight = "5"
        a.reps = "5"
        a.unit = "KG"
        a.images = ["https://www.bodybuilding.com/images/increase-your-back-width-with-5-moves-musclepharm-1-700xh.jpg"]
        listWorkout.append(a)
        
        a = WorkoutModel()
        a.title = "Evan Centopani's 15-Minute Arm Blast"
        a.trainer = "David"
        a.sets = "2"
        a.weight = "10"
        a.reps = "15"
        a.unit = "KG"
        a.images = ["https://www.bodybuilding.com/images/2018/june/evan-centopanis-15-min-arm-workout-1-700xh.jpg"]
        listWorkout.append(a)
        
        a = WorkoutModel()
        a.title = "4 Musts To Maximize Biceps Growth"
        a.trainer = "Tom"
        a.sets = "1"
        a.weight = "30"
        a.reps = "2"
        a.unit = "KG"
        a.images = ["https://www.bodybuilding.com/images/2018/june/4-musts-to-maximize-bicep-growth-header-830x467.jpg"]
        listWorkout.append(a)
        
        a = WorkoutModel()
        a.title = "Get Uplifted With This Squat Workout From Meg Squats!"
        a.trainer = "Bob"
        a.sets = "1"
        a.weight = "20"
        a.reps = "10"
        a.unit = "KG"
        a.images = ["https://www.bodybuilding.com/images/2018/june/get-uplifted-with-this-squat-workout-from-meg-squats-header-830x467.jpg"]
        listWorkout.append(a)
        
        a = WorkoutModel()
        a.title = "Abel Albonetti's Brutal Mass-Building Shoulder Workout"
        a.trainer = "Henry"
        a.sets = "1"
        a.weight = "10"
        a.reps = "5"
        a.unit = "KG"
        a.images = ["https://www.bodybuilding.com/images/2018/june/abel-albonettis-brutal-mass-building-shoulder-workout-header-400x225.jpg"]
        listWorkout.append(a)
        
        a = WorkoutModel()
        a.title = "A Short Workout for Your Triceps Long Head"
        a.trainer = "Jenifer"
        a.sets = "1"
        a.weight = "10"
        a.reps = "5"
        a.unit = "KG"
        a.images = ["https://www.bodybuilding.com/images/2018/june/a-short-workout-for-your-triceps-long-head-header-muscletech-830x467.jpg"]
        listWorkout.append(a)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension ChooseWorkoutController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listWorkout.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: ChooseWorkoutCell.self)
        cell.selectionStyle = .none
        let item = listWorkout[indexPath.row]
        cell.checkImage.isHidden = cell.isSelected ? false : true
        cell.configWith(value: item)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? ChooseWorkoutCell else { return }
        cell.checkImage.isHidden = cell.isSelected ? false : true
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            animateCell(cell: cell)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = myTable.cellForRow(at: indexPath) as? ChooseWorkoutCell else { return }
        cell.checkImage.isHidden = cell.isSelected ? false : true
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        guard let cell = myTable.cellForRow(at: indexPath) as? ChooseWorkoutCell else { return }
        cell.checkImage.isHidden = cell.isSelected ? false : true
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 70
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        footerView = UIView.instanceFromNib()
        footerView?.myBTN.setTitle("SAVE", for: UIControl.State.normal)
        footerView?.myBTN.addTarget(self, action: #selector(didTapSave), for: UIControl.Event.touchUpInside)
        return footerView
    }
    
    @objc fileprivate func didTapSave() {
        if presentSource == .NEW {
            view.window?.rootViewController?.dismiss(animated: true, completion: nil)
            delegate?.didAddWorkout(sender: self)
        }else{
            dismiss(animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}

extension ChooseWorkoutController: UISearchBarDelegate {
    
    
    fileprivate func presentSearchController() {
        let hotSeaches:[String] = ["GYM", "EVENT", "CLASS"]
        let searchViewController = PYSearchViewController.init(hotSearches: hotSeaches, searchBarPlaceholder: "Search", didSearch: { searchViewController, searchBar,searchText in
            let resultViewController = Controllers.shared.searchController()
            resultViewController.searchKey = searchText!
            searchViewController?.navigationController?.pushViewController(resultViewController, animated: true)
        })
        searchViewController?.delegate = self
        searchViewController?.hotSearchStyle =  PYHotSearchStyle.colorfulTag
        
        
        searchViewController?.searchBar.isHeroEnabled = true
        searchViewController?.searchBar.heroID = "mapSearchBox"
        searchViewController?.searchBar.heroModifiers = [.forceNonFade, .spring(stiffness: 300, damping: 25)]
        //searchViewController?.searchSuggestionHidden = true
        let nvc = UINavigationController.init(rootViewController: searchViewController!)
        nvc.isHeroEnabled = true
        self.present(nvc, animated: true, completion: nil)
    }
}

extension ChooseWorkoutController: PYSearchViewControllerDelegate {
    
    
    public func searchViewController(_ searchViewController: PYSearchViewController!, searchTextDidChange searchBar: UISearchBar!, searchText: String!) {
        
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
        //            var searchSuggestionsM = [String]()
        //
        //            for i in (0..<arc4random_uniform(5) + 10).reversed() {
        //
        //                let searchSuggestion:String = String(format: "keyword %@", i)
        //
        //                searchSuggestionsM.append(searchSuggestion)
        //            }
        //            searchViewController.searchSuggestions = searchSuggestionsM
        //        }
    }
}
