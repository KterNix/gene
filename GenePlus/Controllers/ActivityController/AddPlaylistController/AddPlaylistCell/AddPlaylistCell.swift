//
//  AddPlaylistCell.swift
//  GenePlus
//
//  Created by pro on 2/5/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class AddPlaylistCell: UITableViewCell {

    
    
    @IBOutlet weak var titleLBL: UILabel!
    
    @IBOutlet weak var viewQuantity: UIView!
    @IBOutlet weak var quantityLBL: UILabel!
    
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewQuantity.layer.cornerRadius = 8
        viewQuantity.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
