//
//  AddPlaylistController.swift
//  GenePlus
//
//  Created by pro on 2/5/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

protocol AddPlaylistControllerDelegate: class {
    func didAddPlaylist(sender: AddPlaylistController)
}



class AddPlaylistController: UIViewController {

    

    
    @IBOutlet weak var nameWorkoutTXT: SkyFloatingLabelTextField!
    
//    @IBOutlet weak var myTable: UITableView!
    
    @IBOutlet weak var nextBTN: UIButton!
    
    
    
    var shownIndexes : [IndexPath] = []

    fileprivate var searchResults = [PlaylistModel]()
    fileprivate var exercise = [ExercisesModel]()
    var listWorkOuts = [PlaylistModel]()

    var presentSource:PresentSource = .NEW
    weak var delegate:AddPlaylistControllerDelegate?
    var nameToEdit:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        configTable()
        setupDataSource()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        nameWorkoutTXT.becomeFirstResponder()
    }
    
    fileprivate func setupUI() {
        setBackButton()
        whiteNavigationColor()
        applyAttributedNAV()
        listenForKeyboard()
        
        nextBTN.layer.cornerRadius = 5
        nextBTN.layer.masksToBounds = true
        nextBTN.backgroundColor = UIColor.lightGray
        nextBTN.isEnabled = false
    
        nameWorkoutTXT.delegate = self
        
        let buttonTitle = presentSource == .NEW ? "NEXT" : "SAVE"
        nextBTN.setTitle(buttonTitle, for: UIControl.State.normal)
        
        if presentSource == .MODIFY {
            nameWorkoutTXT.text = nameToEdit
        }
        
        if nameWorkoutTXT.text != nil, nameWorkoutTXT.text != "" {
            updateStateButton(enable: true)
        }else{
            updateStateButton(enable: false)
        }
    }
    
    fileprivate func configTable() {
        
//        myTable.register(UINib(nibName: "AddPlaylistCell", bundle: nil), forCellReuseIdentifier: "AddPlaylistCell")
    }

    fileprivate func setupDataSource() {
        var a = PlaylistModel()
        var e = ExercisesModel()
        var d = SetModel()
        
        d.reps = "2"
        d.weight = "12"
        d.duration = "30"
        d.unit = "KG"
        
        e.name = "Smith tricep press"
        e.image = "https://www.bodybuilding.com/images/2016/july/10-best-chest-exercises-for-building-muscle-header-v2-830x467.jpg"
        e.unit = "KG"
        e.setModel.append(d)

        a.name = "Arm & Abs"
        a.exercises.append(e)
        listWorkOuts.append(a)
        
        
        d = SetModel()
        d.reps = "2"
        d.weight = "12"
        d.duration = "30"
        d.unit = "KG"
        
        e = ExercisesModel()
        e.name = "Downward-facing Dog"
        e.image = "https://www.bodybuilding.com/images/2016/july/10-best-chest-exercises-for-building-muscle-header-v2-830x467.jpg"
        e.unit = "KG"
        e.setModel.append(d)

        a = PlaylistModel()
        a.name = "Arms & Abs Home"
        a.exercises.append(e)
        listWorkOuts.append(a)
        
        
        
        d = SetModel()
        d.reps = "2"
        d.weight = "12"
        d.duration = "30"
        d.unit = "KG"
        
        e = ExercisesModel()
        e.name = "Bent-Knee Sit-up / Crunches"
        e.image = "https://www.bodybuilding.com/images/2016/july/10-best-chest-exercises-for-building-muscle-header-v2-830x467.jpg"
        e.unit = "KG"
        e.setModel.append(d)
        
        

        a = PlaylistModel()
        a.name = "Back"
        a.exercises.append(e)
        
        e = ExercisesModel()
        e.name = "Side Plank with Bent Knee"
        e.image = "https://www.bodybuilding.com/images/2016/july/10-best-chest-exercises-for-building-muscle-header-v2-830x467.jpg"
        e.unit = "KG"
        e.setModel.append(d)
        a.exercises.append(e)
        
        e = ExercisesModel()
        e.name = "Supine Reverse Crunches"
        e.image = "https://cdn.muscleandstrength.com/sites/default/files/machine_press_exercise_for_chest_feature.jpg"
        e.unit = "KG"
        e.setModel.append(d)
        a.exercises.append(e)
        
        listWorkOuts.append(a)
        
        
        
        d = SetModel()
        d.reps = "2"
        d.weight = "12"
        d.duration = "30"
        d.unit = "KG"
        
        e = ExercisesModel()
        e.name = "Push-up with Single-leg Raise"
        e.image = "https://www.bodybuilding.com/images/2016/july/10-best-chest-exercises-for-building-muscle-v2-1-700xh.jpg"
        e.unit = "KG"
        e.setModel.append(d)
        
        a = PlaylistModel()
        a.name = "Back & Abs"
        a.exercises.append(e)
        listWorkOuts.append(a)
        
    }
    
    @IBAction func nextAction(_ sender: Any) {
        
        guard nameWorkoutTXT.text != "" else {
            SwiftNotice.noticeOnStatusBar("Please enter workout name first !", autoClear: true, autoClearTime: 3)
            return
        }
        
//        let vc = Controllers.shared.exercisesController()
//        vc.listExercises = exercise
//        vc.title = nameWorkoutTXT.text
//        navigationController?.pushViewController(vc, animated: true)
        if presentSource == .MODIFY {
            dismiss(animated: true, completion: nil)
            return
        }
        
        let vc = Controllers.shared.chooseWorkoutController()
        let nav = UINavigationController(rootViewController: vc)
        vc.presentSource = .NEW
        vc.delegate = self
        DispatchQueue.main.async {
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    
    deinit {
        removeNotifiKeyboard()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//extension AddPlaylistController: UITableViewDelegate, UITableViewDataSource {
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//
//        let item = searchResults.count > 0 ? searchResults.count : listWorkOuts.count
//        return item
//    }
//
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "AddPlaylistCell", for: indexPath) as! AddPlaylistCell
//        cell.selectionStyle = .none
//
//        let item = searchResults.count > 0 ? searchResults[indexPath.row].name : listWorkOuts[indexPath.row].name
//
//        cell.titleLBL.text = item
//
//        return cell
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let name = searchResults.count > 0 ? searchResults[indexPath.row].name : listWorkOuts[indexPath.row].name
//        let item = searchResults.count > 0 ? searchResults[indexPath.row] : listWorkOuts[indexPath.row]
//
//        nameWorkoutTXT.text = name
//        nextBTN.backgroundColor = UIColor.mainColor()
//        nextBTN.isEnabled = true
//
//        exercise = item.exercises
//
//    }
//
//
//
//
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 50
//    }
//
//
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//
//        if (shownIndexes.contains(indexPath) == false) {
//            shownIndexes.append(indexPath)
//
//            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
//
//            animateCell(cell: cell)
//        }
//    }
//}


extension AddPlaylistController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        dismissKeyboard()
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var searchText  = textField.text! + string
        if string == "" {
            searchText = (searchText as String).substring(to: searchText.index(before: searchText.endIndex))
            updateStateButton(enable: false)
        }else{
            updateStateButton(enable: true)
        }
        filterName(text: searchText)
        return true
    }
    
    fileprivate func updateStateButton(enable: Bool) {
        nextBTN.backgroundColor = enable == true ? UIColor.mainColor() : UIColor.lightGray
        nextBTN.isEnabled = enable
    }

    fileprivate func filterName(text: String) {

        
        searchResults = listWorkOuts.filter({ (tex) -> Bool in
            let tmp:NSString = tex.name as NSString
            let range = tmp.range(of: text, options: [.caseInsensitive,.diacriticInsensitive])
            return range.location != NSNotFound
        })
        
        DispatchQueue.main.async {
//            self.myTable.reloadData()
        }
    }
}

extension AddPlaylistController: ChooseWorkoutControllerDelegate {
    
    func didAddWorkout(sender: ChooseWorkoutController) {
        delegate?.didAddPlaylist(sender: self)
    }
    
    
}




















