//
//  PlaylistController.swift
//  GenePlus
//
//  Created by pro on 6/27/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class PlaylistController: BaseViewController {

    
    @IBOutlet weak var myTable: UITableView!

    
    var listWorkout = [WorkoutModel]()
    var playlist = PlaylistModel()

    var shownIndexes : [IndexPath] = []

    
    lazy var menuBTN: UIButton = {
        let b = UIButton(type: .custom)
        b.setImage(UIImage(named: "circleMenu")?.withRenderingMode(.alwaysTemplate), for: UIControl.State.normal)
        let bSize:CGFloat = 30
        b.frame = CGRect(x: 0, y: 0, width: bSize, height: bSize)
        b.addTarget(self, action: #selector(didTapMenu), for: UIControl.Event.touchUpInside)
        return b
    }()
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    
    override func setupUI() {
        title = playlist.name
        setBackButton()
        
        let menu = UIBarButtonItem(customView: menuBTN)
        navigationItem.rightBarButtonItems = [menu]
    }
    
    
    
    override func configTable() {
        myTable.register(nibWithCellClass: ChooseWorkoutCell.self)
        myTable.delegate = self
        myTable.dataSource = self
        
    }
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//
//        guard scrollView.contentOffset.y < 0 else { return }
//        var scale = 1 + fabsf(Float(scrollView.contentOffset.y)) / Float(scrollView.frame.size.height / 5)
//        scale = max(0, scale)
//
//    }
    
    @objc fileprivate func didTapMenu() {
        let vc = Controllers.shared.menuPlaylistController()
        let nav = UINavigationController(rootViewController: vc)
        vc.delegate = self
        DispatchQueue.main.async {
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    private func share(object : ActivityShare) {
        let vc = UIActivityViewController(activityItems: [object.news], applicationActivities: nil)
        vc.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook]
        vc.completionWithItemsHandler = { (activityType, completed, arrayOptions, error) in
            //            print(completed)
            //            print(error?.localizedDescription)
        }
        DispatchQueue.main.async {
            self.navigationController?.present(vc, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
    }
}

extension PlaylistController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playlist.workouts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: ChooseWorkoutCell.self)
        cell.selectionStyle = .none
        let item = playlist.workouts[indexPath.row]
        cell.checkImage.isHidden = true
        cell.configWith(value: item)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            
            animateCell(cell: cell)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = Controllers.shared.startWorkoutController()
        vc.listExercises = playlist.exercises
        vc.playlist = playlist
        navigationController?.pushViewController(vc)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}


extension PlaylistController: MenuPlaylistControllerDelegate {
    func didSelectItem(item: PlaylistMenu, sender: MenuPlaylistController) {
        switch item {
        case .ADD_WORKOUT:
            let vc = Controllers.shared.chooseWorkoutController()
            let nav = UINavigationController(rootViewController: vc)
            vc.presentSource = .MODIFY
            DispatchQueue.main.async {
                self.present(nav, animated: true, completion: nil)
            }
        case .RENAME:
            let vc = Controllers.shared.addPlaylistController()
            let nav = UINavigationController(rootViewController: vc)
            vc.nameToEdit = playlist.name
            vc.presentSource = .MODIFY
            DispatchQueue.main.async {
                self.present(nav, animated: true, completion: nil)
            }
        case .DELETE:
            confirmDelete()
        case .SHARE:
            let shareItem = ActivityShare()
            shareItem.news = "#function"
            
            share(object: shareItem)
        }
    }
    
    fileprivate func confirmDelete() {
        Utils.alertWithAction(title: "DELETE PLAYLIST ?", cancelTitle: "CANCEL", message: "Tap CONFIRM to continue.", actionTitles: ["CONFIRM"], actions: [{ (action) in
            self.dismiss(animated: true, completion: nil)
            }], actionStyle: UIAlertAction.Style.destructive, viewController: self, style: UIAlertController.Style.alert)
    }
    
    
    
}





