//
//  CollImagesCell.swift
//  GenePlus
//
//  Created by pro on 6/29/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class CollImagesCell: UITableViewCell {

    
    
    @IBOutlet weak var myColl: UICollectionView!
    
    var imagesList = [String]() {
        didSet {
            myColl.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupUI()
        configColl()
    }
    
    
    fileprivate func configWith(values: [String]) {
        imagesList = values
    }
    
    fileprivate func setupUI() {
        
    }
    
    fileprivate func configColl() {
        
        myColl.register(UINib(nibName: "WorkoutImageCell", bundle: nil), forCellWithReuseIdentifier: "WorkoutImageCell")
        
        myColl.delegate = self
        myColl.dataSource = self
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension CollImagesCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagesList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WorkoutImageCell", for: indexPath) as! WorkoutImageCell
        let item = imagesList[indexPath.row]
        cell.cellImage.setImageWith(path: item, imageType: .NORMAL_IMAGE)
        cell.cellImage.heroID = "image_\(item)"
        cell.cellImage.heroModifiers = [.forceNonFade,
                                        .spring(stiffness: 300, damping: 25)]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let vc = Controllers.shared.imagePreviewController()
        let nav = UINavigationController(rootViewController: vc)
        let item = imagesList[indexPath.row]
        nav.isHeroEnabled = true
        vc.imageURL = item
        DispatchQueue.main.async {
            self.parentViewController?.present(nav, animated: true, completion: nil)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let imgSize:CGFloat = collectionView.frame.size.width / 3
        
        return CGSize(width: imgSize, height: imgSize)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
}


















