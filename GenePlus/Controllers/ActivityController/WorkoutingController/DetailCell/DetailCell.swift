//
//  DetailCell.swift
//  GenePlus
//
//  Created by pro on 6/29/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class DetailCell: UITableViewCell {

    
    
    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var titleLBL: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
