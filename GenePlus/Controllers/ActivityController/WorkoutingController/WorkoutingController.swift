//
//  WorkoutingController.swift
//  GenePlus
//
//  Created by pro on 6/29/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class WorkoutingController: BaseViewController {

    
    @IBOutlet weak var myTable: UITableView!
    
    var setModel:SetModel?
    var collImagesCellHeight:CGFloat = 100
    var footer:FooterButton? = UIView.instanceFromNib()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func setupUI() {
        setBackButton()
    }
    
    override func configTable() {
        myTable.register(nibWithCellClass: OverviewCell.self)
        myTable.register(nibWithCellClass: DetailCell.self)
        myTable.register(nibWithCellClass: CollImagesCell.self)

        myTable.delegate = self
        myTable.dataSource = self
        
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        if let collCell = myTable.cellForRow(at: IndexPath(row: 2, section: 0)) as? CollImagesCell {
            let collectionViewContentHeight = collCell.myColl.contentSize.height
            let sizeSpace:CGFloat = 58.5
            self.collImagesCellHeight = collectionViewContentHeight + sizeSpace
            self.updateTable(table: self.myTable, withDuration: 0.2)
        }
    }
    
    func updateTable(table: UITableView, withDuration duration: TimeInterval) {
        UIView.animate(withDuration: duration, animations: { () -> Void in
            table.beginUpdates()
            table.endUpdates()
        })
    }
    
    
    deinit {
        footer = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension WorkoutingController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withClass: OverviewCell.self)
            cell.selectionStyle = .none
            cell.titleLBL.text = setModel?.name
            cell.repLBL.text = setModel?.reps
            cell.weightLBL.text = (setModel?.weight ?? "") + " \(setModel?.unit ?? "")"
            
            return cell
        }else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withClass: DetailCell.self)
            cell.selectionStyle = .none
            cell.titleLBL.text = setModel?.description
            return cell
        }
        let cell = tableView.dequeueReusableCell(withClass: CollImagesCell.self)
        cell.selectionStyle = .none
        cell.imagesList = setModel!.images
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 2 {
            return collImagesCellHeight
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        footer?.myBTN.setTitle("FINISH WORKOUT", for: UIControl.State.normal)
        footer?.myBTN.addTarget(self, action: #selector(didTapFinish), for: UIControl.Event.touchUpInside)
        return footer
    }
    
    
    @objc fileprivate func didTapFinish() {
        finishWorkout()
    }
    
    private func finishWorkout() {
        Utils.alertWithAction(title: "FINISH WORKOUT ?", cancelTitle: "CANCEL", message: "Tap CONFIRM to continue.", actionTitles: ["CONFIRM"], actions: [{ (aciton) in
            
            _ = self.navigationController?.popViewController(animated: true)
            
            }], actionStyle: UIAlertAction.Style.destructive, viewController: self, style: UIAlertController.Style.alert)
    }
    
    
    
    
}
