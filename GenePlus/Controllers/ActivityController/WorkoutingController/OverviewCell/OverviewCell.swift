//
//  OverviewCell.swift
//  GenePlus
//
//  Created by pro on 6/29/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class OverviewCell: UITableViewCell {

    
    @IBOutlet weak var titleLBL: UILabel!
    @IBOutlet weak var weightLBL: UILabel!
    @IBOutlet weak var repLBL: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
