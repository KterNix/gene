//
//  MenuPlaylistCell.swift
//  GenePlus
//
//  Created by pro on 6/27/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class MenuPlaylistCell: UITableViewCell {

    
    
    @IBOutlet weak var titleLBL: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    
    
    func configWith(value: BaseContent) {
        titleLBL.text = value.title
        iconImage.image = UIImage(named: value.icon)?.withRenderingMode(.alwaysTemplate)
        iconImage.tintColor = UIColor.white
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        backgroundColor = UIColor.clear
        titleLBL.textColor = UIColor.white
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
