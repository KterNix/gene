//
//  MenuPlaylistController.swift
//  GenePlus
//
//  Created by pro on 6/27/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit


struct BaseContent {
    var title:String = ""
    var icon:String = ""
    
    init(title: String, icon: String) {
        self.title = title
        self.icon = icon
    }
}


protocol MenuPlaylistControllerDelegate: class {
    func didSelectItem(item: PlaylistMenu, sender: MenuPlaylistController)
}

enum PlaylistMenu {
    case ADD_WORKOUT
    case RENAME
    case DELETE
    case SHARE
}


class MenuPlaylistController: BaseViewController {

    @IBOutlet weak var myTable: UITableView!
    
    
    var listMenu:[BaseContent] = [BaseContent(title: "Add workout", icon: "plus-1"),
                                  BaseContent(title: "Rename playlist", icon: "pencil-1"),
                                  BaseContent(title: "Delete playlist", icon: "minus"),
                                  BaseContent(title: "Share", icon: "share")]
    
    
    weak var delegate:MenuPlaylistControllerDelegate?
    var shownIndexes : [IndexPath] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        lightStatusbar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        defaultStatusbar()
    }
    
    override func setupUI() {
        title = "Playlist Menu"
        setBackButton()
        clearNavigationColor()
        updateAttributedNAV(color: UIColor.white)
        let colors = [UIColor.mainColor(),
                      UIColor.mainColor().withAlphaComponent(0.9),
                      UIColor.mainColor().withAlphaComponent(0.85),
                      UIColor.mainColor().withAlphaComponent(0.8)]
        view.applyGradient(withColours: colors, gradientOrientation: GradientOrientation.vertical)
    }
    
    override func configTable() {
        myTable.register(nibWithCellClass: MenuPlaylistCell.self)
        myTable.delegate = self
        myTable.dataSource = self
        myTable.backgroundColor = UIColor.clear
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

extension MenuPlaylistController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: MenuPlaylistCell.self)
        cell.selectionStyle = .none
        let item = listMenu[indexPath.row]
        cell.configWith(value: item)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            delegate?.didSelectItem(item: PlaylistMenu.ADD_WORKOUT, sender: self)
        case 1:
            delegate?.didSelectItem(item: PlaylistMenu.RENAME, sender: self)
        case 2:
            delegate?.didSelectItem(item: PlaylistMenu.DELETE, sender: self)
        case 3:
            delegate?.didSelectItem(item: PlaylistMenu.SHARE, sender: self)
        default:
            break
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            animateCell(cell: cell)
        }
    }
}
