//
//  ManageWorkoutHeader.swift
//  GenePlus
//
//  Created by pro on 6/27/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class ManagePlaylistHeader: UIView {

    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var titleLBL: UILabel!
    @IBOutlet weak var backgroundView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        let colors = [UIColor.mainColor(),
                      UIColor.mainColor().withAlphaComponent(0.9),
                      UIColor.mainColor().withAlphaComponent(0.85),
                      UIColor.mainColor().withAlphaComponent(0.8)]
        
        backgroundView.applyGradient(withColours: colors, gradientOrientation: GradientOrientation.vertical)
    }
    
}
