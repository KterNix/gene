//
//  ManageWorkoutController.swift
//  GenePlus
//
//  Created by pro on 2/8/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import SwipeCellKit


class ManagePlaylistController: BaseViewController {

    
    @IBOutlet weak var myTable: UITableView!
    
    var shownIndexes : [IndexPath] = []
    
    var playlist = [PlaylistModel]() {
        didSet {
            myTable.reloadData()
        }
    }
    
    var listWorkout = [WorkoutModel]()

    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDataSource()
    }
    
    
    override func setupUI() {
        title = "Manage Playlist"
        setBackButton()
        whiteNavigationColor()
        applyAttributedNAV()
    }
    
    override func configTable() {
        myTable.register(UINib(nibName: "CompleteWorkoutCell", bundle: nil), forCellReuseIdentifier: "CompleteWorkoutCell")
    }
    
    fileprivate func setupDataSource() {
        var a = PlaylistModel()
        var e = WorkoutModel()
        
        var f = ExercisesModel()
        var t = ExercisesModel()
        var g = SetModel()
        
        g.name = "This Abs Circuit Workout Will Shock Your Core Into Shape"
        g.reps = "2"
        g.weight = "12"
        g.duration = "30"
        g.unit = "KG"
        g.description = "The main reason that we begin to train is that we are interested in making changes to our physique.\n\nWhen I began to take an interest in training during my teens, my goal was to look like a superhero.\n\nStrength and performance became the main goal as I began to compete in track at the collegiate level.\n\nWhat I learned at this time in my young athletic career was that the better I performed in the gym, the better my body looked."
        g.images = ["https://gogetfunding.com/wp-content/uploads/2015/12/4975401/img/gym-with-people-on-machines.jpg",
                    "https://www.fitness19.com/wp-content/uploads/2016/10/photodune-13379907-fit-people-working-out-in-weights-room-at-the-gym-xs.jpg",
                    "https://cdn-ami-drupal.heartyhosting.com/sites/muscleandfitness.com/files/styles/full_node_image_1090x614/public/media/Cami6.JPG?itok=ggHEtRTQ",
                    "https://blog.codyapp.com/wp-content/uploads/2013/04/beginner_gym_workout_legs_large.jpg",
                    "https://wallpapershome.com/images/wallpapers/girl-4400x2933-fitness-exercise-gym-workout-sportswear-motivation-11064.jpg",
                    "http://watchfit.com/wp-content/uploads/2015/09/training-alone_11-1024x683.jpg",
                    "https://lowellkabnickmd.com/wp-content/uploads/2015/03/iu-7.jpeg",
                    "http://www.thefitness.co.uk/wp-content/uploads/2016/01/gym-workout.jpg",
                    "http://aamirfit.com/wp-content/uploads/2016/12/gym_weekly_workout_schedule.jpg",
                    "https://wallpapersite.com/images/pages/pic_w/64.jpg",
                    "http://jasonferruggia.com/wp-content/uploads/2011/07/how-long-should-your-workout-last.jpg",
                    "https://thenypost.files.wordpress.com/2018/01/workout_yoga1a.jpg?quality=90&strip=all&strip=all",
                    "https://media.glamour.com/photos/59371f8ac610717ae9f5b2bf/master/pass/ledeworkoutathleisure.png",
                    "https://skinnyms.com/wp-content/uploads/2018/01/14-Day-Toned-Legs-Thighs-Workout-Challenge1.jpg",
                    "https://www.active.com/Assets/Fitness/2-week-plan.jpg",
                    "https://media.timeout.com/images/104101397/630/472/image.jpg",
                    "https://tigerfitness-tigerfitness.netdna-ssl.com/pub/media/magefan_blog/morning-workout.jpg",
                    "https://zazabava.com/wp-content/uploads/2017/07/slika-1.jpg",
                    "https://1y2u3hx8yml32svgcf0087imj-wpengine.netdna-ssl.com/wp-content/uploads/2018/06/The-Rocks-Workout-Playlist-Will-Pull-You-Out-of-Any-Fitness-Lull-752x472.jpg",
                    "https://www.bodybuilding.com/images/2018/april/the-best-ab-workout-for-a-six-pack-header-830x467.jpg",
                    "https://www.psychologies.co.uk/sites/default/files/styles/psy2_page_header/public/wp-content/uploads/2010/03/What-is-best-workout-for-you1.jpg",
                    "https://www.freetrainers.com/image/kJkyN6UGohmSJirg7JjLdmPpVF8OoFP51JkrhaUvBfM-ftd-lxiRpZkqR1uqd1ku.jpg",
                    "https://cdn-ami-drupal.heartyhosting.com/sites/muscleandfitness.com/files/media/badass-workout-braun-circuit-2.jpg"]
        
        f.name = "Smith tricep press"
        f.image = "https://cdn.muscleandstrength.com/sites/default/files/machine_press_exercise_for_chest_feature.jpg"
        f.unit = "KG"
        f.setModel.append(g)
        
        e.title = "Increase Your Back Width With 5 Moves"
        e.trainer = "Tommy"
        e.sets = "1"
        e.weight = "5"
        e.reps = "5"
        e.unit = "KG"
        e.images = ["https://www.bodybuilding.com/images/increase-your-back-width-with-5-moves-musclepharm-1-700xh.jpg"]
        
        listWorkout.append(e)
        
        e = WorkoutModel()
        e.title = "Evan Centopani's 15-Minute Arm Blast"
        e.trainer = "David"
        e.sets = "1"
        e.weight = "10"
        e.reps = "15"
        e.unit = "KG"
        e.images = ["https://www.bodybuilding.com/images/2018/june/evan-centopanis-15-min-arm-workout-1-700xh.jpg"]
        listWorkout.append(e)
        
        e = WorkoutModel()
        e.title = "4 Musts To Maximize Biceps Growth"
        e.trainer = "Tom"
        e.sets = "2"
        e.weight = "30"
        e.reps = "2"
        e.unit = "KG"
        e.images = ["https://www.bodybuilding.com/images/2018/june/4-musts-to-maximize-bicep-growth-header-830x467.jpg"]
        listWorkout.append(e)
        
        e = WorkoutModel()
        e.title = "Get Uplifted With This Squat Workout From Meg Squats!"
        e.trainer = "Bob"
        e.sets = "1"
        e.weight = "20"
        e.reps = "10"
        e.unit = "KG"
        e.images = ["https://www.bodybuilding.com/images/2018/june/get-uplifted-with-this-squat-workout-from-meg-squats-header-830x467.jpg"]
        listWorkout.append(e)
        
        e = WorkoutModel()
        e.title = "Abel Albonetti's Brutal Mass-Building Shoulder Workout"
        e.trainer = "Henry"
        e.sets = "1"
        e.weight = "10"
        e.reps = "5"
        e.unit = "KG"
        e.images = ["https://www.bodybuilding.com/images/2018/june/abel-albonettis-brutal-mass-building-shoulder-workout-header-400x225.jpg"]
        listWorkout.append(e)
        
        e = WorkoutModel()
        e.title = "A Short Workout for Your Triceps Long Head"
        e.trainer = "Jenifer"
        e.sets = "1"
        e.weight = "10"
        e.reps = "5"
        e.unit = "KG"
        e.images = ["https://www.bodybuilding.com/images/2018/june/a-short-workout-for-your-triceps-long-head-header-muscletech-830x467.jpg"]
        listWorkout.append(e)
        
        a.name = "Morning"
        a.timeCreated = "MON 2"
        a.workouts = listWorkout
        a.exercises = [f]
        playlist.append(a)
        
        
        g = SetModel()
        g.name = "Full-body split"
        g.reps = "2"
        g.weight = "12"
        g.duration = "30"
        g.unit = "KG"
        g.description = "You’ll begin the program with a full-body training split, meaning you’ll train all major bodyparts in each workout (as opposed to “splitting up” your training). Train three days this first week, performing just one exercise per bodypart in each session. It’s important that you have a day of rest between each workout to allow your body to recover; this makes training Monday, Wednesday and Friday—with Saturday and Sunday being rest days—a good approach.\n\nThe exercises listed in Week 1 are a collection of basic moves that, while also used by advanced lifters, we feel are suitable for the beginner as well. Notice we’re not starting you off with only machine exercises; a handful of free-weight movements are present right off the bat. Reason being, these are the exercises you need to master for long-term gains in muscular size and strength, so you may as well start learning them now. Carefully read all exercise descriptions before attempting them yourself."
        g.images = ["https://cdn.muscleandstrength.com/sites/default/files/machine_press_exercise_for_chest_feature.jpg",
                    "https://www.bodybuilding.com/images/2016/july/10-best-chest-exercises-for-building-muscle-header-v2-830x467.jpg",
                    "https://www.shape.com/sites/shape.com/files/styles/channel_masonry/public/story/trap-bar-deadlift.jpg?itok=2JqJ8x9w",
                    "https://www.hindustantimes.com/rf/image_size_640x362/HT/p2/2016/10/11/Pictures/_578d758e-8f90-11e6-b1ee-4de56c7571da.JPG",
                    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS4j2tCFVSV46lnnPpD2Ga0n0HxtUDJO588FBIxJRty_94UQXwj"]
        
        f = ExercisesModel()
        f.name = "Bent-Knee Sit-up / Crunches"
        f.image = "https://cdn.muscleandstrength.com/sites/default/files/machine_press_exercise_for_chest_feature.jpg"
        f.unit = "KG"
        f.setModel.append(g)
        
        t.name = "Back Flyes With Resistance Bands"
        t.image = "https://www.bodybuilding.com/images/2016/july/10-best-chest-exercises-for-building-muscle-header-v2-830x467.jpg"
        t.unit = "KG"
        t.setModel.append(g)
        
        
        a = PlaylistModel()
        a.name = "My Workout"
        a.timeCreated = "TUE 3"
        a.exercises = [f, t]
        a.workouts = listWorkout
        
        playlist.append(a)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}

extension ManagePlaylistController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playlist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CompleteWorkoutCell", for: indexPath) as! CompleteWorkoutCell
        cell.selectionStyle = .none
        cell.delegate = self
        let item = playlist[indexPath.row]
        cell.titleLBL.text = item.name
        cell.timeLBL.text = item.timeCreated
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = playlist[indexPath.row]

        let vc = Controllers.shared.playlistController()
        vc.playlist = item
        navigationController?.pushViewController(vc, animated: true)
        
//        let vc = Controllers.shared.availableController()
//        navigationController?.pushViewController(vc)
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            
            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            
            animateCell(cell: cell)
        }
    }
}

extension ManagePlaylistController: SwipeTableViewCellDelegate {
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        let activity = UIActivityIndicatorView(style: .gray)
        activity.hidesWhenStopped = true
        if orientation == .right {
            let rename = SwipeAction(style: .default, title: "Rename") { action, indexPath in
                
            }
            rename.textColor = UIColor.lightGray
            rename.font = UIFont.systemFont(ofSize: 12)
            rename.transitionDelegate = ScaleTransition.default
            rename.image = UIImage(named: "pencil")
            rename.backgroundColor = UIColor.clear
            
            let delete = SwipeAction(style: .default, title: "Delete") { action, indexPath in
                
            }
            delete.textColor = UIColor.lightGray
            delete.font = UIFont.systemFont(ofSize: 12)
            delete.transitionDelegate = ScaleTransition.default
            delete.image = UIImage(named: "trash")
            delete.backgroundColor = UIColor.clear
            
            let share = SwipeAction(style: .default, title: "Share") { action, indexPath in
                
            }
            share.textColor = UIColor.lightGray
            share.font = UIFont.systemFont(ofSize: 12)
            share.transitionDelegate = ScaleTransition.default
            share.image = UIImage(named: "shareColor")
            share.backgroundColor = UIColor.clear
            
            
            return [delete, rename, share]
        }
        return nil
    }
    
    
    
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeTableOptions {
        var options = SwipeTableOptions()
        options.backgroundColor = UIColor.clear
        
        if orientation == .right {
            options.expansionDelegate = ScaleAndAlphaExpansion.default
            options.buttonPadding = 15
        }
        
        return options
    }
    
    
    func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) {
        
    }
    
    func tableView(_ tableView: UITableView, didEndEditingRowAt indexPath: IndexPath?, for orientation: SwipeActionsOrientation) {
        
    }
}
































