//
//  ChartController.swift
//  GenePlus
//
//  Created by pro on 7/3/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import DropDown
import Charts

class ChartController: BaseViewController {

    
    @IBOutlet weak var myTable: UITableView!
    
    
    var header:ChartHeader? = UIView.instanceFromNib()
    
    var dropDown:DropDown?
    
    var dropdownDataSource = ["This week", "This Month", "This year"]
    
    var chartEntrys = [BarChartDataEntry]()

    var chartItems = [10, 15, 5, 20]
    var discribeArray = ["01/05", "02/05", "03/05", "04/05", "05/05"]
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configDropdown()
        // Do any additional setup after loading the view.
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        appDelegate.deviceOrientation = .landscapeLeft
        let value = UIInterfaceOrientation.landscapeLeft.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        appDelegate.deviceOrientation = .portrait
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    
    override func setupUI() {
        applyAttributedNAV()
        setBackButton()
        clearNavigationColor()
    }
    
    override func configTable() {
        myTable.register(nibWithCellClass: AreaLineCell.self)
        
        myTable.delegate = self
        myTable.dataSource = self
        
        myTable.tableHeaderView = getHeaderView()
        
        let headerGes = UITapGestureRecognizer(target: self, action: #selector(didTapHeader))
        header?.parentView.addGestureRecognizer(headerGes)
        
        
        let c = BarChartDataEntry(x: 800, y: 20)
        let f = BarChartDataEntry(x: 810, y: 10)
        let d = BarChartDataEntry(x: 820, y: 30)
        chartEntrys = [c, f, d]
        
    }
    
    
    @objc fileprivate func didTapHeader() {
        dropDown?.show()
    }
    
    fileprivate func configDropdown() {
        dropDown = DropDown()
        
        dropDown?.dataSource = dropdownDataSource
        dropDown?.anchorView = header
        dropDown?.bottomOffset = CGPoint(x: 0, y: 70)
        dropDown?.backgroundColor = UIColor.white
        dropDown?.cellHeight = 60
        dropDown?.downScaleTransform = .init(scaleX: 0.8, y: 0.8)
        dropDown?.dimmedBackgroundColor = UIColor.black.withAlphaComponent(0.8)
        dropDown?.cornerRas = 10
        
        dropDown?.selectionAction = { [weak self] itemIndex, item in
            guard let strongSelf = self else { return }
            strongSelf.dropDown?.clearSelection()
            
            strongSelf.header?.titleLBL.text = item
            
            switch itemIndex {
            case 0:
                strongSelf.chartItems = [10, 15, 5, 20]
                strongSelf.discribeArray = ["01/05", "02/05", "03/05", "04/05", "05/05"]
            case 1:
                strongSelf.chartItems = [30, 20, 10, 8, 12, 15, 26, 18, 12, 15]
                strongSelf.discribeArray = ["15/05", "16/05", "17/05", "18/05", "19/05", "20/05", "21/05", "22/05", "23/05", "24/05"]
            case 2:
                strongSelf.chartItems = [10, 15, 5, 20, 12, 10, 5, 15, 20]
                strongSelf.discribeArray = ["01/03", "02/03", "03/05", "04/05", "05/06", "06/06", "07/06", "08/06", "09/06"]
            default:
                break
            }
            
            
            DispatchQueue.main.async {
                strongSelf.myTable.reloadRows(at: [IndexPath(row: 0, section: 0)], with: UITableView.RowAnimation.automatic)
            }
        }
    }
    
    
    fileprivate func getHeaderView() -> UIView {
        
        let headerFrame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: 80)
        let tempView = UIView(frame: headerFrame)
        
        tempView.backgroundColor = UIColor.clear
//        header = ChartHeader(frame: headerFrame)
        header = UIView.instanceFromNib()
        header?.frame.size.height = 60
        
        tempView.addSubview(header!)
        
        return tempView
    }
    
    
    
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        //MARK: - Set Height for tableHeaderView
//        if let headerView = myTable.tableHeaderView {
//
//            let height = headerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height
//            var headerFrame = headerView.frame
//
//            //Comparison necessary to avoid infinite loop
//            if height != headerFrame.size.height {
//                headerFrame.size.height = height
//                headerView.frame = headerFrame
//                myTable.tableHeaderView = headerView
//            }
//        }
//    }
    
//    deinit {
//        header = nil
//
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension ChartController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: AreaLineCell.self)
        cell.selectionStyle = .none
        cell.detailBTN.isHidden = true
        cell.configChart(chartEntrys: chartEntrys)

//        cell.configChart(width: UIScreen.main.bounds.width - 24, items: chartItems, dateArr: discribeArray)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    
}
