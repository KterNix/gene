//
//  ChartHeader.swift
//  GenePlus
//
//  Created by pro on 7/4/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class ChartHeader: UIView {

    @IBOutlet weak var titleLBL: UILabel!
    @IBOutlet weak var rightIcon: UIImageView!
    
    @IBOutlet weak var parentView: UIView!
    
    
    override func awakeFromNib() {
        
        setupUI()
        
    }
    
    private func setupUI() {
        parentView.cornerRadius = 10
        parentView.backgroundColor = UIColor.mainColor().withAlphaComponent(0.1)
        
        rightIcon.image = UIImage(named: "arrowDown")?.withRenderingMode(.alwaysTemplate)
        rightIcon.tintColor = UIColor.mainColor()
        
    }
    
    
}
