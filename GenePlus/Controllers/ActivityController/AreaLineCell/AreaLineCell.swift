//
//  AreaLineCell.swift
//  GenePlus
//
//  Created by pro on 6/30/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import Charts


class AreaLineCell: UITableViewCell {

    
    
    @IBOutlet weak var detailBTN: UIButton!
    @IBOutlet weak var chartView: BarChartView!
    
    
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
        setupChart()
    }

    fileprivate func setupChart() {
        chartView.delegate = self
        
        chartView.drawBarShadowEnabled = false
        chartView.drawValueAboveBarEnabled = false
    }
    
    
    func configChart(chartEntrys: [BarChartDataEntry]) {
        
        
        
        chartView.maxVisibleCount = 1
        
        
        let xAxis = chartView.xAxis
        xAxis.labelPosition = .bottom
        xAxis.labelFont = .systemFont(ofSize: 10)
        xAxis.granularity = 1
        xAxis.labelCount = 7
        xAxis.valueFormatter = DayAxisValueFormatter(chart: chartView)
        
        let leftAxisFormatter = NumberFormatter()
        leftAxisFormatter.minimumFractionDigits = 0
        leftAxisFormatter.maximumFractionDigits = 1
        leftAxisFormatter.negativeSuffix = " KG"
        leftAxisFormatter.positiveSuffix = " KG"

        let leftAxis = chartView.leftAxis
        leftAxis.labelFont = .systemFont(ofSize: 10)
        leftAxis.labelCount = 8
        leftAxis.valueFormatter = DefaultAxisValueFormatter(formatter: leftAxisFormatter)
        leftAxis.labelPosition = .outsideChart
        leftAxis.spaceTop = 0.15
        leftAxis.axisMinimum = 0 // FIXME: HUH?? this replaces startAtZero = YES

        let rightAxis = chartView.rightAxis
        rightAxis.enabled = false
        rightAxis.labelFont = .systemFont(ofSize: 10)
        rightAxis.labelCount = 8
        rightAxis.valueFormatter = leftAxis.valueFormatter
        rightAxis.spaceTop = 0.15
        rightAxis.axisMinimum = 0

//        let l = chartView.legend
//        l.horizontalAlignment = .left
//        l.verticalAlignment = .bottom
//        l.orientation = .horizontal
//        l.drawInside = false
//        l.form = .circle
//        l.formSize = 9
//        l.font = UIFont(name: "HelveticaNeue-Light", size: 11)!
//        l.xEntrySpace = 2
        

        
        var set1: BarChartDataSet! = nil
        set1 = BarChartDataSet(values: chartEntrys, label: "The year 2018")
        set1.colors = [UIColor.mainColor()]
        set1.drawValuesEnabled = false
        
        let data = BarChartData(dataSet: set1)
        data.setValueFont(UIFont(name: "HelveticaNeue-Light", size: 10)!)
        data.barWidth = 0.9
        chartView.data = data
        chartView.animate(yAxisDuration: 1)
//        let marker = XYMarkerView(color: UIColor(white: 180/250, alpha: 1),
//                                  font: .systemFont(ofSize: 12),
//                                  textColor: .white,
//                                  insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8),
//                                  xAxisValueFormatter: chartView.xAxis.valueFormatter!)
//        marker.chartView = chartView
//        marker.minimumSize = CGSize(width: 80, height: 40)
//        chartView.marker = marker
        

        
        
        
        
        detailBTN.backgroundColor = UIColor.mainColor().withAlphaComponent(0.1)
        detailBTN.cornerRadius = 10
        
        detailBTN.setImage(UIImage(named: "expand")?.withRenderingMode(.alwaysTemplate), for: UIControl.State.normal)
        detailBTN.tintColor = UIColor.mainColor()
        
        let edgeInset:CGFloat = 7
        detailBTN.imageEdgeInsets = UIEdgeInsets(top: edgeInset, left: edgeInset, bottom: edgeInset, right: edgeInset)

        detailBTN.imageView?.contentMode = .scaleAspectFit
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension AreaLineCell: ChartViewDelegate {
    
}
