//
//  CompleteWorkoutController.swift
//  GenePlus
//
//  Created by pro on 2/5/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class CompleteWorkoutController: UIViewController {

    
    @IBOutlet weak var myTable: UITableView!
    
    
    var listWorkout = [WorkoutModel]() {
        didSet {
            myTable.reloadData()
        }
    }
    
    var sectionInTable = [String]()
    var shownIndexes : [IndexPath] = []
    var header:BaseHeader?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        configTable()
        setupDataSource()
        
        // Do any additional setup after loading the view.
    }
    
    fileprivate func setupUI() {
        title = "Completed"
        setBackButton()
        whiteNavigationColor()
        applyAttributedNAV()

    }
    
    
    fileprivate func configTable() {
        myTable.register(UINib(nibName: "CompleteWorkoutCell", bundle: nil), forCellReuseIdentifier: "CompleteWorkoutCell")
        myTable.register(nibWithCellClass: ChooseWorkoutCell.self)

    }
    
    fileprivate func setupDataSource() {
        var a = WorkoutModel()
        
        a.title = "Increase Your Back Width With 5 Moves"
        a.trainer = "Tommy"
        a.sets = "1"
        a.weight = "5"
        a.reps = "5"
        a.unit = "KG"
        a.images = ["https://www.bodybuilding.com/images/increase-your-back-width-with-5-moves-musclepharm-1-700xh.jpg"]
        a.createdDate = Date().localDateString()
        listWorkout.append(a)
        
        a = WorkoutModel()
        a.title = "Evan Centopani's 15-Minute Arm Blast"
        a.trainer = "David"
        a.sets = "2"
        a.weight = "10"
        a.reps = "15"
        a.unit = "KG"
        a.images = ["https://www.bodybuilding.com/images/2018/june/evan-centopanis-15-min-arm-workout-1-700xh.jpg"]
        a.createdDate = Date().localDateString()
        listWorkout.append(a)
        
        
        
        
        a = WorkoutModel()
        a.title = "4 Musts To Maximize Biceps Growth"
        a.trainer = "Tom"
        a.sets = "1"
        a.weight = "30"
        a.reps = "2"
        a.unit = "KG"
        a.images = ["https://www.bodybuilding.com/images/2018/june/4-musts-to-maximize-bicep-growth-header-830x467.jpg"]
        a.createdDate = Date().addingTimeInterval(1000000).localDateString()
        listWorkout.append(a)
        
        let date1 = Date().localDateString()
        let date2 = Date().addingTimeInterval(1000000).localDateString()
        sectionInTable.append(date1)
        sectionInTable.append(date2)
        
    }
    
    fileprivate func getItemsInSection(section: Int) -> [WorkoutModel] {
        var items = [WorkoutModel]()
        
        listWorkout.forEach { (workout) in
            let dateCreated = workout.createdDate
            
            if dateCreated == sectionInTable[section] {
                items.append(workout)
            }
        }
        return items
    }
    
    
    deinit {
        header = nil
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension CompleteWorkoutController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionInTable.count
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getItemsInSection(section: section).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: ChooseWorkoutCell.self)
        cell.selectionStyle = .none
        cell.checkImage.isHidden = true
        let item = getItemsInSection(section: indexPath.section)[indexPath.row]
        cell.configWith(value: item)

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let vc = Controllers.shared.completeWorkoutDetailController()
//        navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            
            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            
            animateCell(cell: cell)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        header = UIView.instanceFromNib()
        header?.titleLBL.text = sectionInTable[section]
        return header
    }
    
}
































