//
//  MasterTabbar.swift
//  GenePlus
//
//  Created by pro on 1/8/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class MasterTabbar: UITabBarController {

    
    var userType:UserType!
    
    var isFirstLoad:Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //MARK: - DISABLE RELOAD VIEWCONTROLLER ARRAY
        if isFirstLoad {
            isFirstLoad = false
            setupViewControllers()
        }
    }
    
    fileprivate func setupUI() {
        tabBar.tintColor = UIColor.mainColor()
    }
    
    private func setupViewControllers() {

        switch userType {
        case .TRAINER?:
            viewControllers = [scheduleController(),
                               traineeController(),
                               communityController(),
                               manageWorkoutController(),
                               baseChatController()]
        case .TRAINEE?:
            viewControllers = [homeController(),
                               activityController(),
                               scheduleController(),
                               communityController(),
                               baseChatController()]
        case .BUSINESS?:
            viewControllers = [communityController(),
                               requestController(),
                               profileController()]
            
        default:
            break
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    func customTabbarItem(title: String, image: String) -> UITabBarItem {
        return UITabBarItem(title: title, image: UIImage(named: image), selectedImage: nil)
    }
    
    
}
//MARK: - CUSTOMER
extension MasterTabbar {
    
    fileprivate func homeController() -> UINavigationController {
        let vc = Controllers.shared.homeController()
        vc.tabBarItem = customTabbarItem(title: "Home", image: "homeIcon")
        let nav = UINavigationController(rootViewController: vc)
        vc.userType = userType
        return nav
    }
    
    fileprivate func activityController() -> UINavigationController {
        let vc = Controllers.shared.activityController()
        vc.tabBarItem = customTabbarItem(title: "Activity", image: "activityIcon")
        let nav = UINavigationController(rootViewController: vc)
        return nav
    }
    
    fileprivate func communityController() -> UIViewController {
        let vc = Controllers.shared.communityController()
        vc.tabBarItem = customTabbarItem(title: "Community", image: "communityIcon")
        vc.userType = userType
        return vc
    }
    
    fileprivate func baseChatController() -> UINavigationController {
        let vc = Controllers.shared.baseChatController()
        vc.tabBarItem = customTabbarItem(title: "Messages", image: "mesIcon")
        let nav = UINavigationController(rootViewController: vc)
        return nav
    }
}

//MARK: - TRAINER
extension MasterTabbar {
    fileprivate func scheduleController() -> UIViewController {
        let vc = Controllers.shared.calendarController()
        vc.tabBarItem = customTabbarItem(title: "Schedule", image: "calendar")
        let nav = UINavigationController(rootViewController: vc)
        vc.userType = userType
        
        if userType == .TRAINER {
            return nav
        }
        
        return vc
    }
    
    fileprivate func traineeController() -> UINavigationController {
        let vc = Controllers.shared.traineeController()
        vc.tabBarItem = customTabbarItem(title: "Trainees", image: "trainee")
        let nav = UINavigationController(rootViewController: vc)
        return nav
    }
    
    fileprivate func manageWorkoutController() -> UINavigationController {
        let vc = Controllers.shared.manageWorkoutController()
        vc.tabBarItem = customTabbarItem(title: "Manage Workout", image: "manageIcon")
        let nav = UINavigationController(rootViewController: vc)
        return nav
    }
    
}

//MARK: - BUSINESS
extension MasterTabbar {
    
    fileprivate func requestController() -> UINavigationController {
        let vc = Controllers.shared.requestController()
        vc.tabBarItem = customTabbarItem(title: "Your Request", image: "hourglass")
        let nav = UINavigationController(rootViewController: vc)
        return nav
    }
    
    
    
    fileprivate func profileController() -> UINavigationController {
        let vc = Controllers.shared.profileController()
        vc.tabBarItem = customTabbarItem(title: "Settings", image: "profileIcon")
        let nav = UINavigationController(rootViewController: vc)
        vc.userType = userType
        return nav
    }
}





