//
//  LoginController.swift
//  GenePlus
//
//  Created by pro on 1/6/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class LoginController: UIViewController {

    
    @IBOutlet weak var closeButton: UIButton!
    
    @IBOutlet weak var viewAlpha: UIView!
    @IBOutlet weak var imageBackground: UIImageView!
    
    @IBOutlet weak var emailTXT: SkyFloatingLabelTextField!
    
    @IBOutlet weak var passwordTXT: SkyFloatingLabelTextField!
    
    @IBOutlet weak var signinButton: UIButton!
    
    @IBOutlet weak var forgotPasswordButton: UIButton!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        listenForKeyboard()
    }
    
    
    fileprivate func setupUI() {
        viewAlpha.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        let edgeInset:CGFloat = 15
        closeButton.imageEdgeInsets = UIEdgeInsets(top: edgeInset, left: edgeInset, bottom: edgeInset, right: edgeInset)
        closeButton.setImage(UIImage(named: "close")?.withRenderingMode(.alwaysOriginal), for: UIControl.State.normal)
        
        signinButton.layer.cornerRadius = Constant.buttonCornerRadius
        signinButton.layer.masksToBounds = true
        signinButton.hero.id = "loginBTN"
        
        
        
        emailTXT.delegate = self
        passwordTXT.delegate = self
        
        
    }
    
    
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    
    @IBAction func signinAction(_ sender: Any) {
        let vc = Controllers.shared.masterController()
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        
        Utils.alertWithAction(title: "DEMO", cancelTitle: "CANCEL", message: "CHOOSE USER TYPE", actionTitles: ["TRAINER", "CUSTOMER", "BUSINESS"], actions: [{ (ac1) in
                vc.userType = .TRAINER
                appdelegate.changeRootView(vc, completion_: nil)
            },{ ac2 in
                vc.userType = .TRAINEE
                appdelegate.changeRootView(vc, completion_: nil)
            },{ ac3 in
                vc.userType = .BUSINESS
                appdelegate.changeRootView(vc, completion_: nil)
            }], actionStyle: UIAlertAction.Style.default, viewController: self, style: UIAlertController.Style.alert)
        
    }
    
    
    @IBAction func forgotPasswordAction(_ sender: Any) {
        let vc = Controllers.shared.forgotPassController()
        vc.hero.isEnabled = true
        DispatchQueue.main.async {
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

}


extension LoginController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField.tag {
        case emailTXT.tag:
            passwordTXT.becomeFirstResponder()
        case passwordTXT.tag:
            dismissKeyboard()
        default:
            break
        }
        return true
    }
}













