//
//  BaseLoginController.swift
//  GenePlus
//
//  Created by pro on 1/6/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import GoogleMaps
import FBSDKLoginKit
import GoogleSignIn
import ObjectMapper


class BaseLoginController: UIViewController {

    
    @IBOutlet weak var viewAlpha: UIView!
    @IBOutlet weak var imageBackground: UIImageView!
    
    @IBOutlet weak var signinButton: UIButton!
    @IBOutlet weak var signupButton: UIButton!
    
    @IBOutlet weak var loginFBButton: UIView!
    @IBOutlet weak var loginGGButton: UIView!
    
    @IBOutlet weak var ggImage: UIImageView!
    @IBOutlet weak var fbImage: UIImageView!
    
    
    fileprivate var locationManager = CLLocationManager()
    let facebookLoginManager = FBSDKLoginManager()
    
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lightStatusbar()
        requestAuthorizeLocation()
    }
    fileprivate func requestAuthorizeLocation() {
        if CLLocationManager.authorizationStatus() == .notDetermined {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    fileprivate func setupUI() {
        viewAlpha.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        signinButton.layer.cornerRadius = Constant.buttonCornerRadius
        signinButton.layer.masksToBounds = true
        
        let fbGesture = UITapGestureRecognizer(target: self, action: #selector(didTapLoginFB))
        let ggGesture = UITapGestureRecognizer(target: self, action: #selector(didTapLoginGG))

        
        loginFBButton.addGestureRecognizer(fbGesture)
        loginGGButton.addGestureRecognizer(ggGesture)
        
        
        loginFBButton.layer.cornerRadius = Constant.buttonCornerRadius
        loginFBButton.layer.masksToBounds = true
        
        loginGGButton.layer.cornerRadius = Constant.buttonCornerRadius
        loginGGButton.layer.masksToBounds = true
        
        
        signinButton.hero.id = "loginBTN"
        signupButton.hero.id = "regBTN"
        
        
        ggImage.tintColor = UIColor.mainColor()
        fbImage.tintColor = UIColor.mainColor()

        ggImage.image = UIImage(named: "gg")?.withRenderingMode(.alwaysTemplate)
        fbImage.image = UIImage(named: "fb")?.withRenderingMode(.alwaysTemplate)

        
        
        GIDSignIn.sharedInstance().clientID = "355019175863-c786i38lpsfnufffij9ns1odp3ajn3qp.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        
    }
    
    
    @IBAction func signinAction(_ sender: Any) {
        let vc = Controllers.shared.loginController()
        vc.hero.isEnabled = true
        
        DispatchQueue.main.async {
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func signupAction(_ sender: Any) {
        let vc = Controllers.shared.registerController()
        vc.hero.isEnabled = true
        
        DispatchQueue.main.async {
            self.present(vc, animated: true, completion: nil)
        }    }
    
    
    @objc fileprivate func didTapLoginFB() {
        loginWithFacebook()
    }
    
    
    
    
    @objc fileprivate func didTapLoginGG() {
        loginWithGoogle()
    }
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}


extension BaseLoginController: GIDSignInDelegate, GIDSignInUIDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error == nil {
            
            let email:String = user.profile.email
            let name:String = user.profile.name
            let userID:String = user.userID
            let profileURL = user.profile.imageURL(withDimension: 200)!
            
            var userModel = UserModel()
            
            
            userModel?.email = email
            userModel?.lastname = name
            userModel?.social_id = userID
            userModel?.avatar = profileURL.description
            
            loginSocial(user: userModel)
            
        }
    }
    
    fileprivate func loginWithFacebook() {
        
        facebookLoginManager.logIn(withReadPermissions: ["email", "public_profile"], from: self) { (result, error) in
            if error == nil {
                //                let accessToken = FBSDKAccessToken.current()
                //                guard let accessTokenString = accessToken?.tokenString  else { return }
                //                // Đăng nhập thành công thì lưu token vào UserDefault
                //                let credentials = FacebookAuthProvider.credential(withAccessToken: accessTokenString)
                
                
                FBSDKGraphRequest(graphPath: "/me", parameters: ["fields": "id, name, email"]).start { (connection, result, err) in

                    if err != nil {
                        SwiftNotice.noticeOnStatusBar(err!.localizedDescription, autoClear: true, autoClearTime: 5)
                        return
                    }
                    if let results = result as? [String:AnyObject] {
                        let email = results["email"] as? String
                        let userID = results["id"] as? String
                        let name = results["name"] as? String
                        let facebookProfileUrl = "http://graph.facebook.com/\(userID ?? "")/picture?type=large"
                        
                        var user = UserModel()
                        
                        user?.email = email
                        user?.social_id = userID
                        user?.lastname = name
                        user?.avatar = facebookProfileUrl
                        
                        self.loginSocial(user: user)
                    }
                    
                }
            }
        }
    }
    
    
    fileprivate func loginWithGoogle() {
        GIDSignIn.sharedInstance().signIn()
    }
    
    
    private func loginSocial(user: UserModel?) {
        RequestHelper.shared.loginBySocial(user: user, viewController: self) { (values, error) in
            print(values)
            guard error == .NONE else {
                
                return
            }
            
            if let token = values?.value(forKey: "token") as? String {
                SettingsHelper.setToken(token: token)
            }
            
            if let expires = values?.value(forKey: "expires") as? String {
                SettingsHelper.setExpire(expire: expires)
            }
            
            if let userModel = Mapper<UserModel>().map(JSONObject: values?.value(forKey: "user")) {
                SettingsHelper.saveUser(user: userModel)
            }
            
            
            /*
             let vc = Controllers.shared.masterController()
             let appdelegate = UIApplication.shared.delegate as! AppDelegate
             
             appdelegate.changeRootView(vc, completion_: nil)
             */
            
        }
    }
    
}

















