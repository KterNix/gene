//
//  AddPaymentController.swift
//  GenePlus
//
//  Created by pro on 1/18/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import Braintree

protocol AddPaymentControllerDelegate: class {
    func savePayment(sender: AddPaymentController,card: BTUICardFormView,indexPath: Int?)
}

class AddPaymentController: UIViewController {

    
    @IBOutlet weak var cardFormView: BTUICardFormView!

    
    var paymentEdit = PaymentModel()
    var paymentDuplicate = [PaymentModel]()
    var getIndexPath:Int? = nil
    
    weak var delegate:AddPaymentControllerDelegate?

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        editPayment()
        listenForKeyboard()
        cardFormView.delegate = self
        // Do any additional setup after loading the view.
    }
    
    
    fileprivate func setupUI() {
        if paymentEdit.cardNumber == "" {
            self.title = "Add New Payment"
        }else if paymentEdit.cardNumber != "" {
            self.title = "Edit Payment Method"
        }
        setBackButton()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(badge: nil, image: UIImage.fontAwesomeIcon(name:.save, textColor: UIColor.mainColor(), size: CGSize(width: 30, height: 30)), target: self, action:#selector(addPayment))
    }
    
    func editPayment() {
        if getIndexPath != nil {
            cardFormView.number = paymentEdit.cardNumber
            cardFormView.cvv = paymentEdit.cvv
            cardFormView.setExpirationMonth(paymentEdit.expirationMonth, year: paymentEdit.expirationYear)
            cardFormView.postalCode = paymentEdit.postCode
        }
    }
    
    @objc fileprivate func addPayment() {
        
        if self.getIndexPath == nil {
            for duplicate in self.paymentDuplicate {
                if duplicate.cardNumber.contains(self.cardFormView.number) {
                    SwiftNotice.noticeOnStatusBar("Your card already exist", autoClear: true, autoClearTime: 3)
                    return
                }
            }
        }
        if cardFormView.valid == false {
            SwiftNotice.noticeOnStatusBar("Please enter valid Card form", autoClear: true, autoClearTime: 5)
        }else if cardFormView.valid == true {
            Utils.showAlertWithCompletion(title: "Successfully !", message: "Your card has been saved.", viewController: self, completion: {
                self.delegate?.savePayment(sender: self, card: self.cardFormView, indexPath: self.getIndexPath)
                _ = self.navigationController?.popViewController(animated: true)
            })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    

}


extension AddPaymentController: BTUICardFormViewDelegate {
    
}


















