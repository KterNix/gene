//
//  CategoriesFilterCell.swift
//  GenePlus
//
//  Created by pro on 1/17/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class CategoriesFilterCell: UICollectionViewCell {

    
    
    @IBOutlet weak var titleCategories: UILabel!
    
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        layer.cornerRadius = 5
        layer.masksToBounds = true
        backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
    }

}
