//
//  CategoriesController.swift
//  GenePlus
//
//  Created by pro on 1/17/18.
//  Copyright © 2018 pro. All rights reserved.
//


import UIKit
import XLPagerTabStrip
import PYSearch

class CategoriesController: UIViewController, IndicatorInfoProvider {

    

    @IBOutlet weak var myColl: UICollectionView!
    
    
    var itemInfo:IndicatorInfo = "Categories"
    
    
    
    
    var listCategories = ["GYMS", "PRODUCTS", "CLASS/CLUBS", "EVENTS", "SPECIALS", "WELLNESS"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configColl()
        listenForKeyboard()
        preferredContentSize = CGSize(width: view.bounds.width, height: view.bounds.width)
        view.layoutIfNeeded()

    }
    
    fileprivate func configColl() {
        
        myColl.register(UINib(nibName: "CategoriesFilterCell", bundle: nil), forCellWithReuseIdentifier: "CategoriesFilterCell")
        myColl.register(UINib(nibName: "CategoriesReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "CategoriesReusableView")
        
        
//        myColl.collectionViewLayout = LeftAlignedCollectionViewFlowLayout()
//        if let flowLayouts = myColl.collectionViewLayout as? UICollectionViewFlowLayout {
//            flowLayouts.estimatedItemSize = CGSize(width: 1, height: 1)
//        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
}

extension CategoriesController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listCategories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoriesFilterCell", for: indexPath) as! CategoriesFilterCell
        cell.titleCategories.text = listCategories[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = Controllers.shared.categoriesDetailController()
        vc.title = listCategories[indexPath.row]
        
        switch indexPath.row {
        case 0:
            vc.categoriesType = .gym
        case 1:
            vc.categoriesType = .product
        case 3:
            vc.categoriesType = .event
        default:
            break
        }
        
        let nav = UINavigationController(rootViewController: vc)
        present(nav, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let sizeWidth:CGFloat = view.frame.size.width
        let sizeheight:CGFloat = 56
        return CGSize(width: sizeWidth, height: sizeheight)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                            withReuseIdentifier: "CategoriesReusableView",
                                                                            for: indexPath) as! CategoriesReusableView
            headerView.searchBox.delegate = self
            headerView.searchBox.isHeroEnabled = true
            headerView.searchBox.heroID = "categoriesSearchBox"
            headerView.searchBox.heroModifiers = [.forceNonFade, .spring(stiffness: 300, damping: 25)]
            return headerView
        default:
            assert(false, "Unexpected element kind")
            return UICollectionReusableView()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let sizeWidth:CGFloat = view.frame.size.width / 2 - 20
        let sizeHeight:CGFloat = 40

        return CGSize(width: sizeWidth, height: sizeHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let egdeInset:CGFloat = 10
        return UIEdgeInsets(top: egdeInset, left: egdeInset, bottom: egdeInset, right: egdeInset)
    }
}

extension CategoriesController: UISearchBarDelegate {
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        presentSearchController()
        return false
    }
    
    fileprivate func presentSearchController() {
        let hotSeaches:[String] = ["Son Doong", "Sapa", "Ho Chi Minh"];
        let searchViewController = PYSearchViewController.init(hotSearches: hotSeaches, searchBarPlaceholder: "Search", didSearch: { searchViewController, searchBar,searchText in
            let resultViewController = Controllers.shared.searchController()
            resultViewController.searchKey = searchText!
            searchViewController?.navigationController?.pushViewController(resultViewController, animated: true)
        })
        
        searchViewController?.delegate = self
        searchViewController?.hotSearchStyle =  PYHotSearchStyle.colorfulTag
        searchViewController?.searchBar.isHeroEnabled = true
        searchViewController?.searchBar.heroID = "categoriesSearchBox"
        searchViewController?.searchBar.heroModifiers = [.forceNonFade, .spring(stiffness: 300, damping: 25)]
        //searchViewController?.searchSuggestionHidden = true
        let nvc = UINavigationController.init(rootViewController: searchViewController!)
        nvc.isHeroEnabled = true
        self.present(nvc, animated: true, completion: nil)
    }
}

extension CategoriesController: PYSearchViewControllerDelegate {
    
    public func searchViewController(_ searchViewController: PYSearchViewController!, searchTextDidChange searchBar: UISearchBar!, searchText: String!) {
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
//            var searchSuggestionsM = [String]()
//
//            for i in (0..<arc4random_uniform(5) + 10).reversed() {
//                
//                let searchSuggestion:String = String(format: "keyword %@", i)
//
//                searchSuggestionsM.append(searchSuggestion)
//            }
//            searchViewController.searchSuggestions = searchSuggestionsM
//        }
    }
}




