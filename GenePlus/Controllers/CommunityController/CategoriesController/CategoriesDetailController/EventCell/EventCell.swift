//
//  EventCell.swift
//  GenePlus
//
//  Created by pro on 1/20/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class EventCell: UITableViewCell {

    
    @IBOutlet weak var parentView: UIView!
    
    @IBOutlet weak var backgroundImage: UIImageView!
    
    @IBOutlet weak var titleEvent: UILabel!
    
    
    @IBOutlet weak var addressEvent: UILabel!
    
    @IBOutlet weak var durationEvent: UILabel!
    
    
    @IBOutlet weak var iconAddress: UIImageView!
    
    @IBOutlet weak var iconDuration: UIImageView!
    
    @IBOutlet weak var gradientView: UIView!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        gradientView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        parentView.layer.cornerRadius = 10
        parentView.layer.masksToBounds = true
        
        iconDuration.tintColor = UIColor.white
        iconAddress.tintColor = UIColor.white

        iconDuration.image = UIImage(named: "clock")?.withRenderingMode(.alwaysTemplate)
        iconAddress.image = UIImage(named: "placeMap")?.withRenderingMode(.alwaysTemplate)

        
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
