//
//  ProductCell.swift
//  GenePlus
//
//  Created by pro on 1/19/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class ProductCell: UITableViewCell {

    
    @IBOutlet weak var parentView: UIView!
    
    @IBOutlet weak var productImage: UIImageView!
    
    @IBOutlet weak var productTitle: UILabel!
    
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var viewGradient: GradientViewTop!
    
    @IBOutlet weak var likeLBL: UILabel!
    
    @IBOutlet weak var viewsLBL: UILabel!
    
    
    
    
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        
        
        parentView.layer.cornerRadius = 10
        parentView.layer.masksToBounds = true
        parentView.layer.borderWidth = 1
        parentView.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.1).cgColor
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
