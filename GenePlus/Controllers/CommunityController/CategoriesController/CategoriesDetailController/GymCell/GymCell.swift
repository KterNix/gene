//
//  GymCell.swift
//  GenePlus
//
//  Created by pro on 1/19/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class GymCell: UITableViewCell {

    
    
    @IBOutlet weak var backgroundImage: UIImageView!
    
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var addressLBL: UILabel!
    @IBOutlet weak var phoneLBL: UILabel!
    @IBOutlet weak var durationLBL: UILabel!
    
    @IBOutlet weak var iconDuration: UIImageView!
    @IBOutlet weak var iconPhone: UIImageView!
    @IBOutlet weak var iconAddress: UIImageView!
    
    @IBOutlet weak var viewAlpha: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewAlpha.layer.cornerRadius = 10
        viewAlpha.layer.masksToBounds = true
        
        
        
        backgroundImage.layer.cornerRadius = 10
        backgroundImage.layer.masksToBounds = true
        
        iconDuration.tintColor = UIColor.white
        iconPhone.tintColor = UIColor.white
        iconAddress.tintColor = UIColor.white
        
        
        
        iconDuration.image = UIImage(named: "clock_calendar")?.withRenderingMode(.alwaysTemplate)
        iconPhone.image = UIImage(named: "phone")?.withRenderingMode(.alwaysTemplate)
        iconAddress.image = UIImage(named: "placeMap")?.withRenderingMode(.alwaysTemplate)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
