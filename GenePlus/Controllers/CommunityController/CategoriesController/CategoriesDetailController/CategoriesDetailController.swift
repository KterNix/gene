//
//  CategoriesDetailController.swift
//  GenePlus
//
//  Created by pro on 1/18/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

enum CategoriesType {
    case gym, product, event
}


class CategoriesDetailController: UIViewController {

    
    
    @IBOutlet weak var myTable: UITableView!
    
    var shownIndexes : [IndexPath] = []

    var listProduct = [ProductModel]()
    var listEvent = [EventModel]()
    var listFitness = [FitnessModel]()
    
    
    var categoriesType: CategoriesType = .gym
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        configTable()
        setupDataSource()
        // Do any additional setup after loading the view.
    }
    
    fileprivate func setupUI() {
        setBackButton()
        applyAttributedNAV()
        whiteNavigationColor()

    }
    
    fileprivate func configTable() {
        myTable.register(UINib(nibName: "GymCell", bundle: nil), forCellReuseIdentifier: "GymCell")
        myTable.register(UINib(nibName: "ProductCell", bundle: nil), forCellReuseIdentifier: "ProductCell")
        myTable.register(UINib(nibName: "EventCell", bundle: nil), forCellReuseIdentifier: "EventCell")

        
    }
    
    
    fileprivate func setupDataSource() {
        var a = ProductModel()
        
        a.name = "Strengthen the Body & Relieve Pain"
        a.productImage = "http://nuwatiherbals.com/wp-content/uploads/2015/01/WorkOutPackage.jpg"
        a.price = "$35.00"
        a.likeCount = "10"
        a.viewer = "20"
        listProduct.append(a)
        
        a = ProductModel()
        a.name = "Grey yoga mat and strap & yoga blocks"
        a.productImage = "https://d2j6tswx2otu6e.cloudfront.net/ou6dUZ0YDak8l_GoYyNOcX_KJic=/600x337/2d00/2d00423b944e485aba80fc89d14de846.jpg"
        a.price = "$20.00"
        a.likeCount = "10"
        a.viewer = "1"
        listProduct.append(a)
        
        a = ProductModel()
        a.productImage = "https://www.dhresource.com/0x0s/f2-albu-g2-M00-DD-95-rBVaG1UXkGSAEgEEAAK_yo8WMpc985.jpg/home-gym-package-door-anchor-yoga-exercise.jpg"
        a.name = "Home Gym Package Door anchor Yoga Exercise Resistance Band Stretch Fitness Tubes Cable Workout Yoga Muscle Body building Tool"
        a.price = "US $4.3 - 19.67"
        a.likeCount = "30"
        a.viewer = "50"
        listProduct.append(a)
        
        
        
        
        var b = EventModel()
        
        b.title = "WIN TICKETS TO SELF WORKOUT IN THE PARK NYC"
        b.address = "New York City's famous Central Park"
        b.duration = "May 10th, 2017 - May 10th, 2018"
        b.eventImage = "http://mizzfit.com/Public/Files/post/self_workout_in_the_park_main_stage_fit_event_mizzfit_f5c3a15808.jpg"
        listEvent.append(b)
        
        b = EventModel()
        b.title = "Summer Workouts Return to Millennium Park"
        b.address = "London"
        b.duration = "April 25, 2018 8:35 am"
        b.eventImage = "http://chicagotonight.wttw.com/sites/default/files/field/image/YogaHERO.jpg"
        listEvent.append(b)
        
        b = EventModel()
        b.title = "Women FitnessX "
        b.address = "Paris"
        b.duration = "May 12th 2018"
        b.eventImage = "https://static1.squarespace.com/static/55fc5fc6e4b0fa9f9a82d639/t/57e6d75a893fc0443583c30e/1474746275094/"
        listEvent.append(b)
        
        
        
        
        
        var e = FitnessModel()
        
        e.name = "Gymnasium"
        e.address = "777 West Harrisburg Pike"
        e.phone = "717-948-6000"
        e.openIn = "08:00AM - 18:00PM"
        e.fitnessImage = "http://harrisburg.psu.edu/sites/default/files/fitnesscenter.jpg"
        listFitness.append(e)
        
        e = FitnessModel()
        e.name = "THE FITNESS CENTERS AT WYNN AND ENCORE"
        e.address = "500 University Drive, HS 97"
        e.phone = "717.531.7075"
        e.openIn = "daily from 5:30 a.m. to 8 p.m"
        e.fitnessImage = "http://static.wynnlasvegas.com/~/media/WLV/Amenities/Fitness%20Centers/969_Wynn_Fitness_Center_Weights_Barbara_Kraft.jpg?h=540&la=en&w=1001&vs=1&d=20170623T101840"
        listFitness.append(e)
        
        e = FitnessModel()
        e.name = "Anschutz Health and Wellness Center"
        e.address = "12348 E Montview Blvd. Aurora"
        e.phone = "303-724-9030"
        e.openIn = "08:00AM - 19:00PM"
        e.fitnessImage = "https://anschutzwellness.com/wp-content/uploads/2015/05/facility-and-equipment-hero.jpg"
        listFitness.append(e)
        
        
        
    }
    
    
    
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    



}


extension CategoriesDetailController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch categoriesType {
        case .gym:
            return listFitness.count
        case .event:
            return listEvent.count
        default:
            return listProduct.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch categoriesType {
        case .gym:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "GymCell", for: indexPath) as! GymCell
            cell.selectionStyle = .none
            
            let item = self.listFitness[indexPath.row]
            cell.backgroundImage.setImageWith(path: item.fitnessImage, imageType: .NORMAL_IMAGE)
            
            cell.title.text = item.name
            cell.addressLBL.text = item.address
            cell.phoneLBL.text = item.phone
            cell.durationLBL.text = item.openIn
            
            return cell
            
        case .product:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell", for: indexPath) as! ProductCell
            cell.selectionStyle = .none
            let item = self.listProduct[indexPath.row]

            cell.productImage.setImageWith(path: item.productImage, imageType: .NORMAL_IMAGE)
            cell.productTitle.text = item.name
            cell.likeLBL.text = item.likeCount
            cell.viewsLBL.text = item.viewer
            cell.productPrice.text = item.price
//            cell.productImage.heroID = "demo"
//            cell.productTitle.heroID = "demo2"
//            cell.productPrice.heroID = "demo3"
//            cell.viewGradient.heroID = "demo4"
            return cell
        
        case .event:
            let cell = tableView.dequeueReusableCell(withIdentifier: "EventCell", for: indexPath) as! EventCell
            cell.selectionStyle = .none
            let item = self.listEvent[indexPath.row]

            cell.backgroundImage.setImageWith(path: item.eventImage, imageType: .NORMAL_IMAGE)
            cell.titleEvent.text = item.title
            cell.addressEvent.text = item.address
            cell.durationEvent.text = item.duration
            
            return cell
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch categoriesType {
        case .product:
            let vc = Controllers.shared.productDetailController()
            vc.productDetail = listProduct[indexPath.row]
            let nav = UINavigationController(rootViewController: vc)
            DispatchQueue.main.async {
                self.present(nav, animated: true, completion: nil)
            }
        default:
            break
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch categoriesType {
        case .gym:
            return 198
        case .product:
            return 206
        case .event:
            return 225
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            
            UIView.beginAnimations("rotation", context: nil)
            UIView.setAnimationDuration(0.5)
            cell.transform = CGAffineTransform(translationX: 0, y: 0)
            cell.alpha = 1
            UIView.commitAnimations()
        }
    }
    
    
}




























