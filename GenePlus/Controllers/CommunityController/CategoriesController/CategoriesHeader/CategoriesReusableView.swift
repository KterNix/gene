//
//  CategoriesReusableView.swift
//  GenePlus
//
//  Created by pro on 1/19/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class CategoriesReusableView: UICollectionReusableView {

    
    
    @IBOutlet weak var searchBox: UISearchBar!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        searchBox.maskSearchBar()
    }
    
}
