//
//  ProductDetailController.swift
//  GenePlus
//
//  Created by pro on 1/19/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import GSKStretchyHeaderView

class ProductDetailController: UIViewController {

    
    
    @IBOutlet weak var myTable: UITableView!
    
    
    
    lazy var productDetailHeader:ProductDetailHeader? = UIView.instanceFromNib()
    var stretchyHeaderView:GSKStretchyHeaderView?

    
    
    var shownIndexes : [IndexPath] = []

    
    var productDetail = ProductModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        configTable()
        // Do any additional setup after loading the view.
    }
    
    fileprivate func setupUI() {
        setBackButton()
        applyAttributedNAV()
        clearNavigationColor()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        lightStatusbar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        defaultStatusbar()
    }

    fileprivate func configTable() {
        
        
        productDetailHeader?.frame.size.height = 250
        productDetailHeader?.frame.size.width = myTable.frame.size.width
        
        
        stretchyHeaderView = GSKStretchyHeaderView(frame: CGRect(x: 0, y: 0, width: myTable.frame.size.width, height: 250))
        stretchyHeaderView?.expansionMode = .topOnly
        stretchyHeaderView?.contentShrinks = true
        stretchyHeaderView?.contentExpands = true
        stretchyHeaderView?.minimumContentHeight = 64
        stretchyHeaderView?.maximumContentHeight = 250
        stretchyHeaderView?.addSubview(productDetailHeader!)
        myTable.addSubview(stretchyHeaderView!)
        
        
        
        if let url = URL(string: productDetail.productImage) {
            productDetailHeader?.backgroundView.sd_setImage(with: url, completed: nil)
        }
        productDetailHeader?.likeLBL.text = productDetail.likeCount
        productDetailHeader?.viewerLBL.text = productDetail.viewer
        productDetailHeader?.productPrice.text = productDetail.price
        
        
        
        myTable.register(UINib(nibName: "ProductDetailCell", bundle: nil), forCellReuseIdentifier: "ProductDetailCell")
        myTable.register(UINib(nibName: "ProductOwnerCell", bundle: nil), forCellReuseIdentifier: "ProductOwnerCell")

    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        
        guard scrollView.contentOffset.y < 0 else { return }
        var scale = 1 + fabsf(Float(scrollView.contentOffset.y)) / Float(scrollView.frame.size.height / 5)
        scale = max(0, scale)
        
    
        productDetailHeader?.backgroundView.transform = CGAffineTransform.identity.scaledBy(x: CGFloat(scale), y: CGFloat(scale))
        productDetailHeader?.viewGradient.transform = CGAffineTransform.identity.scaledBy(x: CGFloat(scale), y: CGFloat(scale))
        
    }
    
    
    deinit {
        productDetailHeader = nil
        stretchyHeaderView = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ProductDetailController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductDetailCell", for: indexPath) as! ProductDetailCell
            cell.contentLBL.text = "If you have a specific blend of fitness equipment in mind, let’s talk and we can design a custom gym equipment Package that is a perfect fit. Don’t worry, we can still take advantage of volume discount purchasing and save you a bunch through our connections with equipment manufacturers that we have developed over our thirty plus years in the industry."
            cell.selectionStyle = .none
            return cell
        }else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductOwnerCell", for: indexPath) as! ProductOwnerCell
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 1 {
            return 96
        }else if indexPath.row == 1 {
            return 265
        }
        return 96
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            
            UIView.beginAnimations("rotation", context: nil)
            UIView.setAnimationDuration(0.5)
            cell.transform = CGAffineTransform(translationX: 0, y: 0)
            cell.alpha = 1
            UIView.commitAnimations()
        }
    }
}




























