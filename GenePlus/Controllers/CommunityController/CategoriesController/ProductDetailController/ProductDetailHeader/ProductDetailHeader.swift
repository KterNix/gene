//
//  ProductDetailHeader.swift
//  GenePlus
//
//  Created by pro on 1/19/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class ProductDetailHeader: UIView {

    @IBOutlet weak var backgroundView: UIImageView!
    
    
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    
    @IBOutlet weak var viewGradient: GradientViewTop!
    
    @IBOutlet weak var likeLBL: UILabel!
    
    @IBOutlet weak var viewerLBL: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        backgroundView.heroID = "demo"
//        productTitle.heroID = "demo2"
//        productPrice.heroID = "demo3"
//        viewGradient.heroID = "demo4"
    }
    
    
}
