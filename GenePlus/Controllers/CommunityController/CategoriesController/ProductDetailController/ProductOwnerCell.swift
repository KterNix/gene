//
//  ProductOwnerCell.swift
//  GenePlus
//
//  Created by pro on 1/20/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class ProductOwnerCell: UITableViewCell {

    
    
    
    @IBOutlet weak var ownerTitle: UILabel!
    
    @IBOutlet weak var ownerAddress: UILabel!
    
    @IBOutlet weak var ownerPhone: UILabel!
    
    @IBOutlet weak var ownerEmail: UILabel!
    
    @IBOutlet weak var callBTN: UIButton!
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        callBTN.layer.cornerRadius = Constant.buttonCornerRadius
        callBTN.layer.masksToBounds = true
        callBTN.layer.borderWidth = 1
        callBTN.layer.borderColor = UIColor.lightGray.cgColor
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
