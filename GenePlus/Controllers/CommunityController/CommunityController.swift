//
//  CommunityController.swift
//  GenePlus
//
//  Created by pro on 1/17/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class CommunityController: ButtonBarPagerTabStripViewController {

    var userType:UserType = .TRAINER
    
    override func viewDidLoad() {
        settings.style.selectedBarHeight = 3
        navigationController?.navigationBar.backgroundColor = UIColor.white
        navigationController?.navigationBar.barTintColor = UIColor.white
        super.viewDidLoad()
//        buttonBarView.removeFromSuperview()
//        navigationController?.navigationBar.addSubview(buttonBarView)
        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        defaultStatusbar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        lightStatusbar()
    }
    
    
    func setupUI() {
        buttonBarView.frame.origin.y = 20
        self.buttonBarView.selectedBar.backgroundColor = UIColor.mainColor()
        self.buttonBarView.backgroundColor = UIColor.white
        self.settings.style.buttonBarBackgroundColor = UIColor.white
        self.settings.style.buttonBarItemBackgroundColor = UIColor.white
        self.settings.style.buttonBarItemTitleColor = UIColor.darkGray
        self.settings.style.buttonBarItemFont = UIFont.systemFont(ofSize: 12)
        
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            
            oldCell?.label.textColor = UIColor.darkGray
            newCell?.label.textColor = UIColor.mainColor()
            if animated {
                UIView.animate(withDuration: 0.1, animations: { () -> Void in
                    newCell?.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                    oldCell?.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                })
            }
        }
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        
        let mapController = Controllers.shared.mapController()
        let mapIndicator = IndicatorInfo(stringLiteral: "Map")
        mapController.itemInfo = mapIndicator
        mapController.userType = userType
        
        let categoriesController = Controllers.shared.categoriesController()
        let categoriesIndicator = IndicatorInfo(stringLiteral: "Categories")
        categoriesController.itemInfo = categoriesIndicator
        
        let downloadController = Controllers.shared.downloadController()
        let downloadIndicator = IndicatorInfo(stringLiteral: "Downloads")
        downloadController.itemInfo = downloadIndicator
        downloadController.presentDownloadFrom = .tabbar
        
        if userType == .TRAINER {
            return [mapController, categoriesController]
        }else if userType == .BUSINESS {
            return [mapController]
        }
        
        return [mapController, categoriesController, downloadController]
    }
    
    override func configureCell(_ cell: ButtonBarViewCell, indicatorInfo: IndicatorInfo) {
        super.configureCell(cell, indicatorInfo: indicatorInfo)
        cell.backgroundColor = .clear
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
} 





