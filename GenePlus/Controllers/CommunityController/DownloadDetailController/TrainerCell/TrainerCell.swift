//
//  TrainerCell.swift
//  GenePlus
//
//  Created by pro on 7/6/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class TrainerCell: UITableViewCell {

    
    @IBOutlet weak var trainerPic: UIImageView!
    
    @IBOutlet weak var titleLBL: UILabel!
    
    @IBOutlet weak var hireBTN: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupUI()
    }
    
    private func setupUI() {
        hireBTN.cornerRadius = 10
        hireBTN.backgroundColor = UIColor.mainColor().withAlphaComponent(0.1)
        
        trainerPic.cornerRadius = trainerPic.frame.size.width / 2
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
