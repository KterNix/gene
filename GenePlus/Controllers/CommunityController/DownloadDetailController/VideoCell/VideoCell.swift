//
//  VideoCell.swift
//  GenePlus
//
//  Created by pro on 7/6/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class VideoCell: UITableViewCell {

    @IBOutlet weak var previewView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
