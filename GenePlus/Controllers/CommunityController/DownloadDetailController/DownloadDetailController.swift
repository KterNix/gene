//
//  DownloadDetailController.swift
//  GenePlus
//
//  Created by pro on 1/19/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import GSKStretchyHeaderView
import AVFoundation
import AVKit

class DownloadDetailController: UIViewController {

    
    
    
    @IBOutlet weak var myTable: UITableView!
    
    lazy var downloadDetailHeader: DownloadDetailHeader? = UIView.instanceFromNib()
    lazy var footerButton:ButtonView? = UIView.instanceFromNib()
    var stretchyHeaderView:GSKStretchyHeaderView?

    var downloadModel = DownloadModel()
    
    var shownIndexes : [IndexPath] = []
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        configTable()
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        lightStatusbar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        defaultStatusbar()
    }
    
    
    fileprivate func setupUI() {
        setBackButton()
        clearNavigationColor()
        applyAttributedNAV()
        updateNAVTint(color: .white)
    }
    
    fileprivate func configTable() {
        
        
        downloadDetailHeader?.frame.size.height = 250
        downloadDetailHeader?.frame.size.width = myTable.frame.size.width
        
        stretchyHeaderView = GSKStretchyHeaderView(frame: CGRect(x: 0, y: 0, width: myTable.frame.size.width, height: 250))
        stretchyHeaderView?.expansionMode = .topOnly
        stretchyHeaderView?.contentShrinks = true
        stretchyHeaderView?.contentExpands = true
        stretchyHeaderView?.minimumContentHeight = 64
        stretchyHeaderView?.maximumContentHeight = 250
        stretchyHeaderView?.addSubview(downloadDetailHeader!)
        myTable.addSubview(stretchyHeaderView!)
        
        
        stretchyHeaderView?.stretchDelegate = self
        
        
        downloadDetailHeader?.titleLBL.text = downloadModel.title
        downloadDetailHeader?.postLBL.text = downloadModel.postBy
        downloadDetailHeader?.timeCreated.text = downloadModel.timeCreated
        
        myTable.register(nibWithCellClass: TrainerCell.self)
        myTable.register(nibWithCellClass: DownloadDetailCell.self)
        myTable.register(nibWithCellClass: VideoCell.self)

    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        
//        guard scrollView.contentOffset.y < 0 else { return }
//        var scale = 1 + fabsf(Float(scrollView.contentOffset.y)) / Float(scrollView.frame.size.height / 5)
//        scale = max(0, scale)
//        
//        downloadDetailHeader?.backgroundImage.transform = CGAffineTransform.identity.scaledBy(x: CGFloat(scale), y: CGFloat(scale))
//        downloadDetailHeader?.gradientView.transform = CGAffineTransform.identity.scaledBy(x: CGFloat(scale), y: CGFloat(scale))
        
    }

    
    deinit {
        downloadDetailHeader = nil
        footerButton = nil
        stretchyHeaderView = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension DownloadDetailController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withClass: TrainerCell.self)
            cell.selectionStyle = .none
            cell.titleLBL.text = downloadModel.postBy
            let path = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQWQ-y7NjdLcxVrNpXdfWFaIvIFxKLfNnDqGwuGXmE6dh4AZ9jhfw"
            cell.trainerPic.setImageWith(path: path, imageType: .AVATAR)
            cell.hireBTN.addTarget(self, action: #selector(didTapHire), for: UIControl.Event.touchUpInside)
            return cell
        }else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withClass: DownloadDetailCell.self)
            cell.selectionStyle = .none
            cell.detailLBL.text = "If you have been struggling to make progress with your current workout routine I think I got the right program for you.\n\nThis 8 week pyramid workout routine is a great routine that shocks the body into NEW growth by utilizing higher and lower reps ranges during each exercise to help increase strength and hypertrophy.\n\nWith most workout routines, you will either focus on strength (lower rep ranges with heavier weights) or hypertrophy “muscle gain” (moderate to higher rep ranges with moderate weight).\n\nStrength and hypertrophy routines are effective. Although, even the BEST programs start to get stale and the body will soon adapt.\n\nThis 8 week plateau busting pyramid workout routine will help you break through that plateau! The pyramid routine is based around the rep ranges you use for each exercise."
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withClass: VideoCell.self)
        cell.selectionStyle = .none
        setupVideoPreview(inView: cell.previewView)
    
        return cell
    }
    
    @objc fileprivate func didTapHire() {
        
        let trainer = TrainerModel()
        
        trainer.name = "Senya Ars"
        trainer.address = "1180 First Street South"
        trainer.categories = "Barbell Squats, Stiff Leg Deadlifts, Wide Stance Leg Press"
        trainer.rating = 1.0
        trainer.reviews = "30"
        
        
        
        let vc = Controllers.shared.trainerDetailController()
        let nav = UINavigationController(rootViewController: vc)
        vc.currentTrainer = trainer
        vc.presentSource = .HIRE
        DispatchQueue.main.async {
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    private func setupVideoPreview(inView: UIView) {
        DispatchQueue.global().async {
            if let url = URL(string: "http://jplayer.org/video/m4v/Big_Buck_Bunny_Trailer.m4v") {
                
                let player = AVPlayer(url: url)
                let controller = AVPlayerViewController()
                controller.player = player
                controller.delegate = self
                controller.showsPlaybackControls = true
                DispatchQueue.main.async {
                    self.addChild(controller)
                    let screenSize = inView.size
                    let videoFrame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
                    controller.view.frame = videoFrame
                    inView.addSubview(controller.view)
                }
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 128
        }else if indexPath.row == 1 {
            return UITableView.automaticDimension
        }
        return 280
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 128
        }else if indexPath.row == 1 {
            return 97
        }
        return 280
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 60
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)

            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            animateCell(cell: cell)
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        footerButton?.chooseButton.setTitle("DOWNLOAD", for: UIControl.State.normal)
        footerButton?.chooseButton.addTarget(self, action: #selector(didTapDownload), for: UIControl.Event.touchUpInside)
        return footerButton
    }
    
    @objc fileprivate func didTapDownload() {
        
    }
}

extension DownloadDetailController: GSKStretchyHeaderViewStretchDelegate {
    func stretchyHeaderView(_ headerView: GSKStretchyHeaderView, didChangeStretchFactor stretchFactor: CGFloat) {
        
        downloadDetailHeader?.postLBL.alpha = stretchFactor
        downloadDetailHeader?.postTitle.alpha = stretchFactor
        downloadDetailHeader?.timeCreated.alpha = stretchFactor
        downloadDetailHeader?.titleLBL.alpha = stretchFactor
        
    }
}

extension DownloadDetailController: AVPlayerViewControllerDelegate {
    func playerViewController(_ playerViewController: AVPlayerViewController, failedToStartPictureInPictureWithError error: Error) {
        
    }
}














