//
//  DownloadDetailHeader.swift
//  GenePlus
//
//  Created by pro on 1/22/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class DownloadDetailHeader: UIView {

    
    
    @IBOutlet weak var backgroundImage: UIImageView!
    
    @IBOutlet weak var titleLBL: UILabel!
    
    @IBOutlet weak var gradientView: GradientViewTop!
    
    
    @IBOutlet weak var postTitle: UILabel!
    @IBOutlet weak var postLBL: UILabel!
    
    @IBOutlet weak var timeCreated: UILabel!
    
    
    override func awakeFromNib() {
        
    }
    
}
