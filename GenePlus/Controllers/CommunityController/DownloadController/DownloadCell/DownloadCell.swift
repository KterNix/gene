//
//  DownloadCell.swift
//  GenePlus
//
//  Created by pro on 1/19/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class DownloadCell: UITableViewCell {

    
    @IBOutlet weak var titleLBL: UILabel!
    
    @IBOutlet weak var postLBL: UILabel!
    
    @IBOutlet weak var timeCreatedLBL: UILabel!
    
    
    @IBOutlet weak var iconLike: UIImageView!
    
    @IBOutlet weak var iconViewer: UIImageView!
    
    
    @IBOutlet weak var likeLBL: UILabel!
    
    @IBOutlet weak var viewerLBL: UILabel!
    
    @IBOutlet weak var downloadImage: UIImageView!
    
    
    @IBOutlet weak var parentView: UIView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        iconLike.tintColor = UIColor.lightGray
        iconViewer.tintColor = UIColor.lightGray
        
        
        iconLike.image = UIImage(named: "likeIcon")?.withRenderingMode(.alwaysTemplate)
        iconViewer.image = UIImage(named: "eyeIcon")?.withRenderingMode(.alwaysTemplate)
        
        parentView.cornerRadius = 1
        parentView.borderWidth = 1
        parentView.borderColor = UIColor.lightGray.withAlphaComponent(0.1)
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
