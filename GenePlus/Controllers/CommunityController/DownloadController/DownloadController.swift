//
//  DownloadController.swift
//  GenePlus
//
//  Created by pro on 1/17/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import XLPagerTabStrip

struct DownloadModel {
    var title:String = ""
    var postBy: String = ""
    var likeCount: String = ""
    var viewer: String = ""
    var timeCreated: String = ""
    var banner:String = ""
}

enum PresentDownloadFrom {
    case tabbar,activity
}


class DownloadController: UIViewController, IndicatorInfoProvider {
    
    @IBOutlet weak var myTable: UITableView!
    
    var shownIndexes : [IndexPath] = []

    var itemInfo:IndicatorInfo = "Download"
    
    var listDownload = [DownloadModel]() {
        didSet {
            myTable.reloadData()
        }
    }
    
    var presentDownloadFrom:PresentDownloadFrom = .tabbar
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        configTable()
        setupDataSource()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        whiteNavigationColor()
        updateAttributedNAV(color: .mainColor())
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        clearNavigationColor()
        updateAttributedNAV(color: .white)
    }
    
    
   
    fileprivate func setupUI() {
        setBackButton()
        whiteNavigationColor()
        applyAttributedNAV()
    }
    
    fileprivate func configTable() {
        myTable.register(nibWithCellClass: DownloadCell.self)
    }
    
    
    fileprivate func setupDataSource() {
        let a = DownloadModel(title: "Building the X Frame: 10 Week Muscle Building Workout", postBy: "Raman", likeCount: "20", viewer: "100", timeCreated: "just now", banner: "https://cdn.muscleandstrength.com/sites/default/files/machine_press_exercise_for_chest_feature.jpg")
        let b = DownloadModel(title: "4 Day Rest Pause RP-21 Muscle Building Workout System", postBy: "Henry", likeCount: "10", viewer: "1000", timeCreated: "Yesterday", banner: "https://www.bodybuilding.com/images/2016/july/10-best-chest-exercises-for-building-muscle-header-v2-830x467.jpg")
        let c = DownloadModel(title: "Huge in a Hurry Workout Program", postBy: "David", likeCount: "1", viewer: "20", timeCreated: "3 days ago", banner: "https://www.shape.com/sites/shape.com/files/styles/channel_masonry/public/story/trap-bar-deadlift.jpg?itok=2JqJ8x9w")
        let d = DownloadModel(title: "8 Week Plateau Busting Pyramid Workout Program", postBy: "Jonathan", likeCount: "12", viewer: "120", timeCreated: "10 days ago", banner: "https://www.hindustantimes.com/rf/image_size_640x362/HT/p2/2016/10/11/Pictures/_578d758e-8f90-11e6-b1ee-4de56c7571da.JPG")
        
        listDownload.append(a)
        listDownload.append(b)
        listDownload.append(c)
        listDownload.append(d)

    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
}

extension DownloadController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listDownload.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DownloadCell", for: indexPath) as! DownloadCell
        cell.selectionStyle = .none
        let item = listDownload[indexPath.row]
        cell.titleLBL.text = item.title
        cell.postLBL.text = item.postBy
        cell.likeLBL.text = item.likeCount
        cell.viewerLBL.text = item.viewer
        cell.timeCreatedLBL.text = item.timeCreated
        cell.downloadImage.setImageWith(path: item.banner, imageType: .NORMAL_IMAGE)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch presentDownloadFrom {
        case .tabbar:
            let vc = Controllers.shared.downloadDetailController()
            vc.downloadModel = listDownload[indexPath.row]
            let nav = UINavigationController(rootViewController: vc)
            DispatchQueue.main.async {
                self.present(nav, animated: true, completion: nil)
            }
        case .activity:
            let item = listDownload[indexPath.row]
            let vc = Controllers.shared.downloadedController()
            vc.imageURL = item.banner
            
            navigationController?.pushViewController(vc)
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 215
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            
            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            
            animateCell(cell: cell)
        }
    }
    
    
    
}
