//
//  RequestController.swift
//  GenePlus
//
//  Created by pro on 7/16/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class RequestController: UIViewController {

    
    @IBOutlet weak var myTable: UITableView!
    
    var shownIndexes : [IndexPath] = []

    
    let items = ["PLA GYM", "LUXURY GYM SHOP", "FITNESS PRO", "LUXURY WORKOUT"]
    let itemsStatus = ["Waiting for review", "In review", "Rejected", "Ready"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        configTable()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        defaultStatusbar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        lightStatusbar()
    }
    
    fileprivate func setupUI() {
        title = "Your Request"
        applyAttributedNAV()
        whiteNavigationColor()
    }
    
    fileprivate func configTable() {
        myTable.register(nibWithCellClass: RequestCell.self)
        myTable.delegate = self
        myTable.dataSource = self
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension RequestController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: RequestCell.self)
        let item = items[indexPath.row]
        let itemStatus = itemsStatus[indexPath.row]
        
        cell.titleLBL.text = item
        cell.statusLBL.text = itemStatus
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = Controllers.shared.businessInfoController()
        let nav = UINavigationController(rootViewController: vc)
        
        vc.businessPresentSource = .REQUEST
        
        DispatchQueue.main.async {
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            
            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            animateCell(cell: cell)
        }
    }
    
}
















