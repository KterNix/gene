//
//  RequestCell.swift
//  GenePlus
//
//  Created by pro on 7/16/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class RequestCell: UITableViewCell {

    
    @IBOutlet weak var titleLBL: UILabel!
    @IBOutlet weak var statusLBL: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
