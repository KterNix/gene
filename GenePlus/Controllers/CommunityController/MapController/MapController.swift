//
//  MapController.swift
//  GenePlus
//
//  Created by pro on 1/17/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import GoogleMaps
import PYSearch

class MapController: UIViewController, IndicatorInfoProvider {

    
    var itemInfo:IndicatorInfo = "Map"

    @IBOutlet weak var searchBox: UISearchBar!
    @IBOutlet weak var filterBTN: UIButton!
    
    @IBOutlet weak var submitBTN: UIButton!
    
    
    var userType:UserType = .BUSINESS
    
    var listFitness = [FitnessModel]()
    
    var fitnessMarker = GMSMarker()
    
    @IBOutlet weak var mapView: GMSMapView!
    fileprivate var locationManager = CLLocationManager()
    
    
    lazy var filterImage = UIImage.fontAwesomeIcon(name: .filter, textColor: UIColor.lightGray, size: CGSize(width: 30, height: 30)).withRenderingMode(.alwaysOriginal)

    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
//        mapView.settings.myLocationButton = true
        mapView.isMyLocationEnabled = true
        listenForKeyboard()
        setupDataSource()
        searchBox.maskSearchBar()
        searchBox.delegate = self
        searchBox.hero.isEnabled = true
        searchBox.hero.id = "mapSearchBox"
        searchBox.hero.modifiers = [.forceNonFade, .spring(stiffness: 300, damping: 25)]
        
        setupGoogleMap()
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        authorizeLocation()
    }
    
    
    fileprivate func setupUI() {
        
        filterBTN.backgroundColor = UIColor.white
        filterBTN.setImage(filterImage, for: UIControl.State.normal)
        
        
        
        
        submitBTN.backgroundColor = UIColor.white
        submitBTN.cornerRadius = 5
        submitBTN.borderWidth = 1
        submitBTN.borderColor = UIColor.lightGray.withAlphaComponent(0.3)
        
//        let colors = [UIColor.white, UIColor.lightGray.withAlphaComponent(0.1)]
//        submitBTN.applyGradient(withColours: colors, gradientOrientation: GradientOrientation.vertical)
        
        if userType != .BUSINESS {
            submitBTN.isHidden = true
        }
        
        
    }
    
    
    
    fileprivate func authorizeLocation() {
        if (CLLocationManager.locationServicesEnabled())
        {
            if CLLocationManager.authorizationStatus() == .notDetermined {
                locationManager.requestWhenInUseAuthorization()
            }
        }
    }
    
    private func setupGoogleMap() {
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        locationManager.distanceFilter = 500
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
    }
    
    
    fileprivate func setupDataSource() {
        var a = FitnessModel()
        a.name = "GOODLIFE"
        a.address = "112 ABC STREET"
        a.phone = "022-928999"
        a.openIn = "09:00 AM - 18:00 PM"
        a.promotion = "PROMOTION: Discount 10%"
        a.location = CLLocationCoordinate2D(latitude: 10.7757, longitude: 106.7004)
        listFitness.append(a)
        
        a = FitnessModel()
        a.name = "YOUR FITNESS"
        a.address = "555 highway, US"
        a.phone = "122-9000665"
        a.openIn = "09:00 AM - 18:00 PM"
        a.promotion = "PROMOTION: Discount 50%"
        a.location = CLLocationCoordinate2D(latitude: 10.7844, longitude: 106.6844)
        listFitness.append(a)
        
        a = FitnessModel()
        a.name = "MY LIFE"
        a.address = "55 Road, CA, NY"
        a.phone = "222-5656569"
        a.openIn = "09:00 AM - 18:00 PM"
        a.promotion = "PROMOTION: Discount 20%"
        a.location = CLLocationCoordinate2D(latitude: 10.7578, longitude: 106.7013)
        listFitness.append(a)
        
        setupMarker()
        
    }
    

    fileprivate func setupMarker() {
        for i in listFitness {
            fitnessMarker = GMSMarker()
            fitnessMarker.position = i.location
            fitnessMarker.title = i.name
            fitnessMarker.snippet = "\(i.address)\n\(i.phone)\n\(i.openIn)\n\(i.promotion)"
            fitnessMarker.icon = UIImage(named: "normalMarker")
            fitnessMarker.map = self.mapView
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    
    @IBAction func filterAction(_ sender: Any) {
        let vc = Controllers.shared.filterController()
        vc.delegate = self
        let nav = UINavigationController(rootViewController: vc)
        present(nav, animated: true, completion: nil)
    }
    
    @IBAction func submitAction(_ sender: Any) {
        let vc = Controllers.shared.businessInfoController()
        let nav = UINavigationController(rootViewController: vc)
        
        vc.businessPresentSource = .COMMUNITY
        
        DispatchQueue.main.async {
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    
    
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

extension MapController: CLLocationManagerDelegate, GMSMapViewDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationManager.stopUpdatingLocation()
        if let location = locations.first {
            self.mapView.animate(with: GMSCameraUpdate.setTarget(location.coordinate, zoom: 12))
        }
    }
}

extension MapController: UISearchBarDelegate {
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        presentSearchController()
        return false
    }
    
    fileprivate func presentSearchController() {
        let hotSeaches:[String] = ["GYM", "EVENT", "CLASS"]
        let searchViewController = PYSearchViewController.init(hotSearches: hotSeaches, searchBarPlaceholder: "Search", didSearch: { searchViewController, searchBar,searchText in
                let resultViewController = Controllers.shared.searchController()
                    resultViewController.searchKey = searchText!
                    searchViewController?.navigationController?.pushViewController(resultViewController, animated: true)
        })
        searchViewController?.delegate = self
        searchViewController?.hotSearchStyle =  PYHotSearchStyle.colorfulTag
        
        
        searchViewController?.searchBar.hero.isEnabled = true
        searchViewController?.searchBar.hero.id = "mapSearchBox"
        searchViewController?.searchBar.hero.modifiers = [.forceNonFade, .spring(stiffness: 300, damping: 25)]
        //searchViewController?.searchSuggestionHidden = true
        let nvc = UINavigationController.init(rootViewController: searchViewController!)
        nvc.hero.isEnabled = true
        self.present(nvc, animated: true, completion: nil)
    }
}

extension MapController: PYSearchViewControllerDelegate {
    
    
    public func searchViewController(_ searchViewController: PYSearchViewController!, searchTextDidChange searchBar: UISearchBar!, searchText: String!) {
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
//            var searchSuggestionsM = [String]()
//
//            for i in (0..<arc4random_uniform(5) + 10).reversed() {
//
//                let searchSuggestion:String = String(format: "keyword %@", i)
//
//                searchSuggestionsM.append(searchSuggestion)
//            }
//            searchViewController.searchSuggestions = searchSuggestionsM
//        }
    }
}

extension MapController: FilterControllerDelegate {
    func didTap(value: String, sender: FilterController) {
        print(value)
    }
}



















