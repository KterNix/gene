//
//  BusinessInfoController.swift
//  GenePlus
//
//  Created by pro on 7/16/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import Eureka

class BusinessInfoController: FormViewController {

    
    
    
    var shownIndexes : [IndexPath] = []

    var businessPresentSource:BusinessPresentSource = .COMMUNITY
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        loadForm()
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        defaultStatusbar()
    }
    
    fileprivate func setupUI() {
        title = "Your Infomation"
        setBackButton()
        applyAttributedNAV()
        whiteNavigationColor()
        
        tableView.backgroundColor = UIColor.white
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        
        TextRow.defaultCellUpdate = { cell, row in
            cell.textLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
            cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
            cell.textField.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
            cell.textLabel?.textColor = UIColor.darkGray
            cell.tintColor = UIColor.darkGray
            
        }
        
        PushRow<String>.defaultCellUpdate = { cell, row in
            cell.selectionStyle = .none
            cell.textLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
            cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
            cell.textLabel?.textColor = UIColor.darkGray
            cell.tintColor = UIColor.darkGray
            
        }
        
        TextAreaRow.defaultCellUpdate = { cell, row in
            cell.selectionStyle = .none
            cell.textLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
            cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
            cell.textView.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.light)
            cell.textLabel?.textColor = UIColor.darkGray
            cell.tintColor = UIColor.darkGray
            
        }
        
    }
    
    fileprivate func loadForm() {
        form +++
        Section()
        <<< TextRow() {
            if businessPresentSource == .REQUEST {
                $0.disabled = true
            }
            $0.title = "Name"
            $0.placeholder = "Perfect GYM"
        }
        
            <<< TextRow() {
                if businessPresentSource == .REQUEST {
                    $0.disabled = true
                }
                $0.title = "Address"
                $0.placeholder = "2199 Road, CA, NY"
        }
        
            <<< TextRow() {
                if businessPresentSource == .REQUEST {
                    $0.disabled = true
                }
                $0.title = "Phone"
                $0.placeholder = "123-22335566"
        }
            
        +++ Section(header: "NOTE", footer: "")
            <<< TextAreaRow() {
                if businessPresentSource == .REQUEST {
                    $0.disabled = true
                }
                $0.title = "Note"
                $0.placeholder = "Aa"
        }
        
        
            +++ Section()
            <<< CustomButtonRow { row in
                row.tag = "customButtonRow"
                row.cell.payButton.setTitle("SUBMIT", for: UIControl.State.normal)
                row.cell.payButton.backgroundColor = UIColor.mainColor()
                row.cell.payButton.addTarget(self, action: #selector(didTapSubmit), for: UIControl.Event.touchUpInside)
        }
        
        
        
        if businessPresentSource == .REQUEST {
            let row = form.rowBy(tag: "customButtonRow") as? CustomButtonRow
            row?.hidden = true
            row?.evaluateHidden()
        }
        
    }
    
    @objc fileprivate func didTapSubmit() {
        
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 {
            return 200
        }
        return 60
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)

            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            animateCell(cell: cell)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
