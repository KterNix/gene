//
//  FilterController.swift
//  GenePlus
//
//  Created by pro on 1/23/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

protocol FilterControllerDelegate: class {
    func didTap(value: String, sender: FilterController)
}





class FilterController: UIViewController {

    
    
    @IBOutlet weak var myTable: UITableView!
    
    var shownIndexes : [IndexPath] = []

    
    weak var delegate: FilterControllerDelegate?
    
    var listFilter = ["CLUB","FITNESS","TRAINER"]

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        configTable()
        
        
        
        
        // Do any additional setup after loading the view.
    }
    
    
    fileprivate func setupUI() {
        title = "Filter"
        applyAttributedNAV()
        setBackButton()
        whiteNavigationColor()
    }
    
    
    fileprivate func configTable() {
        
        myTable.register(UINib(nibName: "FilterCell", bundle: nil), forCellReuseIdentifier: "FilterCell")
    }
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension FilterController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listFilter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilterCell", for: indexPath) as! FilterCell
        cell.selectionStyle = .none
        cell.titleLBL.text = listFilter[indexPath.row]
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            self.delegate?.didTap(value: self.listFilter[indexPath.row], sender: self)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            
            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            
            
            cell.transform = CGAffineTransform(translationX: 0, y: 50)
            cell.alpha = 0
            
            UIView.beginAnimations("rotation", context: nil)
            UIView.setAnimationDuration(0.5)
            cell.transform = CGAffineTransform(translationX: 0, y: 0)
            cell.alpha = 1
            UIView.commitAnimations()
        }
    }
    
    
    
    
    
    
    
    
    
    
}





































