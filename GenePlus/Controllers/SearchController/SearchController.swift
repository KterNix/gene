//
//  SearchController.swift
//  GenePlus
//
//  Created by pro on 1/8/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class SearchController: UIViewController {

    
    
    @IBOutlet weak var myTable: UITableView!
    
    var searchKey = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        configTable()
        // Do any additional setup after loading the view.
    }
    
    
    fileprivate func setupUI() {
        setBackButton()
        whiteNavigationColor()
    }
    
    fileprivate func configTable() {
        myTable.register(UINib(nibName: "SearchCell", bundle: nil), forCellReuseIdentifier: "SearchCell")
    }
    
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}

extension SearchController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell", for: indexPath) as! SearchCell
        cell.selectionStyle = .none
        return cell
    }
    
    
}
