//
//  RegisterController.swift
//  GenePlus
//
//  Created by pro on 1/6/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class RegisterController: UIViewController {

    
    @IBOutlet weak var closeButton: UIButton!
    
    @IBOutlet weak var viewAlpha: UIView!
    @IBOutlet weak var imageBackground: UIImageView!
    
    @IBOutlet weak var tempsLBL: UILabel!
    
    
    @IBOutlet weak var nameTXT: SkyFloatingLabelTextField!
    
    @IBOutlet weak var emailTXT: SkyFloatingLabelTextField!
    
    @IBOutlet weak var passwordTXT: SkyFloatingLabelTextField!
    
    @IBOutlet weak var confirmPassTXT: SkyFloatingLabelTextField!
    
    @IBOutlet weak var signupButton: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        listenForKeyboard()
    }
    
    
    fileprivate func setupUI() {
        viewAlpha.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        let edgeInset:CGFloat = 15
        closeButton.imageEdgeInsets = UIEdgeInsets(top: edgeInset, left: edgeInset, bottom: edgeInset, right: edgeInset)
        closeButton.setImage(UIImage(named: "close")?.withRenderingMode(.alwaysOriginal), for: UIControl.State.normal)
        
        signupButton.layer.cornerRadius = Constant.buttonCornerRadius
        signupButton.layer.masksToBounds = true
        signupButton.hero.id = "regBTN"
        
        nameTXT.delegate = self
        emailTXT.delegate = self
        passwordTXT.delegate = self
        confirmPassTXT.delegate = self
        
        
        
        
    }
    
    
    
    
    
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    
    @IBAction func signupAction(_ sender: Any) {
        let vc = Controllers.shared.masterController()
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.changeRootView(vc, completion_: nil)
    }
    
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    

}

extension RegisterController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField.tag {
        case nameTXT.tag:
            emailTXT.becomeFirstResponder()
        case emailTXT.tag:
            passwordTXT.becomeFirstResponder()
        case passwordTXT.tag:
            confirmPassTXT.becomeFirstResponder()
        case confirmPassTXT.tag:
            dismissKeyboard()
        default:
            break
        }
        
        
        
        
        
        return true
    }
}











