//
//  FooterIMGColl.swift
//  GenePlus
//
//  Created by pro on 9/20/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class FooterIMGColl: UIView {

    @IBOutlet weak var myColl: BaseCollectionView!
    
    @IBOutlet weak var bottomBTN: UIButton!
    
    
    var imageList = [String]() {
        didSet {
            myColl.reloadData()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        configColl()
    }
    
    private func setupUI() {
        bottomBTN.cornerRadius = 5
    }
    
    private func configColl() {
        
        myColl.register(UINib(nibName: "ImageLibraryCell", bundle: nil), forCellWithReuseIdentifier: "ImageLibraryCell")
        
        myColl.delegate = self
        myColl.dataSource = self
        
        
        imageList = ["https://skinnyms.com/wp-content/uploads/2016/12/Abs-Butt-Legs-Home-Workout-750x500.jpg",
                     "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQjzgBSsn7KHGbR3CPFgIwVn5IyTWBRIBVoi5RHtSKKKvda3i80",
                     "https://i0.heartyhosting.com/okmagazine.com/wp-content/uploads/2018/07/farrah-abraham-weight-loss-workout-photos-1.jpg?resize=780%2C390&ssl=1",
                     "http://fitbodyhq.wpengine.netdna-cdn.com/wp-content/uploads/2017/05/full-body-fat-burner-workout-featured.jpg",
                     "https://www.cdn.spotebi.com/wp-content/uploads/2016/01/muffin-top-exercises-cardio-abs-obliques-workout-spotebi.jpg",
                     "https://skinnyms.com/wp-content/uploads/2016/09/7-Day-Beginners-10-Minute-Morning-Workout-Challenge.jpg"]
        
    }
    
    
}

extension FooterIMGColl: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageLibraryCell", for: indexPath as IndexPath) as! ImageLibraryCell
        cell.viewAlpha.isHidden = true
        let item = imageList[indexPath.row]
        cell.imageBackground.setImageWith(path: item, imageType: .NORMAL_IMAGE)
        cell.imageBackground.hero.id = "image_\(item)"
        cell.imageBackground.hero.modifiers = [.forceNonFade,
                                         .spring(stiffness: 300, damping: 25)]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = Controllers.shared.imagePreviewController()
        let nav = UINavigationController(rootViewController: vc)
        let item = imageList[indexPath.row]
        nav.hero.isEnabled = true
        vc.imageURL = item
        DispatchQueue.main.async {
            self.parentViewController?.present(nav, animated: true, completion: nil)
        }
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout methods
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemSize = (frame.size.width - 18) / 3
        
        return CGSize(width: itemSize, height: itemSize)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let edgeInset:CGFloat = 8
        return UIEdgeInsets(top: edgeInset, left: edgeInset, bottom: edgeInset, right: edgeInset)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}
