//
//  ManageWorkoutDetail.swift
//  GenePlus
//
//  Created by pro on 9/20/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import Eureka

class ManageWorkoutDetail: BaseFormViewController {

    var shownIndexes : [IndexPath] = []
    var footerSizeHeight:CGFloat = 340

    lazy var footerColl:FooterIMGColl? = UIView.instanceFromNib()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func setupUI() {
        setBackButton()
        applyAttributedNAV()
        whiteNavigationColor()
    }
    
    override func loadForm() {
        form
            +++ Section(header: "Workout Information", footer: "")
            
            <<< LabelRow() {
                $0.title = "Workout name"
                $0.value = "Aa"
            }
            
            <<< LabelRow() {
                $0.title = "Sets"
                $0.value = "1"
            }
            
            <<< LabelRow() {
                $0.title = "Weight"
                $0.value = "5"
            }
            
            <<< LabelRow() {
                $0.title = "Unit"
                $0.value = "KG"
            }
            
            +++ Section(header: "WORKOUT DESCRIPTION", footer: "")
            <<< TextAreaRow() {
                $0.disabled = true
                $0.value = "Create a profile for the USER that only TRAINERS can see. Body weight, strength, weaknesses, body fat, flexibility, injures, health issues, notes, and TRAINER EXPERIENCE"
            }
            
            +++ Section(header: "NOTE", footer: "")
            <<< TextAreaRow() {
                $0.disabled = true
                $0.value = "Create a profile for the USER that only TRAINERS can see. Body weight, strength, weaknesses, body fat, flexibility, injures, health issues, notes, and TRAINER EXPERIENCE"
            }
            
            +++ Section(header: "PICTURES", footer: "")
        
    }
    
    deinit {
        footerColl = nil
    }
    
}

extension ManageWorkoutDetail {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            
            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            animateCell(cell: cell)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 || indexPath.section == 2 {
            if UIDevice.current.isIPad {
                return 300
            }
            return 200
        }
        return 60
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 3 {
            footerColl?.bottomBTN.addTarget(self, action: #selector(didTapAllotments), for: UIControl.Event.touchUpInside)
            return footerColl
        }
        return nil
    }
    
    @objc fileprivate func didTapAllotments() {
        let vc = Controllers.shared.manageAllotmentsController()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section != 3 {
            return 0
        }
        return footerSizeHeight
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
        return 200
    }
}
