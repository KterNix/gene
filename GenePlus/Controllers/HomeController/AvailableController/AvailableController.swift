//
//  AvailableController.swift
//  GenePlus
//
//  Created by pro on 7/9/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import FSCalendar
import ObjectMapper

enum CheckAssets: String {
    case CHECKED = "checked"
    case UN_CHECK = "unCheck"
}


class AvailableController: BaseViewController {

    
    @IBOutlet weak var calendarView: FSCalendar!
    
    @IBOutlet weak var calendarConstrainsHeight: NSLayoutConstraint!
    
    @IBOutlet weak var myTable: UITableView!
    
    @IBOutlet weak var nextBTN: UIButton!
    
    
    
    fileprivate lazy var scopeGesture: UIPanGestureRecognizer = {
        [unowned self] in
        let panGesture = UIPanGestureRecognizer(target: self.calendarView, action: #selector(self.calendarView.handleScopeGesture(_:)))
        panGesture.delegate = self
        panGesture.minimumNumberOfTouches = 1
        panGesture.maximumNumberOfTouches = 2
        return panGesture
        }()
    
    
    var busyDate = ["2018-08-19", "2018-08-25", "2017-08-29"]
    
    fileprivate let gregorian = Calendar(identifier: .gregorian)
    fileprivate weak var eventLabel: UILabel!
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    
    var listEvent = ["2018-02-28","2018-02-26"]
    
    var dateToSelect: Date!
    
    
    var guestsQuantity:Int = 0
    
    var selectedDate:Date?
    
    var currentPage = Date()
    
    var currentMonth:Int = 0
    
    var titleC:String = ""
    
    var minimumMonth:Int = 0
    
    var shownIndexes : [IndexPath] = []
    var allotments = [AllotmentModel]()
    
    var selectedAllotment = [AllotmentModel]() {
        didSet {
            badgeNumber = selectedAllotment.count
        }
    }

    
    var badgeNumber:Int = 0 {
        didSet {
            updateBadgeNumber()
        }
    }
    
    lazy var dateListBTN:UIBarButtonItem = {
        let b = UIBarButtonItem(badge: "", image: (UIImage(named: "dumbbell")?.withRenderingMode(.alwaysTemplate))!, target: self, action: #selector(didTapDateList))
        b.badgeLabel?.backgroundColor = UIColor.clear
        return b
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCalendar()
        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        defaultStatusbar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        lightStatusbar()
    }
    
    override func setupUI() {
        title = "Available"
        setBackButton()
        applyAttributedNAV()
        
        nextBTN.cornerRadius = 5
        updateBTNState(flag: false)
        
        navigationItem.rightBarButtonItems = [dateListBTN]
        
        readFile()
        
    }
    
    fileprivate func readFile() {
        
        if let path = Bundle.main.path(forResource: "AllotmentItems", ofType: "plist") {
            if let arr = NSArray(contentsOfFile: path) {
                guard let models = Mapper<AllotmentModel>().mapArray(JSONObject: arr) else { return }
                allotments = models
            }
        }
    }
    
    override func configTable() {
        myTable.register(nibWithCellClass: TimeCell.self)
        myTable.delegate = self
        myTable.dataSource = self
    }

    fileprivate func setupCalendar() {
        calendarView.register(DIYCalendarCell.self, forCellReuseIdentifier: "cell")
        calendarView.delegate = self
        calendarView.dataSource = self
        
        calendarView.select(Date())
        view.addGestureRecognizer(scopeGesture)
        myTable.panGestureRecognizer.require(toFail: scopeGesture)
        calendarView.setScope(.week, animated: true)
        
    }
    
    fileprivate func updateBadgeNumber() {
        let badgeNAV = navigationItem.rightBarButtonItem
        badgeNAV?.badgeLabel?.isHidden = badgeNumber == 0 ? true : false
        badgeNAV?.badgeLabel?.backgroundColor = badgeNumber == 0 ? UIColor.clear : UIColor.darkGray
        badgeNAV?.badgeLabel?.textColor = badgeNumber == 0 ? UIColor.clear : UIColor.white
        
        navigationItem.rightBarButtonItem?.badgeString = "\(badgeNumber)"
    }
    
    
    
    @objc fileprivate func didTapDateList() {
        let vc = Controllers.shared.dateSelectedListController()
        vc.dateSelected = selectedAllotment
        vc.delegate = self
        navigationController?.pushViewController(vc)
    }
    
    
    @IBAction func nextAction(_ sender: Any) {
        let vc = Controllers.shared.confirmController()
        vc.allotments = selectedAllotment
        navigationController?.pushViewController(vc, animated: true)
    }
    
    fileprivate func updateBTNState(flag: Bool) {
        nextBTN.backgroundColor = flag ? UIColor.mainColor() : UIColor.lightGray
        nextBTN.isEnabled = flag
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension AvailableController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allotments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: TimeCell.self, for: indexPath)
        let item = allotments[indexPath.row]
        cell.configWith(value: item)
        
        
        let isSelectedAllotment = selectedAllotment.contains(item)
        let checkIMG = isSelectedAllotment ? UIImage(named: CheckAssets.CHECKED.rawValue) : UIImage(named: CheckAssets.UN_CHECK.rawValue)
        cell.checkIMG.image = checkIMG

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            animateCell(cell: cell)
        }
        
        guard let cells = cell as? TimeCell else { return }
        let item = allotments[indexPath.row]
        let isSelectedAllotment = selectedAllotment.contains(item)
        
        let checkIMG = isSelectedAllotment ? UIImage(named: CheckAssets.CHECKED.rawValue) : UIImage(named: CheckAssets.UN_CHECK.rawValue)
        cells.checkIMG.image = checkIMG
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        updateBTNState(flag: true)
        
        
        guard let cell = tableView.cellForRow(at: indexPath) as? TimeCell else { return }
        let item = allotments[indexPath.row]
        let isSelectedAllotment = selectedAllotment.contains(item)
        
        
        if isSelectedAllotment == false {
            //MARK: - CHECK IF HAS CURRENT DATE -> RETURN
            for i in selectedAllotment {
                if i.date == item.date {
                    return
                }
            }

            selectedAllotment.append(item)
            cell.checkIMG.image = UIImage(named: CheckAssets.CHECKED.rawValue)
        }else{
            if let allotmentIndex = selectedAllotment.index(of: item) {
                selectedAllotment.remove(at: allotmentIndex)
                cell.checkIMG.image = UIImage(named: CheckAssets.UN_CHECK.rawValue)
            }
        }
    }
}

extension AvailableController: DateSelectedListControllerDelegate {
    func didChangeDate(dates: [AllotmentModel], sender: DateSelectedListController) {
        selectedAllotment = dates
        
        DispatchQueue.main.async {
            self.myTable.reloadData()
        }
    }
}


extension AvailableController {
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        let shouldBegin = self.myTable.contentOffset.y <= -self.myTable.contentInset.top
        if shouldBegin {
            let velocity = self.scopeGesture.velocity(in: self.view)
            switch self.calendarView.scope {
            case .month:
                return velocity.y < 0
            case .week:
                return velocity.y > 0
            }
        }
        return shouldBegin
    }
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.calendarConstrainsHeight.constant = bounds.height
        self.view.layoutIfNeeded()
    }
}


extension AvailableController: FSCalendarDelegate, FSCalendarDataSource, FSCalendarDelegateAppearance {
    
    func calendar(_ calendar: FSCalendar, cellFor date: Date, at position: FSCalendarMonthPosition) -> FSCalendarCell {
        let cell = calendar.dequeueReusableCell(withIdentifier: "cell", for: date, at: position)
        
        return cell
    }
    
    func calendar(_ calendar: FSCalendar, willDisplay cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
        self.configure(cell: cell, for: date, at: position)
    }
    
    func minimumDate(for calendar: FSCalendar) -> Date {
        return Date()
    }
    
//    //MARK: - SHOW DISCOUNT
//    func calendar(_ calendar: FSCalendar, subtitleFor date: Date) -> String? {
//        for i in listEvent {
//            if Utils.compare(dateA: date, dateB: Utils.stringToDate(i, stringFormat: "yyyy-MM-dd")) {
//                return "20%"
//            }
//        }
//        return nil
//    }
//
//    //MARK: - DISABLE DATE NOT AVAILABLE
//    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition)   -> Bool {
//
//        for i in listDateAvailable {
//
//            if Utils.compare(dateA: date, dateB: Utils.stringToDate(i, stringFormat: "yyyy-MM-dd")) {
//                // TO DISABLE -> RETURN FALSE
//                return true
//            }
//        }
//        //monthPosition == .current
//        return false
//    }
    
    func calendar(_ calendar: FSCalendar, shouldDeselect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        return monthPosition == .current
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
//        print("did select date \(self.formatter.string(from: date))")
        self.configureVisibleCells()
    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date) {
//        print("did deselect date \(self.formatter.string(from: date))")
        self.configureVisibleCells()
    }
    
    
    //MARK: - APPEARANCE
    
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]? {
        if self.gregorian.isDateInToday(date) {
            return [UIColor.orange]
        }
        return [appearance.eventDefaultColor]
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
//        for i in listDateAvailable {
//            if Utils.compare(dateA: date, dateB: Utils.stringToDate(i, stringFormat: "yyyy-MM-dd")) {
//                return appearance.titleDefaultColor
//            }
//        }
        return UIColor.darkGray
    }
    
    func calendar(_ calendar: FSCalendar, imageFor date: Date) -> UIImage? {
        for i in busyDate {
            if Utils.compare(dateA: date, dateB: Utils.stringToDateFM(i, stringFormat: .YMD)) {
                return UIImage(named: "circle")
            }
        }
        return nil
    }
    
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleOffsetFor date: Date) -> CGPoint {
        return CGPoint(x: 0, y: 5)
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, subtitleOffsetFor date: Date) -> CGPoint {
        return CGPoint(x: 0, y: 8)
    }
    
    private func configureVisibleCells() {
        calendarView.visibleCells().forEach { (cell) in
            let date = calendarView.date(for: cell)
            let position = calendarView.monthPosition(for: cell)
            self.configure(cell: cell, for: date!, at: position)
        }
    }
    
    
    
    fileprivate func configure(cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
        
        let diyCell = (cell as! DIYCalendarCell)
        // Custom today circle
        diyCell.circleImageView.isHidden = !self.gregorian.isDateInToday(date)
        // Configure selection layer
        if position == .current {
            
            var selectionType = SelectionType.none
            
            if calendarView.selectedDates.contains(date) {
                let previousDate = self.gregorian.date(byAdding: .day, value: -1, to: date)!
                let nextDate = self.gregorian.date(byAdding: .day, value: 1, to: date)!
                if calendarView.selectedDates.contains(date) {
                    if calendarView.selectedDates.contains(previousDate) && calendarView.selectedDates.contains(nextDate) {
                        selectionType = .middle
                    }
                    else if calendarView.selectedDates.contains(previousDate) && calendarView.selectedDates.contains(date) {
                        selectionType = .rightBorder
                    }
                    else if calendarView.selectedDates.contains(nextDate) {
                        selectionType = .leftBorder
                    }
                    else {
                        selectionType = .single
                    }
                }
            }
            else {
                selectionType = .none
            }
            if selectionType == .none {
                diyCell.selectionLayer.isHidden = true
                return
            }
            diyCell.selectionLayer.isHidden = false
            diyCell.selectionType = selectionType
            
        } else {
            diyCell.circleImageView.isHidden = true
            diyCell.selectionLayer.isHidden = true
        }
    }
}
