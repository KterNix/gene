//
//  TimeCell.swift
//  GenePlus
//
//  Created by pro on 8/16/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class TimeCell: UITableViewCell {

    
    
    @IBOutlet weak var titleLBL: UILabel!
    
    @IBOutlet weak var detailLBL: UILabel!
    
    @IBOutlet weak var checkIMG: UIImageView!
    
    
    func configWith(value: AllotmentModel) {
        titleLBL.text = value.startTime
        detailLBL.text = value.endTime
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
