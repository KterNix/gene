//
//  OverviewDataSource.swift
//  GenePlus
//
//  Created by pro on 8/17/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation

class OverviewDataSource: TableViewArrayDataSource<MuscleModel, AreaOverviewCell> {
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
}
