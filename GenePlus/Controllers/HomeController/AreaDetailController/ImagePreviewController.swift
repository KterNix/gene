//
//  ImagePreviewController.swift
//  GenePlus
//
//  Created by pro on 1/22/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import Hero
import SDWebImage

class ImagePreviewController: UIViewController {

    
    
    
    
    
    var imageType:ImageType = .normal

    
    var imageView: AImageView!
    
    var image: UIImage? {
        get { return imageView.image }
        set {
            imageView.image = newValue
            view.setNeedsLayout()
        }
    }
    
    var scrollView: UIScrollView!
    var dTapGR: UITapGestureRecognizer!
    var panGR = UIPanGestureRecognizer()
    
    var topInset: CGFloat = 0 {
        didSet {
            centerIfNeeded()
        }
    }
    
    var imageURL:String = ""
    
    
    lazy var shareImage = UIImage.fontAwesomeIcon(name: .shareSquareO, textColor: UIColor.mainColor(), size: CGSize(width: 26, height: 26))
    lazy var shareBTN: UIButton = {
        let b = UIButton(type: UIButton.ButtonType.custom)
        b.tintColor = UIColor.mainColor()
        b.setImage(shareImage, for: UIControl.State.normal)
        let sizeB:CGFloat = 30
        b.frame = CGRect(x: 0, y: 0, width: sizeB, height: sizeB)
        b.addTarget(self, action: #selector(didTapShare), for: UIControl.Event.touchUpInside)
        return b
    }()
    
    lazy var likeImage = UIImage(named: "likeIcon")?.withRenderingMode(.alwaysTemplate)
    lazy var likeBTN: UIButton = {
        let b = UIButton(type: UIButton.ButtonType.custom)
        b.tintColor = UIColor.mainColor()
        b.setImage(likeImage, for: UIControl.State.normal)
        let sizeB:CGFloat = 30
        b.frame = CGRect(x: 0, y: 0, width: sizeB, height: sizeB)
        b.addTarget(self, action: #selector(didTapLike), for: UIControl.Event.touchUpInside)
        return b
    }()
    
    lazy var closeImage = UIImage(named: "close")?.withRenderingMode(.alwaysTemplate)
    lazy var closeBTN: UIButton = {
        let b = UIButton(type: UIButton.ButtonType.custom)
        b.tintColor = UIColor.mainColor()
        b.setImage(closeImage, for: UIControl.State.normal)
        let edgeInset:CGFloat = 12
        b.imageEdgeInsets = UIEdgeInsets(top: edgeInset, left: edgeInset, bottom: edgeInset, right: edgeInset)
        let sizeB:CGFloat = 30
        b.frame = CGRect(x: 0, y: 0, width: sizeB, height: sizeB)
        b.addTarget(self, action: #selector(didTapClose), for: UIControl.Event.touchUpInside)
        return b
    }()
    
    
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//
//    }
    
    
    
   
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        whiteNavigationColor()
        
        
        dTapGR = UITapGestureRecognizer(target: self, action: #selector(doubleTap(gr:)))
        dTapGR.numberOfTapsRequired = 2
        view.addGestureRecognizer(dTapGR)
        
        
        panGR.delegate = self
        panGR.addTarget(self, action: #selector(pan))
        
        imageView.addGestureRecognizer(panGR)
        imageView.isUserInteractionEnabled = true
        
        let share = UIBarButtonItem(customView: shareBTN)
        let like = UIBarButtonItem(customView: likeBTN)
        navigationItem.rightBarButtonItems = [share, like]
        
        let close = UIBarButtonItem(customView: closeBTN)
        navigationItem.leftBarButtonItems = [close]
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        showImage()
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        scrollView.frame = view.bounds
        let size: CGSize
        if let image = imageView.image {
            let containerSize = CGSize(width: view.bounds.width, height: view.bounds.height - topInset)
            if containerSize.width / containerSize.height < image.size.width / image.size.height {
                size = CGSize(width: containerSize.width, height: containerSize.width * image.size.height / image.size.width )
            } else {
                size = CGSize(width: containerSize.height * image.size.width / image.size.height, height: containerSize.height )
            }
        } else {
            size = CGSize(width: view.bounds.width, height: view.bounds.width)
        }
        imageView.frame = CGRect(origin: .zero, size: size)
        scrollView.contentSize = size
        centerIfNeeded()
    }
    
    @objc fileprivate func didTapLike() {
        
    }
    
    
    @objc fileprivate func didTapClose() {
        dismiss(animated: true, completion: nil)
    }

    
    @objc fileprivate func didTapShare() {
        let shareItem = ActivityShare()
        shareItem.news = ""
        shareItem.url = ""
        share(object: shareItem)
    }
    
    
    
    fileprivate func setupUI() {
        setBackButton()
        applyAttributedNAV()
        
        scrollView = UIScrollView(frame: view.bounds)
        imageView = AImageView(frame: view.bounds)
        imageView.contentMode = .scaleAspectFill
        scrollView.addSubview(imageView)
        scrollView.maximumZoomScale = 3
        scrollView.delegate = self
        scrollView.contentMode = .center
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        
        view.addSubview(scrollView)

    }
    

    
    private func share(object : ActivityShare) {
        let vc = UIActivityViewController(activityItems: [object,object.image,object.news], applicationActivities: nil)
        DispatchQueue.main.async {
            self.navigationController?.present(vc, animated: true, completion: nil)
        }
    }
    
    
    fileprivate func showImage() {
        if imageType == .normal {
            imageView.setImageWith(path: imageURL, imageType: .NORMAL_IMAGE)
        }else {
            DispatchQueue.global().async {
                if let url = URL(string: self.imageURL) {
                    if let imageData = try? Data(contentsOf: url) {
                        DispatchQueue.main.async {
                            self.imageView.add(image: AImage(data: imageData)!)
                            self.imageView.play = true
                        }
                    }
                }
            }
        }
        
        imageView.hero.id = "image_\(imageURL)"
        imageView.hero.modifiers = [.position(CGPoint(x:view.bounds.width/2, y:view.bounds.height+view.bounds.width/2)), .scale(0.6), .forceNonFade, .spring(stiffness: 300, damping: 25)]
        topInset = topLayoutGuide.length
        imageView.isOpaque = true
    }
    
    func centerIfNeeded() {
        var inset = UIEdgeInsets(top: topInset, left: 0, bottom: 0, right: 0)
        if scrollView.contentSize.height < scrollView.bounds.height - topInset {
            let insetV = (scrollView.bounds.height - topInset - scrollView.contentSize.height)/2
            inset.top += insetV
            inset.bottom = insetV
        }
        if scrollView.contentSize.width < scrollView.bounds.width {
            let insetV = (scrollView.bounds.width - scrollView.contentSize.width)/2
            inset.left = insetV
            inset.right = insetV
        }
        scrollView.contentInset = inset
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func doubleTap(gr: UITapGestureRecognizer) {
        if scrollView.zoomScale == 1 {
            scrollView.zoom(to: zoomRectForScale(scale: scrollView.maximumZoomScale, center: gr.location(in: gr.view)), animated: true)
        } else {
            scrollView.setZoomScale(1, animated: true)
        }
    }
    
    fileprivate func zoomRectForScale(scale: CGFloat, center: CGPoint) -> CGRect {
        var zoomRect = CGRect.zero
        zoomRect.size.height = imageView.frame.size.height / scale
        zoomRect.size.width  = imageView.frame.size.width  / scale
        let newCenter = imageView.convert(center, from: scrollView)
        zoomRect.origin.x = newCenter.x - (zoomRect.size.width / 2.0)
        zoomRect.origin.y = newCenter.y - (zoomRect.size.height / 2.0)
        return zoomRect
    }
    
    @objc fileprivate func pan() {
        let translation = panGR.translation(in: nil)
        let progress = translation.y / 2 / view.bounds.height
        
        switch panGR.state {
        case .began:
            hero.dismissViewController()
        case .changed:
            Hero.shared.update(progress)
//            if let cell = self.collectionView?.visibleCells[0]  as? ScrollingImageCell {
                let currentPos = CGPoint(x: translation.x + self.view.center.x, y: translation.y + self.view.center.y)
                Hero.shared.apply(modifiers: [.position(currentPos)], to: imageView)
//            }
            
            
        default:
            
            if progress + panGR.velocity(in: nil).y / imageView!.bounds.height > 0.3 {
                DispatchQueue.main.async {
                    Hero.shared.finish()
                }
            } else {
                Hero.shared.cancel()
            }
        }
    }
}


extension ImagePreviewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        centerIfNeeded()
    }
}


extension ImagePreviewController:UIGestureRecognizerDelegate {
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if scrollView.zoomScale == 1 {
            let v = panGR.velocity(in: nil)
            return v.y > abs(v.x)
        }
        return false
    }
}
















