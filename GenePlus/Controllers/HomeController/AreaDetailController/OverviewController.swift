//
//  OverviewController.swift
//  GenePlus
//
//  Created by pro on 8/17/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import XLPagerTabStrip


class OverviewController: UIViewController, IndicatorInfoProvider {

    
    
    @IBOutlet weak var myTable: UITableView!
    
    var itemInfo:IndicatorInfo = "Overview"

    var muscle:MuscleModel?
    var datascr:OverviewDataSource?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configTable()
        // Do any additional setup after loading the view.
    }

    fileprivate func configTable() {
        datascr = datasource()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
}

extension OverviewController {
    
    fileprivate func datasource() -> OverviewDataSource? {
        guard muscle != nil else { return nil }
        let ds = OverviewDataSource(tableView: myTable, array: [muscle!])
        
        return ds
    }
    
}
