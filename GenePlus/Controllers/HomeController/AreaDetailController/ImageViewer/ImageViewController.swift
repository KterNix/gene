// The MIT License (MIT)
//
// Copyright (c) 2016 Luke Zhao <me@lkzhao.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import UIKit
import Hero
import SDWebImage

class ImageViewController: UICollectionViewController {
  var selectedIndex: IndexPath?
  var panGR = UIPanGestureRecognizer()
    
    var listItems = [String]()
    
    var imageType:ImageType = .normal

  override func viewDidLoad() {
    super.viewDidLoad()
    preferredContentSize = CGSize(width: view.bounds.width, height: view.bounds.width)

    view.layoutIfNeeded()
    collectionView!.reloadData()
    if let selectedIndex = selectedIndex {
      collectionView!.scrollToItem(at: selectedIndex, at: .centeredHorizontally, animated: false)
    }

    panGR.addTarget(self, action: #selector(pan))
    panGR.delegate = self
    collectionView?.addGestureRecognizer(panGR)
    setupUI()
  }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .default
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    
    fileprivate func setupUI() {
        hideBackButton()
        setBackButton()
        applyAttributedNAV()
    }
    
  override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()
    for v in (collectionView!.visibleCells as? [ScrollingImageCell])! {
      v.topInset = topLayoutGuide.length
    }
  }

  @objc func pan() {
    let translation = panGR.translation(in: nil)
    let progress = translation.y / 2 / collectionView!.bounds.height
    switch panGR.state {
    case .began:
      hero_dismissViewController()
    case .changed:
        Hero.shared.update(progress)
        if let cell = self.collectionView?.visibleCells[0]  as? ScrollingImageCell {
            let currentPos = CGPoint(x: translation.x + self.view.center.x, y: translation.y + self.view.center.y)
            Hero.shared.apply(modifiers: [.position(currentPos)], to: cell.imageView)
        }
    default:
      if progress + panGR.velocity(in: nil).y / collectionView!.bounds.height > 0.3 {
        DispatchQueue.main.async {
            Hero.shared.finish()
        }
      } else {
        Hero.shared.cancel()
      }
    }
  }
}

extension ImageViewController {
  override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listItems.count

  }

  override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let imageCell = (collectionView.dequeueReusableCell(withReuseIdentifier: "item", for: indexPath) as? ScrollingImageCell)!
    if imageType == .normal {
        DispatchQueue.global().async {
            let url = URL(string: self.listItems[indexPath.row])
            DispatchQueue.main.async {
                imageCell.imageView.sd_setImage(with: url, placeholderImage: nil, options: SDWebImageOptions.continueInBackground, completed: nil)
            }
        }
        imageCell.imageView.heroID = "image_\(indexPath.item)"

    }else {
        DispatchQueue.global().async {
            let imageItem = self.listItems[indexPath.row]
            if let url = URL(string: imageItem) {
                if let imageData = try? Data(contentsOf: url) {
                    DispatchQueue.main.async {
                        imageCell.imageView.image = UIImage.sd_animatedGIF(with: imageData)
                    }
                }
            }
        }
        imageCell.imageView.heroID = "image_\(indexPath.item)"
    }
    imageCell.imageView.heroModifiers = [.position(CGPoint(x:view.bounds.width/2, y:view.bounds.height+view.bounds.width/2)), .scale(0.6)]
    imageCell.topInset = topLayoutGuide.length
    imageCell.imageView.isOpaque = true
    return imageCell
  }
}

extension ImageViewController: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return view.bounds.size
  }
}

extension ImageViewController:UIGestureRecognizerDelegate {
  func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
    if let cell = collectionView?.visibleCells[0] as? ScrollingImageCell,
       cell.scrollView.zoomScale == 1 {
      let v = panGR.velocity(in: nil)
      return v.y > abs(v.x)
    }
    return false
  }
}
