//
//  AreaOverviewCell.swift
//  GenePlus
//
//  Created by pro on 8/17/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class AreaOverviewCell: UITableViewCell, ConfigurableCell {

    
    
    @IBOutlet weak var titleLBL: UILabel!
    
    @IBOutlet weak var detailLBL: UILabel!
    
    
    func configure(_ item: MuscleModel, at indexPath: IndexPath) {
        titleLBL.text = item.title
        detailLBL.text = item.content
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
