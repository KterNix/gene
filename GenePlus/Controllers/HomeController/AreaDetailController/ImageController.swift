//
//  ImageController.swift
//  GenePlus
//
//  Created by pro on 1/17/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Hero
import SDWebImage
import FLAnimatedImage

enum ImageType {
    case normal, gif
}



class ImageController: UIViewController , IndicatorInfoProvider {
    
    
    @IBOutlet weak var myColl: UICollectionView!
    
    
    
    var itemInfo:IndicatorInfo = "PICTURE"

    
    var muscle = MuscleModel()
    
    
    var imageType: ImageType = .normal
    
    fileprivate var currentIndex:Int = 0
    fileprivate var selectedIndexPath:IndexPath?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configColl()
    }
    
    
    
    
    fileprivate func configColl() {
        myColl.register(UINib(nibName: "ImageCell", bundle: nil), forCellWithReuseIdentifier: "item")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}

extension ImageController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (muscle?.muscleImage?.count ?? 0)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "item", for: indexPath) as! ImageCell

        
        
        if imageType == .gif {

            DispatchQueue.global().async {
                if let imageItem = self.muscle?.muscleImage?[indexPath.row] {
                    if let url = URL(string: imageItem) {
                        if let imageData = try? Data(contentsOf: url) {
                            DispatchQueue.main.async {
                                cell.imageView.add(image: AImage(data: imageData)!)
                                cell.imageView.play = true
                            }
                        }
                    }
                }
            }
            
        }else{
            cell.imageView.setImageWith(path: self.muscle?.muscleImage?[indexPath.row], imageType: .NORMAL_IMAGE)
        }
        

        
        if let item = muscle?.muscleImage?[indexPath.row] {
            cell.imageView.heroID = "image_\(item)"
        }
        
        cell.title.text = muscle?.title
        cell.likeLBL.text = muscle?.likeCount
        cell.viewerLBL.text = muscle?.views
        cell.imageView.heroModifiers = [.scale(0.8), .forceNonFade, .spring(stiffness: 300, damping: 25)]
        cell.imageView.isOpaque = true
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = Controllers.shared.imagePreviewController()
//        vc.isHeroEnabled = true
//        vc.selectedIndex = indexPath
        currentIndex = indexPath.row
        selectedIndexPath = indexPath
        
        vc.imageType = imageType
        if let imageURL = muscle?.muscleImage?[indexPath.row] {
            vc.imageURL = imageURL
        }
        
        let nav = UINavigationController(rootViewController: vc)
        nav.isHeroEnabled = true
        present(nav, animated: true, completion: nil)
        
        
    }
    
//    func viewController(forStoryboardName: String) -> UIViewController {
//        return UIStoryboard(name: forStoryboardName, bundle: nil).instantiateInitialViewController()!
//    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let sizeWidth:CGFloat = view.frame.size.width - 20
        let sizeHeight:CGFloat = view.frame.size.height / 2
        
        return CGSize(width: sizeWidth, height: sizeHeight)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let edgeInset:CGFloat = 10
        return UIEdgeInsets(top: edgeInset, left: 0, bottom: edgeInset, right: 0)
        
    }
}

extension ImageController: HeroViewControllerDelegate {
    func heroWillStartAnimatingTo(viewController: UIViewController) {
        if (viewController as? ImageController) != nil {
            myColl.heroModifiers = [.cascade(delta:0.015, direction:.bottomToTop, delayMatchedViews:true)]
        } else if (viewController as? ImageViewController) != nil {
            let cell = myColl.cellForItem(at: myColl.indexPathsForSelectedItems!.first!)!
            myColl.heroModifiers = [.cascade(delta: 0.015, direction: .radial(center: cell.center), delayMatchedViews: true)]
        } else {
            myColl.heroModifiers = [.cascade(delta:0.015)]
        }
    }
    func heroWillStartAnimatingFrom(viewController: UIViewController) {
        view.heroModifiers = nil
        if (viewController as? ImageController) != nil {
            myColl.heroModifiers = [.cascade(delta:0.015), .delay(0.25)]
        } else {
            myColl.heroModifiers = [.cascade(delta:0.015)]
        }

        let targetAttribute = myColl.layoutAttributesForItem(at: selectedIndexPath!)

        myColl.heroModifiers = [.cascade(delta:0.015, direction:.inverseRadial(center:(targetAttribute?.center)!))]

        if !myColl.indexPathsForVisibleItems.contains(selectedIndexPath!) {
            // make the cell visible
            myColl.scrollToItem(at: selectedIndexPath!,
                                at: .top,
                                animated: false)
        }

        myColl.performBatchUpdates({
            myColl.reloadSections(IndexSet(integer: 0))
        }) { (finish) in
        }
    }
}














