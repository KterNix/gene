//
//  AreaDetailController.swift
//  GenePlus
//
//  Created by pro on 1/17/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import ObjectMapper



class AreaDetailController: ButtonBarPagerTabStripViewController {

    var muscle = MuscleModel()
    var muscleGIF = MuscleModel()
    
    
    
    
    override func viewDidLoad() {
        settings.style.selectedBarHeight = 3
        super.viewDidLoad()
        setupUI()
        whiteNavigationColor()
        setBackButton()
        applyAttributedNAV()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        defaultStatusbar()
    }
    
    
    func setupUI() {
        title = muscle?.title
        
        self.buttonBarView.selectedBar.backgroundColor = UIColor.mainColor()
        self.buttonBarView.backgroundColor = UIColor.white
        self.settings.style.buttonBarBackgroundColor = UIColor.white
        self.settings.style.buttonBarItemBackgroundColor = UIColor.white
        self.settings.style.buttonBarItemTitleColor = UIColor.darkGray
        self.settings.style.buttonBarItemFont = UIFont.systemFont(ofSize: 12)
        
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            
            oldCell?.label.textColor = UIColor.darkGray
            newCell?.label.textColor = UIColor.mainColor()
            
            if animated {
                UIView.animate(withDuration: 0.1, animations: { () -> Void in
                    newCell?.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                    oldCell?.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                })
            }
        }
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        
        let overviewController = Controllers.shared.overviewController()
        let overviewIndicator = IndicatorInfo(title: "OVERVIEW")
        overviewController.itemInfo = overviewIndicator
        overviewController.muscle = muscle
        
        let imgController = Controllers.shared.imageController()
        let mapIndicator = IndicatorInfo(stringLiteral: "PICTURES")
        imgController.imageType = .normal
        imgController.itemInfo = mapIndicator
        imgController.muscle = muscle
        
        let imgGIFController = Controllers.shared.imageController()
        let categoriesIndicator = IndicatorInfo(stringLiteral: "GIF")
        imgGIFController.imageType = .gif
        imgGIFController.itemInfo = categoriesIndicator
        imgGIFController.muscle = muscleGIF
                
        let trainerController = Controllers.shared.trainerController()
        let downloadIndicator = IndicatorInfo(stringLiteral: "TRAINER")
        trainerController.itemInfo = downloadIndicator
        
        
        return [overviewController, imgController, imgGIFController, trainerController]
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

























