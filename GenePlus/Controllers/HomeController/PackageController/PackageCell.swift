//
//  PackageCell.swift
//  GenePlus
//
//  Created by pro on 1/8/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class PackageCell: UICollectionViewCell {

    
    
    @IBOutlet weak var packageTitle: UILabel!
    
    
    @IBOutlet weak var optionsLBL: UILabel!
    @IBOutlet weak var optionsLBL_2: UILabel!
    @IBOutlet weak var optionsLBL_3: UILabel!
    @IBOutlet weak var optionsLBL_4: UILabel!
    
    
    @IBOutlet weak var chooseBTN: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        DispatchQueue.main.async {
            self.layer.cornerRadius = 10
            self.layer.masksToBounds = true
            self.layer.borderWidth = 1
            self.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.1).cgColor
            self.dropShadow()
        }
        chooseBTN.layer.cornerRadius = Constant.buttonCornerRadius
        chooseBTN.layer.masksToBounds = true
    }

}
