//
//  TrainerDetailCell.swift
//  GenePlus
//
//  Created by pro on 1/8/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import DGCollectionViewLeftAlignFlowLayout

class TrainerDetailCell: UITableViewCell {

    
    @IBOutlet weak var titleContent: UILabel!
    
    
    @IBOutlet weak var myColl: UICollectionView!
    
    fileprivate var listCategories:[String] = [] {
        didSet {
            DispatchQueue.main.async {
                self.myColl.reloadData()
            }
        }
    }
    
    var flowLayout = LeftAlignedCollectionViewFlowLayout()
    
    
    
    func configColl(value: [String]) {
        listCategories = value
    }
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        myColl.delegate = self
        myColl.dataSource = self
//        myColl.isScrollEnabled = false
        let cellNib = UINib(nibName: "CategoriesCell", bundle: nil)

        myColl.register(cellNib, forCellWithReuseIdentifier: "CategoriesCell")
        myColl.collectionViewLayout = flowLayout
        if let flowLayouts = myColl.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayouts.estimatedItemSize = CGSize(width: 1, height: 1)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
}

extension TrainerDetailCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listCategories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoriesCell", for: indexPath) as! CategoriesCell
        cell.categoryLBL.text = listCategories[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let sizeWidth:CGFloat = 200
        let sizeHeight:CGFloat = 35
        
        return CGSize(width: sizeWidth, height: sizeHeight)

    }
   
    
}




