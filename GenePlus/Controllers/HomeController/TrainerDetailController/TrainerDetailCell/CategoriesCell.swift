//
//  CategoriesCell.swift
//  GenePlus
//
//  Created by pro on 1/13/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class CategoriesCell: UICollectionViewCell {

    
    
    @IBOutlet weak var categoryLBL: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cornerRadius = Constant.buttonCornerRadius
        borderColor = UIColor.lightGray
        borderWidth = 1
    }
}
