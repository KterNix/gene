//
//  ButtonView.swift
//  GenePlus
//
//  Created by pro on 1/8/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class ButtonView: UIView {

    @IBOutlet weak var chooseButton: UIButton!
    

    override func awakeFromNib() {
        chooseButton.layer.cornerRadius = Constant.buttonCornerRadius
        chooseButton.layer.masksToBounds = true
    }
    
    
}
