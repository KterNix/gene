//
//  RatingCell.swift
//  GenePlus
//
//  Created by pro on 1/8/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class RatingCell: UITableViewCell {

    
    @IBOutlet weak var seeAllButton: UIButton!
    
    @IBOutlet weak var avatarUser: UIImageView!
    @IBOutlet weak var nameUser: UILabel!
    @IBOutlet weak var timeCreated: UILabel!
    
    @IBOutlet weak var contentLBL: UILabel!
    
    @IBOutlet weak var ratingView: CosmosView!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        seeAllButton.layer.borderWidth = 1
        seeAllButton.layer.borderColor = UIColor.lightGray.cgColor
        seeAllButton.layer.cornerRadius = Constant.buttonCornerRadius
        seeAllButton.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
