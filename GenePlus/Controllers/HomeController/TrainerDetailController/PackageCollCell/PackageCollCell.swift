//
//  PackageCollCell.swift
//  GenePlus
//
//  Created by pro on 8/15/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import TGLParallaxCarousel

class PackageCollCell: UITableViewCell {

    
    
    @IBOutlet weak var carouselView: TGLParallaxCarousel!
    
    var listPackage = [PackageModel]()

    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        // Initialization code
        setupCarouselView()
    }

    
    fileprivate func setupCarouselView() {
        
        setupDataSource()
        
        carouselView.delegate = self
        carouselView.datasource = self

    }

    fileprivate func setupDataSource() {
        var a = PackageModel()
        
        a.packageName = "STARTER"
        a.sessions = "5 Sessions"
        a.duration = "50 Minutes"
        a.price = "30$"
        listPackage.append(a)
        
        
        a = PackageModel()
        a.packageName = "BRONZE"
        a.sessions = "10 Sessions"
        a.duration = "70 Minutes"
        a.price = "45$"
        listPackage.append(a)
        
        a = PackageModel()
        a.packageName = "SILVER"
        a.sessions = "20 Sessions"
        a.duration = "160 Minutes"
        a.price = "80$"
        listPackage.append(a)
        
        a = PackageModel()
        a.packageName = "GOLD"
        a.sessions = "30 Sessions"
        a.duration = "280 Minutes"
        a.price = "120$"
        listPackage.append(a)
        
        
    }
    
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        if scrollView == self.myColl {
//            var currentCellOffset = self.myColl.contentOffset
//            currentCellOffset.x += self.myColl.frame.width - 120
//            if let indexPath = self.myColl.indexPathForItem(at: currentCellOffset) {
//                self.myColl.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
//            }
//        }
//    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}

// MARK: - TGLParallaxCarouselDelegate
extension PackageCollCell: TGLParallaxCarouselDelegate, TGLParallaxCarouselDatasource {
    func didTapOnItemAtIndex(index: Int, carousel: TGLParallaxCarousel) {
        print("TAP at index: \(index)")
        pushVC()
    }
    
    func didMovetoPageAtIndex(index: Int) {
        
    }
    
    func numberOfItemsInCarousel(carousel: TGLParallaxCarousel) -> Int {
        return listPackage.count
    }
    
    func viewForItemAtIndex(index: Int, carousel: TGLParallaxCarousel) -> TGLParallaxCarouselItem {
        let item = listPackage[index]
        let v = CardView(frame: CGRect(x: 0, y: 0, width: 300, height: 250) , model: item)
        v.bottomBTN.tag = index
        return v
    }
    
    private func pushVC() {
        let vc = Controllers.shared.availableController()
        parentViewController?.navigationController?.pushViewController(vc, animated: true)
    }
}

