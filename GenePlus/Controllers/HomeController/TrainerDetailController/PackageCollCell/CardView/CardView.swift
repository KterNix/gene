//
//  CardView.swift
//  GenePlus
//
//  Created by pro on 8/16/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import TGLParallaxCarousel

@IBDesignable
class CardView: TGLParallaxCarouselItem {

    fileprivate var containerView: UIView!
    fileprivate let nibName = "CardView"

    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var titleLBL: UILabel!
    
    @IBOutlet weak var priceLBL: UILabel!
    
    @IBOutlet weak var sessionLBL: UILabel!
    
    @IBOutlet weak var durationLBL: UILabel!
    
    @IBOutlet weak var bottomBTN: UIButton!
    
    
    
    func initWith(model: PackageModel) {
        titleLBL.text = model.packageName
        sessionLBL.text = model.sessions
        durationLBL.text = model.duration
        priceLBL.text = model.price
        
        if model.packageName == "GOLD" {
            let color = UIColor.init(hexString: "FF9300")
            topView.backgroundColor = color
            titleLBL.textColor = UIColor.white
            bottomBTN.setTitleColor(color, for: .normal)
        }else if model.packageName == "BRONZE" {
            let color = UIColor.init(hexString: "BC4915")
            topView.backgroundColor = color
            titleLBL.textColor = UIColor.white
            bottomBTN.setTitleColor(color, for: .normal)
        }else if model.packageName == "STARTER" {
            let color = UIColor.init(hexString: "78A9A9")
            topView.backgroundColor = color
            titleLBL.textColor = UIColor.white
            bottomBTN.setTitleColor(color, for: .normal)
        }
        
    }
    
    
    // MARK: - init
    convenience init(frame: CGRect, model: PackageModel) {
        self.init(frame: frame)
        initWith(model: model)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
        setup()
    }
    
    
    func xibSetup() {
        containerView = loadViewFromNib()
        containerView.frame = CGRect(x: 0, y: 100, width: bounds.width, height: bounds.height)
        containerView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        addSubview(containerView)
        containerView.cornerRadius = 5
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    // MARK: - methods
    fileprivate func setup() {
        cornerRadius = 5
        dropShadow()
    }

}
