
//
//  HeaderTrainerDetail.swift
//  GenePlus
//
//  Created by pro on 1/8/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class HeaderTrainerDetail: UIView {

    
    @IBOutlet weak var backgroundImage: UIImageView!
    
    @IBOutlet weak var avatarTrainer: UIImageView!
    @IBOutlet weak var nameTrainer: UILabel!
    
    @IBOutlet weak var favButton: UIButton!
    
    @IBOutlet weak var viewGradient: GradientViewTop!
    
    @IBOutlet weak var ratingView: CosmosView!
    
    @IBOutlet weak var ratingCount: UILabel!
    @IBOutlet weak var reviewLBL: UILabel!
    @IBOutlet weak var sendMesBTN: UIButton!
    
    @IBOutlet weak var detailLBL: UILabel!
    
    
    override func awakeFromNib() {
        avatarTrainer.cornerRadius = avatarTrainer.frame.size.width / 2
        avatarTrainer.borderWidth = 1
        avatarTrainer.borderColor = UIColor.lightGray.withAlphaComponent(0.2)
        favButton.setImage(UIImage(named: "heartNAV")?.withRenderingMode(.alwaysOriginal), for: UIControl.State.normal)
        
        sendMesBTN.cornerRadius = 5
        sendMesBTN.borderWidth = 1
        sendMesBTN.borderColor = UIColor.white
        
    }
    
    
}
