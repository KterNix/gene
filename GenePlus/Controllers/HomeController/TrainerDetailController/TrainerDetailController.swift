//
//  TrainerDetailController.swift
//  GenePlus
//
//  Created by pro on 1/8/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import GSKStretchyHeaderView



class TrainerDetailController: UIViewController {

    
    @IBOutlet weak var myTable: UITableView!
    
    enum Cells: Int, CaseCountable {
        case CATEGORIES = 0
        case EXPERIENCE
        case INFORMATION
        case ATTRIBUTES
        case REVIEW
        case PACKAGE
    }
    
    
    
    lazy var headerTrainerDetail:HeaderTrainerDetail? = UIView.instanceFromNib()
    lazy var footerButton:ButtonView? = UIView.instanceFromNib()
    var stretchyHeaderView:GSKStretchyHeaderView?

    var presentSource:TrainerPresentSource = .HIRE
    
    var shownIndexes : [IndexPath] = []
    
    
    var currentTrainer = TrainerModel()
    
    

    var categoriesCellHeight: CGFloat = 150

    
    var listCategories = ["CYCLING", "BADMINTON", "RACING", "CRICKET", "GYM", "FOOTBALL","CYCLING", "BADMINTON", "RACING", "CRICKET", "GYM", "FOOTBALL"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        configTable()
        // Do any additional setup after loading the view.
    }
    
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        clearNavigationColor()
        updateNAVTint(color: UIColor.white)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        lightStatusbar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        whiteNavigationColor()
        defaultStatusbar()
        updateNAVTint(color: .mainColor())
    }

    fileprivate func setupUI() {
        setBackButton()
        clearNavigationColor()
        
    }
    
    fileprivate func configTable() {
        
        let headerHeight:CGFloat = 400


        
        stretchyHeaderView = GSKStretchyHeaderView(frame: CGRect(x: 0, y: 0, width: myTable.frame.size.width, height: headerHeight))
        stretchyHeaderView?.expansionMode = .topOnly
        stretchyHeaderView?.contentShrinks = true
        stretchyHeaderView?.contentExpands = true
        stretchyHeaderView?.minimumContentHeight = 64
        stretchyHeaderView?.maximumContentHeight = headerHeight
        stretchyHeaderView?.addSubview(headerTrainerDetail!)
        myTable.addSubview(stretchyHeaderView!)
        
        stretchyHeaderView?.stretchDelegate = self
        
        
        
        myTable.register(nibWithCellClass: TrainerDetailCell.self)
        myTable.register(nibWithCellClass: TrainerExperienceCell.self)
        myTable.register(nibWithCellClass: TrainerInfoCell.self)
        myTable.register(nibWithCellClass: RatingCell.self)
        myTable.register(nibWithCellClass: TrainerProfileCell.self)
        myTable.register(nibWithCellClass: PackageCollCell.self)
        
        headerTrainerDetail?.nameTrainer.text = currentTrainer.name
        
        headerTrainerDetail?.ratingView.rating = currentTrainer.rating
        
        headerTrainerDetail?.ratingCount.text = currentTrainer.rating.description
        headerTrainerDetail?.reviewLBL.text = "(\(currentTrainer.reviews) Reviews)"
        
        headerTrainerDetail?.sendMesBTN.addTarget(self, action: #selector(didTapMes), for: UIControl.Event.touchUpInside)
        
    }
    
    @objc fileprivate func didTapMes() {
        let vc = Controllers.shared.chatController()
        let nav = UINavigationController(rootViewController: vc)
        
        DispatchQueue.main.async {
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
//        let navigationAlpha: CGFloat = 0 + (self.myTable.contentOffset.y * 4 / (self.myTable.contentSize.height - self.myTable.frame.size.height))
//
//        if navigationAlpha > 0.8 {
//            defaultStatusbar()
//            updateNAVTint(color: .mainColor())
//        }else{
//            lightStatusbar()
//            updateNAVTint(color: .white)
//        }
        
        /*
         scrollView.contentOffset.y: -64
         scrollView.frame.size.height: size Height
         fabsf -> Chuyển số âm thành dương
         */
        
        
        guard scrollView.contentOffset.y < 0 else { return }
        var scale = 1 + fabsf(Float(scrollView.contentOffset.y)) / Float(scrollView.frame.size.height / 5)
        scale = max(0, scale)
        
        
//        headerTrainerDetail?.backgroundImage.transform = CGAffineTransform.identity.scaledBy(x: CGFloat(scale), y: CGFloat(scale))
//        headerTrainerDetail?.viewGradient.transform = CGAffineTransform.identity.scaledBy(x: CGFloat(scale), y: CGFloat(scale))

    }
    
    func updateTable(table: UITableView, withDuration duration: TimeInterval) {
        UIView.animate(withDuration: duration, animations: { () -> Void in
            table.beginUpdates()
            table.endUpdates()
        })
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if let categoriesCell = myTable.cellForRow(at: IndexPath(row: Cells.CATEGORIES.rawValue, section: 0)) as? TrainerDetailCell {
            let collectionViewContentHeight = categoriesCell.myColl.contentSize.height
            let sizeSpace:CGFloat = 80
            self.categoriesCellHeight = collectionViewContentHeight + sizeSpace
            self.drawLineSeparatorFor(cell: categoriesCell, height: self.categoriesCellHeight - 2)
            self.updateTable(table: self.myTable, withDuration: 0.2)
            
        }
    }

    
    deinit {
        headerTrainerDetail = nil
        footerButton = nil
        stretchyHeaderView = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


extension TrainerDetailController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Cells.caseCount
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.row == Cells.CATEGORIES.rawValue {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TrainerDetailCell", for: indexPath) as! TrainerDetailCell
            cell.selectionStyle = .none
            cell.titleContent.text = "TRAINING CATEGORIES"
            cell.configColl(value: listCategories)
            
            return cell
        }else if indexPath.row == Cells.EXPERIENCE.rawValue {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TrainerExperienceCell", for: indexPath) as! TrainerExperienceCell
            cell.selectionStyle = .none
            //            cell.contentLBL.text = "TRAINING EXPERIENCE"
            cell.contentLBL.text = "Jackson is a lovely and honesty trainer. If away from home too ease to loose. After the training or pay more attention."
            return cell
        }else if indexPath.row == Cells.INFORMATION.rawValue {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TrainerInfoCell", for: indexPath) as! TrainerInfoCell
            cell.selectionStyle = .none
            return cell
        }else if indexPath.row == Cells.ATTRIBUTES.rawValue {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TrainerProfileCell", for: indexPath) as! TrainerProfileCell
            cell.selectionStyle = .none
            return cell
        }else if indexPath.row == Cells.REVIEW.rawValue {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RatingCell", for: indexPath) as! RatingCell
            cell.contentLBL.text = "Jackson is a lovely and honesty trainer. If away from home too ease to loose. After the training or pay more attention."
            cell.seeAllButton.addTarget(self, action: #selector(didTapSeeAll), for: UIControl.Event.touchUpInside)
            cell.selectionStyle = .none
            return cell
        }else if indexPath.row == Cells.PACKAGE.rawValue {
            let cell = tableView.dequeueReusableCell(withClass: PackageCollCell.self)
            return cell
        }
        
        return UITableViewCell()
    }
    
    @objc fileprivate func didTapSeeAll() {
        let vc = Controllers.shared.ratingController()
        let nav = UINavigationController(rootViewController: vc)
        present(nav, animated: true, completion: nil)
    }
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == Cells.INFORMATION.rawValue {
            return 226
        }else if indexPath.row ==  Cells.CATEGORIES.rawValue {
            return categoriesCellHeight
        }else if indexPath.row == Cells.ATTRIBUTES.rawValue {
            return 225
        }else if indexPath.row == Cells.PACKAGE.rawValue {
            return 380
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == Cells.EXPERIENCE.rawValue {
            return 226
        }else if indexPath.row == Cells.CATEGORIES.rawValue {
            return 140
        }else if indexPath.row == Cells.ATTRIBUTES.rawValue {
            return 225
        }else if indexPath.row == Cells.REVIEW.rawValue {
            return 152
        }
        return 98
    }
    
    
    
    
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        if presentSource == .HIRE {
//            return 0
//        }
//        return 60
//    }
//
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//
//        if presentSource == .HIRE {
//            return nil
//        }
//
//        footerButton?.chooseButton.addTarget(self, action: #selector(didTapChooseButton), for: UIControlEvents.touchUpInside)
//        return footerButton
//    }
    
//    @objc fileprivate func didTapChooseButton() {
//        let vc = Controllers.shared.availableController()
//        navigationController?.pushViewController(vc, animated: true)
//    }
    
    
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            if indexPath.row != Cells.CATEGORIES.rawValue {
                drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            }
            animateCell(cell: cell)
        }
    }
}




extension TrainerDetailController: GSKStretchyHeaderViewStretchDelegate {
    func stretchyHeaderView(_ headerView: GSKStretchyHeaderView, didChangeStretchFactor stretchFactor: CGFloat) {
//        guard stretchFactor >= 0.4, stretchFactor <= 1 else { return }
        
        
        headerTrainerDetail?.detailLBL.alpha = stretchFactor
        
        
    }
}














