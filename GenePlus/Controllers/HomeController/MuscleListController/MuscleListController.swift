//
//  MuscleListController.swift
//  GenePlus
//
//  Created by pro on 7/12/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import GSKStretchyHeaderView
import ObjectMapper

class MuscleListController: BaseViewController {

    @IBOutlet weak var myTable: UITableView!
    
    var stretchyHeaderView:GSKStretchyHeaderView?
    lazy var header:MuscleHeader? = UIView.instanceFromNib()
    
    
    var muscleArea:String = ""
    var muscle = [MuscleModel]()
    
    var muscleGIF = [MuscleModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateAttributedNAV(color: UIColor.white)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        lightStatusbar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        defaultStatusbar()
    }
    
    
    override func setupUI() {
        setBackButton()
        clearNavigationColor()
        applyAttributedNAV()
    }
    
    override func configTable() {
        let headerHeight:CGFloat = 150

        stretchyHeaderView = GSKStretchyHeaderView(frame: CGRect(x: 0, y: 0, width: myTable.frame.size.width, height: headerHeight))
        stretchyHeaderView?.expansionMode = .topOnly
        stretchyHeaderView?.contentShrinks = true
        stretchyHeaderView?.contentExpands = true
        stretchyHeaderView?.minimumContentHeight = 130
        stretchyHeaderView?.maximumContentHeight = headerHeight
        stretchyHeaderView?.addSubview(header!)
        myTable.addSubview(stretchyHeaderView!)
        
        stretchyHeaderView?.stretchDelegate = self
        
        header?.titleLBL.text = muscleArea
        header?.detailLBL.text = "\(muscle.count) workouts"

        header?.backgroundImage.setImageWith(path: muscle.first?.muscleImage?.first, imageType: .NORMAL_IMAGE)
        
        
        myTable.register(nibWithCellClass: MuscleCell.self)
        myTable.delegate = self
        myTable.dataSource = self
        
        readFile()
        
    }
    
    fileprivate func readFile() {
        
        if let path = Bundle.main.path(forResource: "muscleGIF", ofType: "plist") {
            if let arr = NSArray(contentsOfFile: path) {
                guard let models = Mapper<MuscleModel>().mapArray(JSONObject: arr) else { return }
                
                muscleGIF = models
            }
        }
    }


    
    deinit {
        stretchyHeaderView = nil
        header = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

extension MuscleListController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return muscle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: MuscleCell.self)
        
        let item = muscle[indexPath.row]
        cell.configWith(value: item)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = Controllers.shared.areaDetailController()
        vc.muscle = muscle[indexPath.row]
        vc.muscleGIF = muscleGIF.first ?? MuscleModel()
        let nav = UINavigationController(rootViewController: vc)
        DispatchQueue.main.async {
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}


extension MuscleListController: GSKStretchyHeaderViewStretchDelegate {
    func stretchyHeaderView(_ headerView: GSKStretchyHeaderView, didChangeStretchFactor stretchFactor: CGFloat) {
        guard stretchFactor >= 0.4, stretchFactor <= 1 else { return }
        
    }
}

















