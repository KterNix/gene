//
//  MuscleHeader.swift
//  GenePlus
//
//  Created by pro on 7/12/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class MuscleHeader: UIView {

    @IBOutlet weak var backgroundImage: UIImageView!
    
    @IBOutlet weak var viewAlpha: UIView!
    
    
    @IBOutlet weak var titleLBL: UILabel!
    
    @IBOutlet weak var detailLBL: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    private func setupUI() {
        viewAlpha.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        
        
        backgroundImage.setImageWith(path: "https://www.healthkart.com/connect/sites/default/files/chest.png", imageType: .NORMAL_IMAGE)
        
    }
   
}
