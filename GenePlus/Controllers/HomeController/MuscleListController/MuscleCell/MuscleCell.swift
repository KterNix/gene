//
//  MuscleCell.swift
//  GenePlus
//
//  Created by pro on 7/12/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class MuscleCell: UITableViewCell {

    
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var profilePic: UIImageView!
    
    @IBOutlet weak var titleLBL: UILabel!
    @IBOutlet weak var detailLBL: UILabel!
    
    
    func configWith(value: MuscleModel) {
        
        titleLBL.text = value.title
        detailLBL.text = "x\(value.reps ?? "") sets"
        
        profilePic.setImageWith(path: value.muscleImage?.first, imageType: .NORMAL_IMAGE)
        
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupUI()
    }
    
    private func setupUI() {
        selectionStyle = .none
        parentView.cornerRadius = 3
        parentView.borderWidth = 1
        parentView.borderColor = UIColor.lightGray.withAlphaComponent(0.2)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
