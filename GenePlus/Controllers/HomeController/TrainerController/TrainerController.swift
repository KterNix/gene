//
//  TrainerController.swift
//  GenePlus
//
//  Created by pro on 1/8/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import FontAwesome_swift
import GoogleMaps
import XLPagerTabStrip

class TrainerController: UIViewController, IndicatorInfoProvider {
    
    

    
    var itemInfo:IndicatorInfo = "TRAINER"

    
    @IBOutlet weak var myTable: UITableView!
    
    
    var shownIndexes : [IndexPath] = []

    lazy var searchImge = UIImage(named: "searchNAV")?.withRenderingMode(.alwaysTemplate)
    lazy var searchButton:UIButton = {
        let b = UIButton(type: UIButton.ButtonType.custom)
        b.setImage(searchImge!, for: UIControl.State.normal)
        let sizeB:CGFloat = 30
        b.frame = CGRect(x: 0, y: 0, width: sizeB, height: sizeB)
        b.addTarget(self, action: #selector(didTapSearch), for: UIControl.Event.touchUpInside)
        return b
    }()
    
    lazy var noticeImage = UIImage.fontAwesomeIcon(name: .bellO, textColor: UIColor.white, size: CGSize(width: 26, height: 26)).withRenderingMode(.alwaysTemplate)
    lazy var noticeButton:UIButton = {
        let b = UIButton(type: UIButton.ButtonType.custom)
        b.setImage(noticeImage, for: UIControl.State.normal)
        let sizeB:CGFloat = 30
        b.frame = CGRect(x: 0, y: 0, width: sizeB, height: sizeB)
        b.addTarget(self, action: #selector(didTapNotice), for: UIControl.Event.touchUpInside)
        return b
    }()
    
//    lazy var trainerHeaderView:TrainerHeaderView? = UIView.instanceFromNib()
    
    
    
    
    var listTrainer = [TrainerModel]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        configTable()
        setupDataSource()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        defaultStatusbar()
        applyAttributedNAV()
    }
    
    
    fileprivate func setupUI() {
//        title = "Nearby"
//        setBackButton()
        
//        let searchBTN = UIBarButtonItem(customView: searchButton)
//        let noticeBTN = UIBarButtonItem(customView: noticeButton)
//        navigationItem.rightBarButtonItems = [searchBTN,noticeBTN]
//
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
//        self.navigationController?.navigationBar.shadowImage = UIImage()
//        self.navigationController?.navigationBar.isTranslucent = true
//        self.navigationController?.view.backgroundColor = UIColor.clear
//        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
        
    }
    
    
    fileprivate func configTable() {
        
//        let contentInset = UIEdgeInsetsMake(-64, 0, 0, 0)
//        myTable.contentInset = contentInset
//        myTable.scrollIndicatorInsets = contentInset
//        myTable.tableHeaderView = trainerHeaderView
//        myTable.tableHeaderView?.layer.zPosition = -11
        
        myTable.register(UINib(nibName: "TrainerListCell", bundle: nil), forCellReuseIdentifier: "TrainerListCell")
    }
    
    
    fileprivate func setupDataSource() {
        var a = TrainerModel()
        
        a.name = "Senya Ars"
        a.address = "1180 First Street South"
        a.categories = "Barbell Squats, Stiff Leg Deadlifts, Wide Stance Leg Press"
        a.rating = 1.0
        a.reviews = "30"
        listTrainer.append(a)
        
        a = TrainerModel()
        a.name = "Henry"
        a.address = "550 Town, CA, NY"
        a.categories = "Pull-Up, Leg Curl: Lying or Seated, Chin-Up"
        a.rating = 3.0
        a.reviews = "15"
        listTrainer.append(a)
        
        a = TrainerModel()
        a.name = "Jonathan Lee"
        a.address = "120 Street"
        a.categories = "Squat, Calf Raise, Single Leg Squat, Hamstring Pull"
        a.rating = 5.0
        a.reviews = "20"
        listTrainer.append(a)
    }
    
    
    
    
    @objc fileprivate func didTapSearch() {
        
    }
    
    @objc fileprivate func didTapNotice() {
        let vc = Controllers.shared.noticeController()
        let nav = UINavigationController(rootViewController: vc)
        present(nav, animated: true, completion: nil)
    }

    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension TrainerController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listTrainer.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TrainerListCell", for: indexPath) as! TrainerListCell
        cell.selectionStyle = .none
        cell.configWithValue(value: listTrainer[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = Controllers.shared.trainerDetailController()
        vc.currentTrainer = listTrainer[indexPath.row]
        vc.presentSource = .HOME
        let nav = UINavigationController(rootViewController: vc)
        DispatchQueue.main.async {
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 112
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            
            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            animateCell(cell: cell)
            
        }
    }
}




















