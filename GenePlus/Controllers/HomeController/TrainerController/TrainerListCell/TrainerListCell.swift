//
//  TrainerListCell.swift
//  GenePlus
//
//  Created by pro on 1/8/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class TrainerListCell: UITableViewCell {

    
    @IBOutlet weak var avatarTrainer: UIImageView!
    @IBOutlet weak var nameTrainer: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var reviewLBL: UILabel!
    @IBOutlet weak var ratingCount: UILabel!
    
    @IBOutlet weak var iconCategories: UIImageView!
    
    @IBOutlet weak var categoriesLBL: UILabel!
    
    @IBOutlet weak var addressLBL: UILabel!
    
    
    
    func configWithValue(value: TrainerModel) {
        nameTrainer.text = value.name
        addressLBL.text = value.address
        categoriesLBL.text = value.categories
        
        reviewLBL.text = "(\(value.reviews) Reviews)"
        ratingCount.text = value.rating.description
        ratingView.rating = value.rating
        
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    
    
    fileprivate func setupUI() {
        avatarTrainer.layer.cornerRadius = avatarTrainer.frame.size.width / 2
        avatarTrainer.layer.masksToBounds = true
        avatarTrainer.layer.borderWidth = 1
        avatarTrainer.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.2).cgColor
        
        
        iconCategories.image = UIImage(named: "manLifting")?.withRenderingMode(.alwaysTemplate)
        iconCategories.tintColor = UIColor.lightGray
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
