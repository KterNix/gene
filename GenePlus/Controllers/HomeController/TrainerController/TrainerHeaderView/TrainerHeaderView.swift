//
//  TrainerHeaderView.swift
//  GenePlus
//
//  Created by pro on 1/12/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import GoogleMaps

class TrainerHeaderView: UIView {

    
    
    @IBOutlet weak var mapView: GMSMapView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        loadMap()
    }
    
    
    fileprivate func loadMap() {
        mapView.mapStyle(withFilename: "customMap", andType: "json")
    }
    
}

extension GMSMapView {
    func mapStyle(withFilename name: String, andType type: String) {
        do {
            if let styleURL = Bundle.main.url(forResource: name, withExtension: type) {
                self.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
    }
}
