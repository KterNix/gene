//
//  HomeController.swift
//  GenePlus
//
//  Created by pro on 1/8/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import CoreGraphics
import Hero
import PYSearch
import ObjectMapper




enum AssetsImage: String {
    case menFront = "menFrontBody_6"
    case menBack = "menBackBody_6"
    case womenFront = "womenFrontBody_6"
    case womenBackBody = "womenBackBody_6"
    
    func getArrayFromBodyType() -> ([String], [String]) {
        
        let left:[String]
        let right:[String]
        
        switch self {
        case .menFront, .womenFront:
            left = ["Chest", "Neck", "Shoulders", "Forearms", "Biceps"]
            right = ["Thighs", "Abs", "Calves", "", ""]
            return (left, right)
        case .menBack, .womenBackBody:
            left = ["Back", "Shoulders", "Triceps", "Abs", "Forearms"]
            right = ["Hamstrings", "Calves", "Glutes", "", ""]
            return (left, right)
        }
    }
    
    
}




class HomeController: UIViewController {

    
    
    
    @IBOutlet weak var mySegment: UISegmentedControl!
    
    @IBOutlet weak var rotateLeftBTN: UIButton!
    @IBOutlet weak var rotateRightBTN: UIButton!

    
    @IBOutlet weak var bodyImage: UIImageView!
    
    @IBOutlet weak var lStack: UIStackView!
    @IBOutlet weak var rStack: UIStackView!
    
    
    var bodyType:AssetsImage = .menFront
    var userType:UserType = .TRAINEE
    
    lazy var shapeLayerFillColor = UIColor.white.withAlphaComponent(alpha).cgColor
    let shapeLayerClearColor = UIColor.clear.cgColor
    
    lazy var arrowLImage = UIImage(named: "arrowL")?.withRenderingMode(.alwaysTemplate)
    lazy var arrowRImage = UIImage(named: "arrowR")?.withRenderingMode(.alwaysTemplate)

    
    lazy var profileButton:UIButton = {
        let b = UIButton(type: UIButton.ButtonType.custom)
        b.tintColor = UIColor.init(hexString: "6F7179")
        b.setImage(UIImage(named: "profileIcon")?.withRenderingMode(.alwaysTemplate), for: UIControl.State.normal)
        let sizeB:CGFloat = 30
        b.frame = CGRect(x: 0, y: 0, width: sizeB, height: sizeB)
        b.addTarget(self, action: #selector(didTapProfile), for: UIControl.Event.touchUpInside)
        return b
    }()
    
    lazy var searchButton:UIButton = {
        let b = UIButton(type: UIButton.ButtonType.custom)
        b.tintColor = UIColor.init(hexString: "6F7179")
        b.setImage(UIImage(named: "searchNAV")?.withRenderingMode(.alwaysTemplate), for: UIControl.State.normal)
        let sizeB:CGFloat = 30
        b.frame = CGRect(x: 0, y: 0, width: sizeB, height: sizeB)
        b.addTarget(self, action: #selector(didTapSearch), for: UIControl.Event.touchUpInside)
        return b
    }()
    
    lazy var noticeButton:UIButton = {
        let b = UIButton(type: UIButton.ButtonType.custom)
        b.tintColor = UIColor.init(hexString: "6F7179")
        b.setImage(UIImage(named: "bell")?.withRenderingMode(.alwaysTemplate), for: UIControl.State.normal)
        let sizeB:CGFloat = 30
        b.frame = CGRect(x: 0, y: 0, width: sizeB, height: sizeB)
        b.addTarget(self, action: #selector(didTapNotice), for: UIControl.Event.touchUpInside)
        return b
    }()
    
    
    
    var alpha:CGFloat = 1
    
    fileprivate var snapShotImage:UIImage?
    
    
    var listArea = [BodyModel]()
    var listMuscle = [MuscleModel]()
    
    var menFrontLeftTitle = ["Chest", "Neck", "Shoulders", "Forearms", "Biceps"]
    var menFrontRightTitle = ["Thighs", "Abs", "Calves", "", ""]
    var menBackLeftBody = ["Back", "Shoulders", "Triceps", "Forearms", "Abs"]
    var menBackRightBody = ["Hamstrings", "Glutes", "Calves", "", ""]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        hero.isEnabled = true
        changeImageBody(type: .menFront)
        bodyImage.hero.isEnabled = true
        bodyImage.hero.id = "layer"
        bodyImage.hero.modifiers = [.forceNonFade,
                                   .spring(stiffness: 300, damping: 25)]
        
        setupDataSource()
        
//        bodyView.addTarget(self, action: #selector(tap), for: UIControlEvents.touchUpInside)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        loadStackView()
    }
    
    
    fileprivate func setupStackView(bodyType: AssetsImage) {
        
        for i in 0..<lStack.subviews.count {
            if let a = lStack.subviews[i] as? UIButton {
                a.setTitle(bodyType.getArrayFromBodyType().0[i], for: UIControl.State.normal)
            }
        }
        
        for i in 0..<rStack.subviews.count {
            if let a = rStack.subviews[i] as? UIButton {
                a.setTitle(bodyType.getArrayFromBodyType().1[i], for: UIControl.State.normal)
            }
        }
    }
    
    
    @IBAction func buttonsHandle(_ sender: UIButton) {
        guard sender.titleLabel?.text != nil else { return }
        let vc = Controllers.shared.muscleListController()
        let nav = UINavigationController(rootViewController: vc)
        vc.muscleArea = sender.titleLabel?.text ?? ""
        vc.muscle = listMuscle.filter { $0.area == sender.titleLabel?.text ?? "" }
        DispatchQueue.main.async {
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    
    
    fileprivate func setupDataSource() {
//        var b = BodyModel()
//
//        b.area = "Chest"
//        b.listTraining = ["Barbell incline Bench Press","Bench Press","Butterfly","Cable Crossover","Decline Dumbbell"]
//        listArea.append(b)
//
//        b = BodyModel()
//        b.area = "Abdominals"
//        b.listTraining = ["Bent knee hip raise","Cross body crunch","Crunches","Decline Crunch","Leg Raise"]
//        listArea.append(b)
//
//        b = BodyModel()
//        b.area = "Shoulders"
//        b.listTraining = ["Armold dumbnell press","Barbell upright row","Bent over low-pulley side lateral","Dumbbell shoulder press","Front cable raise"]
//        listArea.append(b)
//
//        b = BodyModel()
//        b.area = "Legs"
//        b.listTraining = ["Barbell lunge","Barbell squat","Butt lift(Bridge)","Leg press"]
//        listArea.append(b)
//

        
        readFile()
    }
    
    
    fileprivate func readFile() {
        
        if let path = Bundle.main.path(forResource: "MuscleItems", ofType: "plist") {
            if let arr = NSArray(contentsOfFile: path) {
                guard let models = Mapper<MuscleModel>().mapArray(JSONObject: arr) else { return }

                listMuscle = models
            }
        }
    }
    
    
    
    @IBAction func rotateLeftAction(_ sender: Any) {
        rotateHandle()
    }
    
    @IBAction func rotateRightAction(_ sender: Any) {
        rotateHandle()
    }

    
    
    private func rotateHandle() {
        switch bodyType {
        case .menFront:
            changeImageBody(type: .menBack)
        case .menBack:
            changeImageBody(type: .menFront)
        case .womenFront:
            changeImageBody(type: .womenBackBody)
        case .womenBackBody:
            changeImageBody(type: .womenFront)
        }
    }
    
    
    
    @objc fileprivate func tap(sender: BodyView) {
        
    }
    

    override func viewDidAppear(_ animated: Bool) {
        defaultStatusbar()
    }
    
    fileprivate func setupUI() {
        applyAttributedNAV()
        clearNavigationColor()
        
        rotateLeftBTN.setImage(arrowLImage, for: UIControl.State.normal)
        rotateRightBTN.setImage(arrowRImage, for: UIControl.State.normal)
        rotateLeftBTN.tintColor = UIColor.init(hexString: "6F7179")
        rotateRightBTN.tintColor = UIColor.init(hexString: "6F7179")

        
        let profileBTN = UIBarButtonItem(customView: profileButton)
        navigationItem.leftBarButtonItem = profileBTN
        
        let searchBTN = UIBarButtonItem(customView: searchButton)
        let noticeBTN = UIBarButtonItem(customView: noticeButton)
        navigationItem.rightBarButtonItems = [searchBTN, noticeBTN]
        
        searchButton.hero.isEnabled = true
        searchButton.hero.id = "homeSearch"
        
    }

    
    fileprivate func loadStackView() {
        let colorsf = [UIColor.init(hexString: "E0E2D6"), UIColor.init(hexString: "E0E2D6").withAlphaComponent(0)]
        let edgeInset:CGFloat = 5
        
        for i in lStack.subviews {
            i.applyGradient(withColours: colorsf, gradientOrientation: GradientOrientation.horizontal)
        }
        
        for i in rStack.subviews {
            i.applyGradient(withColours: colorsf, gradientOrientation: GradientOrientation.reverseHorizontal)
        }
        
        for i in 0..<lStack.subviews.count {
            if let a = lStack.subviews[i] as? UIButton {
                a.titleLabel?.numberOfLines = 0
                a.titleLabel?.lineBreakMode = .byWordWrapping
                a.titleEdgeInsets = UIEdgeInsets(top: 0, left: edgeInset, bottom: 0, right: 0)
                a.setTitle(menFrontLeftTitle[i], for: UIControl.State.normal)
            }
        }
        
        for i in 0..<rStack.subviews.count {
            if let a = rStack.subviews[i] as? UIButton {
                a.titleLabel?.numberOfLines = 0
                a.titleLabel?.lineBreakMode = .byWordWrapping
                a.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: edgeInset)
                a.setTitle(menFrontRightTitle[i], for: UIControl.State.normal)
                
            }
        }
    }

    
    @IBAction func segmentAction(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        case 0:
            changeImageBody(type: .menFront)
        default:
            changeImageBody(type: .womenFront)
        }
    }
    
    @objc fileprivate func didTapProfile() {
        let vc = Controllers.shared.profileController()
        let nav = UINavigationController(rootViewController: vc)
        vc.userType = userType
        DispatchQueue.main.async {
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    @objc fileprivate func didTapNotice() {
        let vc = Controllers.shared.noticeController()
        let nav = UINavigationController(rootViewController: vc)
        
        DispatchQueue.main.async {
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    @objc fileprivate func didTapSearch() {
        presentSearchController()
    }
    
    fileprivate func presentSearchController() {
        let hotSeaches:[String] = ["Chest", "Shoulders", "Forearms"]
        let searchViewController = PYSearchViewController.init(hotSearches: hotSeaches, searchBarPlaceholder: "Search", didSearch: { searchViewController, searchBar,searchText in
            let resultViewController = Controllers.shared.searchController()
            resultViewController.searchKey = searchText ?? ""
            searchViewController?.navigationController?.pushViewController(resultViewController, animated: true)
        })
        searchViewController?.delegate = self
        searchViewController?.hotSearchStyle = PYHotSearchStyle.colorfulTag
        
        
        searchViewController?.searchBar.hero.isEnabled = true
        searchViewController?.searchBar.hero.id = "homeSearch"
        //searchViewController?.searchSuggestionHidden = true
        let nvc = UINavigationController.init(rootViewController: searchViewController!)
        nvc.hero.isEnabled = true
        self.present(nvc, animated: true, completion: nil)
    }
    
    fileprivate func changeImageBody(type: AssetsImage) {
        switch type {
        case .menFront:
            bodyType = .menFront
            bodyImage.image = UIImage(named: DeviceScreen.shared.menFrontBodyImageFromSizeScreen().rawValue)
//            bodyImage.layer.sublayers?.removeAll()
//            DispatchQueue.main.async {
//                ShapeLayer.shared.drawMenFrontPathFor(image: self.bodyImage,color: self.shapeLayerClearColor)
//            }
        case .menBack:
            bodyType = .menBack
            bodyImage.image = UIImage(named: DeviceScreen.shared.menBackBodyImageFromSizeScreen().rawValue)
//            bodyImage.layer.sublayers?.removeAll()
//            DispatchQueue.main.async {
//                ShapeLayer.shared.drawMenBackPathFor(image: self.bodyImage, color: self.shapeLayerClearColor)
//            }
        case .womenFront:
            bodyType = .womenFront
            bodyImage.image = UIImage(named: DeviceScreen.shared.womenFrontBodyImageFromSizeScreen().rawValue)
//            bodyImage.layer.sublayers?.removeAll()
//            DispatchQueue.main.async {
//                ShapeLayer.shared.drawWomenFrontPathFor(image: self.bodyImage, color: self.shapeLayerClearColor)
//            }
        case .womenBackBody:
            bodyType = .womenBackBody
            bodyImage.image = UIImage(named: DeviceScreen.shared.womenBackBodyImageFromSizeScreen().rawValue)
//            bodyImage.layer.sublayers?.removeAll()
//            DispatchQueue.main.async {
//                ShapeLayer.shared.drawWomenBackPathFor(image: self.bodyImage, color: self.shapeLayerClearColor)
//            }
        }
        setupStackView(bodyType: type)

    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension HomeController {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            // get tapped position
            // loop all sublayers
            let point = touch.location(in: bodyImage)
            
            guard let sublayers = bodyImage.layer.sublayers as? [CAShapeLayer] else { return }
            sublayers.forEach { (layer) in
                if let path = layer.path {
                    if path.contains(point) {
                        layer.fillColor = shapeLayerFillColor
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                            self.snapShotImage = self.bodyImage.createImage(frame: self.bodyImage.frame)
                            //MARK: - PRESENT VIEWCONTROLLER
//                            vc.areaDetail = self.filter(content: layer.contents)
                        })
                        
                    }else{
                        layer.fillColor = shapeLayerClearColor
                        print("OUT")
                    }
                }
            }
//            for layer in sublayers {
//                if let path = layer.path {
//                    if path.contains(point) {
//                        layer.fillColor = shapeLayerFillColor
//                        let vc = Controllers.shared.homeDetailController()
//                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
//                            self.snapShotImage = self.bodyImage.createImage(frame: self.bodyImage.frame)
//                            vc.image = self.snapShotImage!
//                            vc.isHeroEnabled = true
//                            vc.areaDetail = self.filter(content: layer.contents)
//                            self.present(vc, animated: true, completion: nil)
//                        })
//
//                    }else{
//                        layer.fillColor = shapeLayerClearColor
//                        print("OUT")
//                    }
//                }
//            }
        }
    }
    
    
    fileprivate func filter(content: Any?) -> BodyModel {
        if let contents = content as? String {
            switch contents {
                case ContentKeyForLayer.legs.rawValue :
                    
                    return filterArea(content: contents)
                
                case ContentKeyForLayer.legs.rawValue :
                    
                    return filterArea(content: contents)
                
                case ContentKeyForLayer.shoulders.rawValue :
                    
                    return filterArea(content: contents)
                
                case ContentKeyForLayer.shoulders.rawValue :
                    
                    return filterArea(content: contents)
                
                case ContentKeyForLayer.abdominals.rawValue :
                    
                    return filterArea(content: contents)
                
                case ContentKeyForLayer.chest.rawValue :
                    
                    return filterArea(content: contents)
                
            default:
                break
            }
        }
        return BodyModel()
    }
    
    
    
    fileprivate func filterArea(content: String) -> BodyModel {
        for i in listArea {
            if i.area.contains(content) {
                return i
            }
        }
        return BodyModel()
    }
}

extension HomeController: PYSearchViewControllerDelegate {
    
    
    public func searchViewController(_ searchViewController: PYSearchViewController!, searchTextDidChange searchBar: UISearchBar!, searchText: String!) {
        
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
        //            var searchSuggestionsM = [String]()
        //
        //            for i in (0..<arc4random_uniform(5) + 10).reversed() {
        //
        //                let searchSuggestion:String = String(format: "keyword %@", i)
        //
        //                searchSuggestionsM.append(searchSuggestion)
        //            }
        //            searchViewController.searchSuggestions = searchSuggestionsM
        //        }
    }
    
}


extension UIView {
    
    func createImage(frame: CGRect) -> UIImage? {
        
        UIGraphicsBeginImageContext(self.frame.size)
//        UIGraphicsBeginImageContextWithOptions(self.frame.size, false, 0)
        let context = UIGraphicsGetCurrentContext()
        self.layer.render(in: context!)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img
        
    }
}





