//
//  ConfirmController.swift
//  GenePlus
//
//  Created by pro on 1/8/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class ConfirmController: BaseViewController {

    enum Cells: Int, CaseCountable {
        case TRAINER = 0
        case TRAINING
        case PACKAGE
        case PAYMENT
        case NOTE
    }
    
    
    @IBOutlet weak var myTable: UITableView!
    
    
    var shownIndexes : [IndexPath] = []
    lazy var footerButton:ButtonView? = UIView.instanceFromNib()
    var navigationView = UIView()

    var paymentModel:PaymentModel?
    var allotments:[AllotmentModel]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        defaultStatusbar()
    }
    
    
    override func setupUI() {
        title = "Confirm"
        setBackButton()
        applyAttributedNAV()
        let statusbarHeight : CGFloat = UIApplication.shared.statusBarFrame.size.height
        let navibarHeight : CGFloat = 44
        navigationView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: navibarHeight + statusbarHeight)
        navigationView.backgroundColor = UIColor.white
        navigationView.alpha = 0.0
        view.addSubview(navigationView)
        
        paymentModel = PaymentModel()
        paymentModel?.title = "dat.nguyen@bantayso.com"
        paymentModel?.image = UIImage(named: "paypal")!
        
    }
    
    override func configTable() {
        myTable.register(nibWithCellClass: ConfirmCell.self)
        myTable.register(nibWithCellClass: TrainingCell.self)
        myTable.register(nibWithCellClass: ConfirmPackageCell.self)
        myTable.register(nibWithCellClass: ConfirmPaymentCell.self)
        myTable.register(nibWithCellClass: NoteCell.self)

    }
    
    deinit {
        footerButton = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ConfirmController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Cells.caseCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == Cells.TRAINER.rawValue {
            let cell = tableView.dequeueReusableCell(withClass: ConfirmCell.self)
            return cell
        }else if indexPath.row == Cells.TRAINING.rawValue {
            let cell = tableView.dequeueReusableCell(withClass: TrainingCell.self)
            return cell
        }else if indexPath.row == Cells.PACKAGE.rawValue {
            let cell = tableView.dequeueReusableCell(withClass: ConfirmPackageCell.self)
            if allotments != nil {
                cell.allotments = allotments!
            }
            return cell
        }else if indexPath.row == Cells.PAYMENT.rawValue {
            let cell = tableView.dequeueReusableCell(withClass: ConfirmPaymentCell.self)
            
            cell.titleLBL.text = paymentModel?.title
            cell.iconIMG.image = paymentModel?.image
            return cell
        }else if indexPath.row == Cells.NOTE.rawValue {
            let cell = tableView.dequeueReusableCell(withClass: NoteCell.self)
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == Cells.TRAINER.rawValue {
            return 129
        }else if indexPath.row == Cells.TRAINING.rawValue {
            return 130
        }else if indexPath.row == Cells.PACKAGE.rawValue {
            return 190
        }else if indexPath.row == Cells.PAYMENT.rawValue {
            return 110
        }else if indexPath.row == Cells.NOTE.rawValue {
            return 175
        }
        
        return 50
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == Cells.PAYMENT.rawValue {
            let vc = Controllers.shared.paymentController()
            vc.presentFrom = .continueBooking
            vc.delegate = self
            let nav = UINavigationController(rootViewController: vc)
            DispatchQueue.main.async {
                self.present(nav, animated: true, completion: nil)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        footerButton?.chooseButton.setTitle("CONFIRM", for: UIControl.State.normal)
        footerButton?.chooseButton.addTarget(self, action: #selector(didTapConfirm), for: UIControl.Event.touchUpInside)
        return footerButton
    }
    
    @objc fileprivate func didTapConfirm() {
        let vc = Controllers.shared.completeBookingController()
        let nav = UINavigationController(rootViewController: vc)
        present(nav, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            
            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            animateCell(cell: cell)
        }
    }
}

extension ConfirmController: PaymentControllerDelegate {
    func didSelectPayment(payment: PaymentModel, sender: PaymentController) {
        self.paymentModel = payment
        
        DispatchQueue.main.async {
            self.myTable.reloadData()
        }
    }
}




























