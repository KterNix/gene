//
//  NoteCell.swift
//  GenePlus
//
//  Created by pro on 8/17/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class NoteCell: UITableViewCell {

    
    
    @IBOutlet weak var titleLBL: UILabel!
    @IBOutlet weak var contentTXT: SAMTextView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        // Initialization code
        contentTXT.placeholder = "Type something..."
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
