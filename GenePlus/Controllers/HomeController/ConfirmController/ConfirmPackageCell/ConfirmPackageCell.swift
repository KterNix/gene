//
//  ConfirmPackageCell.swift
//  GenePlus
//
//  Created by pro on 1/12/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class ConfirmPackageCell: UITableViewCell {

    
    @IBOutlet weak var myColl: UICollectionView!
    
    @IBOutlet weak var packTitle: UILabel!
    
    var allotments = [AllotmentModel]() {
        didSet {
            configColl()
        }
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        // Initialization code
    }
    
    fileprivate func configColl() {
        
        myColl.register(UINib(nibName: "AllotmentCell", bundle: nil), forCellWithReuseIdentifier: "AllotmentCell")
        myColl.delegate = self
        myColl.dataSource = self
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension ConfirmPackageCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allotments.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AllotmentCell", for: indexPath) as! AllotmentCell
        let item = allotments[indexPath.row]
        cell.configWith(value: item)
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let sizeWidth:CGFloat = 175
        let sizeHeight:CGFloat = 100
        
        return CGSize(width: sizeWidth, height: sizeHeight)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let edgeInset:CGFloat = 10
        return UIEdgeInsets(top: edgeInset, left: edgeInset, bottom: edgeInset, right: edgeInset)
    }
    
    
    
}








