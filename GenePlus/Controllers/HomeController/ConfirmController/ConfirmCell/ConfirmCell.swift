//
//  ConfirmCell.swift
//  GenePlus
//
//  Created by pro on 1/8/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class ConfirmCell: UITableViewCell {

    
    @IBOutlet weak var avatarTrainer: UIImageView!
    
    @IBOutlet weak var nameTrainer: UILabel!
    
    @IBOutlet weak var ratingView: CosmosView!
    
    @IBOutlet weak var ratingCount: UILabel!
    
    @IBOutlet weak var reviewsLBL: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
