//
//  ConfirmPaymentCell.swift
//  GenePlus
//
//  Created by pro on 1/13/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class ConfirmPaymentCell: UITableViewCell {

    
    @IBOutlet weak var titleLBL: UILabel!
    @IBOutlet weak var iconIMG: UIImageView!
    
    @IBOutlet weak var iconRight: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
