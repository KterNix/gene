//
//  AllotmentCell.swift
//  GenePlus
//
//  Created by pro on 8/20/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class AllotmentCell: UICollectionViewCell {

    
    
    @IBOutlet weak var startLBL: UILabel!
    @IBOutlet weak var endLBL: UILabel!
    @IBOutlet weak var dayLBL: UILabel!
    @IBOutlet weak var monthLBL: UILabel!
    @IBOutlet weak var yearLBL: UILabel!
    
    
    
    func configWith(value: AllotmentModel) {
        startLBL.text = value.startTime
        endLBL.text = value.endTime
        
        
        let date = Utils.stringToDateFM(value.date ?? "", stringFormat: .YMD)
        let day = date.day()?.description
        let month = date.month()
        let year = date.year()?.description
        
        
        dayLBL.text = day
        monthLBL.text = month
        yearLBL.text = year
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        setupUI()
    }
    
    
    fileprivate func setupUI() {
        cornerRadius = 5
        borderWidth = 1
        borderColor = UIColor.lightGray.withAlphaComponent(0.1)
    }

}
