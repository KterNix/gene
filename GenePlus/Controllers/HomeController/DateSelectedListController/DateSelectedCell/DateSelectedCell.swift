//
//  DateSelectedCell.swift
//  GenePlus
//
//  Created by pro on 8/17/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class DateSelectedCell: UITableViewCell, ConfigurableCell {

    @IBOutlet weak var startLBL: UILabel!
    @IBOutlet weak var endLBL: UILabel!
    @IBOutlet weak var dateLBL: UILabel!
    
    @IBOutlet weak var checkIMG: UIImageView!
    
    
    @IBOutlet weak var quantityLBL: UILabel!
    
    
    
    func configure(_ item: AllotmentModel, at indexPath: IndexPath) {
        startLBL.text = item.startTime
        endLBL.text = item.endTime
        dateLBL.text = item.date
        
        quantityLBL.text = (indexPath.row + 1).description
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
