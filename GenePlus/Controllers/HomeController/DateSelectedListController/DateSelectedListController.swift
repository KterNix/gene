//
//  DateSelectedListController.swift
//  GenePlus
//
//  Created by pro on 8/17/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

protocol DateSelectedListControllerDelegate: class {
    func didChangeDate(dates: [AllotmentModel], sender: DateSelectedListController)
}


class DateSelectedListController: BaseViewController {

    
    @IBOutlet weak var myTable: UITableView!
    
    
    
    weak var delegate:DateSelectedListControllerDelegate?
    
    var datascr:DateSelectedDataSource?
    var dateSelected:[AllotmentModel]?
    var dateArr:[AllotmentModel]?
    
    var shownIndexes : [IndexPath] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        defaultStatusbar()
    }
    
    override func setupUI() {
        title = "Allotments"
        setBackButton()
    }
    
    override func configTable() {
        myTable.register(nibWithCellClass: DateSelectedCell.self)
        myTable.delegate = self
        myTable.dataSource = self
        
        guard dateSelected != nil else { return }
        dateArr = dateSelected
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

extension DateSelectedListController {
    
    fileprivate func datasource() -> DateSelectedDataSource? {
        guard dateSelected != nil else { return nil }
        let ds = DateSelectedDataSource(tableView: myTable, array: dateSelected!)
        
        return ds
    }
    
    
}

extension DateSelectedListController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = dateSelected != nil ? dateSelected!.count : 0
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: DateSelectedCell.self, for: indexPath)
        if dateSelected != nil {
            let item = dateSelected![indexPath.row]
            cell.configure(item, at: indexPath)
            
        }
        
        if dateArr != nil && dateSelected != nil {
            let item = dateSelected![indexPath.row]
            let isSelectedAllotment = dateArr!.contains(item)
            
            let checkIMG = isSelectedAllotment ? UIImage(named: CheckAssets.CHECKED.rawValue) : UIImage(named: CheckAssets.UN_CHECK.rawValue)
            cell.checkIMG.image = checkIMG
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            animateCell(cell: cell)
        }
        
        guard let cells = cell as? DateSelectedCell else { return }
        guard dateSelected != nil else { return }
        guard dateArr != nil else { return }
        
        let item = dateSelected![indexPath.row]
        let isSelectedAllotment = dateArr!.contains(item)
        let checkIMG = isSelectedAllotment ? UIImage(named: CheckAssets.CHECKED.rawValue) : UIImage(named: CheckAssets.UN_CHECK.rawValue)
        cells.checkIMG.image = checkIMG
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        guard let cell = tableView.cellForRow(at: indexPath) as? DateSelectedCell else { return }
        guard dateSelected != nil else { return }
        guard dateArr != nil else { return }
        let item = dateSelected![indexPath.row]
        let isSelectedAllotment = dateArr!.contains(item)
        
        
        if isSelectedAllotment == false {

            dateArr!.append(item)
            cell.checkIMG.image = UIImage(named: CheckAssets.CHECKED.rawValue)

        }else{
            if let allotmentIndex = dateArr!.index(of: item) {
                dateArr!.remove(at: allotmentIndex)
                cell.checkIMG.image = UIImage(named: CheckAssets.UN_CHECK.rawValue)
            }
        }
        
        delegate?.didChangeDate(dates: dateArr!, sender: self)
    }
}
