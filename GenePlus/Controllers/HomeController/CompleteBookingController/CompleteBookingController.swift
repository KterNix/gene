//
//  CompleteBookingController.swift
//  GenePlus
//
//  Created by pro on 1/13/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class CompleteBookingController: UIViewController {

    
    
    
    @IBOutlet weak var imageComplete: UIImageView!
    
    
    @IBOutlet weak var bookingIDLBL: UILabel!
    
    @IBOutlet weak var manageBookingBTN: UIButton!
    
    
    lazy var appDelegate = UIApplication.shared.delegate as! AppDelegate

    
    
    
    
//    lazy var backImage = UIImage(named: "close")?.withRenderingMode(.alwaysTemplate)
//    lazy var backBTN:UIButton = {
//        let b = UIButton(type: UIButtonType.custom)
//        b.setImage(backImage, for: UIControlState.normal)
//        let edgeInset:CGFloat = 12
//        b.imageEdgeInsets = UIEdgeInsets(top: edgeInset, left: edgeInset, bottom: edgeInset, right: edgeInset)
//        let sizeB:CGFloat = 30
//        b.frame = CGRect(x: 0, y: 0, width: sizeB, height: sizeB)
//        b.addTarget(self, action: #selector(dismissController), for: UIControlEvents.touchUpInside)
//        return b
//    }()
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    fileprivate func setupUI() {
        applyAttributedNAV()

        clearNavigationColor()
        
        
        
        manageBookingBTN.layer.cornerRadius = Constant.buttonCornerRadius
        manageBookingBTN.layer.masksToBounds = true
        

    }
    
    @objc fileprivate func dismissController() {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func manageBookingAction(_ sender: Any) {
        
        let profile = Controllers.shared.profileController()
        let history = Controllers.shared.historyController()
        let nav = UINavigationController(rootViewController: profile)
        view.window?.rootViewController?.dismiss(animated: false, completion: {
            self.appDelegate.window?.rootViewController?.present(nav, animated: false, completion: {
                profile.navigationController?.pushViewController(history, animated: false)
            })
        })
    }
    
    
    @IBAction func backToHomeAction(_ sender: Any) {
        view.window?.rootViewController?.dismiss(animated: false, completion: nil)
    }
    
    
    
    
    
    
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}













