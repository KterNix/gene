//
//  ImageFromLibraryController.swift
//  GenePlus
//
//  Created by pro on 7/16/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import Photos

protocol ImageFromLibraryControllerDelegate: class {
    func didSelectedImages(images: [PHAsset], sender: ImageFromLibraryController)
}

class ImageFromLibraryController: UIViewController {

    
    @IBOutlet weak var myColl: UICollectionView!
    
    weak var delegate:ImageFromLibraryControllerDelegate?
    
    var images = [PHAsset]()
    var imagesSelected = [PHAsset]()

    
    lazy var doneButton: UIButton = {
        let b = UIButton(type: .custom)
        let bSize = CGSize(width: 60, height: 30)
        b.frame = CGRect(x: 0, y: 0, width: bSize.width, height: bSize.height)
        b.setTitle("DONE", for: UIControl.State.normal)
        b.setTitleColor(UIColor.mainColor(), for: UIControl.State.normal)
        b.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
        b.titleLabel?.textAlignment = .right
        b.addTarget(self, action: #selector(didTapDone), for: UIControl.Event.touchUpInside)
        return b
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        configColl()
        
        getImages()

        let bDone = UIBarButtonItem(customView: doneButton)
        navigationItem.rightBarButtonItems = [bDone]
        
        // Do any additional setup after loading the view.
    }
    
    fileprivate func setupUI() {
        setBackButton()
        applyAttributedNAV()
        whiteNavigationColor()
    }
    
    fileprivate func configColl() {
        myColl.register(UINib(nibName: "ImageLibraryCell", bundle: nil), forCellWithReuseIdentifier: "ImageLibraryCell")
        myColl.delegate = self
        myColl.dataSource = self
        myColl.allowsMultipleSelection = true
    }
    
    func getImages() {
        let assets = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: nil)
        assets.enumerateObjects({ (object, count, stop) in
            // self.cameraAssets.add(object)
            self.images.append(object)
        })
        
        //In order to get latest image first, we just reverse the array
        self.images.reverse()
        
        // To show photos, I have taken a UICollectionView
        self.myColl.reloadData()
    }
    

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        defaultStatusbar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        lightStatusbar()
    }
    
    @objc fileprivate func didTapDone() {
        delegate?.didSelectedImages(images: imagesSelected, sender: self)
        dismiss(animated: true, completion: nil)
    }
    
//    func getAssetThumbnail(asset: PHAsset, size: CGFloat) -> UIImage {
//        let retinaScale = UIScreen.main.scale
//        let retinaSquare = CGSize(width: size * retinaScale, height: size * retinaScale)//CGSizeMake(size * retinaScale, size * retinaScale)
//        let cropSizeLength = min(asset.pixelWidth, asset.pixelHeight)
//        let square = CGRect(x: 0, y: 0, width: cropSizeLength, height: cropSizeLength)//CGRectMake(0, 0, CGFloat(cropSizeLength), CGFloat(cropSizeLength))
//        let cropRect = square.applying(CGAffineTransform(scaleX: 1.0/CGFloat(asset.pixelWidth), y: 1.0/CGFloat(asset.pixelHeight)))
////
//        let manager = PHImageManager.default()
//        let options = PHImageRequestOptions()
//        var thumbnail = UIImage()
////
//        options.isSynchronous = true
//        options.deliveryMode = .fastFormat
//        options.resizeMode = .exact
//        options.normalizedCropRect = cropRect
//        manager.requestImage(for: asset, targetSize: retinaSquare, contentMode: .aspectFit, options: options, resultHandler: {(result, info)->Void in
//            thumbnail = result ?? UIImage()
//        })
//        return thumbnail
//    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}

extension ImageFromLibraryController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    // MARK: UICollectionViewDataSource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return images.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageLibraryCell", for: indexPath as IndexPath) as! ImageLibraryCell
        cell.viewAlpha.isHidden = cell.isSelected ? false : true
        let asset = images[indexPath.row]
        let manager = PHImageManager.default()
        if cell.tag != 0 {
            manager.cancelImageRequest(PHImageRequestID(cell.tag))
        }
        cell.tag = Int(manager.requestImage(for: asset,
                                            targetSize: CGSize(width: 120.0, height: 120.0),
                                            contentMode: .aspectFill,
                                            options: nil) { (result, _) in
                                            cell.imageBackground.image = result
        })
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = myColl.cellForItem(at: indexPath) as? ImageLibraryCell else { return }
        cell.viewAlpha.isHidden = cell.isSelected ? false : true
        let item = images[indexPath.row]
        imagesSelected.append(item)
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        guard let cell = myColl.cellForItem(at: indexPath) as? ImageLibraryCell else { return }
        cell.viewAlpha.isHidden = cell.isSelected ? false : true
        let item = images[indexPath.row]
        if let itemIndex = imagesSelected.index(of: item) {
            imagesSelected.remove(at: itemIndex)
        }
    }
    
    
    // MARK: - UICollectionViewDelegateFlowLayout methods
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.view.frame.width * 0.32
        let height = self.view.frame.height * 0.179910045
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2.5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    // UIImagePickerControllerDelegate Methods
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        picker.dismiss(animated: true, completion: nil)
    }
}


