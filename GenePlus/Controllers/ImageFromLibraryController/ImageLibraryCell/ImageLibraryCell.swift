//
//  ImageLibraryCell.swift
//  GenePlus
//
//  Created by pro on 7/16/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class ImageLibraryCell: UICollectionViewCell {

    
    @IBOutlet weak var viewAlpha: UIView!
    
    @IBOutlet weak var imageBackground: UIImageView!
    
    @IBOutlet weak var checkImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupUI()
    }
    
    private func setupUI() {
        viewAlpha.backgroundColor = UIColor.black.withAlphaComponent(0)
        viewAlpha.borderWidth = 4
        viewAlpha.borderColor = UIColor.mainColor()
    }

}
