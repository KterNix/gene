//
//  ManageAllotmentCell.swift
//  GenePlus
//
//  Created by pro on 9/24/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import FSCalendar

class ManageAllotmentCell: UITableViewCell {

    @IBOutlet weak var calendarView: FSCalendar!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
