//
//  ManageAllotments.swift
//  GenePlus
//
//  Created by pro on 9/20/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class ManageAllotments: BaseViewController {

    
    @IBOutlet weak var myTable: BaseTableView!
    
    
    var shownIndexes : [IndexPath] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func setupUI() {
        title = "Manage Allotments"
        setBackButton()
    }
    
    override func configTable() {
        myTable.register(nibWithCellClass: ManageAllotmentCell.self)
        myTable.delegate = self
        myTable.dataSource = self
    }

}

extension ManageAllotments: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: ManageAllotmentCell.self)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
}
