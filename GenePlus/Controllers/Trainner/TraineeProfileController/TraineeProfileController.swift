//
//  TraineeProfileController.swift
//  GenePlus
//
//  Created by pro on 7/12/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import GSKStretchyHeaderView

class TraineeProfileController: BaseViewController {
    
    
    @IBOutlet weak var myTable: UITableView!
    

    var stretchyHeaderView:GSKStretchyHeaderView?
    lazy var header:TraineeHeader? = UIView.instanceFromNib()
    lazy var footerButton:FooterButton? = UIView.instanceFromNib()

    var traineeInfo = ["BODY WEIGHT", "BODY HEIGHT", "STRENGTH", "WEAKNESSES", "BODY FAT", "FLEXIBILITY", "INJURES", "HEALTH ISSUES"]
    var traineeDetail = ["80 KG", "170 CM", "Strong", "Sluggish", "Fat", "Yes", "No", "Fast heartbeat"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        lightStatusbar()
        updateAttributedNAV(color: UIColor.white)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        defaultStatusbar()
    }
    
    override func setupUI() {
        setBackButton()
        clearNavigationColor()
        applyAttributedNAV()
    }
    
    override func configTable() {
        let headerHeight:CGFloat = 230
        header?.frame.size.height = headerHeight
        header?.frame.size.width = myTable.frame.size.width
        
        stretchyHeaderView = GSKStretchyHeaderView(frame: CGRect(x: 0, y: 0, width: myTable.frame.size.width, height: headerHeight))
        stretchyHeaderView?.expansionMode = .topOnly
        stretchyHeaderView?.contentShrinks = true
        stretchyHeaderView?.contentExpands = true
        stretchyHeaderView?.minimumContentHeight = 64
        stretchyHeaderView?.maximumContentHeight = headerHeight
        stretchyHeaderView?.addSubview(header!)
        myTable.addSubview(stretchyHeaderView!)
        
        stretchyHeaderView?.stretchDelegate = self
        
        myTable.delegate = self
        myTable.dataSource = self
        myTable.register(nibWithCellClass: ContentCell.self)
        myTable.register(nibWithCellClass: DetailCell.self)
        
                
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        guard scrollView.contentOffset.y < 0 else { return }
        var scale = 1 + fabsf(Float(scrollView.contentOffset.y)) / Float(scrollView.frame.size.height / 5)
        scale = max(0, scale)
        header?.gradientView.transform = CGAffineTransform.identity.scaledBy(x: CGFloat(scale), y: CGFloat(scale))
        
    }
    
    
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        //MARK: - Set Height for tableHeaderView
//        if let headerView = myTable.tableHeaderView {
//
//            let height = headerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height
//            var headerFrame = headerView.frame
//
//            //Comparison necessary to avoid infinite loop
//            if height != headerFrame.size.height {
//                headerFrame.size.height = height
//                headerView.frame = headerFrame
//                myTable.tableHeaderView = headerView
//            }
//        }
//    }
    
    
    
    deinit {
        stretchyHeaderView = nil
        header = nil
        footerButton = nil
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension TraineeProfileController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return traineeInfo.count + 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == traineeInfo.count {
            let cell = tableView.dequeueReusableCell(withClass: DetailCell.self)
            cell.headerTitle.text = "NOTE"
            cell.titleLBL.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)
            cell.titleLBL.text = "Create a profile for the USER that only TRAINERS can see. Body weight, strength, weaknesses, body fat, flexibility, injures, health issues, notes, and TRAINER EXPERIENCE"
            return cell
        }else if indexPath.row == traineeInfo.count + 1 {
            
            let cell = tableView.dequeueReusableCell(withClass: DetailCell.self)
            cell.headerTitle.text = "TRAINER EXPERIENCE"
            cell.titleLBL.text = "GOOD"
            return cell
        }
        
        
        let cell = tableView.dequeueReusableCell(withClass: ContentCell.self)
        let item = traineeInfo[indexPath.row]
        let detail = traineeDetail[indexPath.row]
        
        cell.titleLBL.text = item
        cell.detailLBL.text = detail
        
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        footerButton?.myBTN.setTitle("MODIFY", for: UIControl.State.normal)
        footerButton?.myBTN.addTarget(self, action: #selector(didTapEdit), for: UIControl.Event.touchUpInside)
        return footerButton
    }
    
    @objc fileprivate func didTapEdit() {
        let vc = Controllers.shared.modifyTraineeProfile()
        navigationController?.pushViewController(vc)
    }
    
}



extension TraineeProfileController: GSKStretchyHeaderViewStretchDelegate {
    func stretchyHeaderView(_ headerView: GSKStretchyHeaderView, didChangeStretchFactor stretchFactor: CGFloat) {
        guard stretchFactor >= 0.4, stretchFactor <= 1.0 else { return }
        
        header?.profilePic.transform = CGAffineTransform.init(scaleX: stretchFactor, y: stretchFactor)
        
    }
}
