//
//  ContentCell.swift
//  GenePlus
//
//  Created by pro on 7/12/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class ContentCell: UITableViewCell {

    
    @IBOutlet weak var titleLBL: UILabel!
    @IBOutlet weak var detailLBL: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
