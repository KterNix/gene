//
//  ModifyTraineeProfile.swift
//  GenePlus
//
//  Created by pro on 7/12/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import Eureka

class ModifyTraineeProfile: FormViewController {

    
    var shownIndexes : [IndexPath] = []
    lazy var headerTitle:TraineeTitleHeader? = UIView.instanceFromNib()
    lazy var footerButton:FooterButton? = UIView.instanceFromNib()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        loadForm()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        whiteNavigationColor()
        updateAttributedNAV(color: UIColor.mainColor())
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        clearNavigationColor()
    }
    
    
    fileprivate func setupUI() {
        title = "Modify"
        setBackButton()
        
        
        tableView.backgroundColor = UIColor.white
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        
        
        TextRow.defaultCellUpdate = { cell, row in
            cell.textLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
            cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
            cell.textField.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
            cell.textLabel?.textColor = UIColor.darkGray
            cell.tintColor = UIColor.darkGray
            
        }
        
        PushRow<String>.defaultCellUpdate = { cell, row in
            cell.selectionStyle = .none
            cell.textLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
            cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
            cell.textLabel?.textColor = UIColor.darkGray
            cell.tintColor = UIColor.darkGray
            
        }
        
        TextAreaRow.defaultCellUpdate = { cell, row in
            cell.selectionStyle = .none
            cell.textLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
            cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
            cell.textView.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.light)
            cell.textLabel?.textColor = UIColor.darkGray
            cell.tintColor = UIColor.darkGray
            
        }
        
        
    }
    
    fileprivate func loadForm() {
        form +++
        Section()
            <<< TextRow() {
                $0.title = "BODY WEIGHT"
                $0.placeholder = "80 KG"
        }
        
            <<< TextRow() {
                $0.title = "BODY HEIGHT"
                $0.placeholder = "170 CM"
        }
        
            <<< TextRow() {
                $0.title = "STRENGTH"
                $0.placeholder = "Strong"

        }
        
            <<< TextRow() {
                $0.title = "WEAKNESSES"
                $0.placeholder = "Sluggish"
        }
        
            <<< TextRow() {
                $0.title = "BODY FAT"
                $0.placeholder = "Fat"
        }
        
            <<< TextRow() {
                $0.title = "FLEXIBILITY"
                $0.placeholder = "Yes"
        }
        
            <<< TextRow() {
                $0.title = "INJURES"
                $0.placeholder = "No"
        }
        
            <<< TextRow() {
                $0.title = "HEALTH ISSUES"
                $0.placeholder = "Fast heartbeat"
        }
            <<< PushRow<String>() {
                $0.title = "TRAINER EXPERIENCE"
                $0.options = ["GOOD", "BAD"]
                $0.value = "GOOD"
            }
        
        +++ Section(header: "NOTE", footer: "")
            <<< TextAreaRow() {
                $0.value = "Create a profile for the USER that only TRAINERS can see. Body weight, strength, weaknesses, body fat, flexibility, injures, health issues, notes, and TRAINER EXPERIENCE"
        }
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 {
            return 150
        }
        return 60
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)

            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)

            animateCell(cell: cell)
        }
    }
    
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 0
        }
        return 100
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            return nil
        }
        return headerTitle
    }
    
    
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }
        return 70
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 0 {
            return nil
        }
        footerButton?.myBTN.setTitle("SUBMIT", for: UIControl.State.normal)
        footerButton?.myBTN.addTarget(self, action: #selector(didTapSubmit), for: UIControl.Event.touchUpInside)
        return footerButton
    }
    
    @objc fileprivate func didTapSubmit() {
        
    }
    
    
}
