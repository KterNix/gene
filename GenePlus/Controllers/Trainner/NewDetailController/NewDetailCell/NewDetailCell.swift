//
//  NewDetailCell.swift
//  GenePlus
//
//  Created by pro on 8/9/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class NewDetailCell: UITableViewCell, ConfigurableCell {

    @IBOutlet weak var titleLBL: UILabel!
    
    @IBOutlet weak var detailLBL: UILabel!
    
    
    func configure(_ item: NewsModel, at indexPath: IndexPath) {
        titleLBL.text = item.title
        detailLBL.text = item.content
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
