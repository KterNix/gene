//
//  NewDetailDataSource.swift
//  GenePlus
//
//  Created by pro on 8/9/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation

class NewDetailDataSource: TableViewArrayDataSource<NewsModel, NewDetailCell> {
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}
