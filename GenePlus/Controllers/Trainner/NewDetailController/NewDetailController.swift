//
//  NewDetailController.swift
//  GenePlus
//
//  Created by pro on 8/9/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import GSKStretchyHeaderView

class NewDetailController: BaseViewController {

    
    @IBOutlet weak var myTable: UITableView!
    
    var new = NewsModel()
    
    var datas:NewDetailDataSource?
    var stretchyHeaderView:GSKStretchyHeaderView?
    lazy var header:PicHeader? = UIView.instanceFromNib()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        clearNavigationColor()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        whiteNavigationColor()
    }
    
    override func setupUI() {
        setBackButton()
    }
    
    override func configTable() {
        
        let headerHeight:CGFloat = 250
       
        stretchyHeaderView = GSKStretchyHeaderView(frame: CGRect(x: 0, y: 0, width: myTable.frame.size.width, height: headerHeight))
        stretchyHeaderView?.expansionMode = .topOnly
        stretchyHeaderView?.contentShrinks = true
        stretchyHeaderView?.contentExpands = true
        stretchyHeaderView?.minimumContentHeight = 64
        stretchyHeaderView?.maximumContentHeight = headerHeight
        stretchyHeaderView?.addSubview(header!)
        myTable.addSubview(stretchyHeaderView!)
        
        header?.backgroundImage.setImageWith(path: new.picture, imageType: .NORMAL_IMAGE)
        header?.backgroundImage.hero.isEnabled = true
        header?.backgroundImage.hero.id = new.picture
        header?.backgroundImage.hero.modifiers = [.forceNonFade,
                                                  .spring(stiffness: 300, damping: 25)]
        
        
        datas = datasource()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension NewDetailController {
    
    fileprivate func datasource() -> NewDetailDataSource? {
        
        let ds = NewDetailDataSource(tableView: myTable, array: [new])
        
        
        return ds
    }
    
}







