//
//  TraineeDetailController.swift
//  GenePlus
//
//  Created by pro on 5/17/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import GSKStretchyHeaderView
import SwipeCellKit

class TraineeDetailController: UIViewController {

    
    @IBOutlet weak var myTable: UITableView!
    var shownIndexes : [IndexPath] = []

    var stretchyHeaderView:GSKStretchyHeaderView?
    lazy var header:TraineeHeader? = UIView.instanceFromNib()
    
    
    lazy var viewProfileBTN: UIButton = {
       let b = UIButton(type: .custom)
        let bTitle = "View Profile"
        b.contentHorizontalAlignment = .right
        b.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
        b.setTitleColor(UIColor.white, for: UIControl.State.normal)
        b.setTitle(bTitle, for: UIControl.State.normal)
        b.frame = CGRect(x: 0, y: 0, width: 100, height: 30)
        b.addTarget(self, action: #selector(didTapProfile), for: UIControl.Event.touchUpInside)
        return b
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        configTable()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        lightStatusbar()
        updateAttributedNAV(color: UIColor.white)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        defaultStatusbar()
    }
    
    fileprivate func setupUI() {
        setBackButton()
        clearNavigationColor()
        applyAttributedNAV()
        
        let viewProfile = UIBarButtonItem(customView: viewProfileBTN)
        navigationItem.rightBarButtonItems = [viewProfile]
        
    }
    
    fileprivate func configTable() {
        let headerHeight:CGFloat = 230
        header?.frame.size.height = headerHeight
        header?.frame.size.width = myTable.frame.size.width
        
        stretchyHeaderView = GSKStretchyHeaderView(frame: CGRect(x: 0, y: 0, width: myTable.frame.size.width, height: headerHeight))
        stretchyHeaderView?.expansionMode = .topOnly
        stretchyHeaderView?.contentShrinks = true
        stretchyHeaderView?.contentExpands = true
        stretchyHeaderView?.minimumContentHeight = 64
        stretchyHeaderView?.maximumContentHeight = headerHeight
        stretchyHeaderView?.addSubview(header!)
        myTable.addSubview(stretchyHeaderView!)
        
        stretchyHeaderView?.stretchDelegate = self
        
        myTable.delegate = self
        myTable.dataSource = self
        myTable.separatorStyle = .none
        myTable.register(nibWithCellClass: TraineeDetailCell.self)
        
        
    }
    
    @objc fileprivate func didTapProfile() {
        let vc = Controllers.shared.traineeProfileController()
        let nav = UINavigationController(rootViewController: vc)
        
        DispatchQueue.main.async {
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        guard scrollView.contentOffset.y < 0 else { return }
        var scale = 1 + fabsf(Float(scrollView.contentOffset.y)) / Float(scrollView.frame.size.height / 5)
        scale = max(0, scale)
        header?.gradientView.transform = CGAffineTransform.identity.scaledBy(x: CGFloat(scale), y: CGFloat(scale))

    }
    
    deinit {
        stretchyHeaderView = nil
        header = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension TraineeDetailController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: TraineeDetailCell.self)
        cell.selectionStyle = .none
        cell.delegate = self
        if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2 {
            cell.iconImage.image = UIImage(named: "successIcon")?.withRenderingMode(.alwaysTemplate)
            cell.iconImage.tintColor = UIColor.red.withAlphaComponent(0.8)
        }
        
        cell.titleLBL.text = "Two-day split: Upper body/Lower body"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            animateCell(cell: cell)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
}

extension TraineeDetailController: SwipeTableViewCellDelegate {
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {

        if orientation == .right {
            let completeWorkout = SwipeAction(style: .default, title: "Complete") { action, indexPath in
                tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.right)
            }
            
            completeWorkout.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
            completeWorkout.textColor = UIColor.lightGray
            completeWorkout.transitionDelegate = ScaleTransition.default
            completeWorkout.image = UIImage(named: "done")
            completeWorkout.backgroundColor = UIColor.clear
            
            return [completeWorkout]
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeTableOptions {
        var options = SwipeTableOptions()
        options.backgroundColor = UIColor.clear
        
        if orientation == .right {
            options.expansionDelegate = ScaleAndAlphaExpansion.default
            options.buttonPadding = 40
        }
        return options
    }
    
}


extension TraineeDetailController: GSKStretchyHeaderViewStretchDelegate {
    func stretchyHeaderView(_ headerView: GSKStretchyHeaderView, didChangeStretchFactor stretchFactor: CGFloat) {
        guard stretchFactor >= 0.4, stretchFactor <= 1 else { return }
        
        header?.profilePic.transform = CGAffineTransform.init(scaleX: stretchFactor, y: stretchFactor)
    }
}
