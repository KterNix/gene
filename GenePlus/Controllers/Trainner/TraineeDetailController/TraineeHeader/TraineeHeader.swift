//
//  TraineeHeader.swift
//  GenePlus
//
//  Created by pro on 7/11/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class TraineeHeader: UIView {

    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var profilePic: UIImageView!
    
    @IBOutlet weak var titleLBL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupUI()
        
    }
    
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        let colors = [UIColor.mainColor().withAlphaComponent(0.8),
                      UIColor.mainColor().withAlphaComponent(0.3)]
        gradientView.applyGradient(withColours: colors, gradientOrientation: .topLeftBottomRight)
    }
    
    private func setupUI() {
        
        
        
        profilePic.cornerRadius = profilePic.frame.size.width / 2
        
        profilePic.setImageWith(path: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSO1Awoj9EJMFEIRo0EAs6GnR4Xsulbgefvh6XFVckdPA43yarwUw", imageType: .NORMAL_IMAGE)
        
    }
    
}
