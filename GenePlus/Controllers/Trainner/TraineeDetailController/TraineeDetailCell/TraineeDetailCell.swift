//
//  TraineeDetailCell.swift
//  GenePlus
//
//  Created by pro on 5/17/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import SwipeCellKit

class TraineeDetailCell: SwipeTableViewCell {

    
    @IBOutlet weak var timeLBL: UILabel!
    @IBOutlet weak var dateLBL: UILabel!
    @IBOutlet weak var titleLBL: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
