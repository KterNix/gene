//
//  NewsController.swift
//  GenePlus
//
//  Created by pro on 4/13/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class NewsController: UIViewController {

    @IBOutlet weak var myTable: UITableView!
    
    
    
    
    
    var listNews = [NewsModel]() {
        didSet {
            DispatchQueue.main.async {
                self.myTable.reloadData()
            }
        }
    }
    
    var newsDataSource: NewsDataSource?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
        configTable()

        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        defaultStatusbar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        lightStatusbar()
    }
    
    
    fileprivate func setupUI() {
        title = "News"
        setBackButton()
        applyAttributedNAV()
        whiteNavigationColor()
    }
    
    fileprivate func configTable() {
        myTable.register(nibWithCellClass: NewsCell.self)
        myTable.register(cellWithClass: PlaceholderCell.self)

        newsDataSource = NewsDataSource(listNews: listNews)
        newsDataSource?.didSelectRow = didSelectRow(item:cell:)
        myTable.dataSource = newsDataSource
        myTable.delegate = newsDataSource
    }
    
    fileprivate func setupDataSource() {
        let a = NewsModel(title: "Priyanka Chopra chooses selfie over workout at the gym.",
                          content: "Shorts and I have had a turbulent relationship over the years. I love my thick thighs, but finding a pair of pint-sized pants with hems that don’t act like cheese wires, or chafe so badly that they leave me with painful welts down my legs has been tricky. With summer here, I’ve been looking for something more lightweight for the heat, and while historically you’d often find me on the men’s side of shops to get my sport short fix (the choice has been infinitely better), the women’s section is getting much better by the day.\n\nIf you’re looking to swap your leggings for something a little more airy this summer, these five workout shorts provide a step up on the competition in terms of design, tech and cut.\n\nSuper soft and incredibly lightweight, this super stretchy pair is a personal favourite of mine. Available in three colourways - Pink Dust, Ruby Wine and Quiet Blue - they’ll keep you cool and extremely comfortable.",
                          picture: "https://static.independent.co.uk/s3fs-public/styles/story_large/public/thumbnails/image/2018/04/10/13/child-learning.jpg")
        let b = NewsModel(title: "FITNESS: THE LATEST FITNESS AND WORKOUT NEWS AND TRENDS",
                          content: "THEY’RE POPPING UP IN GYMS, SPAS AND YOGA STUDIOS NEAR YOU, AND THEY’RE ALREADY HUGE IN THE US. HERE’S WHAT INFRARED SAUNAS CAN DO FOR YOUR SKIN, STRENGTH AND SLEEP, AND WHY IT’S DIFFERENT FROM YOUR AVERAGE SWEAT BOX EXPERIENCE\n\nRead up on the latest fitness advice, exercise programmes and workout trends sweeping the fitness world on Get The Gloss. With expert personal trainers on hand to guide you and great HIIT workouts to keep you moving, all you need for a kick-start is right here\n\nLeaping into a sauna in August may not be at the top of you ‘to do’ activities, but that’s exactly what the likes of London’s soon to launch Glow Bar are encouraging. The holistic wellness space, whose US outposts are frequented by Meghan Markle when she’s back home, will arrive on Mortimer Street at the end of August and will incorporate a natural beauty shop, stocking everything from adaptogenic supplements to organic skincare and vegan condoms, with a health café and private infrared sauna pods that will be bookable for up to 45 minutes.\n\nSure, you may be accustomed to browsing the shops and grabbing a latte during your lunch break, but scheduling a sauna session before you head back to your desk seems like a distinctly alien concept, not to mention one that will leave you v sweaty for the afternoon shift. This isn’t your traditional sauna encounter, however, and the effects can have an impact on everything from skin health to flexibility. Here’s what an infrared sauna session could add to your life (and what it can’t).\n\nNot in terms of in vogue tendencies, although that too. Far infrared light won’t create the oppressive heat that can accompany your regular Finnish situation, as it warms your core body temperature rather than the environment around you. As such, you can comfortably tolerate kicking back in an infrared sauna pod for a far longer period of time than you would a conventional sauna, and the infrared energy elevates body heat more gently, so you’re less likely to feel suddenly stifled.\n\nOr so initial studies seem to suggest, although they’re small and research is currently relatively scant. A study of 20 participants published in Yonsei Medical Journal concluded that regular, gradual exposure to infrared radiation increased collagen and elastin production, with a mean improvement of 25 to 50 per cent in skin texture also occurring after six months of infrared therapy. All patients also noted a decrease in skin roughness and increase in tightness, as well as more even skin tone, although infrared sessions didn’t result in a decrease in hyperpigmentation. Specialists concluded that infrared radiation stimulated fibroblasts to increase collagen and elastin synthesis, slowing the formation of wrinkles, and that infrared treatment could be a safe and effective way of rejuvenating photo-aged skin over time, but that more research is required regarding the mechanism and therapeutic potential of infrared therapy in the context of clinical dermatology.\n\nThe thermal effect of infrared energy also stimulates circulation, helping to deliver more oxygen and nutrients to the skin’s surface and generally bringing about a much-coveted healthy glow. You needn’t worry about side-effects either- unlike UV light emitted by the likes of sunbeds, infrared saunas don’t cause DNA damage and are considered to be safe and well tolerated by all skin types.",
                          picture: "https://static.independent.co.uk/s3fs-public/styles/story_large/public/thumbnails/image/2018/04/05/12/woman-eating-doughnuts.jpg")
        let c = NewsModel(title: "WHY EVERYONE IN WELLNESS IS BOOKING AN INFRARED SAUNA SESSION",
                          content: "IT’S NOT A PROVEN MEDICAL CONDITION, BUT IF YOU’RE CONSTANTLY STRESSED, OVERTIRED OR RUN DOWN, IT COULD BE THAT ADRENAL FATIGUE HAS SET IN. HERE’S YOUR GUIDE TO WHAT TO LOOK OUT FOR, AND HOW TO HANDLE IT…\n\nAdrenal fatigue is a term that we’re hearing crop up more and more often, but it’s not something that a medical professional is likely to diagnose in clinic. If you’re undergoing a period of prolonged stress, struggling to fully recover from illness or generally feeling too tired to function,however, you may find that some of your symptoms fit with the description of adrenal fatigue.\n\nIf you’ve excluded other causes but are still feeling distinctly ropey, consider Omniya hormonal specialist and GP Dr Sohère Roked’s overview of adrenal fatigue, from exactly what defines it, to what you can do about it.\n\nAdrenal fatigue is a common source of tiredness, yet not many people know a lot about it. The adrenals are two small glands that sit on top of the kidneys, each about the size of a walnut. Despite being small, they are very important for health and wellbeing.\n\nThe adrenals affect virtually every system in our body. They aid our bodies in responding to stress, maintaining energy, regulating the immune system and our heart rate. They also maintain levels of minerals and keep blood sugar, fluid levels and blood pressure within a healthy range.\n\nIn addition they produce adrenaline and noradrenaline: the so-called ‘fight or flight’ hormones which help the body deal with acute stress. These are the hormones that give a person superhuman strength, such as to lift up a car when a child is trapped underneath it or the energy to run away from an attacker.\n\nThe adrenal glands manufacture over 30 different steroids including cortisol, DHEA and cortisone which help the body to process fats, proteins, carbohydrates, regulate insulin levels, reduce inflammation and influence the immune system. When the adrenal glands are healthy, they secrete precise amounts of the steroid hormones.\n\nHowever, too much physical, emotional, environmental or psychological stress causes imbalances in their functioning. This can result in adrenal fatigue, when the adrenal glands are no longer coping with the strains put on them.",
                          picture: "https://static.independent.co.uk/s3fs-public/styles/story_large/public/thumbnails/image/2018/03/23/12/stop-exercise-depression.jpg")
        
        listNews.append(a)
        listNews.append(b)
        listNews.append(c)

    }
    
    //MARK: TABLEVIEW DELEGATE
    func didSelectRow(item: NewsModel, cell: UITableViewCell) {
        
        let vc = Controllers.shared.newDetailController()
        vc.new = item
        navigationController?.isHeroEnabled = true
        navigationController?.pushViewController(vc, animated: true)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
