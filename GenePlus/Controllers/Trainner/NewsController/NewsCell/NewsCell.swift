//
//  NewsCell.swift
//  GenePlus
//
//  Created by pro on 4/13/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class NewsCell: UITableViewCell {

    
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var titleLBL: UILabel!
    
    @IBOutlet weak var parentView: UIView!
    
    
    
    func configWith(value: NewsModel) {
        
        titleLBL.text = value.title
        
        backgroundImage.setImageWith(path: value.picture, imageType: .NORMAL_IMAGE)
        backgroundImage.isHeroEnabled = true
        backgroundImage.heroID = value.picture
        backgroundImage.heroModifiers = [.forceNonFade,
                                         .spring(stiffness: 300, damping: 25)]
    }

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupUI()
    }

    fileprivate func setupUI() {
        parentView.cornerRadius = 3
        parentView.borderWidth = 1
        parentView.borderColor = UIColor.lightGray.withAlphaComponent(0.3)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    
    
    
}
