//
//  NewsDataSource.swift
//  GenePlus
//
//  Created by pro on 4/13/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation

class NewsDataSource: NSObject {
    
    var listNews = [NewsModel]()
    var shownIndexes : [IndexPath] = []

    
    var didSelectRow: (( _ item: NewsModel, _ cell: UITableViewCell) -> Void)?
    
    init(listNews: [NewsModel]) {
        self.listNews = listNews
    }
    
}

extension NewsDataSource: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = listNews.count > 0 ? listNews.count : 1
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if listNews.count > 0 {
            let cell = tableView.dequeueReusableCell(withClass: NewsCell.self, for: indexPath)
            cell.selectionStyle = .none
            let item = listNews[indexPath.row]
            cell.configWith(value: item)
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceholderCell", for: indexPath) as! PlaceholderCell
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard listNews.count > 0 else { return }
        let cell = tableView.cellForRow(at: indexPath)!
        let item = listNews[indexPath.row]
        
        if let selectedRow = didSelectRow {
            selectedRow(item, cell)
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellHeight:CGFloat = listNews.count > 0 ? UITableView.automaticDimension : 120
        return cellHeight
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            
            let additionalSeparatorThickness = CGFloat(1)
            let additionalSeparator = UIView(frame: CGRect(x: 8, y: cell.frame.size.height - additionalSeparatorThickness, width: cell.frame.size.width - 16, height: additionalSeparatorThickness))
            additionalSeparator.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
            cell.addSubview(additionalSeparator)
            
            
            cell.transform = CGAffineTransform(translationX: 0, y: 50)
            cell.alpha = 0
            
            UIView.beginAnimations("rotation", context: nil)
            UIView.setAnimationDuration(0.5)
            cell.transform = CGAffineTransform(translationX: 0, y: 0)
            cell.alpha = 1
            UIView.commitAnimations()
        }
    }
    
}
