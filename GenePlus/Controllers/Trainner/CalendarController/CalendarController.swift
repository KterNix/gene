//
//  CalendarController.swift
//  GenePlus
//
//  Created by pro on 4/13/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import FSCalendar
import ObjectMapper

class CalendarController: UIViewController {

    
    @IBOutlet weak var calendarView: FSCalendar!
    
    @IBOutlet weak var calendarHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var myTable: UITableView!
    

    
    var listSchedule = [ScheduleTraining]() {
        didSet {
            DispatchQueue.main.async {
                self.myTable.reloadData()
            }
        }
    }
    
    var userType:UserType = .TRAINER
    var shownIndexes : [IndexPath] = []

    
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter
    }()
    
    fileprivate lazy var scopeGesture: UIPanGestureRecognizer = {
        [unowned self] in
        let panGesture = UIPanGestureRecognizer(target: self.calendarView, action: #selector(self.calendarView.handleScopeGesture(_:)))
        panGesture.delegate = self
        panGesture.minimumNumberOfTouches = 1
        panGesture.maximumNumberOfTouches = 2
        return panGesture
    }()
    
    
    lazy var profileButton:UIButton = {
        let b = UIButton(type: UIButton.ButtonType.custom)
        b.tintColor = UIColor.init(hexString: "6F7179")
        b.setImage(UIImage(named: "profileIcon")?.withRenderingMode(.alwaysTemplate), for: UIControl.State.normal)
        let sizeB:CGFloat = 30
        b.frame = CGRect(x: 0, y: 0, width: sizeB, height: sizeB)
        b.addTarget(self, action: #selector(didTapProfile), for: UIControl.Event.touchUpInside)
        return b
    }()
    
    lazy var noticeButton:UIButton = {
        let b = UIButton(type: UIButton.ButtonType.custom)
        b.tintColor = UIColor.init(hexString: "6F7179")
        b.setImage(UIImage(named: "bell")?.withRenderingMode(.alwaysTemplate), for: UIControl.State.normal)
        let sizeB:CGFloat = 30
        b.frame = CGRect(x: 0, y: 0, width: sizeB, height: sizeB)
        b.addTarget(self, action: #selector(didTapNotice), for: UIControl.Event.touchUpInside)
        return b
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        configTable()
        setupCalendar()
        readFile()
        // Do any additional setup after loading the view.
    }
    
   
    fileprivate func readFile() {
        let pathName = userType == .TRAINER ? "trainerSchedule" : "traineeSchedule"
        if let path = Bundle.main.path(forResource: pathName, ofType: "plist") {
            if let arr = NSArray(contentsOfFile: path) {
                guard let models = Mapper<ScheduleTraining>().mapArray(JSONObject: arr) else { return }
                listSchedule = models
            }
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        defaultStatusbar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        lightStatusbar()
    }
    
    
    fileprivate func setupUI() {
        
        whiteNavigationColor()
        applyAttributedNAV()
        
        let notice = UIBarButtonItem(customView: noticeButton)
        navigationItem.rightBarButtonItems = [notice]
        let profile = UIBarButtonItem(customView: profileButton)
        navigationItem.leftBarButtonItems = [profile]
        
    }
    
    
    fileprivate func configTable() {
        myTable.register(nibWithCellClass: CalendarCell.self)
        myTable.register(nibWithCellClass: PlaceholderCell.self)

        myTable.delegate = self
        myTable.dataSource = self
        
    }
    
    @objc fileprivate func didTapProfile() {
        let vc = Controllers.shared.profileController()
        let nav = UINavigationController(rootViewController: vc)
        vc.userType = userType
        DispatchQueue.main.async {
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    @objc fileprivate func didTapNotice() {
        let vc = Controllers.shared.noticeController()
        let nav = UINavigationController(rootViewController: vc)
        
        DispatchQueue.main.async {
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    

    
    
    fileprivate func setupCalendar() {
        calendarView.appearance.subtitleDefaultColor = UIColor.mainColor()
        calendarView.appearance.headerTitleFont = UIFont.boldSystemFont(ofSize: 20)
        
        self.calendarView.select(Date())
        self.view.addGestureRecognizer(self.scopeGesture)
        self.myTable.panGestureRecognizer.require(toFail: self.scopeGesture)
        self.calendarView.setScope(.week, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension CalendarController: FSCalendarDelegate, FSCalendarDataSource, UIGestureRecognizerDelegate {
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        let shouldBegin = self.myTable.contentOffset.y <= -self.myTable.contentInset.top
        if shouldBegin {
            let velocity = self.scopeGesture.velocity(in: self.view)
            switch self.calendarView.scope {
            case .month:
                return velocity.y < 0
            case .week:
                return velocity.y > 0
            }
        }
        return shouldBegin
    }
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.calendarHeightConstraint.constant = bounds.height
        self.view.layoutIfNeeded()
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        print("did select date \(self.dateFormatter.string(from: date))")
        let selectedDates = calendar.selectedDates.map({self.dateFormatter.string(from: $0)})
        print("selected dates is \(selectedDates)")
        if monthPosition == .next || monthPosition == .previous {
            calendar.setCurrentPage(date, animated: true)
        }
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        print("\(self.dateFormatter.string(from: calendar.currentPage))")
    }
}

extension CalendarController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listSchedule.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CalendarCell", for: indexPath) as! CalendarCell
        cell.selectionStyle = .none
        let item = listSchedule[indexPath.row]
        cell.configWith(value: item, userType: userType)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellHeight:CGFloat = 80
        return cellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = Controllers.shared.scheduleDetailController()
        let item = listSchedule[indexPath.row]

        vc.schedule = item
        vc.userType = userType
        
        if userType == .TRAINER {
            navigationController?.pushViewController(vc)
        }else{
            let nav = UINavigationController(rootViewController: vc)
            
            DispatchQueue.main.async {
                self.present(nav, animated: true, completion: nil)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            
            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            animateCell(cell: cell)
        }
    }
    
}








