//
//  CalendarCell.swift
//  GenePlus
//
//  Created by pro on 4/13/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class CalendarCell: UITableViewCell {

    
    
    @IBOutlet weak var hourLBL: UILabel!
    @IBOutlet weak var dateLBL: UILabel!
    
    @IBOutlet weak var nameLBL: UILabel!
    @IBOutlet weak var subTitleLBL: UILabel!
    
    
    
    
    func configWith(value: ScheduleTraining, userType: UserType) {
        let name = userType == .TRAINER ? value.trainee : value.trainer
        nameLBL.text = name
        subTitleLBL.text = value.workout
        
        hourLBL.text = value.startIn
        dateLBL.text = value.date
    }
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
