//
//  TraineeCell.swift
//  GenePlus
//
//  Created by pro on 5/17/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class TraineeCell: UITableViewCell {

    
    @IBOutlet weak var profilePic: UIImageView!
    
    @IBOutlet weak var titleLBL: UILabel!
    
    @IBOutlet weak var detailTitleLBL: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        // Initialization code
    }
    
    private func setupUI() {
        profilePic.cornerRadius = profilePic.frame.size.width / 2
        profilePic.borderWidth = 2
        profilePic.borderColor = UIColor.lightGray
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
