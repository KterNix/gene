//
//  MyTraineeController.swift
//  GenePlus
//
//  Created by pro on 4/13/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class MyTraineeController: UIViewController {

    
    @IBOutlet weak var myTable: UITableView!
    
    var shownIndexes : [IndexPath] = []
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        configTable()
    }
    
   
    
    override func viewDidAppear(_ animated: Bool) {
        defaultStatusbar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        lightStatusbar()
        whiteNavigationColor()
    }
    
    fileprivate func setupUI() {
        title = "Trainees"
        applyAttributedNAV()
        whiteNavigationColor()
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        }
    }
    
    fileprivate func configTable() {
    
        myTable.delegate = self
        myTable.dataSource = self
        myTable.register(nibWithCellClass: TraineeCell.self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension MyTraineeController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TraineeCell", for: indexPath) as! TraineeCell
        cell.selectionStyle = .none
        cell.titleLBL.text = "Jason"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = Controllers.shared.traineeDetailController()
        let nav = UINavigationController(rootViewController: vc)
        
        DispatchQueue.main.async {
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            animateCell(cell: cell)
        }
    }
}


