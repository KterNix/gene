//
//  PersonInfoCell.swift
//  GenePlus
//
//  Created by pro on 8/21/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

typealias ButtonHandle = (PersonInfoCell) -> Void


class PersonInfoCell: UITableViewCell {

    
    @IBOutlet weak var profilePic: UIImageView!
    
    @IBOutlet weak var titleLBL: UILabel!
    
    @IBOutlet weak var mesBTN: UIButton!
    
    var buttonHandle:ButtonHandle?
    
    func configWith(value: ScheduleTraining, userType: UserType, buttonHandle: ButtonHandle?) {
        profilePic.setImageWith(path: "https://cdn.pixabay.com/photo/2018/02/07/20/58/girl-3137998_960_720.jpg", imageType: .AVATAR)
        
        
        let name = userType == .TRAINER ? value.trainee : value.trainer
        
        titleLBL.text = name
        
        self.buttonHandle = buttonHandle
        
    }
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        setupUI()
    }
    
    fileprivate func setupUI() {
        profilePic.cornerRadius = profilePic.width / 2
        mesBTN.cornerRadius = 3
        mesBTN.borderWidth = 1
        mesBTN.borderColor = UIColor.lightGray
    }
    
    
    @IBAction func mesAction(_ sender: Any) {
        
        buttonHandle?(self)
        
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
