//
//  ScheduleDetailController.swift
//  GenePlus
//
//  Created by pro on 8/20/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class ScheduleDetailController: BaseViewController {

    enum Cells: Int, CaseCountable {
        case TRAINER = 0
        case TITLE
        case START_IN
        case END_IN
        case DATE
    }
    
    
    
    
    
    @IBOutlet weak var myTable: UITableView!
    
    var shownIndexes : [IndexPath] = []
    var schedule:ScheduleTraining?
    var userType:UserType = .TRAINER
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        defaultStatusbar()
    }
    
    override func setupUI() {
        setBackButton()
        applyAttributedNAV()
        whiteNavigationColor()
    }
    
    override func configTable() {
        myTable.register(nibWithCellClass: PersonInfoCell.self)
        myTable.register(nibWithCellClass: ContentCell.self)
        myTable.register(nibWithCellClass: ScheduleInformationCell.self)


        myTable.delegate = self
        myTable.dataSource = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ScheduleDetailController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Cells.caseCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
            
        case Cells.TRAINER.rawValue:
            let cell = tableView.dequeueReusableCell(withClass: PersonInfoCell.self)
            cell.configWith(value: schedule ?? ScheduleTraining()!, userType: userType, buttonHandle: buttonHandle())
            return cell
        case Cells.TITLE.rawValue:
            let cell = tableView.dequeueReusableCell(withClass: ScheduleInformationCell.self)
            cell.titleLBL.text = schedule?.workout
            return cell
        case Cells.START_IN.rawValue:
            let cell = tableView.dequeueReusableCell(withClass: ContentCell.self)
            cell.titleLBL.text = "START IN"
            cell.detailLBL.text = schedule?.startIn
            return cell
        case Cells.END_IN.rawValue:
            let cell = tableView.dequeueReusableCell(withClass: ContentCell.self)
            cell.titleLBL.text = "END IN"
            cell.detailLBL.text = schedule?.endIn

            return cell
        case Cells.DATE.rawValue:
            let cell = tableView.dequeueReusableCell(withClass: ContentCell.self)
            cell.titleLBL.text = "DATE"
            cell.detailLBL.text = schedule?.date
            
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case Cells.TRAINER.rawValue:
            return 90
        case Cells.TITLE.rawValue:
            return UITableView.automaticDimension
        case Cells.START_IN.rawValue, Cells.END_IN.rawValue, Cells.DATE.rawValue:
            return 60
        default:
            return 60
        }
    }
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case Cells.TITLE.rawValue:
            return 60
        default:
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            
            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            animateCell(cell: cell)
        }
    }
    
    
    func buttonHandle() -> ButtonHandle? {
        return { [unowned self] PersonInfoCell in
            let vc = Controllers.shared.chatController()
            let nav = UINavigationController(rootViewController: vc)
            
            DispatchQueue.main.async {
                self.present(nav, animated: true, completion: nil)
            }
        }
    }
    
  
}
