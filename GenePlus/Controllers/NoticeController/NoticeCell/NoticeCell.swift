//
//  NoticeCell.swift
//  GenePlus
//
//  Created by pro on 1/8/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class NoticeCell: UITableViewCell, ConfigurableCell {

    
    @IBOutlet weak var titleLBL: UILabel!
    
    @IBOutlet weak var iconType: UIImageView!
    
    @IBOutlet weak var timeCreatedLBL: UILabel!
    
    
    
    func configure(_ item: NoticeModel, at indexPath: IndexPath) {
        titleLBL.text = item.title
        
        if item.nType == NoticeTypes.PROMOTION.rawValue {
            iconType.image = NoticeTypes.getAsset(NoticeTypes.PROMOTION)()
        }else if item.nType == NoticeTypes.NEWS.rawValue {
            iconType.image = NoticeTypes.getAsset(NoticeTypes.NEWS)()
        }else if item.nType == NoticeTypes.ADS.rawValue {
            iconType.image = NoticeTypes.getAsset(NoticeTypes.ADS)()
        }else if item.nType == NoticeTypes.OTHER.rawValue {
            iconType.image = NoticeTypes.getAsset(NoticeTypes.OTHER)()
        }
        
        if let time = item.timeCreated {
            let date = Utils.stringToDate(time)
            timeCreatedLBL.text = Utils.timeAgoSince(date)
        }
        
        
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
