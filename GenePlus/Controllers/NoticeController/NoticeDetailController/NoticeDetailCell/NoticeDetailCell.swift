//
//  NoticeDetailCell.swift
//  GenePlus
//
//  Created by pro on 8/17/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class NoticeDetailCell: UITableViewCell, ConfigurableCell {

    
    @IBOutlet weak var titleLBL: UILabel!
    
    @IBOutlet weak var contentLBL: UILabel!
    
    
    
    
    
    
    
    func configure(_ item: NoticeModel, at indexPath: IndexPath) {
        titleLBL.text = item.title
        contentLBL.text = item.content
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
