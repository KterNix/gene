//
//  NoticeDetailController.swift
//  GenePlus
//
//  Created by pro on 8/17/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import GSKStretchyHeaderView

class NoticeDetailController: UIViewController {

    
    @IBOutlet weak var myTable: UITableView!
    
    var notice = [NoticeModel]()
    var datascr:NoticeDetailDataSource?
    var stretchyHeaderView:GSKStretchyHeaderView?
    lazy var header:NoticeDetailHeader? = UIView.instanceFromNib()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        configTable()
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        clearNavigationColor()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        lightStatusbar()
        updateNAVTint(color: .white)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        whiteNavigationColor()
        defaultStatusbar()
        updateNAVTint(color: .mainColor())
    }
    
    
    fileprivate func setupUI() {
        setBackButton()
    }
    
    fileprivate func configTable() {
        
        let headerHeight:CGFloat = 300
        
        
        stretchyHeaderView = GSKStretchyHeaderView(frame: CGRect(x: 0, y: 0, width: myTable.frame.size.width, height: headerHeight))
        stretchyHeaderView?.expansionMode = .topOnly
        stretchyHeaderView?.contentShrinks = true
        stretchyHeaderView?.contentExpands = true
        stretchyHeaderView?.minimumContentHeight = 64
        stretchyHeaderView?.maximumContentHeight = headerHeight
        stretchyHeaderView?.addSubview(header!)
        myTable.addSubview(stretchyHeaderView!)
        
        
        
        header?.backgroundImage.setImageWith(path: notice.first?.banner, imageType: .NORMAL_IMAGE)
        
        
        datascr = datasource()
    }
    
    
    
    
    deinit {
        header = nil
        stretchyHeaderView = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}


extension NoticeDetailController {
    
    fileprivate func datasource() -> NoticeDetailDataSource? {
        
        let ds = NoticeDetailDataSource(tableView: myTable, array: notice)
        
        ds.tableViewItemSelectionHandler = { indexPath in
            
        }
        
        return ds
    }
    
    
}
