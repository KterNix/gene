//
//  NoticeDetailDataSource.swift
//  GenePlus
//
//  Created by pro on 8/17/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation

class NoticeDetailDataSource: TableViewArrayDataSource<NoticeModel, NoticeDetailCell> {
    
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            
            let additionalSeparatorThickness = CGFloat(1)
            let additionalSeparator = UIView(frame: CGRect(x: 8, y: cell.frame.size.height - additionalSeparatorThickness, width: cell.frame.size.width - 16, height: additionalSeparatorThickness))
            additionalSeparator.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
            cell.addSubview(additionalSeparator)
        }
    }
    
    
}
