//
//  NoticeController.swift
//  GenePlus
//
//  Created by pro on 1/8/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import ObjectMapper

class NoticeController: UIViewController {

    @IBOutlet weak var myTable: UITableView!
    
    
    
    var shownIndexes : [IndexPath] = []

    var datascr:NoticeDataSource?
    var notices = [NoticeModel]() {
        didSet {
            myTable.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        configTable()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        defaultStatusbar()
    }
    
    fileprivate func setupUI() {
        title = "Notice"
        setBackButton()
        applyAttributedNAV()
        
        
        
    }
    
    fileprivate func configTable() {
        readFile()
    }
    
    fileprivate func readFile() {
        
        if let path = Bundle.main.path(forResource: "NoticeItems", ofType: "plist") {
            if let arr = NSArray(contentsOfFile: path) {
                guard let models = Mapper<NoticeModel>().mapArray(JSONObject: arr) else { return }
                notices = models
                datascr = datasource()
            }
        }
    }
    
    
    @objc fileprivate func dismissController() {
        dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

extension NoticeController {
 
    fileprivate func datasource() -> NoticeDataSource? {
        
        let ds = NoticeDataSource(tableView: myTable, array: notices)
        
        ds.tableViewItemSelectionHandler = { [weak self] indexPath in
            guard let strongSelf = self else { return }
            let vc = Controllers.shared.noticeDetailController()
            vc.notice = [strongSelf.notices[indexPath.row]]
            strongSelf.navigationController?.pushViewController(vc)
            
        }
        return ds
    }
    
    
}













