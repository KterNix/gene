//
//  BaseFormViewController.swift
//  neoLock
//
//  Created by pro on 5/13/18.
//  Copyright © 2018 BTS Solutions. All rights reserved.
//

import UIKit
import Eureka

public protocol StatusBarStyleHandler {
    var preferredStatusBarStyle: UIStatusBarStyle { get }
}

public class CustomNavigationCotnroller: UINavigationController {
    
    public override var preferredStatusBarStyle: UIStatusBarStyle {
        if let statusBarHandler = visibleViewController as? StatusBarStyleHandler {
            return statusBarHandler.preferredStatusBarStyle
        }
        
        return .default
    }
}


class BaseFormViewController: FormViewController, StatusBarStyleHandler {

    
    var statusbarStyle:UIStatusBarStyle = .default {
        didSet {
            setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        
        return statusbarStyle
    }

    
    
//    private var loader:MyLoading?
    private let maximumDisplayTime:TimeInterval = 3

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        setupUI()
        setupForm()
        loadForm()
        // Do any additional setup after loading the view.
    }
    
    
    public func setupUI() {
        
    }
    
    private func setupForm() {
        tableView.backgroundColor = UIColor.white
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        
        let color = UIColor.lightGray.withAlphaComponent(0.1)
        let fontSize:CGFloat = 15
        setupLabel(size: fontSize, backgroundColor: color)
        setupTextRow(size: fontSize, backgroundColor: color)
        setupTextAreaRow(size: fontSize, backgroundColor: color)
        setupTimeRow(size: fontSize, backgroundColor: color)
        setupDateTimeInlineRow(size: fontSize, backgroundColor: color)
        setupDateTimeRow(size: fontSize, backgroundColor: color)
        setupPickerInputRow(size: fontSize, backgroundColor: color)
        setupSwitchRow(size: fontSize, backgroundColor: color)
        setupCheckRow(size: fontSize, backgroundColor: color)
    }
    
    
    public func loadForm() {
        
    }

    
//    func showLoader(type: LoadingType, withStatus status: String) {
//        dismissLoader()
//        loader = UIView.instanceFromNib()
//        loader?.show(type: type, with: status, viewcontroller: self)
//
//        if type != .loading {
//            addRunLoop()
//        }
//    }
//
//    func dismissLoader() {
//        loader?.dismissLoader()
//        loader = nil
//    }
    
    
//    func addRunLoop() {
//        self.perform(#selector(fireTime), with: self, afterDelay: maximumDisplayTime)
//    }
//
//    @objc fileprivate func fireTime() {
//        dismissLoader()
//    }
    
    override func viewWillDisappear(_ animated: Bool) {
//        RunLoop.cancelPreviousPerformRequests(withTarget: self, selector: #selector(fireTime), object: nil)
//        dismissLoader()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension BaseFormViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

extension BaseFormViewController {
    public func setupLabel(size: CGFloat, backgroundColor: UIColor) {
        LabelRow.defaultCellUpdate = { cell, row in
            cell.textLabel?.font = UIFont.systemFont(ofSize: size, weight: UIFont.Weight.semibold)
            cell.detailTextLabel?.font = UIFont.systemFont(ofSize: size, weight: UIFont.Weight.semibold)
            cell.textLabel?.textColor = UIColor.darkGray
            cell.tintColor = UIColor.darkGray
            cell.backgroundColor = backgroundColor
        }
    }
    
    public func setupTextRow(size: CGFloat, backgroundColor: UIColor) {
        TextRow.defaultCellUpdate = { cell, row in
            cell.textLabel?.font = UIFont.systemFont(ofSize: size, weight: UIFont.Weight.semibold)
            cell.detailTextLabel?.font = UIFont.systemFont(ofSize: size, weight: UIFont.Weight.semibold)
            cell.textField.font = UIFont.systemFont(ofSize: size, weight: UIFont.Weight.semibold)
            cell.textLabel?.textColor = UIColor.darkGray
            cell.tintColor = UIColor.darkGray
            cell.backgroundColor = backgroundColor
        }
    }
    
    public func setupSwitchRow(size: CGFloat, backgroundColor: UIColor) {
        SwitchRow.defaultCellUpdate = { cell, row in
            cell.textLabel?.font = UIFont.systemFont(ofSize: size, weight: UIFont.Weight.semibold)
            cell.detailTextLabel?.font = UIFont.systemFont(ofSize: size, weight: UIFont.Weight.semibold)
            cell.textLabel?.textColor = UIColor.darkGray
            cell.tintColor = UIColor.darkGray
            cell.backgroundColor = backgroundColor
        }
    }
    
    public func setupCheckRow(size: CGFloat, backgroundColor: UIColor) {
        CheckRow.defaultCellUpdate = { cell, row in
            cell.textLabel?.font = UIFont.systemFont(ofSize: size, weight: UIFont.Weight.semibold)
            cell.detailTextLabel?.font = UIFont.systemFont(ofSize: size, weight: UIFont.Weight.semibold)
            cell.textLabel?.textColor = UIColor.darkGray
            cell.tintColor = UIColor.darkGray
            cell.backgroundColor = backgroundColor
        }
    }
    
    public func setupIntRow(size: CGFloat, backgroundColor: UIColor) {
        IntRow.defaultCellUpdate = { cell, row in
            cell.textLabel?.font = UIFont.systemFont(ofSize: size, weight: UIFont.Weight.semibold)
            cell.detailTextLabel?.font = UIFont.systemFont(ofSize: size, weight: UIFont.Weight.semibold)
            cell.textField.font = UIFont.systemFont(ofSize: size, weight: UIFont.Weight.semibold)
            cell.textLabel?.textColor = UIColor.darkGray
            cell.tintColor = UIColor.darkGray
            cell.backgroundColor = backgroundColor
        }
    }
    
    public func setupPushRow(size: CGFloat, backgroundColor: UIColor) {
        PushRow<String>.defaultCellUpdate = { cell, row in
            cell.selectionStyle = .none
            cell.textLabel?.font = UIFont.systemFont(ofSize: size, weight: UIFont.Weight.semibold)
            cell.detailTextLabel?.font = UIFont.systemFont(ofSize: size, weight: UIFont.Weight.semibold)
            cell.textLabel?.textColor = UIColor.darkGray
            cell.tintColor = UIColor.darkGray
            cell.backgroundColor = backgroundColor
            
            row.onPresentCallback = { _, to in
                let _ = to.navigationItem
                let _ = to.view
                let backgroundView = UIView(frame: CGRect(x: 0, y: 0, width: (self.tableView?.bounds.size.width)!, height: (self.tableView?.bounds.size.height)!))
                backgroundView.backgroundColor = .white
                to.tableView?.backgroundView = backgroundView
                to.setBackButton()
                to.title = row.title
            }
        }
    }
    
    public func setupTextAreaRow(size: CGFloat, backgroundColor: UIColor) {
        TextAreaRow.defaultCellUpdate = { cell, row in
            cell.selectionStyle = .none
            cell.textLabel?.font = UIFont.systemFont(ofSize: size, weight: UIFont.Weight.semibold)
            cell.detailTextLabel?.font = UIFont.systemFont(ofSize: size, weight: UIFont.Weight.semibold)
            cell.textView.backgroundColor = UIColor.clear
            cell.textView.font = UIFont.systemFont(ofSize: size, weight: UIFont.Weight.light)
            cell.placeholderLabel?.font = UIFont.systemFont(ofSize: size, weight: UIFont.Weight.semibold)
            cell.textLabel?.textColor = UIColor.darkGray
            cell.tintColor = UIColor.darkGray
            cell.backgroundColor = backgroundColor
            
        }
    }
    
    public func setupMultiSelectorRow(size: CGFloat, backgroundColor: UIColor) {
        MultipleSelectorRow<String>.defaultCellUpdate = { cell, row in
            cell.selectionStyle = .none
            cell.textLabel?.font = UIFont.systemFont(ofSize: size, weight: UIFont.Weight.semibold)
            cell.detailTextLabel?.font = UIFont.systemFont(ofSize: size, weight: UIFont.Weight.semibold)
            cell.textLabel?.textColor = UIColor.darkGray
            cell.tintColor = UIColor.darkGray
            cell.backgroundColor = backgroundColor
            
            row.onPresentCallback = { _, to in
                let _ = to.navigationItem
                let _ = to.view
                let backgroundView = UIView(frame: CGRect(x: 0, y: 0, width: (self.tableView?.bounds.size.width)!, height: (self.tableView?.bounds.size.height)!))
                backgroundView.backgroundColor = .white
                to.tableView?.backgroundView = backgroundView
                to.setBackButton()
                to.title = row.title
                
            }
        }
    }
    
    public func setupPickerInputRow(size: CGFloat, backgroundColor: UIColor) {
        PickerInputRow<String>.defaultCellSetup = { cell, row in
            cell.textLabel?.font = UIFont.systemFont(ofSize: size, weight: UIFont.Weight.semibold)
            cell.textLabel?.textColor = UIColor.lightGray
            cell.detailTextLabel?.font = UIFont.systemFont(ofSize: size, weight: UIFont.Weight.semibold)
            cell.tintColor = UIColor.lightGray
            cell.backgroundColor = backgroundColor
        }
    }
    
    public func setupDateTimeInlineRow(size: CGFloat, backgroundColor: UIColor) {
        DateTimeInlineRow.defaultCellSetup = { cell, row in
            cell.textLabel?.font = UIFont.systemFont(ofSize: size, weight: UIFont.Weight.semibold)
            cell.textLabel?.textColor = UIColor.lightGray
            cell.detailTextLabel?.font = UIFont.systemFont(ofSize: size, weight: UIFont.Weight.semibold)
            cell.tintColor = UIColor.lightGray
            cell.backgroundColor = backgroundColor
        }
    }
    
    public func setupDateTimeRow(size: CGFloat, backgroundColor: UIColor) {
        DateTimeRow.defaultCellSetup = { cell, row in
            cell.textLabel?.font = UIFont.systemFont(ofSize: size, weight: UIFont.Weight.semibold)
            cell.textLabel?.textColor = UIColor.lightGray
            cell.detailTextLabel?.font = UIFont.systemFont(ofSize: size, weight: UIFont.Weight.semibold)
            cell.tintColor = UIColor.lightGray
            cell.backgroundColor = backgroundColor
        }
    }
    
    public func setupTimeRow(size: CGFloat, backgroundColor: UIColor) {
        TimeRow.defaultCellSetup = { cell, row in
            cell.textLabel?.font = UIFont.systemFont(ofSize: size, weight: UIFont.Weight.semibold)
            cell.textLabel?.textColor = UIColor.lightGray
            cell.detailTextLabel?.font = UIFont.systemFont(ofSize: size, weight: UIFont.Weight.semibold)
            cell.tintColor = UIColor.lightGray
            cell.backgroundColor = backgroundColor
        }
    }
    
    public func setupDateRow(size: CGFloat, backgroundColor: UIColor) {
        DateRow.defaultCellSetup = { cell, row in
            cell.textLabel?.font = UIFont.systemFont(ofSize: size, weight: UIFont.Weight.semibold)
            cell.textLabel?.textColor = UIColor.lightGray
            cell.detailTextLabel?.font = UIFont.systemFont(ofSize: size, weight: UIFont.Weight.semibold)
            cell.tintColor = UIColor.lightGray
            cell.backgroundColor = backgroundColor
        }
    }
}
