//
//  ForgotPassController.swift
//  GenePlus
//
//  Created by pro on 6/5/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ForgotPassController: UIViewController {

    @IBOutlet weak var emailTXT: SkyFloatingLabelTextField!
    
    @IBOutlet weak var sendBTN: UIButton!
    @IBOutlet weak var haveCodeBTN: UIButton!
    @IBOutlet weak var closeBTN: UIButton!
    
    @IBOutlet weak var backgroundImage: UIImageView!
    
    @IBOutlet weak var viewAlpha: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        listenForKeyboard()
        // Do any additional setup after loading the view.
    }
    
    fileprivate func setupUI() {
        let edgeInset:CGFloat = 15
        closeBTN.imageEdgeInsets = UIEdgeInsets(top: edgeInset, left: edgeInset, bottom: edgeInset, right: edgeInset)
        closeBTN.setImage(UIImage(named: "close")?.withRenderingMode(.alwaysOriginal), for: UIControl.State.normal)
        viewAlpha.backgroundColor = UIColor.black.withAlphaComponent(0.2)

        sendBTN.layer.cornerRadius = 5
        sendBTN.layer.masksToBounds = true
        sendBTN.hero.id = "loginBTN"

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sendAction(_ sender: Any) {
    }
    
    
    @IBAction func haveCodeAction(_ sender: Any) {
        let vc = Controllers.shared.verifyPassController()
        vc.hero.isEnabled = true
        DispatchQueue.main.async {
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

}
