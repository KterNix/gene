//
//  ManageWorkoutController.swift
//  GenePlus
//
//  Created by pro on 7/16/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class ManageWorkoutController: BaseViewController {

    
    @IBOutlet weak var myTable: UITableView!
    
    
    lazy var addButton: UIButton = {
        let b = UIButton(type: .custom)
        let bSize:CGFloat = 30
        b.frame = CGRect(x: 0, y: 0, width: bSize, height: bSize)
        b.setImage(UIImage(named: "plus")?.withRenderingMode(.alwaysTemplate), for: UIControl.State.normal)
        b.addTarget(self, action: #selector(didTapAdd), for: UIControl.Event.touchUpInside)
        return b
    }()
    
    
    var listWorkout = [WorkoutModel]() {
        didSet {
            myTable.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDataSource()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        defaultStatusbar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        lightStatusbar()
    }
    
    
    override func setupUI() {
        
        title = "Manage Workout"
        
        applyAttributedNAV()
        whiteNavigationColor()
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        }
        
        let addBTN = UIBarButtonItem(customView: addButton)
        navigationItem.rightBarButtonItems = [addBTN]
        
    }
    
    override func configTable() {
        myTable.register(nibWithCellClass: ChooseWorkoutCell.self)
        myTable.delegate = self
        myTable.dataSource = self
    }
    
    
    @objc fileprivate func didTapAdd() {
        let vc = Controllers.shared.uploadWorkoutController()
        let nav = UINavigationController(rootViewController: vc)
        
        DispatchQueue.main.async {
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    fileprivate func setupDataSource() {
        var a = WorkoutModel()
        
        a.title = "Increase Your Back Width With 5 Moves"
        a.trainer = "Waiting for review"
        a.sets = "1"
        a.weight = "5"
        a.reps = "5"
        a.unit = "KG"
        a.images = ["https://www.bodybuilding.com/images/increase-your-back-width-with-5-moves-musclepharm-1-700xh.jpg"]
        a.createdDate = Date().localDateString()
        listWorkout.append(a)
        
        a = WorkoutModel()
        a.title = "Evan Centopani's 15-Minute Arm Blast"
        a.trainer = "In review"
        a.sets = "2"
        a.weight = "10"
        a.reps = "15"
        a.unit = "KG"
        a.images = ["https://www.bodybuilding.com/images/2018/june/evan-centopanis-15-min-arm-workout-1-700xh.jpg"]
        a.createdDate = Date().localDateString()
        listWorkout.append(a)
        
        
        
        
        a = WorkoutModel()
        a.title = "4 Musts To Maximize Biceps Growth"
        a.trainer = "Rejected"
        a.sets = "1"
        a.weight = "30"
        a.reps = "2"
        a.unit = "KG"
        a.images = ["https://www.bodybuilding.com/images/2018/june/4-musts-to-maximize-bicep-growth-header-830x467.jpg"]
        a.createdDate = Date().addingTimeInterval(1000000).localDateString()
        listWorkout.append(a)
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}

extension ManageWorkoutController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listWorkout.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withClass: ChooseWorkoutCell.self)
        cell.selectionStyle = .none
        cell.checkImage.isHidden = true
        
        let item = listWorkout[indexPath.row]
        
        cell.configWith(value: item)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = Controllers.shared.manageWorkoutDetailController()
        let nav = UINavigationController(rootViewController: vc)
        
        DispatchQueue.main.async {
            self.present(nav, animated: true, completion: nil)
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    
}
