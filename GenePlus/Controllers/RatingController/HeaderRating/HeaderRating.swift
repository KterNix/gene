//
//  HeaderRating.swift
//  GenePlus
//
//  Created by pro on 1/8/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation
import UIKit

class HeaderRating: UIView {
    
    
    @IBOutlet weak var profilePic: UIImageView!
    
    
    @IBOutlet weak var titleLBL: UILabel!
    
    @IBOutlet weak var parentView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        setupUI()
    }
    
    private func setupUI() {
        let colors = [UIColor.mainColor().withAlphaComponent(0.9),
                      UIColor.mainColor().withAlphaComponent(0.5)]
        applyGradient(withColours: colors, gradientOrientation: .topLeftBottomRight)
        
        
        profilePic.cornerRadius = profilePic.width / 2
        profilePic.borderWidth = 2
        profilePic.borderColor = UIColor.white
        
    }
    
}
