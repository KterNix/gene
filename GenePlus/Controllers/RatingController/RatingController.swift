//
//  RatingController.swift
//  GenePlus
//
//  Created by pro on 1/8/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit
import PopupDialog
import GSKStretchyHeaderView

class RatingController: UIViewController {

    
    @IBOutlet weak var myTable: UITableView!
    
    
    lazy var backImage = UIImage(named: "close")?.withRenderingMode(.alwaysTemplate)
    lazy var backBTN:UIButton = {
        let b = UIButton(type: UIButton.ButtonType.custom)
        b.setImage(backImage, for: UIControl.State.normal)
        let edgeInset:CGFloat = 12
        b.imageEdgeInsets = UIEdgeInsets(top: edgeInset, left: edgeInset, bottom: edgeInset, right: edgeInset)
        let sizeB:CGFloat = 30
        b.frame = CGRect(x: 0, y: 0, width: sizeB, height: sizeB)
        b.addTarget(self, action: #selector(dismissController), for: UIControl.Event.touchUpInside)
        return b
    }()
    
    lazy var reviewsImage = UIImage(named: "writeReview")?.withRenderingMode(.alwaysTemplate)
    lazy var writeReviewBTN:UIButton = {
        let b = UIButton(type: UIButton.ButtonType.custom)
        b.setImage(reviewsImage, for: UIControl.State.normal)
        let sizeB:CGFloat = 30
        b.frame = CGRect(x: 0, y: 0, width: sizeB, height: sizeB)
        b.addTarget(self, action: #selector(didTapWriteReview), for: UIControl.Event.touchUpInside)
        return b
    }()
    
    lazy var headerRating:HeaderRating? = UIView.instanceFromNib()
    var stretchyHeaderView:GSKStretchyHeaderView?

    var shownIndexes : [IndexPath] = []
//    var navigationView = UIView()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        configTable()
        navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        lightStatusbar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        defaultStatusbar()
    }
    
    
    
    fileprivate func setupUI() {
        hideBackButton()
        let back = UIBarButtonItem(customView: backBTN)
        navigationItem.leftBarButtonItem = back
        
        let writeReview = UIBarButtonItem(customView: writeReviewBTN)
        navigationItem.rightBarButtonItem = writeReview
        
        clearNavigationColor()
        updateNAVTints(color: .white)
        
        
    }
    
    
    
    fileprivate func configTable() {
        
        let headerHeight:CGFloat = 200
        
        
        stretchyHeaderView = GSKStretchyHeaderView(frame: CGRect(x: 0, y: 0, width: myTable.frame.size.width, height: headerHeight))
        stretchyHeaderView?.expansionMode = .topOnly
        stretchyHeaderView?.contentShrinks = true
        stretchyHeaderView?.minimumContentHeight = 64
        stretchyHeaderView?.maximumContentHeight = headerHeight
        stretchyHeaderView?.addSubview(headerRating!)
        myTable.addSubview(stretchyHeaderView!)
        
        stretchyHeaderView?.stretchDelegate = self
        
        headerRating?.profilePic.setImageWith(path: "https://medias.laprovence.com/KDSRHGONPVXSjSxe-XZPqGe2iBI=/850x575/top/smart/f0d816277d7a4700a60a20822b2bed5f/france-gall-en-colere-contre-jenifer_reference.jpg", imageType: .NORMAL_IMAGE)
        
        myTable.register(nibWithCellClass: RateCell.self)
        
    }

   
    @objc fileprivate func dismissController() {
        dismiss(animated: true, completion: nil)
    }
    
    
    @objc fileprivate func didTapWriteReview() {
        showPopupReview()
    }

    
    fileprivate func updateNAVTints(color: UIColor) {
        navigationController?.navigationBar.tintColor = color
        let nsAttributed = [NSAttributedString.Key.foregroundColor : color]
        navigationController?.navigationBar.titleTextAttributes = nsAttributed
    }
    
    
    
    fileprivate func showPopupReview() {
        let vc = PopupReviewController(nibName: "PopupReviewController", bundle: nil)
        let popupController = PopupDialog(viewController: vc, buttonAlignment: NSLayoutConstraint.Axis.horizontal, transitionStyle: .zoomIn, preferredWidth: view.frame.size.width - 60, tapGestureDismissal: true, hideStatusBar: true) {
            
        }
        
        let cancelButton = CancelButton(title: "CANCEL", height: 44, dismissOnTap: true, action: nil)
        
        let submitButton = DefaultButton(title: "SUBMIT", height: 44, dismissOnTap: false) {
            
        }
        
        popupController.addButtons([cancelButton, submitButton])
        
        present(popupController, animated: true, completion: nil)
    }
    
    deinit {
        headerRating = nil
        stretchyHeaderView = nil
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

extension RatingController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RateCell", for: indexPath) as! RateCell
        cell.selectionStyle = .none
        cell.contentLBL.text = "Jackson is a lovely and honesty trainer. If away from home too ease to loose. After the training or pay more attention."
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 106
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            
            drawLineSeparatorFor(cell: cell, height: cell.frame.size.height)
            animateCell(cell: cell)
        }
    }
}

extension RatingController: GSKStretchyHeaderViewStretchDelegate {
    
    func stretchyHeaderView(_ headerView: GSKStretchyHeaderView, didChangeStretchFactor stretchFactor: CGFloat) {
        headerRating?.profilePic.alpha = stretchFactor
        headerRating?.titleLBL.alpha = stretchFactor
        
    }
    
}

















