//
//  AllotmentModel.swift
//  GenePlus
//
//  Created by pro on 8/18/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation
import ObjectMapper

public struct AllotmentModel: Mappable, Equatable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let title = "title"
        static let id = "id"
        static let date = "date"
        static let startTime = "startTime"
        static let endTime = "endTime"
        
    }
    
    // MARK: Properties
    public var title: String?
    public var id: String?
    public var date: String?
    public var startTime: String?
    public var endTime: String?

    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public init?(map: Map){
        
    }
    public init?() {}
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public mutating func mapping(map: Map) {
        title <- map[SerializationKeys.title]
        id <- map[SerializationKeys.id]
        date <- map[SerializationKeys.date]
        startTime <- map[SerializationKeys.startTime]
        endTime <- map[SerializationKeys.endTime]

    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = title { dictionary[SerializationKeys.title] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = date { dictionary[SerializationKeys.date] = value }
        if let value = startTime { dictionary[SerializationKeys.startTime] = value }
        if let value = endTime { dictionary[SerializationKeys.endTime] = value }

        return dictionary
    }
    
}













