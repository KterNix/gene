//
//  NoticeModel.swift
//  GenePlus
//
//  Created by pro on 8/17/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation
import ObjectMapper

public struct NoticeModel: Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let title = "title"
        static let content = "content"
        static let nType = "nType"
        static let banner = "banner"
        static let timeCreated = "timeCreated"
        
    }
    
    // MARK: Properties
    public var title: String?
    public var content: String?
    public var nType: Int?
    public var banner: String?
    public var timeCreated: String?
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public init?(map: Map){
        
    }
    public init?() {}
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public mutating func mapping(map: Map) {
        title <- map[SerializationKeys.title]
        content <- map[SerializationKeys.content]
        nType <- map[SerializationKeys.nType]
        banner <- map[SerializationKeys.banner]
        timeCreated <- map[SerializationKeys.timeCreated]
        
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = title { dictionary[SerializationKeys.title] = value }
        if let value = content { dictionary[SerializationKeys.content] = value }
        if let value = nType { dictionary[SerializationKeys.nType] = value }
        if let value = banner { dictionary[SerializationKeys.banner] = value }
        if let value = timeCreated { dictionary[SerializationKeys.timeCreated] = value }
        
        return dictionary
    }
    
}
