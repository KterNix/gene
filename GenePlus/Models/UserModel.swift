//
//  UserModel.swift
//  GenePlus
//
//  Created by pro on 9/26/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation
import ObjectMapper


public struct UserModel: Mappable, Equatable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let code = "code"
        static let firstname = "firstname"
        static let lastname = "lastname"
        static let email = "email"
        static let address = "address"
        static let avatar = "avatar"
        static let mobile = "mobile"
        static let city = "city"
        static let country = "country"
        static let lastlogin = "lastlogin"
        static let point = "point"
        static let createdDate = "createdDate"
        static let wishlist = "wishlist"
        static let password = "password"
        static let social_id = "social_id"
        
    }
    
    // MARK: Properties
    public var code: String?
    public var firstname: String?
    public var lastname: String?
    public var email: String?
    public var address: String?
    public var avatar: String?
    public var mobile: String?
    public var city: String?
    public var country: String?
    public var lastlogin: String?
    public var point: String?
    public var createdDate: String?
    public var wishlist: String?
    public var password: String?
    public var social_id: String?

    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public init?(map: Map){
        
    }
    public init?() {}
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public mutating func mapping(map: Map) {
        code <- map[SerializationKeys.code]
        firstname <- map[SerializationKeys.firstname]
        lastname <- map[SerializationKeys.lastname]
        email <- map[SerializationKeys.email]
        address <- map[SerializationKeys.address]
        avatar <- map[SerializationKeys.avatar]
        mobile <- map[SerializationKeys.mobile]
        city <- map[SerializationKeys.city]
        country <- map[SerializationKeys.country]
        lastlogin <- map[SerializationKeys.lastlogin]
        point <- map[SerializationKeys.point]
        createdDate <- map[SerializationKeys.createdDate]
        wishlist <- map[SerializationKeys.wishlist]
        password <- map[SerializationKeys.password]
        social_id <- map[SerializationKeys.social_id]


    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = code { dictionary[SerializationKeys.code] = value }
        if let value = firstname { dictionary[SerializationKeys.firstname] = value }
        if let value = lastname { dictionary[SerializationKeys.lastname] = value }
        if let value = email { dictionary[SerializationKeys.email] = value }
        if let value = address { dictionary[SerializationKeys.address] = value }
        if let value = avatar { dictionary[SerializationKeys.avatar] = value }
        if let value = mobile { dictionary[SerializationKeys.mobile] = value }
        if let value = city { dictionary[SerializationKeys.city] = value }
        if let value = country { dictionary[SerializationKeys.country] = value }
        if let value = lastlogin { dictionary[SerializationKeys.lastlogin] = value }
        if let value = point { dictionary[SerializationKeys.point] = value }
        if let value = createdDate { dictionary[SerializationKeys.createdDate] = value }
        if let value = wishlist { dictionary[SerializationKeys.wishlist] = value }
        if let value = password { dictionary[SerializationKeys.password] = value }
        if let value = social_id { dictionary[SerializationKeys.social_id] = value }

        return dictionary
    }
    
}
