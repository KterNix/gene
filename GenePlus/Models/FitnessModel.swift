//
//  FitnessModel.swift
//  GenePlus
//
//  Created by pro on 1/17/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation
import CoreLocation

class FitnessModel {
    var name:String = ""
    var address:String = ""
    var openIn:String = ""
    var phone:String = ""
    var promotion:String = ""
    var location = CLLocationCoordinate2D()
    var fitnessImage: String = ""
}

