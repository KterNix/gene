//
//  SetModel.swift
//  GenePlus
//
//  Created by pro on 2/6/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation

struct SetModel {
    var name: String = ""
    var unit: String = ""
    var weight:String = ""
    var reps:String = ""
    var duration:String = ""
    var description:String = ""
    var images = [String]()
}
