//
//  ActivityModel.swift
//  GenePlus
//
//  Created by pro on 1/23/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation


class ActivityModel {
    var title: String = ""
    var createdTime: String = ""
    var time: String = ""
    var muscleGroup: String = ""
    var duration: String = ""
    var repeatWorkout: String = ""
    var imageWorkout: [String] = [String]()
}
