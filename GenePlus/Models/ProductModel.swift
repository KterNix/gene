//
//  ProductModel.swift
//  GenePlus
//
//  Created by pro on 1/23/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation

class ProductModel {
    
    var name: String = ""
    var price: String = ""
    var likeCount: String = ""
    var viewer: String = ""
    var productImage: String = ""
    
}
