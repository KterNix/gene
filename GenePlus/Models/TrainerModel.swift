//
//  TrainerModel.swift
//  GenePlus
//
//  Created by pro on 1/22/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation


class TrainerModel {
    
    
    var avatar: String = ""
    var name: String = ""
    var address: String = ""
    var categories:String = ""
    var rating: Double = 0.0
    var reviews: String = ""
    
}
