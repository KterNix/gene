//
//  WorkoutModel.swift
//  GenePlus
//
//  Created by pro on 1/24/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation

class WorkoutModel {
    
    var title: String = ""
    var images = [String]()
    var descriptions = [String]()
    var trainer: String = ""
    var weight: String = ""
    var unit: String = ""
    var reps:String = ""
    var sets:String = ""
    var createdDate:String = ""
    
    
}
