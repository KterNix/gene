//
//  NewsModel.swift
//  GenePlus
//
//  Created by pro on 4/13/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation

struct NewsModel {
    
    var title:String = ""
    var content:String = ""
    var picture:String = ""
    
    
}
