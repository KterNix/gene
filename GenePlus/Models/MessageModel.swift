//
//  MessageModel.swift
//  GenePlus
//
//  Created by pro on 7/9/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation

struct MessageModel {
    var title: String = ""
    var senderCode: String = ""
}
