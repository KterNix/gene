//
//  PaymentModel.swift
//  GenePlus
//
//  Created by pro on 1/18/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation


public class PaymentModel {
    var cardNumber: String = ""
    var expirationMonth: Int = 0
    var expirationYear: Int = 0
    var cvv: String = ""
    var postCode: String = ""
    var image: UIImage = UIImage.fontAwesomeIcon(name:.ccVisa, textColor: UIColor.white, size: CGSize(width: 26, height: 26))
    var title: String = ""
}
