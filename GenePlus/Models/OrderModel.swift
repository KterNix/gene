//
//  OrderModel.swift
//  GenePlus
//
//  Created by pro on 8/21/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation
import ObjectMapper

public struct OrderModel: Mappable, Equatable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let title = "title"
        static let trainer = "trainer"
        static let sets = "sets"
        static let package = "package"
        static let status = "status"
        static let createdDate = "createdDate"

    }
    
    // MARK: Properties
    public var title: String?
    public var trainer: String?
    public var sets: Int? = 0
    public var package: String?
    public var status: Int? = 0
    public var createdDate: String?

    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public init?(map: Map){
        
    }
    public init?() {}
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public mutating func mapping(map: Map) {
        title <- map[SerializationKeys.title]
        trainer <- map[SerializationKeys.trainer]
        sets <- map[SerializationKeys.sets]
        package <- map[SerializationKeys.package]
        status <- map[SerializationKeys.status]
        createdDate <- map[SerializationKeys.createdDate]

    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = title { dictionary[SerializationKeys.title] = value }
        if let value = trainer { dictionary[SerializationKeys.trainer] = value }
        if let value = sets { dictionary[SerializationKeys.sets] = value }
        if let value = package { dictionary[SerializationKeys.package] = value }
        if let value = status { dictionary[SerializationKeys.status] = value }
        if let value = createdDate { dictionary[SerializationKeys.createdDate] = value }

        return dictionary
    }
    
}
