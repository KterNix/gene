//
//  Overview.swift
//  GenePlus
//
//  Created by pro on 8/17/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation

struct OverviewModel {
    
    var title:String = ""
    var content:String = ""
    
}
