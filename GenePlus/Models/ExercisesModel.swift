//
//  ExercisesModel.swift
//  GenePlus
//
//  Created by pro on 2/5/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation

struct ExercisesModel {
    
    var name: String = ""
    var image: String = ""
    var unit: String = ""
    var setModel = [SetModel]()
    
    
}
