//
//  PlaylistModel.swift
//  GenePlus
//
//  Created by pro on 2/5/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation


struct PlaylistModel {
    
    var name: String = ""
    var timeCreated: String = ""
    var duration: String = ""
    var burn: String = ""
    var sets: String  = ""
    var reps: String = ""
    var exercises = [ExercisesModel]()
    var workouts = [WorkoutModel]()
    var note: String = ""
}
