//
//  ScheduleTraining.swift
//  GenePlus
//
//  Created by pro on 4/13/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation



import ObjectMapper

public struct ScheduleTraining: Mappable, Equatable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let trainee = "trainee"
        static let startIn = "startIn"
        static let endIn = "endIn"
        static let date = "date"
        static let workout = "workout"
        static let trainer = "trainer"
        
    }
    
    // MARK: Properties
    public var trainee: String?
    public var startIn: String?
    public var endIn: String?
    public var date: String?
    public var workout: String?
    public var trainer: String?
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public init?(map: Map){
        
    }
    public init?() {}
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public mutating func mapping(map: Map) {
        trainee <- map[SerializationKeys.trainee]
        startIn <- map[SerializationKeys.startIn]
        endIn <- map[SerializationKeys.endIn]
        date <- map[SerializationKeys.date]
        workout <- map[SerializationKeys.workout]
        trainer <- map[SerializationKeys.trainer]
        
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = trainee { dictionary[SerializationKeys.trainee] = value }
        if let value = startIn { dictionary[SerializationKeys.startIn] = value }
        if let value = endIn { dictionary[SerializationKeys.endIn] = value }
        if let value = date { dictionary[SerializationKeys.date] = value }
        if let value = workout { dictionary[SerializationKeys.workout] = value }
        if let value = trainer { dictionary[SerializationKeys.trainer] = value }
        
        return dictionary
    }
    
}


