//
//  EventModel.swift
//  GenePlus
//
//  Created by pro on 1/23/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation


class EventModel {
    
    var title: String = ""
    var address: String = ""
    var duration: String = ""
    var eventImage: String = ""
    
}
