//
//  MuscleModel.swift
//  GenePlus
//
//  Created by pro on 1/23/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation
import ObjectMapper

public struct MuscleModel: Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let area = "area"
        static let title = "title"
        static let content = "content"
        static let likeCount = "likeCount"
        static let views = "views"
        static let muscleImage = "muscleImage"
        static let reps = "reps"

    }
    
    // MARK: Properties
    public var area: String?
    public var title: String?
    public var content: String?
    public var likeCount: String?
    public var views: String?
    public var muscleImage: [String]?
    public var reps: String?
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public init?(map: Map){
        
    }
    public init?() {}
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public mutating func mapping(map: Map) {
        area <- map[SerializationKeys.area]
        title <- map[SerializationKeys.title]
        content <- map[SerializationKeys.content]
        likeCount <- map[SerializationKeys.likeCount]
        views <- map[SerializationKeys.views]
        muscleImage <- map[SerializationKeys.muscleImage]
        reps <- map[SerializationKeys.reps]

    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = area { dictionary[SerializationKeys.area] = value }
        if let value = title { dictionary[SerializationKeys.title] = value }
        if let value = content { dictionary[SerializationKeys.content] = value }
        if let value = likeCount { dictionary[SerializationKeys.likeCount] = value }
        if let value = views { dictionary[SerializationKeys.views] = value }
        if let value = muscleImage { dictionary[SerializationKeys.muscleImage] = value }
        if let value = reps { dictionary[SerializationKeys.reps] = value }

        return dictionary
    }
    
}
