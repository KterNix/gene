//
//  BaseURL.swift
//  GenePlus
//
//  Created by pro on 9/26/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation

class BaseURL {
    
    static let url:String = "http://206.189.145.160"
    
    
    static let URL_LOGIN = url + "/token"
    static let URL_REGISTER = url + "/register"
    static let TOKEN_BY_SOCIAL = url + "/tokenBySocial"
    static let WORKOUTS = url + "/workouts"
    static let PLACES = url + "/places"
    static let REVIEWS = url + "/reviews"
    static let DOWNLOADS = url + "/downloads"
    static let WORKOUT_CATEGORIES = url + "/workoutCategories"
    static let TRAINING_CATEGORIES = url + "/trainingCategories"
    static let PACKAGE = url + "/package"
    static let PLAYLISTS = url + "/playlists"
    static let EVENTS = url + "/events"
    
}
