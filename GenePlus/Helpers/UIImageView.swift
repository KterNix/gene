//
//  UIImageView.swift
//  GenePlus
//
//  Created by pro on 5/15/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation
import UIKit

enum PlaceHolder:String {
    case AVATAR = "avatarPlaceHolder"
    case NORMAL_IMAGE = "placeholderImage"
}

extension UIImageView {
    
    func setImageWith(path: String?,imageType: PlaceHolder) {
        
        DispatchQueue.global().async {
            if let image = path {
                var fullPath:String = ""
                if image.contains("googleusercontent.com") || image.contains("graph.facebook.com") || image.contains("firebasestorage") {
                    fullPath = image
                }else{
                    fullPath = image
                }
                let url = URL(string: fullPath)
                DispatchQueue.main.async {
                    self.sd_setImageWithURLAndFade(url: url, placeholderImage: UIImage(named: imageType.rawValue))
                }
            }
        }
    }
    
    public func sd_setImageWithURLAndFade(url: URL!, placeholderImage placeholder: UIImage!) {
        self.sd_setImage(with: url, placeholderImage: placeholder) { (image, error, cacheType, url) -> Void in
            if let downLoadedImage = image {
                
                if cacheType == .none {
                    self.alpha = 0
                    UIView.transition(with: self, duration: 0.5, options: UIView.AnimationOptions.transitionCrossDissolve, animations: { () -> Void in
                        self.image = downLoadedImage
                        self.alpha = 1
                    }, completion: nil)
                }
            }else{
                self.image = placeholder
            }
        }
    }
}
