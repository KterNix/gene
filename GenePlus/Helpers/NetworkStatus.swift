//
//  NetworkStatus.swift
//  ClickFood
//
//  Created by mac on 9/18/17.
//  Copyright © 2017 pro. All rights reserved.
//

//import Foundation
import Alamofire
//import SVProgressHUD
//
class NetworkStatus {
    
    static let sharedInstance = NetworkStatus()

    private init() {}

    let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.apple.com")

    func startNetworkReachabilityObserver() {
        reachabilityManager?.listener = { status in

            switch status {

            case .notReachable:
                break
            case .unknown :
                break
            case .reachable(.ethernetOrWiFi):
                break
            case .reachable(.wwan):
                break
            }
        }
        reachabilityManager?.startListening()
    }
}

