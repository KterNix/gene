//
//  String.swift
//  neoLock
//
//  Created by pro on 10/5/17.
//  Copyright © 2017 BTS Solutions. All rights reserved.
//

import UIKit


extension String {
//    func toMD5() -> String {
//        let length = Int(CC_MD5_DIGEST_LENGTH)
//        var digest = [UInt8](repeating: 0, count: length)
//        if let d = self.data(using: String.Encoding.utf8) {
//            _ = d.withUnsafeBytes { (body: UnsafePointer<UInt8>) in
//                CC_MD5(body, CC_LONG(d.count), &digest)
//            }
//        }
//        return (0..<length).reduce("") {
//            $0 + String(format: "%02x", digest[$1])
//        }
//    }
    
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    
    
    func toInt() -> Int {
        let numberFormatter = NumberFormatter()
        if let number = numberFormatter.number(from: self) {
            return Int((round(number.doubleValue)))
        }else {
            return 0
        }
    }
    
    
    
    func removeMailPath() -> String {
        
        let component = self.characters.split(separator: "@")
//        let words = component.count - 1
        let head = component.dropLast(1).map(String.init)[0]
//        let tail = component.dropFirst(words).map(String.init)[0]
        
        return head
    }
    
    func toArray() -> [String] {
        return self.components(separatedBy: ",")
    }
    
}
extension NSString {
    
    func replacePrefix() -> NSString {
        return replace(string: "neolock_", with: "")
    }
    
    func replaceSpace() -> NSString {
        return replace(string: " ", with: "")
    }
    func replace(string: String,with: String) -> NSString {
        return self.replacingOccurrences(of: string, with: with) as NSString
    }
    func replaceDateFormat() -> String {
        return replace(string: "+0000", with: "") as String
    }
}



extension NSMutableArray {
    func filter(_ block: @escaping (Any) -> Bool) {
        let predicate = NSPredicate { object, bindings in
            return block(object)
        }
        filter(using: predicate)
    }
}






extension Array where Element == String {
    func toString() -> String {
        return self.joined(separator: ",")
    }
    
    var unique: [Element] {
        return Array(Set(self))
    }
}







