//
//  UIBarButtonItem.swift
//  RestaurantBTS
//
//  Created by pro on 7/10/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit


extension UILabel {
    convenience init(badgeText: String, color: UIColor = UIColor.white, fontSize: CGFloat = UIFont.smallSystemFontSize) {
        self.init()
        text = " \(badgeText) "
        textColor = UIColor.darkGray
        backgroundColor = color
        
        font = UIFont.systemFont(ofSize: fontSize)
        layer.cornerRadius = fontSize * CGFloat(0.6)
        clipsToBounds = true
        
        translatesAutoresizingMaskIntoConstraints = false
        addConstraint(NSLayoutConstraint(item: self, attribute: .width, relatedBy: .greaterThanOrEqual, toItem: self, attribute: .height, multiplier: 1, constant: 0))
    }
    func makeCircleWithGhostType()  {
        self.layer.cornerRadius = self.frame.size.height/2
        self.layer.borderWidth = 1
        self.layer.borderColor = self.textColor.cgColor
    }
}
extension UIButton {
    /// show background as rounded rect, like mail addressees
    var rounded: Bool {
        get { return layer.cornerRadius > 0 }
        set { roundWithTitleSize(newValue ? titleSize : 0) }
    }
    
    /// removes other title attributes
    var titleSize: CGFloat {
        get {
            let titleFont = attributedTitle(for: UIControl.State())?.attribute(NSAttributedString.Key.font, at: 0, effectiveRange: nil) as? UIFont
            return titleFont?.pointSize ?? UIFont.buttonFontSize
        }
        set {
            // TODO: use current attributedTitleForState(.Normal) if defined
            if UIFont.buttonFontSize == newValue || 0 == newValue {
                setTitle(currentTitle, for: UIControl.State())
            } else {
                let attrTitle = NSAttributedString(string: currentTitle ?? "", attributes:
                    [NSAttributedString.Key.font: UIFont.systemFont(ofSize: newValue), NSAttributedString.Key.foregroundColor: currentTitleColor]
                )
                setAttributedTitle(attrTitle, for: UIControl.State())
            }
            
            if rounded {
                roundWithTitleSize(newValue)
            }
        }
    }
    
    func roundWithTitleSize(_ size: CGFloat) {
        let padding = size / 4
        layer.cornerRadius = padding + size * 1.2 / 2
        let sidePadding = padding * 1.5
        contentEdgeInsets = UIEdgeInsets(top: padding, left: sidePadding, bottom: padding, right: sidePadding)
        
        if size.isZero {
            backgroundColor = UIColor.clear
            setTitleColor(tintColor, for: UIControl.State())
        }
        else {
            backgroundColor = tintColor
            let currentTitleColor = titleColor(for: UIControl.State())
            if currentTitleColor == nil || currentTitleColor == tintColor {
                setTitleColor(UIColor.white, for: UIControl.State())
            }
        }
    }
}



extension UIBarButtonItem {
    convenience init(badge: String?, button: UIButton, target: AnyObject?, action: Selector) {
        button.addTarget(target, action: action, for: .touchUpInside)
        button.sizeToFit()
        
        let badgeLabel = UILabel(badgeText: badge ?? "")
        button.addSubview(badgeLabel)
        button.addConstraint(NSLayoutConstraint(item: badgeLabel, attribute: .top, relatedBy: .equal, toItem: button, attribute: .top, multiplier: 1, constant: 0))
        button.addConstraint(NSLayoutConstraint(item: badgeLabel, attribute: .centerX, relatedBy: .equal, toItem: button, attribute: .trailing, multiplier: 1, constant: 0))
        if nil == badge {
            badgeLabel.isHidden = true
        }
        badgeLabel.tag = UIBarButtonItem.badgeTag
        self.init(customView: button)
    }
    convenience init(badge: String?, image: UIImage, target: AnyObject?, action: Selector) {
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height)
        button.setBackgroundImage(image, for: UIControl.State())
        
        self.init(badge: badge, button: button, target: target, action: action)
    }
    convenience init(badge: String?, title: String, target: AnyObject?, action: Selector) {
        let button = UIButton(type: .system)
        button.setTitle(title, for: UIControl.State())
        button.titleLabel?.font = UIFont.systemFont(ofSize: UIFont.buttonFontSize)
        
        self.init(badge: badge, button: button, target: target, action: action)
    }
    var badgeLabel: UILabel? {
        return customView?.viewWithTag(UIBarButtonItem.badgeTag) as? UILabel
    }
    var badgedButton: UIButton? {
        return customView as? UIButton
    }
    var badgeString: String? {
        get { return badgeLabel?.text?.trimmingCharacters(in: CharacterSet.whitespaces) }
        set {
            if let badgeLabel = badgeLabel {
                badgeLabel.text = nil == newValue ? nil : " \(newValue!) "
                badgeLabel.textColor = UIColor.white
                badgeLabel.sizeToFit()
                badgeLabel.isHidden = nil == newValue
            }
        }
    }
    var badgedTitle: String? {
        get { return badgedButton?.title(for: UIControl.State()) }
        set { badgedButton?.setTitle(newValue, for: UIControl.State()); badgedButton?.sizeToFit() }
    }
    fileprivate static let badgeTag = 7373
    
   

    
    
    
}

