//
//  Controllers.swift
//  GenePlus
//
//  Created by pro on 1/6/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation
import UIKit

class Controllers: NSObject {
    
    static let shared = Controllers()
    
    private let deviceIdiom = UIScreen.main.traitCollection.userInterfaceIdiom
    lazy var storyboard = deviceIdiom == .phone ? UIStoryboard(storyboard: .Main) : UIStoryboard(storyboard: .MainStoryboard_iPad)
    
    func baseLoginController() -> BaseLoginController {
        
        let toViewController: BaseLoginController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func loginController() -> LoginController {
        
        let toViewController: LoginController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func registerController() -> RegisterController {
        
        let toViewController: RegisterController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func forgotPassController() -> ForgotPassController {
        
        let toViewController: ForgotPassController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func verifyPassController() -> VerifyPassController {
        
        let toViewController: VerifyPassController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func masterController() -> MasterTabbar {
        
        let toViewController: MasterTabbar = storyboard.instantiateViewController()
        return toViewController
    }
    
    func homeController() -> HomeController {
        
        let toViewController: HomeController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func areaDetailController() -> AreaDetailController {
        
        let toViewController: AreaDetailController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func imageController() -> ImageController {
        
        let toViewController: ImageController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func overviewController() -> OverviewController {
        
        let toViewController: OverviewController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func imagePreviewController() -> ImagePreviewController {
        
        let toViewController: ImagePreviewController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func trainerController() -> TrainerController {
        
        let toViewController: TrainerController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func traineeController() -> MyTraineeController {
        
        let toViewController: MyTraineeController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func traineeDetailController() -> TraineeDetailController {
        
        let toViewController: TraineeDetailController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func traineeProfileController() -> TraineeProfileController {
        
        let toViewController: TraineeProfileController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func modifyTraineeProfile() -> ModifyTraineeProfile {
        
        let toViewController: ModifyTraineeProfile = storyboard.instantiateViewController()
        return toViewController
    }
    
    func trainerDetailController() -> TrainerDetailController {
        
        let toViewController: TrainerDetailController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func confirmController() -> ConfirmController {
        
        let toViewController: ConfirmController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func completeBookingController() -> CompleteBookingController {
        
        let toViewController: CompleteBookingController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func noticeController() -> NoticeController {
        
        let toViewController: NoticeController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func noticeDetailController() -> NoticeDetailController {
        
        let toViewController: NoticeDetailController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func searchController() -> SearchController {
        
        let toViewController: SearchController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func activityController() -> ActivityController {
        
        let toViewController: ActivityController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func activityDetailController() -> ActivityDetailController {
        
        let toViewController: ActivityDetailController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func addPlaylistController() -> AddPlaylistController {
        
        let toViewController: AddPlaylistController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func exercisesController() -> ExercisesController {
        
        let toViewController: ExercisesController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func exercisesDetailController() -> ExercisesDetailController {
        
        let toViewController: ExercisesDetailController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func startWorkoutController() -> StartWorkoutController {
        
        let toViewController: StartWorkoutController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func trainingTimeController() -> TrainingTimeController {
        
        let toViewController: TrainingTimeController = storyboard.instantiateViewController()
        return toViewController
    }
    
    
    func completeWorkoutController() -> CompleteWorkoutController {
        
        let toViewController: CompleteWorkoutController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func completeWorkoutDetailController() -> CompleteWorkoutDetailController {
        
        let toViewController: CompleteWorkoutDetailController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func addActivityController() -> AddActivityController {
        
        let toViewController: AddActivityController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func plannedWorkoutController() -> PlannedWorkoutController {
        
        let toViewController: PlannedWorkoutController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func activityResultsController() -> ActivityResultsController {
        
        let toViewController: ActivityResultsController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func communityController() -> CommunityController {
        
        let toViewController: CommunityController = storyboard.instantiateViewController()
        return toViewController
    }
    
    
    func mapController() -> MapController {
        
        let toViewController: MapController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func filterController() -> FilterController {
        
        let toViewController: FilterController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func categoriesController() -> CategoriesController {
        
        let toViewController: CategoriesController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func categoriesDetailController() -> CategoriesDetailController {
        
        let toViewController: CategoriesDetailController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func productDetailController() -> ProductDetailController {
        
        let toViewController: ProductDetailController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func downloadController() -> DownloadController {
        
        let toViewController: DownloadController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func downloadedController() -> DownloadedController {
        
        let toViewController: DownloadedController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func downloadDetailController() -> DownloadDetailController {
        
        let toViewController: DownloadDetailController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func workoutDetailController() -> WorkoutDetailController {
        
        let toViewController: WorkoutDetailController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func managePlaylistController() -> ManagePlaylistController {
        
        let toViewController: ManagePlaylistController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func ratingController() -> RatingController {
        
        let toViewController: RatingController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func baseChatController() -> BaseChatController {
        
        let toViewController: BaseChatController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func chatController() -> ChatController {
        
        let toViewController: ChatController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func profileController() -> ProfileController {
        
        let toViewController: ProfileController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func profileDetailController() -> ProfileDetailController {
        
        let toViewController: ProfileDetailController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func historyController() -> HistoryController {
        
        let toViewController: HistoryController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func historyDetailController() -> HistoryDetailController {
        
        let toViewController: HistoryDetailController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func inviteFriendController() -> InviteFriendController {
        
        let toViewController: InviteFriendController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func addFriendsController() -> AddFriendsController {
        
        let toViewController: AddFriendsController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func settingsController() -> SettingsController {
        
        let toViewController: SettingsController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func aboutUSController() -> AboutUSController {
        
        let toViewController: AboutUSController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func contactUSController() -> ContactUSController {
        
        let toViewController: ContactUSController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func helpCenterController() -> HelpCenterController {
        
        let toViewController: HelpCenterController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func privacyController() -> PrivacyController {
        
        let toViewController: PrivacyController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func paymentController() -> PaymentController {
        
        let toViewController: PaymentController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func addPaymentController() -> AddPaymentController {
        
        let toViewController: AddPaymentController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func calendarController() -> CalendarController {
        
        let toViewController: CalendarController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func newsController() -> NewsController {
        
        let toViewController: NewsController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func newDetailController() -> NewDetailController {
        
        let toViewController: NewDetailController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func chooseWorkoutController() -> ChooseWorkoutController {
        
        let toViewController: ChooseWorkoutController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func playlistController() -> PlaylistController {
        
        let toViewController: PlaylistController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func menuPlaylistController() -> MenuPlaylistController {
        
        let toViewController: MenuPlaylistController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func workoutImagePreviewController() -> WorkoutImagePreview {
        
        let toViewController: WorkoutImagePreview = storyboard.instantiateViewController()
        return toViewController
    }
    
    func workoutingController() -> WorkoutingController {
        
        let toViewController: WorkoutingController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func chartController() -> ChartController {
        
        let toViewController: ChartController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func availableController() -> AvailableController {
        
        let toViewController: AvailableController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func dateSelectedListController() -> DateSelectedListController {
        
        let toViewController: DateSelectedListController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func muscleListController() -> MuscleListController {
        
        let toViewController: MuscleListController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func businessInfoController() -> BusinessInfoController {
        
        let toViewController: BusinessInfoController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func requestController() -> RequestController {
        
        let toViewController: RequestController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func manageWorkoutController() -> ManageWorkoutController {
        
        let toViewController: ManageWorkoutController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func manageWorkoutDetailController() -> ManageWorkoutDetail {
        
        let toViewController: ManageWorkoutDetail = storyboard.instantiateViewController()
        return toViewController
    }
    
    func manageAllotmentsController() -> ManageAllotments {
        
        let toViewController: ManageAllotments = storyboard.instantiateViewController()
        return toViewController
    }
    
    func uploadWorkoutController() -> UploadWorkoutController {
        
        let toViewController: UploadWorkoutController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func imageFromLibraryController() -> ImageFromLibraryController {
        
        let toViewController: ImageFromLibraryController = storyboard.instantiateViewController()
        return toViewController
    }
    
    func scheduleDetailController() -> ScheduleDetailController {
        
        let toViewController: ScheduleDetailController = storyboard.instantiateViewController()
        return toViewController
    }
    
}

