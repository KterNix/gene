//
//  Utils.swift
//  RestaurantBTS
//
//  Created by pro on 7/10/17.
//  Copyright © 2017 pro. All rights reserved.
//

import UIKit





open class Utils {
    
    static let shared = Utils()
    
    static let userDefault = UserDefaults.standard
    
    
    static let avatarDefault = "https://firebasestorage.googleapis.com/v0/b/neolock-a17c1.appspot.com/o/avatars%2FavatarDefault.png?alt=media&token=42e34b09-84ae-43f6-9c00-bd8581f00c1b"
    
    
    
    
    
    public static func getUserDefaults(withKey:String) -> Bool {
        return (userDefault.value(forKey: withKey) != nil) ? true:false
    }
    
    
    
//
//    open static func checkLogedIn() -> Bool {
//        if getUserDefaults(withKey: Config.kFirebaseID) {
//            return true
//        } else {
//            return false
//        }
//    }
    
//    open static func getUserID() -> String {
//        guard let token = userDefault.value(forKey: Config.kFirebaseID) as? String else { return "" }
//        return token
//    }
//
//
//    open static func saveModelForUser(_ user: UserModel) {
//        Utils.userDefault.set(user.dictionaryRepresentation(), forKey: Config.kUserModel)
//        Utils.userDefault.synchronize()
//    }
//
//    open static func saveLockToken(_ token: TokenModel) {
//        Utils.userDefault.set(token.dictionaryRepresentation(), forKey: Config.kLockToken)
//        Utils.userDefault.synchronize()
//    }
//
//    open static func getLockToken() -> TokenModel {
//        guard let lockToken = Utils.userDefault.value(forKey: Config.kLockToken) as? [String:Any] else { return TokenModel()! }
//        guard let currentLock = Mapper<TokenModel>().map(JSON: lockToken) else { return TokenModel()! }
//        return currentLock
//    }
    
    
    public static func getCurrentDateTime() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm"
        formatter.timeZone = TimeZone(identifier: TimeZone.current.identifier)
        let date = Date()
        return formatter.string(from: date)
    }
    
    
    
//    open static func getCurrentToken() -> String? {
//        guard let baseModel = Utils.userDefault.value(forKey: Config.kBaseUserModel) as? [String: Any] else { return "" }
//        guard let baseModels = Mapper<BaseUserModel>().map(JSON: baseModel) else { return "" }
//        guard let token = baseModels.token else { return ""}
//        return "Bearer " + token
//    }
    
    public static func compare(dateA:Date,dateB:Date) -> Bool {
        let gregorian = NSCalendar(calendarIdentifier: .gregorian)
        
        let compA:DateComponents = (gregorian?.components([NSCalendar.Unit.day,NSCalendar.Unit.year,NSCalendar.Unit.month], from: dateA))!
        
        let compB:DateComponents = (gregorian?.components([NSCalendar.Unit.day,NSCalendar.Unit.year,NSCalendar.Unit.month], from: dateB))!
        
        if compA.day == compB.day && compA.month == compB.month && compA.year == compB.year {
            return true
        }
        return false
    }
    
    
    
    public static func localeDate() -> Date {
        let date = Date()
        let zone = TimeZone.ReferenceType.system
        let interVal = zone.secondsFromGMT(for: date)
        return date.addingTimeInterval(TimeInterval(interVal))
    }
    
    
    
    
    public static func calculateContentSize(scrollView: UIScrollView) -> CGSize {
        var topPoint = CGFloat()
        var height = CGFloat()
        
        for subview in scrollView.subviews {
            if subview.frame.origin.y > topPoint {
                topPoint = subview.frame.origin.y
                height = subview.frame.size.height
            }
        }
        return CGSize(width: scrollView.frame.size.width, height: height + topPoint)
    }
    
    public static func showAlertWithCompletion(title: String, message: String,viewController: UIViewController,completion completionHandler: (() -> Void)?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (action) in
            completionHandler?()
        }
        alert.addAction(action)
        viewController.present(alert, animated: true, completion: nil)
    }
    
    public static func alertWithAction(title: String?,cancelTitle:String = "OK", message: String?, actionTitles:[String?], actions:[((UIAlertAction) -> Void)?],actionStyle: UIAlertAction.Style = .default,viewController: UIViewController,style: UIAlertController.Style) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: style)
        let cancel = UIAlertAction(title: cancelTitle, style: .cancel, handler: nil)
        for (index, title) in actionTitles.enumerated() {
            let action = UIAlertAction(title: title, style: actionStyle, handler: actions[index])
            alert.addAction(action)
        }
        alert.addAction(cancel)
        
//        let deviceIdiom = UIScreen.main.traitCollection.userInterfaceIdiom
//        if deviceIdiom == .pad {
//            alert.popoverPresentationController?.sourceView = viewController.view
//        }
        
        DispatchQueue.main.async {
            viewController.present(alert, animated: true, completion: nil)
        }
    }

    // get current time
//    open static func getCurrentDateTime() -> String {
//        let formatter = DateFormatter()
//        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        let date = Date()
//        return formatter.string(from: date)
//    }
    
    static func stringToDateFM(_ dateString: String, stringFormat: FormatDate) -> Date {
        if(dateString != "") {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = stringFormat.rawValue
            dateFormatter.timeZone = TimeZone.ReferenceType.system
            if let d = dateFormatter.date( from: dateString ) {
                return d
            }
        }
        return Date()
    }
    
    
    
    public static func getDayOfWeek(today:String)->Int {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let todayDate = formatter.date(from: today)!
        let myCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let myComponents = myCalendar.components(.weekday, from: todayDate)
        let weekDay = myComponents.weekday
        return weekDay!
    }
    
    public static func stringToDate(_ dateString: String) -> Date {
        let stringFormat: String = "yyyy-MM-dd HH:mm:ss"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = stringFormat //"yyyy-MM-dd HH:mm:ss"
        return dateFormatter.date(from: dateString)!
    }
    
    public static func StringToDateFormat(_ dateString: String, stringFormat: String = "HH:mm") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        let date:Date = dateFormatter.date( from: dateString )!
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter.string(from: date)
    }
    
    public static func StringDateFormat(_ dateString: String, stringFormat: String = "dd/MM/yyyy HH:mm") -> String {
        let dateFormatter =  DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date:Date = dateFormatter.date( from: dateString )!
        dateFormatter.dateFormat = stringFormat
        return dateFormatter.string(from: date)
    }
    public static func getDayName(_ day: Int) ->String {
        switch day {
        case 1:
            return "Thứ 2"
        case 2:
            return "Thứ 3"
        case 3:
            return "Thứ 4"
        case 4:
            return "Thứ 5"
        case 5:
            return "Thứ 6"
        case 6:
            return "Thứ 7"
        case 7:
            return "Chủ nhật"
        default:
            return ""
        }
    }
    public static func timeAgoSince(_ date: Date, lang: String = "vn") -> String {
        
        let calendar = Calendar.current
        let now = Date()
        let unitFlags: NSCalendar.Unit = [.second, .minute, .hour, .day, .weekOfYear, .month, .year]
        let components = (calendar as NSCalendar).components(unitFlags, from: date, to: now, options: [])
        var prefix:String = ""
        
        
            if components.year! >= 2 {
                prefix = (components.year?.description)! as String
                
                return "\(prefix) year ago"
            }
            
            if components.year! >= 1 {
                return "1 year ago"
            }
            
            if components.month! >= 2 {
                prefix = (components.month?.description)! as String
                return "\(prefix) month ago"
            }
            
            if components.month! >= 1 {
                return "1 month ago"
            }
            
            if components.weekOfYear! >= 2 {
                prefix = (components.weekOfYear?.description)! as String
                return "\(prefix) week ago"
            }
            
            if components.weekOfYear! >= 1 {
                return "1 week ago"
            }
            
            if components.day! >= 2 {
                prefix = (components.day?.description)! as String
                return prefix + " day ago"
            }
            
            if components.day! >= 1 {
                return "Yesterday"
            }
            
            if components.hour! >= 2 {
                prefix = (components.hour?.description)! as String
                return "\(prefix) hour ago"
            }
            
            if components.hour! >= 1 {
                return "1 hour ago"
            }
            
            if components.minute! >= 2 {
                prefix = (components.minute?.description)! as String
                return prefix + " minutes ago"
            }
            
            if components.minute! >= 1 {
                return "1 minutes ago"
            }
            
            if components.second! >= 3 {
                prefix = (components.second?.description)! as String
                return "\(prefix) second ago"
            }
            
            return "Just now"
        
    }
    
    public static func openUrl(scheme: String) {
        if let url = URL(string: scheme) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:],
                                          completionHandler: {
                                            (success) in
                                            print("Open \(scheme): \(success)")
                })
            } else {
                let success = UIApplication.shared.openURL(url)
                print("Open \(scheme): \(success)")
            }
        }
    }

    
}
