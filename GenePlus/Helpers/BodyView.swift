//
//  ImageBody.swift
//  GenePlus
//
//  Created by pro on 1/9/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation
import UIKit


@IBDesignable class BodyView: UIControl {
    

    public var locationTouch = CGPoint()
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        FootLeft.drawFootLeft(frame: self.bounds, resizing: FootLeft.ResizingBehavior.aspectFit)
        FootLeft.drawFootRight(frame: self.bounds, resizing: FootLeft.ResizingBehavior.aspectFit)
    }
    override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        super.beginTracking(touch, with: event)
        let point = touch.location(in: self)
        self.locationTouch = point
        
        DispatchQueue.main.async {
            self.sendActions(for: .touchUpInside)
        }
        return true
    }
    
    override func continueTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        super.continueTracking(touch, with: event)
        
        return false
    }
    
    override func endTracking(_ touch: UITouch?, with event: UIEvent?) {
        super.endTracking(touch, with: event)
        
    }
    
    
    
}

