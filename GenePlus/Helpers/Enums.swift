//
//  Enums.swift
//  GenePlus
//
//  Created by pro on 4/13/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation


enum UserType {
    case TRAINER
    case TRAINEE
    case BUSINESS
}


enum TrainerPresentSource {
    case HIRE
    case HOME
}

enum BusinessPresentSource {
    case COMMUNITY
    case REQUEST
}


enum NoticeTypes: Int {

    case PROMOTION = 0
    case NEWS = 1
    case ADS
    case OTHER
    
    func getAsset() -> UIImage {
        switch self {
        case .PROMOTION:
            return UIImage(named: "sale")!
        case .NEWS:
            return UIImage(named: "news")!
        case .ADS:
            return UIImage(named: "advertising")!
        case .OTHER:
            return UIImage(named: "info")!
        }
    }
}

enum FormatDate: String {
    
    case FULL = "yyyy-MM-dd HH:mm:ss"
    case YMD = "yyyy-MM-dd"
    
}


enum OrderStatus: Int {
    
    case PENDING = 0
    case REJECTED
    case SUCCESS
    
    
}







