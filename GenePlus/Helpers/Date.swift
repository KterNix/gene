//
//  Date.swift
//  soAir
//
//  Created by pro on 12/4/17.
//  Copyright © 2017 pro. All rights reserved.
//

import Foundation

extension Date {
    @nonobjc static var localFormatter: DateFormatter = {
        let dateStringFormatter = DateFormatter()
        dateStringFormatter.dateStyle = .medium
        dateStringFormatter.timeStyle = .none
        return dateStringFormatter
    }()
    
    func localDateString() -> String
    {
        return Date.localFormatter.string(from: self)
    }
    
    func day() -> Int? {
        return Calendar.current.dateComponents([.day], from: self).day
    }
    
    func year() -> Int? {
        return Calendar.current.dateComponents([.year], from: self).year
    }
    
    func month() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM"
        let strMonth = dateFormatter.string(from: self)
        return strMonth
    }
    
    
}
