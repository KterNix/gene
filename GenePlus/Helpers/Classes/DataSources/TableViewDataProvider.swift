//
//  TableViewDataProvider.swift
//  neoReal
//
//  Created by pro on 4/17/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

public protocol TableViewDataProvider {
    associatedtype T
    func numberOfSections() -> Int
    func numberOfRows(in section: Int) -> Int
    func item(at indexPath: IndexPath) -> T?
    func updateItem(at indexPath: IndexPath, value: T)
}
