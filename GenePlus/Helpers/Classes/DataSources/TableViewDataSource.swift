//
//  TableViewDataSource.swift
//  neoReal
//
//  Created by pro on 4/17/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

public typealias TableViewItemSelectionHandlerType = (IndexPath) -> Void
public typealias ErrorTableViewHandle = () -> Void
public typealias ItemPageHandle = () -> Void

open class TableViewDataSource<Provider: TableViewDataProvider, Cell: UITableViewCell>:
    NSObject,
    UITableViewDataSource,
    UITableViewDelegate
    where Cell: ConfigurableCell, Provider.T == Cell.T
{
    // MARK: - Delegates
    public var tableViewItemSelectionHandler: TableViewItemSelectionHandlerType?
    public var errorTableViewHandle: ErrorTableViewHandle?
    public var itemPageHandle: ItemPageHandle?
    
    // MARK: - Private Properties
    let provider: Provider
    let tableView: UITableView
    var cellHeightDefault:CGFloat?
    var shownIndexes : [IndexPath] = []
    var defaultItemsInPage = 25
    
    // MARK: - Lifecycle
    init(tableView: UITableView, provider: Provider) {
        self.tableView = tableView
        self.provider = provider
        super.init()
        setup()
    }

    func setup() {
        
        tableView.register(UINib(nibName: "PlaceholderCell", bundle: nil), forCellReuseIdentifier: "PlaceholderCell")
        tableView.register(UINib(nibName: Cell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: Cell.reuseIdentifier)

        tableView.dataSource = self
        tableView.delegate = self
    }
    
    //MARK: SCROLL DELEGATE
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {

    }
    
    
    
    // MARK: - UITableViewDataSource
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return provider.numberOfSections()
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let providerCount = provider.numberOfRows(in: section)
        let count = providerCount > 0 ? providerCount : 1
        return count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if provider.numberOfRows(in: 0) > 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: Cell.reuseIdentifier, for: indexPath) as? Cell
            cell?.selectionStyle = .none
            let item = provider.item(at: indexPath)
            if let item = item {
                cell?.configure(item, at: indexPath)
            }
            cell?.tag = indexPath.row
            return cell!
        }
        
        //SHOW PLACE CELL WHEN MODEL EMPTY OR ERROR
//        let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceholderCell", for: indexPath) as! PlaceholderCell
//        cell.selectionStyle = .none
//        cell.tryBTN.isHidden = Utils.errorString == Constants.ERROR.CANT_REQUEST ? false : true
//        cell.titleLBL.isHidden = Utils.errorString == Constants.ERROR.CANT_REQUEST ? false : true
//        let hideWhenEmpty = Utils.errorString != Constants.ERROR.CANT_REQUEST ? false : true
//        cell.titleLBL.isHidden = hideWhenEmpty
//        cell.titleLBL.text = Utils.errorString
//        cell.tryBTN.addTarget(self, action: #selector(didTapTry), for: UIControlEvents.touchUpInside)
        return UITableViewCell()
    }
    
    @objc fileprivate func didTapTry() {
        errorTableViewHandle?()
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard provider.numberOfRows(in: 0) > 0 else { return }
        tableViewItemSelectionHandler?(indexPath)
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeightDefault == nil ? UITableView.automaticDimension : cellHeightDefault!
    }
    
    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 0
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    public func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        //MARK: - FOR LOAD MORE TABLEVIEW
        let providerCount = provider.numberOfRows(in: indexPath.section)
        if indexPath.row == providerCount - 1 && providerCount >= defaultItemsInPage {
            itemPageHandle?()
        }
    }
    
}
