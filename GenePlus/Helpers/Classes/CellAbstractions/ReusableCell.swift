//
//  ReusableCell.swift
//  neoReal
//
//  Created by pro on 4/17/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

public protocol ReusableCell {
    static var reuseIdentifier: String { get }
}

public extension ReusableCell {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}
