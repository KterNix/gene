//
//  ActivityShare.swift
//  soAir
//
//  Created by pro on 12/2/17.
//  Copyright © 2017 pro. All rights reserved.
//

import Foundation


import UIKit

class ActivityShare: NSObject, UIActivityItemSource {
    var news: String = ""
    var url : String = ""
    var image: UIImage = UIImage()
    
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return news
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        return news + "via @iOS soAir"
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, subjectForActivityType activityType: UIActivity.ActivityType?) -> String {
        return news + "via @iOS soAir"
    }
}
