//
//  DeviceScreen.swift
//  GenePlus
//
//  Created by pro on 1/10/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation
import UIKit

enum AssetsImages : String {
    
    case menFrontBody_5 = "menFrontBody_5"
    case menFrontBody_6 = "menFrontBody_6"
    case menFrontBody_6P = "menFrontBody_6P"
    case menFrontBody_X = "menFrontBody_X"
    
    case menBackBody_5 = "menBackBody_5"
    case menBackBody_6 = "menBackBody_6"
    case menBackBody_6P = "menBackBody_6P"
    case menBackBody_X = "menBackBody_X"
    
    case womenFrontBody_5 = "womenFrontBody_5"
    case womenFrontBody_6 = "womenFrontBody_6"
    case womenFrontBody_6P = "womenFrontBody_6P"
    case womenFrontBody_X = "womenFrontBody_X"

    case womenBackBody_5 = "womenBackBody_5"
    case womenBackBody_6 = "womenBackBody_6"
    case womenBackBody_6P = "womenBackBody_6P"
    case womenBackBody_X = "womenBackBody_X"
    
    
    case menFrontBody_Pad5th = "menFrontBody_Pad5th"
    case menBackBody_Pad5th = "menBackBody_Pad5th"
    case womenFrontBody_Pad5th = "womenFrontBody_Pad5th"
    case womenBackBody_Pad5th = "womenBackBody_Pad5th"
    
    
    case unknown = "unknown"
}






class DeviceScreen : NSObject {
    
    static let shared = DeviceScreen()
    
    func getDeviceScreen() -> UIDevice.ScreenType {
//        guard UIDevice.current.isIPhone else { return .Unknown }
        return UIDevice.current.screenType.unsafelyUnwrapped
    }
    
    
    
    
    func menFrontBodyImageFromSizeScreen() -> AssetsImages {
        switch getDeviceScreen() {
        case .iPhone5:
            return .menFrontBody_5
        case .iPhone6:
            return .menFrontBody_6
        case .iPhone6Plus:
            return .menFrontBody_6P
        case .iPhoneX:
            return .menFrontBody_X
        case .iPad5thGeneration:
            return .menFrontBody_Pad5th
        default:
            break
        }
        return .unknown
    }
    
    func menBackBodyImageFromSizeScreen() -> AssetsImages {
        switch getDeviceScreen() {
        case .iPhone5:
            return .menBackBody_5
        case .iPhone6:
            return .menBackBody_6
        case .iPhone6Plus:
            return .menBackBody_6P
        case .iPhoneX:
            return .menBackBody_X
        case .iPad5thGeneration:
            return .menBackBody_Pad5th
        default:
            break
        }
        return .unknown
    }
    
    func womenFrontBodyImageFromSizeScreen() -> AssetsImages {
        switch getDeviceScreen() {
        case .iPhone5:
            return .womenFrontBody_5
        case .iPhone6:
            return .womenFrontBody_6
        case .iPhone6Plus:
            return .womenFrontBody_6P
        case .iPhoneX:
            return .womenFrontBody_X
        case .iPad5thGeneration:
            return .womenFrontBody_Pad5th
        default:
            break
        }
        return .unknown
    }
    
    func womenBackBodyImageFromSizeScreen() -> AssetsImages {
        switch getDeviceScreen() {
        case .iPhone5:
            return .womenBackBody_5
        case .iPhone6:
            return .womenBackBody_6
        case .iPhone6Plus:
            return .womenBackBody_6P
        case .iPhoneX:
            return .womenBackBody_X
        case .iPad5thGeneration:
            return .womenBackBody_Pad5th
        default:
            break
        }
        return .unknown
    }
    
}

//MARK: - CGPATH FOR MEN
extension DeviceScreen {
    
    func pathForMenFrontFootLeft() -> CGPath? {
        switch getDeviceScreen() {
        case .iPad5thGeneration:
            return BezierPath_iPad.shared.menFrontFootLeft_Pad5th().cgPath
        case .iPhone6:
            return BezierPath.shared.menFrontFootLeft_6().cgPath
        default:
            break
        }
        return nil
    }
    
    func pathForMenFrontFootRight() -> CGPath? {
        switch getDeviceScreen() {
        case .iPad5thGeneration:
            return BezierPath_iPad.shared.menFrontFootRight_Pad5th().cgPath
        case .iPhone6:
            return BezierPath.shared.menFrontFootRight_6().cgPath
        default:
            break
        }
        return nil
    }
    
    func pathForMenFrontHandLeft() -> CGPath? {
        switch getDeviceScreen() {
        case .iPad5thGeneration:
            return BezierPath_iPad.shared.menFrontHandLeft_Pad5th().cgPath
        case .iPhone6:
            return BezierPath.shared.menFrontHandLeft_6().cgPath
        default:
            break
        }
        return nil
    }
    
    func pathForMenFrontHandRight() -> CGPath? {
        switch getDeviceScreen() {
        case .iPad5thGeneration:
            return BezierPath_iPad.shared.menFrontHandRight_Pad5th().cgPath
        case .iPhone6:
            return BezierPath.shared.menFrontHandRight_6().cgPath
        default:
            break
        }
        return nil
    }
    
    func pathForMenFrontAbs() -> CGPath? {
        switch getDeviceScreen() {
        case .iPad5thGeneration:
            return BezierPath_iPad.shared.menFrontAbs_Pah5th().cgPath
        case .iPhone6:
            return BezierPath.shared.menFrontAbs_6().cgPath
        default:
            break
        }
        return nil
    }
    
    func pathForMenFrontChest() -> CGPath? {
        switch getDeviceScreen() {
        case .iPad5thGeneration:
            return BezierPath_iPad.shared.menFrontChest_Pah5th().cgPath
        case .iPhone6:
            return BezierPath.shared.menFrontChest_6().cgPath
        default:
            break
        }
        return nil
    }
    
    func pathForMenBackFootLeft() -> CGPath? {
        switch getDeviceScreen() {
        case .iPad5thGeneration:
            return BezierPath_iPad.shared.menBackFootLeft_Pad5th().cgPath
        case .iPhone6:
            return BezierPath.shared.menBackFootLeft_6().cgPath
        default:
            break
        }
        return nil
    }
    
    func pathForMenBackFootRight() -> CGPath? {
        switch getDeviceScreen() {
        case .iPad5thGeneration:
            return BezierPath_iPad.shared.menBackFootRight_Pad5th().cgPath
        case .iPhone6:
            return BezierPath.shared.menBackFootRight_6().cgPath
        default:
            break
        }
        return nil
    }
    
    func pathForMenBackHandLeft() -> CGPath? {
        switch getDeviceScreen() {
        case .iPad5thGeneration:
            return BezierPath_iPad.shared.menBackHandLeft_Pad5th().cgPath
        case .iPhone6:
            return BezierPath.shared.menBackHandLeft_6().cgPath
        default:
            break
        }
        return nil
    }
    
    func pathForMenBackHandRight() -> CGPath? {
        switch getDeviceScreen() {
        case .iPad5thGeneration:
            return BezierPath_iPad.shared.menBackHandRight_Pad5th().cgPath
        case .iPhone6:
            return BezierPath.shared.menBackHandRight_6().cgPath
        default:
            break
        }
        return nil
    }
    
    func pathForMenBack() -> CGPath? {
        
        switch getDeviceScreen() {
        case .iPad5thGeneration:
            return BezierPath_iPad.shared.menBack_Pad5th().cgPath
        case .iPhone6:
            return BezierPath.shared.menBack_6().cgPath
        default:
            break
        }
        return nil
    }
    
    func pathForMenGlutes() -> CGPath? {
        switch getDeviceScreen() {
        case .iPad5thGeneration:
            return BezierPath_iPad.shared.menBackGlutes_Pad5th().cgPath
        case .iPhone6:
            return BezierPath.shared.menBackGluteus_6().cgPath
        default:
            break
        }
        return nil
    }
}

//MARK: CGPATH FOR WOMEN
extension DeviceScreen {
    
    func pathForWomenFrontFootLeft() -> CGPath? {
        switch getDeviceScreen() {
        case .iPad5thGeneration:
            return BezierPath_iPad.shared.womenFrontFootLeft_Pad5th().cgPath
        case .iPhone6:
            return BezierPath.shared.womenFrontFootLeft_6().cgPath
        default:
            break
        }
        return nil
    }
    
    func pathForWomenFrontFootRight() -> CGPath? {
        switch getDeviceScreen() {
        case .iPad5thGeneration:
            return BezierPath_iPad.shared.womenFrontFootRight_Pad5th().cgPath
        case .iPhone6:
            return BezierPath.shared.womenFrontFootRight_6().cgPath
        default:
            break
        }
        return nil
    }
    
    func pathForWomenFrontHandLeft() -> CGPath? {
        switch getDeviceScreen() {
        case .iPad5thGeneration:
            return BezierPath_iPad.shared.womenFrontHandLeft_Pad5th().cgPath
        case .iPhone6:
            return BezierPath.shared.womenFrontHandLeft_6().cgPath
        default:
            break
        }
        return nil
    }
    
    func pathForWomenFrontHandRight() -> CGPath? {
        switch getDeviceScreen() {
        case .iPad5thGeneration:
            return BezierPath_iPad.shared.womenFrontHandRight_Pad5th().cgPath
        case .iPhone6:
            return BezierPath.shared.womenFrontHandRight_6().cgPath
        default:
            break
        }
        return nil
    }
    
    func pathForWomenFrontAbs() -> CGPath? {
        switch getDeviceScreen() {
        case .iPad5thGeneration:
            return BezierPath_iPad.shared.womenFrontAbs_Pad5th().cgPath
        case .iPhone6:
            return BezierPath.shared.womenFrontAbs_6().cgPath
        default:
            break
        }
        return nil
    }
    
    func pathForWomenFrontBreast() -> CGPath? {
        switch getDeviceScreen() {
        case .iPad5thGeneration:
            return BezierPath_iPad.shared.womenFrontBreast_Pad5th().cgPath
        case .iPhone6:
            return BezierPath.shared.womenFrontBreast_6().cgPath
        default:
            break
        }
        return nil
    }
    
    func pathForWomenBackFootLeft() -> CGPath? {
        switch getDeviceScreen() {
        case .iPad5thGeneration:
            return BezierPath_iPad.shared.womenBackFootLeft_Pad5th().cgPath
        case .iPhone6:
            return BezierPath.shared.womenBackFootLeft_6().cgPath
        default:
            break
        }
        return nil
    }
    
    func pathForWomenBackFootRight() -> CGPath? {
        switch getDeviceScreen() {
        case .iPad5thGeneration:
            return BezierPath_iPad.shared.womenBackFootRight_Pad5th().cgPath
        case .iPhone6:
            return BezierPath.shared.womenBackFootRight_6().cgPath
        default:
            break
        }
        return nil
    }
    
    func pathForWomenBackHandLeft() -> CGPath? {
        switch getDeviceScreen() {
        case .iPad5thGeneration:
            return BezierPath_iPad.shared.womenBackHandLeft_Pad5th().cgPath
        case .iPhone6:
            return BezierPath.shared.womenBackHandLeft_6().cgPath
        default:
            break
        }
        return nil
    }
    
    func pathForWomenBackHandRight() -> CGPath? {
        switch getDeviceScreen() {
        case .iPad5thGeneration:
            return BezierPath_iPad.shared.womenBackHandRight_Pad5th().cgPath
        case .iPhone6:
            return BezierPath.shared.womenBackHandRight_6().cgPath
        default:
            break
        }
        return nil
    }
    
    func pathForWomenBackMiddle() -> CGPath? {
        switch getDeviceScreen() {
        case .iPad5thGeneration:
            return BezierPath_iPad.shared.womenBackMiddle_Pad5th().cgPath
        case .iPhone6:
            return BezierPath.shared.womenBackMiddle_6().cgPath
        default:
            break
        }
        return nil
    }
    
    func pathForWomenGlutes() -> CGPath? {
        switch getDeviceScreen() {
        case .iPad5thGeneration:
            return BezierPath_iPad.shared.womenBackGlutes_Pad5th().cgPath
        case .iPhone6:
            return BezierPath.shared.womenBackGlutes_6().cgPath
        default:
            break
        }
        return nil
    }
}






