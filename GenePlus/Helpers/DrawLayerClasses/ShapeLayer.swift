//
//  ShapeLayer.swift
//  GenePlus
//
//  Created by pro on 1/12/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation
import UIKit


// MARK: - Dùng key phân biệt layer
enum ContentKeyForLayer:String {
    case chest = "Chest"
    case abdominals = "Abdominals"
    case shoulders = "Shoulders"
    case legs = "Legs"
}




class ShapeLayer: NSObject {
    
    static let shared = ShapeLayer()
    
    private let differenceBlendMode:String = "differenceBlendMode"
    private let round = "round"
    
    //MARK: - DeviceScreen.shared.pathForMenFrontFootLeft() -> Available for multi screen size
    func drawMenFrontPathFor(image: UIImageView,color: CGColor) {
        var layer = CAShapeLayer()
        //BezierPath.shared.menFrontFootLeft_6().cgPath
        layer.path = DeviceScreen.shared.pathForMenFrontFootLeft()
        layer.fillColor = color
        layer.lineJoin = CAShapeLayerLineJoin(rawValue: round)
        layer.compositingFilter = differenceBlendMode
        layer.contents = ContentKeyForLayer.legs.rawValue
        image.layer.addSublayer(layer)

        layer = CAShapeLayer()
        layer.path = DeviceScreen.shared.pathForMenFrontFootRight()
        layer.fillColor = color
        layer.lineJoin = CAShapeLayerLineJoin(rawValue: round)
        layer.compositingFilter = differenceBlendMode
        layer.contents = ContentKeyForLayer.legs.rawValue
        image.layer.addSublayer(layer)
        
        layer = CAShapeLayer()
        layer.path = DeviceScreen.shared.pathForMenFrontAbs()
        layer.fillColor = color
        layer.lineJoin = CAShapeLayerLineJoin(rawValue: round)
        layer.compositingFilter = differenceBlendMode
        layer.contents = ContentKeyForLayer.abdominals.rawValue
        image.layer.addSublayer(layer)
        
        layer = CAShapeLayer()
        layer.path = DeviceScreen.shared.pathForMenFrontChest()
        layer.fillColor = color
        layer.lineJoin = CAShapeLayerLineJoin(rawValue: round)
        layer.compositingFilter = differenceBlendMode
        layer.contents = ContentKeyForLayer.chest.rawValue
        image.layer.addSublayer(layer)
        
        layer = CAShapeLayer()
        layer.path = DeviceScreen.shared.pathForMenFrontHandLeft()
        layer.fillColor = color
        layer.lineJoin = CAShapeLayerLineJoin(rawValue: round)
        layer.compositingFilter = differenceBlendMode
        layer.contents = ContentKeyForLayer.shoulders.rawValue
        image.layer.addSublayer(layer)
        
        layer = CAShapeLayer()
        layer.path = DeviceScreen.shared.pathForMenFrontHandRight()
        layer.fillColor = color
        layer.lineJoin = CAShapeLayerLineJoin(rawValue: round)
        layer.compositingFilter = differenceBlendMode
        layer.contents = ContentKeyForLayer.shoulders.rawValue
        image.layer.addSublayer(layer)
    }
    
    func drawMenBackPathFor(image: UIImageView,color: CGColor) {
        var layer = CAShapeLayer()
        layer.path = DeviceScreen.shared.pathForMenBackFootLeft()
        layer.fillColor = color
        layer.lineJoin = CAShapeLayerLineJoin(rawValue: round)
        layer.compositingFilter = differenceBlendMode
        image.layer.addSublayer(layer)
        
        layer = CAShapeLayer()
        layer.path = DeviceScreen.shared.pathForMenBackFootRight()
        layer.fillColor = color
        layer.lineJoin = CAShapeLayerLineJoin(rawValue: round)
        layer.compositingFilter = differenceBlendMode
        image.layer.addSublayer(layer)
        
        layer = CAShapeLayer()
        layer.path = DeviceScreen.shared.pathForMenBack()
        layer.fillColor = color
        layer.lineJoin = CAShapeLayerLineJoin(rawValue: round)
        layer.compositingFilter = differenceBlendMode
        image.layer.addSublayer(layer)
        
        
        layer = CAShapeLayer()
        layer.path = DeviceScreen.shared.pathForMenBackHandLeft()
        layer.fillColor = color
        layer.lineJoin = CAShapeLayerLineJoin(rawValue: round)
        layer.compositingFilter = differenceBlendMode
        image.layer.addSublayer(layer)
        
        layer = CAShapeLayer()
        layer.path = DeviceScreen.shared.pathForMenBackHandRight()
        layer.fillColor = color
        layer.lineJoin = CAShapeLayerLineJoin(rawValue: round)
        layer.compositingFilter = differenceBlendMode
        image.layer.addSublayer(layer)
        
        layer = CAShapeLayer()
        layer.path = DeviceScreen.shared.pathForMenGlutes()
        layer.fillColor = color
        layer.lineJoin = CAShapeLayerLineJoin(rawValue: round)
        layer.compositingFilter = differenceBlendMode
        image.layer.addSublayer(layer)
    }
    
    
    func drawWomenFrontPathFor(image: UIImageView,color: CGColor) {
        
        var layer = CAShapeLayer()
        layer.path = DeviceScreen.shared.pathForWomenFrontFootLeft()
        layer.fillColor = color
        layer.lineJoin = CAShapeLayerLineJoin(rawValue: round)
        layer.compositingFilter = differenceBlendMode
        image.layer.addSublayer(layer)
        
        layer = CAShapeLayer()
        layer.path = DeviceScreen.shared.pathForWomenFrontFootRight()
        layer.fillColor = color
        layer.lineJoin = CAShapeLayerLineJoin(rawValue: round)
        layer.compositingFilter = differenceBlendMode
        image.layer.addSublayer(layer)
        
        layer = CAShapeLayer()
        layer.path = DeviceScreen.shared.pathForWomenFrontHandLeft()
        layer.fillColor = color
        layer.lineJoin = CAShapeLayerLineJoin(rawValue: round)
        layer.compositingFilter = differenceBlendMode
        image.layer.addSublayer(layer)
        
        layer = CAShapeLayer()
        layer.path = DeviceScreen.shared.pathForWomenFrontHandRight()
        layer.fillColor = color
        layer.lineJoin = CAShapeLayerLineJoin(rawValue: round)
        layer.compositingFilter = differenceBlendMode
        image.layer.addSublayer(layer)
        
        layer = CAShapeLayer()
        layer.path = DeviceScreen.shared.pathForWomenFrontBreast()
        layer.fillColor = color
        layer.lineJoin = CAShapeLayerLineJoin(rawValue: round)
        layer.compositingFilter = differenceBlendMode
        image.layer.addSublayer(layer)
        
        layer = CAShapeLayer()
        layer.path = DeviceScreen.shared.pathForWomenFrontAbs()
        layer.fillColor = color
        layer.lineJoin = CAShapeLayerLineJoin(rawValue: round)
        layer.compositingFilter = differenceBlendMode
        image.layer.addSublayer(layer)
    }
    
    func drawWomenBackPathFor(image: UIImageView,color: CGColor) {
        var layer = CAShapeLayer()
        layer.path = DeviceScreen.shared.pathForWomenBackFootLeft()
        layer.fillColor = color
        layer.lineJoin = CAShapeLayerLineJoin(rawValue: round)
        layer.compositingFilter = differenceBlendMode
        image.layer.addSublayer(layer)
        
        layer = CAShapeLayer()
        layer.path = DeviceScreen.shared.pathForWomenBackFootRight()
        layer.fillColor = color
        layer.lineJoin = CAShapeLayerLineJoin(rawValue: round)
        layer.compositingFilter = differenceBlendMode
        image.layer.addSublayer(layer)
        
        layer = CAShapeLayer()
        layer.path = DeviceScreen.shared.pathForWomenBackHandLeft()
        layer.fillColor = color
        layer.lineJoin = CAShapeLayerLineJoin(rawValue: round)
        layer.compositingFilter = differenceBlendMode
        image.layer.addSublayer(layer)
        
        layer = CAShapeLayer()
        layer.path = DeviceScreen.shared.pathForWomenBackHandRight()
        layer.fillColor = color
        layer.lineJoin = CAShapeLayerLineJoin(rawValue: round)
        layer.compositingFilter = differenceBlendMode
        image.layer.addSublayer(layer)
        
        layer = CAShapeLayer()
        layer.path = DeviceScreen.shared.pathForWomenBackMiddle()
        layer.fillColor = color
        layer.lineJoin = CAShapeLayerLineJoin(rawValue: round)
        layer.compositingFilter = differenceBlendMode
        image.layer.addSublayer(layer)
        
        layer = CAShapeLayer()
        layer.path = DeviceScreen.shared.pathForWomenGlutes()
        layer.fillColor = color
        layer.lineJoin = CAShapeLayerLineJoin(rawValue: round)
        layer.compositingFilter = differenceBlendMode
        image.layer.addSublayer(layer)
        
    }
    
    
}















