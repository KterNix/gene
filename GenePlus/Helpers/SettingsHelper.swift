//
//  SettingsHelper.swift
//  GenePlus
//
//  Created by pro on 9/26/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation
import ObjectMapper

class SettingsHelper {
    
    /**
     * SET LOGIN TOKEN
     */
    static func setToken(token: String) {
        let userDefault = Utils.userDefault
        userDefault.set(token, forKey: Constant.APP_SETTINGS.TOKEN_KEY)
        userDefault.synchronize()
    }
    
    /**
     * GET LOGIN TOKEN
     */
    static func getToken() -> String? {
        let userDefault = Utils.userDefault
        return userDefault.value(forKey: Constant.APP_SETTINGS.TOKEN_KEY) as? String
    }
    
    /**
     * SET EXPIRE TIME
     */
    static func setExpire(expire: String) {
        let userDefault = Utils.userDefault
        userDefault.set(expire, forKey: Constant.APP_SETTINGS.EXPIRE_KEY)
        userDefault.synchronize()
    }
    
    /**
     * GET EXPIRE TIME
     */
    static func getExpire() -> String? {
        let userDefault = Utils.userDefault
        return userDefault.value(forKey: Constant.APP_SETTINGS.EXPIRE_KEY) as? String
    }
    
    
    /**
     * SET CURRENT USER MODEL
     */
    static func saveUser(user: UserModel?) {
        let userDefault = Utils.userDefault
        userDefault.set(user?.dictionaryRepresentation(), forKey: Constant.APP_SETTINGS.USER_MODEL)
        userDefault.synchronize()
    }
    
    static func getUser() -> UserModel? {
        let userDefault = Utils.userDefault
        if let user = userDefault.dictionary(forKey: Constant.APP_SETTINGS.USER_MODEL) {
            return Mapper<UserModel>().map(JSON: user)
        }
        return nil
    }
    
    
    
}
