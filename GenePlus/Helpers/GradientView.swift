//
//  GradientView.swift
//  GenePlus
//
//  Created by pro on 1/8/18.
//  Copyright © 2018 pro. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable open class GradientViewTop: UIView {
    
    @IBInspectable var colorTop:UIColor {
        get {
            return self.colorTop.withAlphaComponent(self.alphaTop)
        }
        set {
            self.colorTop = newValue.withAlphaComponent(self.alphaTop)
        }
    }
    
    @IBInspectable var alphaTop:CGFloat {
        get {
            return self.alphaTop
        }
        set {
            self.alphaTop = newValue
        }
    }
    
    
    @IBInspectable var colorCenter:UIColor {
        get {
            return self.colorCenter.withAlphaComponent(self.alphaCenter)
        }
        set {
            self.colorCenter = newValue.withAlphaComponent(alphaCenter)
        }
    }
    
    @IBInspectable var alphaCenter:CGFloat {
        get {
            return self.alphaCenter
        }
        set {
            self.alphaCenter = newValue
        }
    }
    
    
    
    @IBInspectable var colorBottom:UIColor {
        get {
            return self.colorBottom.withAlphaComponent(self.alphaBottom)
        }
        set {
            self.colorBottom = newValue.withAlphaComponent(self.alphaBottom)
        }
    }
    
    @IBInspectable var alphaBottom:CGFloat {
        get {
            return self.alphaBottom
        }
        set {
            self.alphaBottom = newValue
        }
    }
    
    
    override open class var layerClass: AnyClass {
        return CAGradientLayer.classForCoder()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let gradientLayer = self.layer as! CAGradientLayer
        gradientLayer.colors = [
            UIColor.black.withAlphaComponent(0).cgColor,
            UIColor.black.withAlphaComponent(0.1).cgColor,
            UIColor.black.withAlphaComponent(0.2).cgColor,
            UIColor.black.withAlphaComponent(0.4).cgColor
        ]
    }
}
