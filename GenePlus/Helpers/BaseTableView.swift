//
//  CustomTableView.swift
//  GenePlus
//
//  Created by pro on 4/13/18.
//  Copyright © 2018 pro. All rights reserved.
//

import UIKit

class BaseTableView: UITableView {
    
    
    var shownIndexes : [IndexPath] = []

    

    

    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    
    private func setup() {
        self.separatorStyle = .none
        self.showsVerticalScrollIndicator = false
    }

}
extension BaseTableView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            
            let additionalSeparatorThickness = CGFloat(1)
            let additionalSeparator = UIView(frame: CGRect(x: 8, y: height - additionalSeparatorThickness, width: cell.frame.size.width - 16, height: additionalSeparatorThickness))
            additionalSeparator.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
            cell.addSubview(additionalSeparator)
            
            
            cell.transform = CGAffineTransform(translationX: 0, y: 50)
            cell.alpha = 0
            
            UIView.beginAnimations("rotation", context: nil)
            UIView.setAnimationDuration(0.5)
            cell.transform = CGAffineTransform(translationX: 0, y: 0)
            cell.alpha = 1
            UIView.commitAnimations()
        }
    }
}
