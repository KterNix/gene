//
//  RequestHelper.swift
//  GenePlus
//
//  Created by pro on 6/12/18.
//  Copyright © 2018 pro. All rights reserved.
//


import UIKit
import TSMessages
import Alamofire
import ObjectMapper

enum ErrorString: String {
    case CANT_REQUEST = "CANT_REQUEST"
    case NONE = "NONE"
    case MESSAGE = "MASSAGE"
    
}

typealias RequestHandle = (_ values: NSDictionary?,_ error: ErrorString) -> Void

class RequestHelper: NSObject {
    
    static let shared = RequestHelper()
    
    var header = [String: String]()
    
    func login(username: String, password: String, viewController: UIViewController, completionHandler: @escaping RequestHandle) {
        
        let param = ["username": username,
                     "password": password]
        
        
        requestMethod(url: BaseURL.URL_LOGIN, method: .post, param: param, header: nil, encoding: JSONEncoding.default, viewController: viewController, completionHandler: completionHandler)
    }
    
    func register(user: UserModel?,viewController: UIViewController, completionHandler: @escaping RequestHandle) {
        
        var param = [String: String]()
        
        param["email"] = user?.email ?? ""
        param["mobile"] = user?.mobile ?? ""
        param["firstname"] = user?.firstname ?? ""
        param["lastname"] = user?.lastname ?? ""
        param["avatar"] = user?.avatar ?? ""
        param["password"] = user?.password ?? ""
        
        
        requestMethod(url: BaseURL.URL_REGISTER, method: .post, param: param, header: nil, encoding: JSONEncoding.default, viewController: viewController, completionHandler: completionHandler)

    }
    
    func loginBySocial(user: UserModel?, viewController: UIViewController, completionHandler: @escaping RequestHandle) {
        
        
        var param = [String: String]()
        
        param["email"] = user?.email ?? ""
        param["social_id"] = user?.social_id ?? ""
        param["firstname"] = user?.firstname ?? ""
        param["lastname"] = user?.lastname ?? ""
        param["avatar"] = user?.avatar ?? ""
        
        
        requestMethod(url: BaseURL.TOKEN_BY_SOCIAL, method: .post, param: param, header: nil, encoding: JSONEncoding.default, viewController: viewController, completionHandler: completionHandler)
        
    }
    
    
    func getWorkouts(page: Int, viewController: UIViewController, completionHandler: @escaping RequestHandle) {
        
        requestMethod(url: BaseURL.WORKOUTS + "?limit=25&from=\(page)&cate=0&keyword", method: .get, param: nil, header: initHeader(), encoding: JSONEncoding.default, viewController: viewController, completionHandler: completionHandler)
    }
    
    
    func getPlaces(viewController: UIViewController, completionHandler: @escaping RequestHandle) {
        
        requestMethod(url: BaseURL.PLACES, method: .get, param: nil, header: initHeader(), encoding: JSONEncoding.default, viewController: viewController, completionHandler: completionHandler)
    }
    
    
    func getReviews(workoutID: String, viewController: UIViewController, completionHandler: @escaping RequestHandle) {
        
        requestMethod(url: BaseURL.REVIEWS + "/\(workoutID)", method: .get, param: nil, header: initHeader(), encoding: JSONEncoding.default, viewController: viewController, completionHandler: completionHandler)
    }
    
    
    func getDownloads(page: Int, viewController: UIViewController, completionHandler: @escaping RequestHandle) {
        
        requestMethod(url: BaseURL.DOWNLOADS + "?limit=25&from=\(page)&keyword&trainer", method: .get, param: nil, header: initHeader(), encoding: JSONEncoding.default, viewController: viewController, completionHandler: completionHandler)
    }
    
    
    func getWorkoutCategories(viewController: UIViewController, completionHandler: @escaping RequestHandle) {
        
        requestMethod(url: BaseURL.WORKOUT_CATEGORIES, method: .get, param: nil, header: initHeader(), encoding: JSONEncoding.default, viewController: viewController, completionHandler: completionHandler)
    }
    
    func getTrainingCategories(viewController: UIViewController, completionHandler: @escaping RequestHandle) {
        
        requestMethod(url: BaseURL.TRAINING_CATEGORIES, method: .get, param: nil, header: initHeader(), encoding: JSONEncoding.default, viewController: viewController, completionHandler: completionHandler)
    }
    
    func getPackage(memberID: String, viewController: UIViewController, completionHandler: @escaping RequestHandle) {
        
        
        requestMethod(url: BaseURL.PACKAGE + "/\(memberID)", method: .get, param: nil, header: initHeader(), encoding: JSONEncoding.default, viewController: viewController, completionHandler: completionHandler)
    }
    
    func getPlaylist(viewController: UIViewController, completionHandler: @escaping RequestHandle) {
        
        requestMethod(url: BaseURL.PLAYLISTS, method: .get, param: nil, header: initHeader(), encoding: JSONEncoding.default, viewController: viewController, completionHandler: completionHandler)
    }
    
    func getEvents(date: String, viewController: UIViewController, completionHandler: @escaping RequestHandle) {
        
        requestMethod(url: BaseURL.EVENTS + "?date=\(date)", method: .get, param: nil, header: initHeader(), encoding: JSONEncoding.default, viewController: viewController, completionHandler: completionHandler)
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    //MARK: - BASE FUNCTION
    private func requestMethod(url: String, method: HTTPMethod, param: [String: Any]?, header: [String: String]?, encoding:ParameterEncoding, viewController: UIViewController, completionHandler: @escaping RequestHandle) {
        
        //MARK: CHECK INTERNET CONNECTION
        guard checkInternetConnection(viewController: viewController) else {
            return
        }
        
        loadingOnStatusBar(isShow: true)
        ManagerNetworking.shared.manager.request(url, method: method, parameters: param, encoding: encoding, headers: header).responseJSON { (response) in
            self.loadingOnStatusBar(isShow: false)
            switch response.result {
            case .failure:
                //MARK: - REQUEST ERROR: internet connection, timeout...
                completionHandler(nil, .CANT_REQUEST)
                break
            case .success:
                
                let values = response.result.value as? NSDictionary
                guard let apiERROR = values?.value(forKey: "error") as? Bool else {
                    //MARK: API ERROR
                    completionHandler(values, .CANT_REQUEST)
                    return
                }
                if apiERROR {
                    completionHandler(values, .MESSAGE)
                    return
                }
                completionHandler(values, .NONE)
            }
        }
    }
    
    /**
     * Header will reload when token has changed
     */
    private func initHeader() -> [String: String] {
        let token = SettingsHelper.getToken()
        let param = ["Authorization": "Bearer \(token ?? "")"]
        
        return param
    }
    
    private func checkInternetConnection(viewController: UIViewController) -> Bool {
        guard viewController.internetAvailable() else {
            return false
        }
        return true
    }
    
    func loadingOnStatusBar(isShow: Bool) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = isShow
    }
}
